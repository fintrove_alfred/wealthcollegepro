<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UserBundle\Controller;

use AppBundle\Entity\User;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends \FOS\UserBundle\Controller\RegistrationController
{

    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(false);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $recaptcha = $this->get('app.recaptcha');
            $remoteIp = $request->getClientIp();
            $response = $request->get('g-recaptcha-response');
            if (!$recaptcha->verify($response, $remoteIp)) {
                $form->addError(new FormError(
                   "The reCAPTCHA could not be validated. Please complete the reCAPTCHA request before submitting."
                ));
            }
            $formData = $request->get('fos_user_registration_form');
            if (empty($formData['agreedTerms'])) {
                $form->addError(new FormError("You must agree to the Terms and Conditions to register"));
            }
        }

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            // add FA role
            $user->addRole('ROLE_FA');

            $userManager->updateUser($user);
            $this->get('app.manager.email_manager')->sendRegistrationNotification($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('register_confirmed_inactive');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(
               FOSUserEvents::REGISTRATION_COMPLETED,
               new FilterUserResponseEvent($user, $request, $response)
            );

            return $response;
        } else {
         if ($form->getErrors(true)->count() > 0) {
            foreach ($form->getErrors(true) as $formError) {
               $this->addFlash('error', $formError->getMessage());
            }
         }

        }
        $singaporeCountry = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Country')->findOneBy(
           ['code' => 'SG']
        );

        return $this->render(
           'FOSUserBundle:Registration:register.html.twig',
           array(
              'form' => $form->createView(),
              'singaporeCountryId' => $singaporeCountry->getId(),
           )
        );
    }


}
