<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends BaseController
{


    /**
     * @Route("/dashboard", name="dashboard")
     * @Template("AppBundle:Dashboard:index.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function indexAction() {
        
        $financialAdviser = $this->getUser()->getFinancialAdviser();
        if (!$financialAdviser) {
            $this->addFlash("error", "No FAR record found for this user login");
            return $this->redirectToRoute('fos_user_security_logout');
        }
        $today = new \DateTime();
        $today->setTime(0,0,0);
        $oneWeekFromNow = new \DateTime();
        $oneWeekFromNow->modify('+7days');
        $oneWeekFromNow->setTime(23,59,59);

        $calendarEvents = $this->get('app.manager.calendar_manager')->getCalendarEvents($this->getUser(), $today, $oneWeekFromNow);
        $clientDateReminders = $this->get('app.manager.client')->getSpecialDatesReminders($financialAdviser, $today, $oneWeekFromNow);
        $clientDates = $this->get('app.manager.client')->getSpecialDates($financialAdviser, $today, $oneWeekFromNow);
        
        return [
            'calendarEvents' => $calendarEvents,
            'financialAdviser' => $financialAdviser,
            'clientDateReminders' => $clientDateReminders,
            'clientDates' => $clientDates
        ];
    }

}
