<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AjaxAdminVerifiableReviewController extends BaseAjaxController
{
    /**
     * @Route("/admin/verify-review", name="admin_verify_review", options={"expose": true})
     */
    public function verifyReview(Request $request)
    {
        $id = $request->get('id');

        $review = $this->getEm()->getRepository('AppBundle:Review')->find($id);
        $this->get('app.manager.review_manager')->verifyReview($review);

        return $this->getResponse();
    }

    
}
