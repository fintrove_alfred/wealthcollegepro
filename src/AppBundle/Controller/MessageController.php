<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends BaseController
{


    /**
     * @Route("/messages", name="message_list")
     */
    public function listAction(Request $request)
    {

        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $filter = [
            'tray' => 'inbox'
        ];
        $messageManager = $this->getMessageManager();
        $query = $messageManager->getListQuery($this->getUser(), $filter);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $rows
        );

        // parameters to template
        return $this->render('AppBundle:Message:list.html.twig', [
            'pagination' => $pagination,
            'unread_count' => $messageManager->getUnreadMessageCount($this->getUser()),
            'filter' => $filter
        ]);
    }

    /**
     * @Route("/messages/sent", name="message_sent")
     */
    public function sentAction(Request $request)
    {

        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $filter = [
            'tray' => 'sent'
        ];
        $messageManager = $this->getMessageManager();
        $query = $messageManager->getListQuery($this->getUser(), $filter);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $rows
        );

        // parameters to template
        return $this->render('AppBundle:Message:list.html.twig', [
            'pagination' => $pagination,
            'unread_count' => $messageManager->getUnreadMessageCount($this->getUser()),
            'filter' => $filter
        ]);
    }

    /**
     * @Route("/messages/trashed", name="message_trash_list")
     */
    public function trashAction(Request $request)
    {

        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $filter = [
            'tray' => 'trash'
        ];
        $messageManager = $this->getMessageManager();
        $query = $messageManager->getListQuery($this->getUser(), $filter);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $rows
        );

        // parameters to template
        return $this->render('AppBundle:Message:list.html.twig', [
            'pagination' => $pagination,
            'unread_count' => $messageManager->getUnreadMessageCount($this->getUser()),
            'filter' => $filter
        ]);
    }

}
