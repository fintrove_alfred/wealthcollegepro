<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AuditLog;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\User;
use AppBundle\Form\Type\FinancialAdviserRegistration;
use AppBundle\Form\Type\UserType;
use FOS\UserBundle\Model\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class AdminUserController extends BaseController
{
    /**
     * @Route("/admin/user", name="admin_user")
     * @Template("AppBundle:Admin:user/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $search = $request->get('search', '');

        $filter = [];

        $filter['search'] = $search ? $search : '';
        $filter['page'] = $page;
        $filter['rows'] = $rows;
        $filter['role'] = $request->get('role', '');
        $query = $this->getUserManager()->getQuery($filter);


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $rows
        );

        if (count($pagination->getItems()) == 0) {
            $filter['page'] = $page = 1;
            $query = $this->getUserManager()->getQuery($filter);

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
               $query,
               $page,
               $rows
            );
        }


        $viewData = [
            'pagination' => $pagination,
            'filter' => $filter
        ];

        return $viewData;
    }

    /**
     * @Route("/admin/user/view/{id}", name="admin_user_view")
     * @Template("AppBundle:Admin:user/view.html.twig")
     */
    public function viewAction($id)
    {
        $user = $this->getRepo('User')->find($id);
        if (!$user) {
            $this->addFlash('error', "Unknown user ID");
            return $this->redirectToRoute('admin_user');
        }

        return [
            'user' => $user
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/user/edit/{id}", name="admin_user_edit")
     * @Template("AppBundle:Admin:user/edit.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction($id, Request $request)
    {
        $roleNames = \AppBundle\Entity\User::getRoleNames();
        $userRoles = $this->getUser()->getRoles();
        $roleNames = array_merge($roleNames, $userRoles);
        $roleNames = array_combine($roleNames, $roleNames);

        $currentUser = $this->getRepo('AppBundle:User')->find($id); /* @var $currentUser User */
        $this->getEm()->detach($currentUser);
        $user = $this->getRepo('AppBundle:User')->find($id);

        $roleNames = array_merge($roleNames, array_combine($currentUser->getRoles(), $currentUser->getRoles()));

        $form = $this->createForm(UserType::class, $user, ['allow_extra_fields' => ['roles' => $roleNames]]);

        $form->handleRequest($request);


        if ($form->isValid()) {

            $userManager = $this->get('fos_user.user_manager'); /* @var $userManager UserManager */
            //$uploadManager = $this->getUploadManager();

            $existing = $this->getRepo('User')->findOneBy(['email' => $form->getData()->getEmail()]);
            // check user doesn't already exist
            if ($existing && $existing->getId() != $user->getId()) {
                $form->addError(new FormError("That email address is already in use"));
            } else {
                $user = $form->getData(); /* @var $user User */

                $userManager->updateUser($user);

                // allow role installer to be unchecked
//                if ($user->getInstaller()) {
//                    $user->addRole('ROLE_INSTALLER');
//                    $this->getEm()->persist($user);
//                }


                $this->getEm()->flush();

                if (!$currentUser->isEnabled() && $user->isEnabled()) {
                    $this->get('app.manager.email_manager')->sendAccountActivatedEmail($user);
                }

                $this->addFlash("success", "User updated successfully");
                return $this->redirect($this->generateUrl('admin_user_edit', ['id' => $user->getId()]));
            }

        }

        return [
            'form' => $form->createView(),
            'user' => $user
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/fa/edit/{id}", name="admin_fa_edit")
     * @Template("AppBundle:Admin:user/edit-fa.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editFAAction($id, Request $request)
    {

        $fa = $this->getRepo('AppBundle:FinancialAdviser')->find($id); /* @var $fa FinancialAdviser */

        $form = $this->createForm(\AppBundle\Form\Type\FinancialAdviser::class, $fa->getUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->persistEntity($fa)->flush($fa);

            $this->addFlash("success", "Financial Adviser updated successfully");
            return $this->redirect($this->generateUrl('admin_fa_edit', ['id' => $fa->getId()]));

        }

        return [
            'form' => $form->createView(),
            'financialAdviser' => $fa,
            'user' => $fa->getUser()
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/fa/show/{id}", name="admin_fa_show")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function faShowAction($id, Request $request)
    {

        $fa = $this->getRepo('AppBundle:FinancialAdviser')->find($id); /* @var $fa FinancialAdviser */
        $fa->setHidden(false);
        $this->getEm()->persist($fa);
        $this->getEm()->flush();

        $this->addFlash("success", "Adviser profile is visible in adviser search");

        return $this->redirectToRoute('admin_user_edit', ['id' => $fa->getUser()->getId()]);
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/fa/hide/{id}", name="admin_fa_hide")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function faHideAction($id, Request $request)
    {

        $fa = $this->getRepo('AppBundle:FinancialAdviser')->find($id); /* @var $fa FinancialAdviser */
        $fa->setHidden(true);
        $this->getEm()->persist($fa);
        $this->getEm()->flush();

        $this->addFlash("success", "Adviser profile is hidden in adviser search");

        return $this->redirectToRoute('admin_user_edit', ['id' => $fa->getUser()->getId()]);
    }

    /**
     * @Route("/admin/user/add", name="admin_user_add")
     * @Template("AppBundle:Admin:user/add.html.twig")
     */
    public function addAction(Request $request)
    {
        $roleNames = \AppBundle\Entity\User::getRoleNames();
        $userRoles = $this->getUser()->getRoles();
        $roleNames = array_merge($roleNames, $userRoles);
        $roleNames = array_combine($roleNames, $roleNames);

        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['allow_extra_fields' => ['roles' => $roleNames]]);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $userManager = $this->get('fos_user.user_manager'); /* @var $userManager UserManager */

            if (!$user->getPlainPassword()) {
                $form->addError(new FormError("Please enter a password"));
                return ['form' => $form->createView()];
            }

            // check user doesn't already exist
            if ($this->getRepo('AppBundle:User')->findOneBy(['email' => $user->getEmail()])) {
                $form->addError(new FormError("That email address is already in use"));
            } else {

                $allowedRoles = [];
                foreach ($user->getRoles() as $role) {
                    if (in_array($role, ['ROLE_SUPER_ADMIN', 'ROLE_DEVELOPER'])) {
                        if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN') || $this->getUser()->hasRole('ROLE_DEVELOPER')) {
                            $allowedRoles[] = $role;
                        }
                    } else {
                        $allowedRoles[] = $role;
                    }
                }
                $user->setRoles($allowedRoles);
                $user->setEnabled(true);

                if (in_array('ROLE_FA', $allowedRoles)) {
                    $financialAdviser = new FinancialAdviser();
                    $financialAdviser->setUser($user);
                    $user->setFinancialAdviser($financialAdviser);
                }
                $userManager->updateUser($user);



                return $this->redirect($this->generateUrl('admin_user_view', ['id' => $user->getId()]));
            }
        }

        return [
            'form' => $form->createView()
        ];
    }


    /**
     * @Route("/admin/login-as-user/{id}", name="admin_login_as_user")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function loginAsUser($id) {

        $user = $this->getRepo('User')->find($id);
        $url = $this->container->get('router')->generate('dashboard');
        $response = new RedirectResponse($url);

        try {
            $this->getUserManager()->logAction($user, $this->getUser(), AuditLog::ACTION_READ, $this->getUser()->getName()." Logged in as user ".$user->getName());
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }

        return $response;
    }

    /**
     * @Route("/admin/user/delete", name="admin_user_delete")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteUserAction(Request $request) {

        $user = $this->getRepo('User')->find($request->get('id')); /* @var $user User */
        if (!$user) {
            $this->addFlash('error', "Unknown user ID");
            return $this->redirectToRoute('admin_user');
        }
        if ($request->get('token') !== $this->get('security.csrf.token_manager')->getToken('delete_user')->getValue()) {
            $this->addFlash('error', "Invalid CSRF Token. Please try again");
            return $this->redirectToRoute('admin_user_view');
        }

        try {

            if ($user->getFinancialAdviser()) {
                $this->getEm()->remove($user->getFinancialAdviser());
            }
            $this->getEm()->remove($user);
            $this->getEm()->flush();
            $this->addFlash('success', "User deleted successfully: ".$user->getName().' '.$user->getEmail());

        } catch (\Exception $ex) {
            $this->addFlash('error', $ex->getMessage());
        }

        return $this->redirectToRoute('admin_user');
    }

    /**
     * @Route("/set_timezone", name="set_timezone", options={"expose": true})
     */
    public function setTimezoneAction(Request $request) {

        $timezone = $request->get('timezone');

        $user = $this->getUser();
        $preferences = $user->getPreferences();
        $preferences->setTimezone($timezone);
        $user->setPreferences($preferences);
        $this->persistEntity($user)->flush($user);

        return new JsonResponse(['success' => true]);
    }


}
