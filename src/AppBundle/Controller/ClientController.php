<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\ClientDate;
use AppBundle\Entity\ClientRelation;
use AppBundle\Entity\ClientWPM;
use AppBundle\Entity\ClientWPMInsurancePolicy;
use AppBundle\Form\Type\ClientEvent;
use AppBundle\Form\Type\ClientEventTypes;
use AppBundle\Form\Type\ClientProfile;
use AppBundle\Form\Type\ClientWPMCashflow;
use AppBundle\Form\Type\ClientWPMEstates;
use AppBundle\Form\Type\ClientWPMInsurancePolicies;
use AppBundle\Form\Type\ClientWPMInvestments;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ClientController extends BaseController
{

    /**
     * @Route("/clients", name="clients", options={"expose": true})
     * @Template("AppBundle:Client:index.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function indexAction(Request $request) {
        
        $webinars = $this->get('app.manager.webinar_manager')->getWebinars($this->getUser()->getFinancialAdviser(), ['dirty' => 0]);

        $filter = $request->get('filter', []);
        if (!isset($filter['typeId'])) {
            $filter['typeId'] = 'all';
        }
        $filter['financialAdviser'] = $this->getUser()->getFinancialAdviser()->getId();
        $clientManager = $this->get('app.manager.client');
        $clients = $clientManager->getClients($filter);

        return [
            'client_form' => $this->createForm(\AppBundle\Form\Type\Client::class, null, [
                    'action' => $this->generateUrl('ajax_client_save')
                ])->createView(),
            'clients' => $clients,
            'financialAdviser' => $this->getUser()->getFinancialAdviser(),
            'webinars' => $webinars,
            'filter' => $filter
        ];
    }

    /**
     * @Route("/clients-import", name="clients_import")
     * @Template("AppBundle:Client:import.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function importAction(Request $request) {

        $viewData = [];

        if ($request->isMethod('POST') && $request->files->has('contactsFile')) {
            ini_set("auto_detect_line_endings", true);

            $uploadedFile = $request->files->get('contactsFile'); /* @var $uploadedFile UploadedFile */
            if ($uploadedFile && $uploadedFile->isFile() && $uploadedFile->isReadable()) {


                $file = $uploadedFile->openFile();
                $contactsData = [];
                $columns = 0;
                while($line = $file->fgetcsv()) {
                    $contactsData[] = $line;
                    if (count($line) > $columns) {
                        $columns = count($line);
                    }
                }
                $viewData['contactsData'] = $contactsData;
                $viewData['numColumns'] = $columns;
            } else {
                $this->addFlash("error", "Please select a valid CSV file for import");
            }


        }


        $viewData['financialAdviser'] = $this->getUser()->getFinancialAdviser();
        $viewData['columnTypes'] = $this->get('app.client_import')->getFieldTypes();
        return $viewData;
    }

    /**
     * @Route("/clients-export", name="clients_export")
     * @Security("has_role('ROLE_FA')")
     */
    public function exportAction(Request $request) {

        $fa = $this->getUser()->getFinancialAdviser();
        $exporter = $this->get('app.client_export');
        try {
            $filename = $exporter->exportCSV($fa);
            if ($filename === false || $exporter->hasErrors()) {
                $this->addFlash("error", implode('. ',$exporter->getErrors()));
                return $this->redirectToRoute('clients');
            }
            $response = new BinaryFileResponse($filename, 200, ['Content-Type' => 'text/csv']);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'FAHub-contacts-export.csv');
            return $response;
        } catch (\Exception $e) {
            $this->getLogger()->crit($e->getMessage()."\n".$e->getTraceAsString());
            $this->addFlash("error", $e->getMessage());
            if ($exporter->hasErrors()) {
                $this->addFlash("error", implode('. ',$exporter->getErrors()));
            }
            return $this->redirectToRoute('clients');
        }

    }

    /**
     * @Route("/client/wpm-edit/{id}", name="client_wpm_edit")
     * @Template("AppBundle:Client:wpm-edit.html.twig")
     * @Security("has_role('ROLE_FA')")
     *
     */
    public function wpmEditAction($id) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $this->redirectToRoute('client_detail', ['id' => $id]);
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */
        if (!$client || $client->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->createAccessDeniedException();
        }

        // Ensure certain objects are created
        if (!$client->getClientWPM()) {
            $clientWPM = new ClientWPM();
            $clientWPM->setClient($client);
            $client->setClientWPM($clientWPM);
            $this->persistEntity($clientWPM)->flush();
        }
        if (!$client->getClientWPMCashflow()) {
            $clientWPMCashflow = new \AppBundle\Entity\ClientWPMCashflow();
            $clientWPMCashflow->setClient($client);
            $client->setClientWPMCashflow($clientWPMCashflow);
            $this->persistEntity($clientWPMCashflow)->flush();
        }


        return [
           'client' => $client,
           'cashflowForm' => $this->createForm(ClientWPMCashflow::class, $client->getClientWPMCashflow(), [
              'action' => $this->generateUrl('ajax_client_save_client_wpm_cashflow', ['id' => $client->getId()])
           ])->createView(),
           'insurancePoliciesForm' => $this->createForm(ClientWPMInsurancePolicies::class, $client, [
              'action' => $this->generateUrl('ajax_client_save_client_wpm_insurance_policy', ['id' => $client->getId()])
           ])->createView(),
           'investmentsForm' => $this->createForm(ClientWPMInvestments::class, $client, [
              'action' => $this->generateUrl('ajax_client_save_client_wpm_investment', ['id' => $client->getId()])
           ])->createView(),
           'estatesForm' => $this->createForm(ClientWPMEstates::class, $client, [
              'action' => $this->generateUrl('ajax_client_save_client_wpm_estate', ['id' => $client->getId()])
           ])->createView(),
        ];
    }

    /**
     * @Route("/client/detail/{id}", name="client_detail", options={"expose": true})
     * @Template("AppBundle:Client:detail.html.twig")
     * @Security("has_role('ROLE_FA')")
     *
     */
    public function detailAction($id) {

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */
        if (!$client || $client->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->createAccessDeniedException();
        }

        if ($client->getClientRelations()->count() == 0) {
            $client->addClientRelation(new ClientRelation());
        }
        if ($client->getClientDates()->count() == 0) {
            $client->addClientDate(new ClientDate());
        }

        $clientEvents = $this->getRepo('AppBundle:ClientEvent')->findBy(['client' => $client->getId()], ['date' => 'asc']);

        return [
            'profile_form' => $this->createForm(ClientProfile::class, $client, [
                'action' => $this->generateUrl('ajax_client_save_profile', ['id' => $client->getId()])
            ])->createView(),
            'client_event_form' => $this->createForm(ClientEvent::class, new \AppBundle\Entity\ClientEvent(), [
                'action' => $this->generateUrl('ajax_client_save_client_event', ['id' => $client->getId()])
            ])->createView(),
            'client_event_types_form' => $this->createForm(ClientEventTypes::class, $this->getUser()->getFinancialAdviser(), [
                'action' => $this->generateUrl('ajax_client_save_client_event_types', ['id' => $client->getId()])
            ])->createView(),
            'client' => $client,
            'clientEvents' => $clientEvents
        ];
    }


    /**
     * Download a document
     * @param Int $id   insurance policy ID
     * @Route("/client/insurance-policy/document-download/{id}", name="client_insurance_policy_download", options={"expose": true})
     */
    public function downloadDocumentAction($id) {

        $clientWPMInsurancePolicy = $this->getRepo('AppBundle:ClientWPMInsurancePolicy')->find($id); /* @var $clientWPMInsurancePolicy \AppBundle\Entity\ClientWPMInsurancePolicy */

        if ($clientWPMInsurancePolicy->getClient()->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }

        $headers = array(
           'Content-Type' => $clientWPMInsurancePolicy->getDocumentUpload()->getType(),
           'Content-Disposition' => 'attachment; filename="'.$clientWPMInsurancePolicy->getDocumentUpload()->getFilename().'"',
           'Content-Transfer-Encoding' => 'binary'
        );

        return new Response(file_get_contents($clientWPMInsurancePolicy->getDocumentUpload()->getLocation()), 200, $headers);
    }




}
