<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GoogleController extends BaseController
{

    /**
     * Google auth response handler
     *
     * @Route("/google/oauth/response", name="google_oauth_response")
     */
    public function oAuthResponseAction(Request $request) {

        if ($request->get('error')) {
            $this->addFlash('error', 'Unable to authorise with Google ('.$request->get('error').'), please try again.');
            return $this->redirectToRoute('webinar');
        }

        $authCode = $request->get('code');
        if ($authCode) {
            try {
                $tokens = $this->get('app.google_api_client')->getAccessTokenUsingAuthCode($authCode);

                $preferences = $this->getUser()->getPreferences();
                $preferences->setGoogleAccessToken($tokens['accessToken']);
                $preferences->setGoogleRefreshToken($tokens['refreshToken']);
                $this->getUser()->setPreferences($preferences);
                $this->persistEntity($this->getUser())->flush();

            } catch (\Exception $e) {
                $this->addFlash('error', 'Sorry there was a problem authorising with Google, please try again. If the problem persists contact support.');
                $this->getLogger()->log(Logger::CRITICAL, $e->getMessage());
                return $this->redirectToRoute('webinar');
            }

        }

        return $this->redirectToRoute('webinar');
    }

}
