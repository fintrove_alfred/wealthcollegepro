<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends BaseController
{
    /**
     * @Route("/admin", name="admin")
     * @Template("AppBundle:Admin:index.html.twig")
     */
    public function indexAction()
    {

        return [];
    }

}
