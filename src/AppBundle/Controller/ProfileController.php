<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\FinancialAdviserPhoto;
use AppBundle\Entity\ReviewTemplate;
use AppBundle\Form\Type\ProfileBasicInfo;
use AppBundle\Form\Type\ProfileContactAddress;
use AppBundle\Form\Type\ProfileDescription;
use AppBundle\Form\Type\ProfileEssentialDetails;
use AppBundle\Form\Type\ProfileWriteUp;
use AppBundle\Form\Type\Review;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends BaseController
{


    /**
     * @Route("/profile", name="profile")
     * @Template("AppBundle:Profile:view.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function indexAction(Request $request) {

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $viewData = $this->getProfileViewData($financialAdviser->getId(), $request);

        return $viewData;
    }

    /**
     * @Route("/profile/view/{id}", name="profile_view", options={"expose"=true})
     * @Template("AppBundle:Profile:view.html.twig")     
     */
    public function viewAction($id, Request $request) {

        $reviewManager = $this->get('app.manager.review_manager');
        $viewData = $this->getProfileViewData($id, $request);
        if ($viewData == false) {
            $this->addFlash('error', 'Invalid User');
            return $this->redirectToRoute('dashboard');
        }

        $financialAdviser = $viewData['financialAdviser']; /* @var $financialAdviser FinancialAdviser */

        if ($financialAdviser->getHidden() || !$financialAdviser->getUser()->isEnabled()) {
            $this->addFlash("error", "This FAR is no longer active");
            return $this->redirectToRoute('fa_search');
        }


        $token = $request->get('token');
        if ($token) {
            // review request token is present, so show the review form to the user
            $review = $this->getRepo('AppBundle:Review')->findOneBy(['token' => $token]); /* @var $review \AppBundle\Entity\Review */

            $review = $reviewManager->buildReviewForForm($review);
            $form = $this->createForm(Review::class, $review);

            $viewData['review'] = $review;
            $viewData['form'] = $form->createView();
            $viewData['token'] = $token;

        }

        return $viewData;
    }

    /**
     * @Route("/profile/view/reviews/{id}", name="profile_view_reviews")
     * @Template("AppBundle:Profile:reviews.html.twig")
     */
    public function viewReviewsAction($id, Request $request) {

        $financialAdviser = $this->getRepo('AppBundle:FinancialAdviser')->find($id);
        if (!$financialAdviser) {
            return false;
        }
        $reviewManager = $this->get('app.manager.review_manager');


        $reviewsOrder = $request->get('reviewsOrder', 'best');
        $filter = [];
        $filter['reviewsOrder'] = $reviewsOrder;
        $reviews = $reviewManager->getPublishedReviewsForFA($financialAdviser, $filter);

        $recentReview = $reviewManager->getMostRecentReview($financialAdviser);

        $viewData = [
           'financialAdviser' => $financialAdviser,
           'reviews' => $reviews,
           'recentReview' => $recentReview,
           'filter' => $filter
        ];

        return $viewData;
    }

    /**
     * @Route("/profile/view/webinars/{id}", name="profile_view_webinars")
     * @Template("AppBundle:Profile:webinars.html.twig")
     */
    public function viewWebinarsAction($id, Request $request) {

        $financialAdviser = $this->getRepo('AppBundle:FinancialAdviser')->find($id);
        $webinarManager = $this->get('app.manager.webinar_manager');


        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $filter = [];

        $filter['page'] = $page;
        $filter['rows'] = $rows;
        $query = $webinarManager->getQuery($financialAdviser, $filter);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
           $query,
           $page,
           $rows
        );

        $viewData = [
            'pagination' => $pagination,
            'filter' => $filter,
            'financialAdviser' => $financialAdviser
        ];


        return $viewData;
    }

    /**
     * @param $id
     * @param Request $request
     * @return array|bool
     */
    private function getProfileViewData($id, Request $request) {

        $financialAdviser = $this->getRepo('AppBundle:FinancialAdviser')->find($id); /* @var $financialAdviser FinancialAdviser */
        if (!$financialAdviser) {
            return false;
        }
        $reviewManager = $this->get('app.manager.review_manager');


        $reviewsOrder = $request->get('reviewsOrder', 'best');
        $filter = [];
        $filter['reviewsOrder'] = $reviewsOrder;
        $reviews = $reviewManager->getPublishedReviewsForFA($financialAdviser, $filter);

        $recentReview = $reviewManager->getMostRecentReview($financialAdviser);

        $photos = $financialAdviser->getFinancialAdviserPhotos();
        $faPhotos = [];
        foreach ($photos as $photo) { /* @var $photo FinancialAdviserPhoto */
            if (in_array($photo->getType(), [FinancialAdviserPhoto::TYPE_PHOTOS, FinancialAdviserPhoto::TYPE_LOGOS])) {
                $faPhotos[] = $photo;
            }
        }

        $viewData = [
            'financialAdviser' => $financialAdviser,
            'financialAdviserPhotos' => $faPhotos,
            'reviews' => $reviews,
            'recentReview' => $recentReview,
            'filter' => $filter
        ];
        
        return $viewData;
    }

    /**
     * Review preview method. this is here because it needs the same view data as the profile view method.
     *
     * @Route("/review/preview", name="review_preview", options={"expose": true})
     * @Template("AppBundle:Profile:view.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function previewAction(Request $request) {

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $viewData = $this->getProfileViewData($financialAdviser->getId(), $request);

        $reviewTemplateId = $request->get('review_template_id');
        if ($reviewTemplateId) {
            $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($reviewTemplateId); /* @var $reviewTemplate ReviewTemplate */
            /* @var $reviewTemplate ReviewTemplate */
            if ($reviewTemplate->getFinancialAdviser() && $reviewTemplate->getFinancialAdviser()->getId() != $financialAdviser->getId()) {
                // invalid review template selected
                $reviewTemplate = null;
            }
        }

        $viewData['reviewPreview'] = true;
        $review = new \AppBundle\Entity\Review();
        $review->setReviewTemplate(!empty($reviewTemplate) ? $reviewTemplate : null);
        $review->setStatusId(\AppBundle\Entity\Review::STATUS_SENT);
        $review->setTypeId(!empty($reviewTemplate) ? $reviewTemplate->getTypeId() : \AppBundle\Entity\Review::TYPE_FEEDBACK);
        //$review->setEmailSubject($request->get('subject'));
        //$review->setEmailMessage($request->get('message'));

        $this->get('app.manager.review_manager')->buildReviewForForm($review);
        $form = $this->createForm(Review::class, $review);

        $viewData['review'] = $review;
        $viewData['form'] = $form->createView();

        return $viewData;
    }

    /**
     * @Route("/profile/edit", name="profile_edit")
     * @Template("AppBundle:Profile:edit.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function editAction() {
        return [
            'financialAdviser' => $this->getUser()->getFinancialAdviser(),
            'basic_info_form' => $this->createForm(ProfileBasicInfo::class, $this->getUser())->createView(),
            'write_up_form' => $this->createForm(ProfileWriteUp::class, $this->getUser()->getFinancialAdviser())->createView(),
            'contact_address_form' => $this->createForm(ProfileContactAddress::class, $this->getUser())->createView(),
            'description_form' => $this->createForm(ProfileDescription::class, $this->getUser())->createView(),
            'essential_details_form' => $this->createForm(ProfileEssentialDetails::class, $this->getUser())->createView()
        ];

    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/profile/show", name="profile_show")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction(Request $request)
    {

        $fa = $this->getUser()->getFinancialAdviser(); /* @var $fa FinancialAdviser */
        $fa->setHidden(false);
        $this->getEm()->persist($fa);
        $this->getEm()->flush();

        $this->addFlash("success", "Your profile is now visible");

        return $this->redirectToRoute('profile_edit');
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/profile/hide", name="profile_hide")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function hideAction(Request $request)
    {

        $fa = $this->getUser()->getFinancialAdviser(); /* @var $fa FinancialAdviser */
        $fa->setHidden(true);
        $this->getEm()->persist($fa);
        $this->getEm()->flush();

        $this->addFlash("success", "Your profile is now hidden");

        return $this->redirectToRoute('profile_edit');
    }
}
