<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
use AppBundle\Service\AuctionManager;
use AppBundle\Service\AuditLogManager;
use AppBundle\Service\DanceRaceManager;
use AppBundle\Service\DocumentGroupManager;
use AppBundle\Service\InstallerInsuranceManager;
use AppBundle\Service\InstallerManager;
use AppBundle\Service\InstallerPreQualAnswerManager;
use AppBundle\Service\InstallerPreQualPersonManager;
use AppBundle\Service\InstallerPreQualDocumentManager;
use AppBundle\Service\InstallerPreQualProductManager;
use AppBundle\Service\MeasureBookingManager;
use AppBundle\Service\MeasureManager;
use AppBundle\Service\MeasureSubmissionManager;
use AppBundle\Service\MessageManager;
use AppBundle\Service\PQQManager;
use AppBundle\Service\PurchaseOrderManager;
use AppBundle\Service\UploadManager;
use AppBundle\Service\UserManager;
use AppBundle\Service\MeasureDocumentManager;
use AppBundle\Service\MeasureNoteManager;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseController extends Controller
{

    protected function checkAccess($roles) {
        $roles = is_array($roles) ? $roles : array($roles);
        $allowed = false;
        foreach ($roles as $roleName) {
            if (false !== $this->get('security.authorization_checker')->isGranted($roleName)) {
                $allowed = true;
                break;
            }
        }
        if (!$allowed) {
            throw $this->createAccessDeniedException();
        }
    }

    /**
     * Has access to all the roles provided
     * @param $roles
     * @return bool
     */
    protected function hasAccess($roles) {
        $roles = is_array($roles) ? $roles : array($roles);
        foreach ($roles as $roleName) {
            if (false !== $this->get('security.authorization_checker')->isGranted($roleName)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Get the logged in user
     * @return User|NULL
     */
    public function getUser() {
        return parent::getUser();
    }

    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Persist an entity
     * @param $entity
     * @return $this
     */
    protected function persistEntity($entity) {
        $this->getEm()->persist($entity);
        return $this;
    }

    /**
     * Flush all persisted entities to DB
     */
    protected function flush($entity = NULL) {
        $this->getEm()->flush($entity);
    }

    /**
     * Get entity repository
     * @return \Doctrine\ORM\EntityRepository|EntityRepository
     */
    public function getRepo($entityName) {
        if (strpos($entityName, ':') === FALSE) {
            // not specified bundle, set default
            $entityName = 'AppBundle:'.$entityName;
        }
        return $this->getEm()->getRepository($entityName);
    }




    /**
     * @return UploadManager
     */
    public function getUploadManager() {
        return $this->get('app.manager.upload_manager');
    }


    /**
     * @return UserManager
     */
    public function getUserManager() {
        return $this->get('app.manager.user_manager');
    }


    /**
     * @return MessageManager
     */
    public function getMessageManager() {
        return $this->get('app.manager.message_manager');
    }



    /**
     * @return AuditLogManager
     */
    public function getAuditLogManager() {
        return $this->get('app.manager.audit_log_manager');
    }

    /**
     * Get the logger
     * @return Logger
     */
    protected function getLogger() {
        return $this->get('logger');
    }

    /**
     * Is there a user logged in?
     *
     * @return mixed
     */
    protected function isLoggedIn() {
        return $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
    }

    /**
     * Get error messages from a form
     *
     * @param \Symfony\Component\Form\Form $form
     * @return array
     */
    protected function getFormErrorMessages(\Symfony\Component\Form\Form $form) {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrorMessages($child);
            }
        }

        return $errors;
    }

    /**
     * Check if a subscription is active for this user
     * @return bool
     */
    public function checkIsSubscribed() {
        if ($this->getUser()->getFinancialAdviser() && $this->getUser()->getFinancialAdviser()->isSubscribed()) {
            return true;
        }
        return false;
    }
}
