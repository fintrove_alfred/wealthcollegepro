<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\ClientClientGroup;
use AppBundle\Entity\ClientGroup;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxClientGroupController extends BaseAjaxController
{


    /**
     * @Route("/ajax/client-group/save", name="ajax_client_group_save")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $clientGroupId = $request->get('id');
        $clientGroup = $clientGroupId ? $this->getRepo('AppBundle:ClientGroup')->find($clientGroupId) : new ClientGroup();

        if ($clientGroup->getFinancialAdviser() && $this->getUser()->getId() != $clientGroup->getFinancialAdviser()->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $form = $this->createForm('AppBundle\Form\Type\ClientGroup', $clientGroup);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if (!$clientGroup->getFinancialAdviser()) {
                $clientGroup->setFinancialAdviser($this->getUser()->getFinancialAdviser());
            }

            // save the event
            $this->persistEntity($clientGroup)->flush();
            $this->setData([
                'id' => $clientGroup->getId()
            ]);
            $this->addFlash('success', 'Group '.$clientGroup->getName().' saved successfully');
            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/client-group/delete", name="ajax_client_group_delete", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function deleteAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        
        $clientGroupId = $request->get('id');
        $clientGroup = $clientGroupId ? $this->getRepo('AppBundle:ClientGroup')->find($clientGroupId) : new ClientGroup();

        if ($clientGroup->getFinancialAdviser() && $this->getUser()->getId() != $clientGroup->getFinancialAdviser()->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        // remove
        $this->getEm()->remove($clientGroup);
        $this->getEm()->flush();

        return $this->getResponse();

    }

    /**
     * @Route("/ajax/client-group/data/{id}", name="ajax_client_group_data", options={"expose"=true})
     * @Security("has_role('ROLE_FA')")
     */
    public function getClientGroupDataAction($id, Request $request) {

        $clientGroup = $this->getRepo('AppBundle:ClientGroup')->find($id); /* @var $clientGroup ClientGroup */

        if ($clientGroup->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure('Access denied')->getResponse();
        }

        $this->setData([
            'id' => $clientGroup->getId(),
            'name' => $clientGroup->getName()
        ]);
        return $this->getResponse();

    }

    /**
     * @Route("/ajax/client-group/add-contact/{clientGroupId}", name="ajax_client_group_client_add", options={"expose"=true})
     * @Security("has_role('ROLE_FA')")
     */
    public function addContactAction($clientGroupId, Request $request) {

        $clientGroup = $this->getRepo('AppBundle:ClientGroup')->find($clientGroupId); /* @var $clientGroup ClientGroup */
        $clientId = $request->get('clientId');

        if (!$clientId || $clientGroup->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->getAccessDeniedResponse();
        }
        $client = $this->getRepo('AppBundle:Client')->find($clientId); /* @var $client Client */
        if (!$client || $client->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->getAccessDeniedResponse();
        }

        if (!$client->inGroup($clientGroup)) {
            $clientClientGroup = new ClientClientGroup();
            $clientClientGroup->setClientGroup($clientGroup);
            $clientClientGroup->setClient($client);
            $this->persistEntity($clientClientGroup)->flush();

        } else {
            $this->setFailure('This contact ('.$client->getPerson()->getFullName().') has already been added to this group ('.$clientGroup->getName().')');
        }

        return $this->getResponse();
    }

    /**
     * @Route("/ajax/client-group/remove-contact/{clientGroupId}", name="ajax_client_group_client_remove", options={"expose"=true})
     * @Security("has_role('ROLE_FA')")
     */
    public function removeContactAction($clientGroupId, Request $request) {

        $clientGroup = $this->getRepo('AppBundle:ClientGroup')->find($clientGroupId); /* @var $clientGroup ClientGroup */
        $clientId = $request->get('clientId');

        if (!$clientId || $clientGroup->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->getAccessDeniedResponse();
        }
        $client = $this->getRepo('AppBundle:Client')->find($clientId); /* @var $client Client */
        if (!$client || $client->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->getAccessDeniedResponse();
        }

        $clientClientGroup = $this->getRepo('AppBundle:ClientClientGroup')
            ->findOneBy([
                'client' => $client->getId(),
                'clientGroup' => $clientGroup->getId(),
            ]);
        if ($clientClientGroup) {
            $this->getEm()->remove($clientClientGroup);
            $this->flush($clientClientGroup);

        }

        return $this->getResponse();
    }


}
