<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ReviewTemplate;
use AppBundle\Entity\ReviewTemplateQuestion;
use AppBundle\Form\Type\Review;
use AppBundle\Form\Type\ReviewTemplateSystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReviewController extends BaseController
{

    /**
     * @Route("/reviews", name="reviews")
     * @Template("AppBundle:Review:index.html.twig")
     */
    public function indexAction(Request $request) {

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $reviews = $this->get('app.manager.review_manager')->getAllReviewsForFA($financialAdviser);

        $clientsData = $this->get('app.manager.client')->getFAClientsSelectData($financialAdviser);
        $groupsData = $this->get('app.manager.client')->getFAGroupsSelectData($financialAdviser);

        return [
            'financialAdviser' => $financialAdviser,
            'reviews' => $reviews,
            'clientsData' => json_encode($clientsData),
            'groupsData' => json_encode($groupsData),
        ];
    }


}
