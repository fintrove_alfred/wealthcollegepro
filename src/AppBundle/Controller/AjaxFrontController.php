<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\ClientClientGroup;
use AppBundle\Entity\ClientGroup;
use AppBundle\Entity\Person;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxFrontController extends BaseAjaxController
{


    /**
     * @Route("/register-interest-submit", name="register_interest_submit")
     */
    public function submitRegisterInterestAction(Request $request) {

        if ($request->isMethod('POST')) {


            //$user = $this->getRepo('AppBundle:User')->findOneBy(['email' => 'gulzamankhan@hotmail.com']); /* @var $user User */
            $user = $this->getRepo('AppBundle:User')->findOneBy(['email' => 'fahubsg@gmail.com']); /* @var $user User */

            if (!$user || !$user->getFinancialAdviser()) {
                return $this->setFailure("Unable add you to a contact list, please contact support")->getResponse();
            }

            $person = new Person();
            $person->setFirstName($request->get('firstName'));
            $person->setLastName($request->get('lastName'));
            $person->setEmail($request->get('email'));

            $client = new Client();
            $client->setFinancialAdviser($user->getFinancialAdviser());
            $client->setPerson($person);
            $client->setTypeId(Client::TYPE_PROSPECT);

            $clientGroups = $user->getFinancialAdviser()->getClientGroups();
            foreach ($clientGroups as $clientGroup) { /* @var $clientGroup ClientGroup */
                if ($clientGroup->getName() == 'Interested People') {
                    $clientClientGroup = new ClientClientGroup();
                    $clientClientGroup->setClientGroup($clientGroup);
                    $clientClientGroup->setClient($client);
                    $this->persistEntity($clientClientGroup);
                    $client->addClientGroup($clientGroup);
                }
            }

            $this->persistEntity($person);
            $this->persistEntity($client);
            $this->flush();
            
            $this->get('app.manager.email_manager')->sendRegisteredInterestWelcomeMessage($person->getFullName(), $person->getEmail());
        }

        return $this->setSuccess()->getResponse();
    }

    /**
     * @Route("/contact-submit", name="contact_submit")
     */
    public function contactSubmitAction(Request $request) {

        
        if ($request->isMethod('POST')) {

            $recaptcha = $this->get('app.recaptcha');
            $remoteIp = $request->getClientIp();
            $response = $request->get('g-recaptcha-response');
            if (!$recaptcha->verify($response, $remoteIp)) {
                $this->addError("The reCAPTCHA could not be validated. Please complete the reCAPTCHA request before submitting.");
                if ($recaptcha->hasErrors()) {
                    $this->addError($recaptcha->getErrors());
                }
                return $this->setFailure()->getResponse();
            }

            $this->get('app.manager.email_manager')->sendContactEmail($request->get('firstName'), $request->get('lastName'), $request->get('email'), $request->get('message'));
        }


        return $this->setSuccess()->getResponse();
    }

    /**
     * @Route("/send-far-email", name="send_far_email")
     */
    public function sendFarEmailAction(Request $request) {


        if ($request->isMethod('POST')) {

            $recaptcha = $this->get('app.recaptcha');
            $remoteIp = $request->getClientIp();
            $response = $request->get('g-recaptcha-response');
            if (!$recaptcha->verify($response, $remoteIp)) {
                $this->addError("The reCAPTCHA could not be validated. Please complete the reCAPTCHA request before submitting.");
                if ($recaptcha->hasErrors()) {
                    $this->addError($recaptcha->getErrors());
                }
                return $this->setFailure()->getResponse();
            }

            $id = $request->get('id');
            $financialAdviser = $this->getRepo('AppBundle:FinancialAdviser')->find($id);
            if (!$financialAdviser) {
                return $this->addError("Invalid FA ID")->getResponse();
            }
            $subject = $request->get('subject');
            $message = $request->get('message');

            if (!$subject) {
                return $this->addError("Please enter a subject")->getResponse();
            }
            if (!$message) {
                return $this->addError("Please enter a message")->getResponse();
            }

            $result = $this->get('app.manager.email_manager')->sendFarEmail($financialAdviser, $request->get('subject'), $request->get('message'));
            if (!$result) {
                return $this->addError("There was a problem sending your email")->getResponse();
            }
        }


        return $this->setSuccess()->getResponse();
    }

    /**
     * @Route("/site-search", name="site_search", options={"expose": true})
     */
    public function siteSearchAction(Request $request) {

        $search = $request->get('search');

        if ($this->getUser() && $this->getUser()->getFinancialAdviser()) {

            $clients = $this->get('app.manager.client')->getBySearch($this->getUser()->getFinancialAdviser(), $search);

            $data = ['contacts' => []];
            foreach ($clients as $client) { /* @var $client Client */
                $data['contacts'][] = [
                    'id' => $client->getId(),
                    'firstName' => $client->getPerson()->getFirstName(),
                    'lastName' => $client->getPerson()->getLastName(),
                    'initials' => $client->getPerson()->getInitials()
                ];
            }

            $this->setData($data);
        }



        return $this->getResponse();

    }
}
