<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\ClientClientGroup;
use AppBundle\Entity\ClientEventType;
use AppBundle\Entity\ClientGroup;
use AppBundle\Entity\ClientRelation;
use AppBundle\Form\Type\ClientEvent;
use AppBundle\Form\Type\ClientEventTypes;
use AppBundle\Form\Type\ClientWPMCashflow;
use AppBundle\Form\Type\ClientWPMEstates;
use AppBundle\Form\Type\ClientWPMInsurancePolicies;
use AppBundle\Form\Type\ClientWPMInsurancePolicy;
use AppBundle\Form\Type\ClientWPMInvestments;
use AppBundle\Service\UploadManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxClientController extends BaseAjaxController
{


    /**
     * @Route("/ajax/client/save", name="ajax_client_save")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $clientId = $request->get('id');
        $client = $clientId ? $this->getRepo('AppBundle:Client')->find($clientId) : new Client();

        if ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $form = $this->createForm('AppBundle\Form\Type\Client', $client);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if (!$client->getFinancialAdviser()) {
                $client->setTypeId(Client::TYPE_CLIENT);
                $client->setFinancialAdviser($this->getUser()->getFinancialAdviser());
            }

            // save the event
            $this->persistEntity($client)->flush();
            $this->setData([
                'id' => $client->getId()
            ]);
            $this->addFlash('success', 'Client '.$client->getPerson()->getFullName().' saved successfully');
            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/client/save-profile/{id}", name="ajax_client_save_profile")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveProfileAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $form = $this->createForm('AppBundle\Form\Type\ClientProfile', $client);
        $form->handleRequest($request);

        if ($form->isValid()) {

            // remove and add client groups
            if ($client->getClientClientGroups()) {
                foreach ($client->getClientClientGroups() as $clientClientGroup) {
                    /* @var $clientClientGroup ClientClientGroup */
                    foreach ($client->getClientGroups() as $clientGroup) {
                        /* @var $clientGroup ClientGroup */
                        if ($clientGroup->getId() == $clientClientGroup->getClientGroup()->getId()) {
                            continue 2;
                        }
                    }
                    // client group removed
                    $client->removeClientClientGroup($clientClientGroup);
                }
            }
            if ($client->getClientGroups()) {
                foreach ($client->getClientGroups() as $clientGroup) {
                    /* @var $clientGroup ClientGroup */
                    foreach ($client->getClientClientGroups() as $clientClientGroup) {
                        /* @var $clientClientGroup ClientClientGroup */
                        if ($clientGroup->getId() == $clientClientGroup->getClientGroup()->getId()) {
                            continue 2;
                        }
                    }
                    $clientClientGroup = new ClientClientGroup();
                    $clientClientGroup->setClient($client);
                    $clientClientGroup->setClientGroup($clientGroup);
                    $client->addClientClientGroup($clientClientGroup);
                }
            }


            $this->persistEntity($client);

            if ($client->getClientRelations()) {
                foreach ($client->getClientRelations() as $clientRelation) {
                    /* @var $clientRelation ClientRelation */
                    $this->persistEntity($clientRelation->getRelatedClient());
                    //$this->persistEntity($clientRelation->getClientRelation());

                    $this->flush($clientRelation->getRelatedClient());
                    //$this->flush($clientRelation->getClientRelation());
                }
                $this->flush();

                foreach ($client->getClientRelations() as $clientRelation) { /* @var $clientRelation ClientRelation */
                    //$clientRelation->setRelationshipTypeId($clientRelation->getRelationshipTypeId());
                    $this->persistEntity($clientRelation);
                    //$this->persistEntity($clientRelation->getClientRelation());
                    $this->flush($clientRelation);
                    //$this->flush($clientRelation->getClientRelation());

                }
            }

            $this->flush();

            $this->setData([
               'id' => $client->getId()
            ]);
            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/client/save-client-event/{id}", name="ajax_client_save_client_event")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveClientEventAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $clientEvent = new \AppBundle\Entity\ClientEvent();
        $form = $this->createForm(ClientEvent::class, $clientEvent);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $clientEvent->setClient($client);
            $this->persistEntity($clientEvent);

            $this->flush();
            $this->setData([
               'id' => $clientEvent->getId(),
                'eventHTML' => $this->renderView('AppBundle:Client/widget:pipeline-client-event.html.twig', [
                    'financialAdviser' => $client->getFinancialAdviser(),
                    'clientEvent' => $clientEvent,
                ])
            ]);
            return $this->setSuccess()->getResponse();
        } else {
            // return error
            $errors = $form->getErrors(true);
            if ($errors) {
                foreach ($errors as $error) {
                    /* @var $error FormError */
                    $this->addError($error->getMessage());
                }
            } else {
                $this->addError('Sorry, there was a problem saving the event');
            }
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/client/save-client-wpm-cashflow/{id}", name="ajax_client_save_client_wpm_cashflow")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveWPMCashflowAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $wpmCashflow = $client->getClientWPMCashflow();
        $form = $this->createForm(ClientWPMCashflow::class, $wpmCashflow);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->persistEntity($wpmCashflow);
            $this->flush();

            $this->get('app.manager.client')->recalculateWPMValues($client);

            return $this->setSuccess()->getResponse();
        } else {
            // return error
            $errors = $form->getErrors(true);
            if ($errors) {
                foreach ($errors as $error) {
                    /* @var $error FormError */
                    $this->addError($error->getMessage());
                }
            } else {
                $this->addError('Sorry, there was a problem saving the event');
            }
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/client/save-client-wpm-insurance-policy/{id}", name="ajax_client_save_client_wpm_insurance_policy")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveWPMInsurancePolicyAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $form = $this->createForm(ClientWPMInsurancePolicies::class, $client);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($client->getClientWPMInsurancePolicies()->count() > 0) {
                foreach ($client->getClientWPMInsurancePolicies() as $clientWPMInsurancePolicy) {
                    $clientWPMInsurancePolicy->setClient($client);
                    $this->persistEntity($clientWPMInsurancePolicy);
                }
            }

            $this->persistEntity($client);
            $this->flush();

            $this->get('app.manager.client')->recalculateWPMValues($client);

            if ($request->isXmlHttpRequest()) {
                return $this->setSuccess()->getResponse();
            } else {
                return $this->redirect($this->generateUrl('client_wpm_edit', ['id' => $client->getId()]).'#insurance');
            }

        } else {
            // return error
            $errors = $form->getErrors(true);
            if ($errors) {
                foreach ($errors as $error) {
                    /* @var $error FormError */
                    $this->addError($error->getMessage());
                }
            } else {
                $this->addError('Sorry, there was a problem saving the event');
            }
        }
        if ($request->isXmlHttpRequest()) {
            return $this->setFailure()->getResponse();
        } else {
            return $this->redirect($this->generateUrl('client_wpm_edit', ['id' => $client->getId()]).'#insurance');
        }
    }

    /**
     * @Route("/ajax/client/save-client-wpm-investment/{id}", name="ajax_client_save_client_wpm_investment")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveWPMInvestmentAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $form = $this->createForm(ClientWPMInvestments::class, $client);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($client->getClientWPMInvestments()->count() > 0) {
                foreach ($client->getClientWPMInvestments() as $clientWPMInvestment) {
                    $clientWPMInvestment->setClient($client);
                    $this->persistEntity($clientWPMInvestment);
                }
            }

            $this->persistEntity($client);
            $this->flush();

            $this->get('app.manager.client')->recalculateWPMValues($client);

            if ($request->isXmlHttpRequest()) {
                return $this->setSuccess()->getResponse();
            } else {
                return $this->redirect($this->generateUrl('client_wpm_edit', ['id' => $client->getId()]).'#investment');
            }

        } else {
            // return error
            $errors = $form->getErrors(true);
            if ($errors) {
                foreach ($errors as $error) {
                    /* @var $error FormError */
                    $this->addError($error->getMessage());
                }
            } else {
                $this->addError('Sorry, there was a problem saving the event');
            }
        }
        if ($request->isXmlHttpRequest()) {
            return $this->setFailure()->getResponse();
        } else {
            return $this->redirect($this->generateUrl('client_wpm_edit', ['id' => $client->getId()]).'#investment');
        }
    }

    /**
     * @Route("/ajax/client/save-client-wpm-estate/{id}", name="ajax_client_save_client_wpm_estate")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveWPMEstateAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $form = $this->createForm(ClientWPMEstates::class, $client);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($client->getClientWPMEstates()->count() > 0) {
                foreach ($client->getClientWPMEstates() as $clientWPMEstate) {
                    $clientWPMEstate->setClient($client);
                    $this->persistEntity($clientWPMEstate);
                }
            }

            $this->persistEntity($client);
            $this->flush();

            $this->get('app.manager.client')->recalculateWPMValues($client);

            if ($request->isXmlHttpRequest()) {
                return $this->setSuccess()->getResponse();
            } else {
                return $this->redirect($this->generateUrl('client_wpm_edit', ['id' => $client->getId()]).'#estate');
            }

        } else {
            // return error
            $errors = $form->getErrors(true);
            if ($errors) {
                foreach ($errors as $error) {
                    /* @var $error FormError */
                    $this->addError($error->getMessage());
                }
            } else {
                $this->addError('Sorry, there was a problem saving the event');
            }
        }
        if ($request->isXmlHttpRequest()) {
            return $this->setFailure()->getResponse();
        } else {
            return $this->redirect($this->generateUrl('client_wpm_edit', ['id' => $client->getId()]).'#estate');
        }
    }

    /**
     * @Route("/ajax/client/add-client-wpm-insurance-policy/{id}", name="ajax_client_add_client_wpm_insurance_policy", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function addWPMInsurancePolicyAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $clientWPMInsurancePolicy = new \AppBundle\Entity\ClientWPMInsurancePolicy();
        $clientWPMInsurancePolicy->setClient($client);
        $client->addClientWPMInsurancePolicy($clientWPMInsurancePolicy);
        $this->persistEntity($client)->flush();

        $form = $this->createForm(ClientWPMInsurancePolicies::class, $client)->createView();
        // get the insurance policy form for the last one that was added
        $insurancePolicyForm = $form->offsetGet('clientWPMInsurancePolicies')->offsetGet($client->getClientWPMInsurancePolicies()->count()-1);

        return $this->setSuccess()->setData([
            'id' => $clientWPMInsurancePolicy->getId(),
            'formHTML' => $this->renderView('AppBundle:Client/widget:wpm-tab-insurance-policy-form.html.twig', [
               'insurancePolicyForm' => $insurancePolicyForm,
               'client' => $client,
               'index' => $request->get('numPolicies')+1
            ])
        ])->getResponse();

    }
    
    /**
     * @Route("/ajax/client/add-client-wpm-investment/{id}", name="ajax_client_add_client_wpm_investment", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function addWPMInvestmentAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $clientWPMInvestment = new \AppBundle\Entity\ClientWPMInvestment();
        $clientWPMInvestment->setClient($client);
        $client->addClientWPMInvestment($clientWPMInvestment);
        $this->persistEntity($client)->flush();

        $form = $this->createForm(ClientWPMInvestments::class, $client)->createView();
        // get the investment policy form for the last one that was added
        $investmentForm = $form->offsetGet('clientWPMInvestments')->offsetGet($client->getClientWPMInvestments()->count()-1);

        return $this->setSuccess()->setData([
           'id' => $clientWPMInvestment->getId(),
           'formHTML' => $this->renderView('AppBundle:Client/widget:wpm-tab-investment-form.html.twig', [
              'investmentForm' => $investmentForm,
              'client' => $client,
              'index' => $request->get('numInvestments')+1
           ])
        ])->getResponse();

    }

    /**
     * @Route("/ajax/client/add-client-wpm-estate/{id}", name="ajax_client_add_client_wpm_estate", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function addWPMEstateAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $clientWPMEstate = new \AppBundle\Entity\ClientWPMEstate();
        $clientWPMEstate->setClient($client);
        $client->addClientWPMEstate($clientWPMEstate);
        $this->persistEntity($client)->flush();

        $form = $this->createForm(ClientWPMEstates::class, $client)->createView();
        // get the estate policy form for the last one that was added
        $estateForm = $form->offsetGet('clientWPMEstates')->offsetGet($client->getClientWPMEstates()->count()-1);

        return $this->setSuccess()->setData([
           'id' => $clientWPMEstate->getId(),
           'formHTML' => $this->renderView('AppBundle:Client/widget:wpm-tab-estate-form.html.twig', [
              'estateForm' => $estateForm,
              'client' => $client,
              'index' => $request->get('numEstates')+1
           ])
        ])->getResponse();

    }
    
    /**
     * @Route("/ajax/client/remove-client-event/{id}", name="ajax_client_event_remove", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function removeClientEventAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $clientEvent = $this->getRepo('AppBundle:ClientEvent')->find($id); /* @var $clientEvent \AppBundle\Entity\ClientEvent */

        if (!$clientEvent || ($this->getUser()->getId() != $clientEvent->getClient()->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $this->getEm()->remove($clientEvent);
        $this->getEm()->flush();

        return $this->setSuccess()->getResponse();

    }

    /**
     * @Route("/ajax/client/remove-wpm-insurance-policy/{id}", name="ajax_client_wpm_remove_insurance_policy", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function removeWPMInsurancePolicyAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $clientWPMInsurancePolicy = $this->getRepo('AppBundle:ClientWPMInsurancePolicy')->find($id); /* @var $clientWPMInsurancePolicy \AppBundle\Entity\ClientWPMInsurancePolicy */

        if (!$clientWPMInsurancePolicy || ($this->getUser()->getId() != $clientWPMInsurancePolicy->getClient()->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $this->getEm()->remove($clientWPMInsurancePolicy);
        $this->getEm()->flush();

        return $this->setSuccess()->getResponse();

    }

    /**
     * @Route("/ajax/client/remove-wpm-investment/{id}", name="ajax_client_wpm_remove_investment", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function removeWPMInvestmentAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $clientWPMInvestment = $this->getRepo('AppBundle:ClientWPMInvestment')->find($id); /* @var $clientWPMInvestment \AppBundle\Entity\ClientWPMInvestment */

        if (!$clientWPMInvestment || ($this->getUser()->getId() != $clientWPMInvestment->getClient()->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $this->getEm()->remove($clientWPMInvestment);
        $this->getEm()->flush();

        return $this->setSuccess()->getResponse();

    }

    /**
     * @Route("/ajax/client/remove-wpm-estate/{id}", name="ajax_client_wpm_remove_estate", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function removeWPMEstateAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $clientWPMEstate = $this->getRepo('AppBundle:ClientWPMEstate')->find($id); /* @var $clientWPMEstate \AppBundle\Entity\ClientWPMEstate */

        if (!$clientWPMEstate || ($this->getUser()->getId() != $clientWPMEstate->getClient()->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $this->getEm()->remove($clientWPMEstate);
        $this->getEm()->flush();

        return $this->setSuccess()->getResponse();

    }
    
    /**
     * @Route("/ajax/client/save-client-event-types/{id}", name="ajax_client_save_client_event_types")
     * @Security("has_role('ROLE_FA')")
     */
    public function saveClientEventTypesAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $form = $this->createForm(ClientEventTypes::class, $financialAdviser);
        $form->handleRequest($request);

        foreach ($financialAdviser->getClientEventTypes() as $clientEventType) { /* @var $clientEventType ClientEventType */
            if (!$clientEventType->getId() && !$clientEventType->getName()) {
                $financialAdviser->getClientEventTypes()->removeElement($clientEventType);
                continue;
            }
            if ($clientEventType->getId() && !$clientEventType->getName()) {
                $existingClientEventType = $this->getRepo('AppBundle:ClientEventType')->find($clientEventType->getId());
                $form->addError(new FormError("Pipeline event ".$existingClientEventType->getName()." name cannot be changed to be blank"));
                continue;
            }
            $clientEventType->setFinancialAdviser($financialAdviser);
        }

        if ($form->isValid()) {

            $this->persistEntity($financialAdviser);

            $this->flush();

            $this->setData([
                'id' => $financialAdviser->getId(),
                'clientEventTypeOptions' => $this->get('app.manager.client')->getFAClientEventTypeOptions($financialAdviser)
            ]);
            return $this->setSuccess()->getResponse();
        } else {
            // return error
            $errors = $form->getErrors(true);
            if ($errors) {
                foreach ($errors as $error) {
                    /* @var $error FormError */
                    $this->addError($error->getMessage());
                }
            } else {
                $this->addError('Sorry, there was a problem saving the event');
            }
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/client/get-client-event-types-form/{id}", name="ajax_client_event_types_form", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function getClientEventTypesForm($id, Request $request) {

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if (!$client || ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId())) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        return $this->render('AppBundle:Client/widget:client-event-types-form.html.twig', [
           'client_event_types_form' => $this->createForm(ClientEventTypes::class, $this->getUser()->getFinancialAdviser(), [
              'action' => $this->generateUrl('ajax_client_save_client_event_types', ['id' => $client->getId()])
           ])->createView()
        ]);
    }

    /**
     * @Route("/ajax/client/delete", name="ajax_client_delete", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function deleteAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $clientId = $request->get('id');
        $client = $clientId ? $this->getRepo('AppBundle:Client')->find($clientId) : new Client(); /* @var $client Client */

        if ($client->getFinancialAdviser() && $this->getUser()->getId() != $client->getFinancialAdviser()->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }
        $reviews = $this->getRepo('AppBundle:Review')->findBy(['client' => $client->getId()]);
        if ($reviews) {
            return $this->setFailure("There are reviews associated with this client so it cannot be delete")->getResponse();
        }

        $this->get('app.manager.client')->deleteClient($client);

        return $this->getResponse();

    }

    /**
     * @Route("/ajax/client/data/{id}", name="ajax_client_data", options={"expose"=true})
     * @Security("has_role('ROLE_FA')")
     */
    public function getClientDataAction($id, Request $request) {

        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if ($client->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure('Access denied')->getResponse();
        }

        $this->setData([
            'id' => $client->getId(),
            'title' => $client->getPerson()->getTitle(),
            'firstName' => $client->getPerson()->getFirstName(),
            'lastName' => $client->getPerson()->getLastName(),
            'email' => $client->getPerson()->getEmail(),
            'telephone' => $client->getPerson()->getTelephone(),
            'mobile' => $client->getPerson()->getMobile(),
            'dob' => $client->getPerson()->getDob() ? $client->getPerson()->getDob()->format('Y-m-d') : '',
            'address1' => $client->getAddress()->getAddress1(),
            'address2' => $client->getAddress()->getAddress2(),
            'city' => $client->getAddress()->getCity(),
            'postCode' => $client->getAddress()->getPostCode(),
            'country' => $client->getAddress()->getCountry() ? $client->getAddress()->getCountry()->getId() : '',
        ]);
        return $this->getResponse();

    }

    /**
     * @Route("/ajax/client/photo-upload/{id}", name="ajax_client_photo_upload", options={"expose"=true})
     * @Security("has_role('ROLE_FA')")
     */
    public function photoUploadAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $allowedTypes = ['image/jpg', 'image/jpeg', 'image/png'];
        $client = $this->getRepo('AppBundle:Client')->find($id); /* @var $client Client */

        if ($client->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $uploadedFile = $request->files->get('photo_upload_id'); // just upload one
        $uploads = $this->getUploadManager()->saveUploads($this->getUser(), [$uploadedFile], UploadManager::TYPE_PUBLIC, $allowedTypes);

        $this->flush();

        if (count($uploads) == 1) {
            $upload = array_shift($uploads); /* @var $upload Upload */

            // remove old profile image
            if ($client->getPhotoUpload()) {
                $this->getEm()->remove($client->getPhotoUpload());
            }
            // set new profile image
            $client->setPhotoUpload($upload);
            $this->persistEntity($client)->flush();

            $this->setData([
               'id' => $upload->getId(),
               'name' => $upload->getFilename(),
               'type' => $upload->getType(),
               'has_meta_data' => $upload->getMetaData() ? 1 : 0,
               'url' => $upload->getWebPath()
            ]);
            return $this->getResponse();
        }



        $this->setFailure($this->getUploadManager()->getErrors());
        return $this->getResponse();

    }



    /**
     * @Route("/ajax/client/insurance-policy-upload/{id}", name="ajax_client_insurance_policy_upload", options={"expose"=true})
     * @Security("has_role('ROLE_FA')")
     */
    public function insurancePolicyUploadAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $allowedTypes = ['image/jpg', 'image/jpeg', 'image/png', 'application/pdf'];
        $clientWPMInsurancePolicy = $this->getRepo('AppBundle:ClientWPMInsurancePolicy')->find($id); /* @var $clientWPMInsurancePolicy \AppBundle\Entity\ClientWPMInsurancePolicy */

        if ($clientWPMInsurancePolicy->getClient()->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $uploadedFile = $request->files->get('document_upload_id'); // just upload one
        $uploads = $this->getUploadManager()->saveUploads($this->getUser(), [$uploadedFile], UploadManager::TYPE_PRIVATE, $allowedTypes);

        $this->flush();

        if (count($uploads) == 1) {
            $upload = array_shift($uploads); /* @var $upload Upload */

            // remove old profile image
            if ($clientWPMInsurancePolicy->getDocumentUpload()) {
                $this->getEm()->remove($clientWPMInsurancePolicy->getDocumentUpload());
            }
            // set new profile image
            $clientWPMInsurancePolicy->setDocumentUpload($upload);
            $this->persistEntity($clientWPMInsurancePolicy)->flush();

            $this->setData([
               'id' => $upload->getId(),
               'name' => $upload->getFilename(),
               'type' => $upload->getType(),
               'has_meta_data' => $upload->getMetaData() ? 1 : 0,
               'url' => $upload->getWebPath()
            ]);
            return $this->getResponse();
        }



        $this->setFailure($this->getUploadManager()->getErrors());
        return $this->getResponse();

    }

    /**
     * Action to import client data
     *
     * @Route("/ajax/client/import", name="ajax_client_import")
     * @Security("has_role('ROLE_FA')")
     */
    public function importAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $contactTypeId = $request->get('contactTypeId');
        $columnTypes = $request->get('columnTypes');
        $contactsData = $request->get('contactsData');

        $financialAdviser = $this->getUser()->getFinancialAdviser();

        $clientImporter = $this->get('app.client_import');
        $clientImport = $clientImporter->import($financialAdviser, $columnTypes, $contactsData, $contactTypeId);

        $this->setData(['count' => $clientImport->getNumClients()]);
        return $this->getResponse();

    }
}
