<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ReviewTemplate;
use AppBundle\Entity\ReviewTemplateQuestion;
use AppBundle\Form\Type\Review;
use AppBundle\Form\Type\ReviewTemplateSystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReviewTemplateController extends BaseController
{



    /**
     * @Route("/review-templates", name="review_templates")
     * @Template("AppBundle:ReviewTemplate:index.html.twig")
     */
    public function indexAction(Request $request) {

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $clientsData = $this->get('app.manager.client')->getFAClientsSelectData($financialAdviser);
        $groupsData = $this->get('app.manager.client')->getFAGroupsSelectData($financialAdviser);

        $filter = [];
        $filter['type'] = $request->get('type', \AppBundle\Entity\Review::TYPE_EXPERIENCE);
        $reviewTemplates = $this->get('app.manager.review_template_manager')->getQueryForFA($financialAdviser, $filter)->getResult();

        return [
           'financialAdviser' => $financialAdviser,
           'clientsData' => json_encode($clientsData),
           'groupsData' => json_encode($groupsData),
           'reviewTemplates' => $reviewTemplates,
           'filter' => $filter,
           'showRequestReview' => $request->get('showRequestReview', 0)
        ];
    }

    /**
     * @Route("/review-request-standard", name="review_request_standard")
     * @Template("AppBundle:ReviewTemplate:request-standard.html.twig")
     */
    public function reviewRequestStandardAction(Request $request) {

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $clientsData = $this->get('app.manager.client')->getFAClientsSelectData($financialAdviser);
        $groupsData = $this->get('app.manager.client')->getFAGroupsSelectData($financialAdviser);

        $filter = [];

        $reviewTemplates = $this->get('app.manager.review_template_manager')->getQueryForFA($financialAdviser, $filter)->getResult();

        return [
           'financialAdviser' => $financialAdviser,
           'clientsData' => json_encode($clientsData),
           'groupsData' => json_encode($groupsData),

        ];
    }


    /**
     * @param         $id
     * @param Request $request
     * @Route("/review-template/edit/{id}", name="review_template_edit")
     * @Template("AppBundle:ReviewTemplate:edit.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction($id, Request $request)
    {

        $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($id); /* @var $reviewTemplate ReviewTemplate */

        if (!$reviewTemplate->getFinancialAdviser() || ($reviewTemplate->getFinancialAdviser() && $reviewTemplate->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId())) {
            $this->addFlash('error', 'Access Denied');
            return $this->redirectToRoute('review_templates', ['type' => \AppBundle\Entity\Review::TYPE_CUSTOM]);
        }

        $form = $this->createForm(\AppBundle\Form\Type\ReviewTemplate::class, $reviewTemplate);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $archivedQuestions = $request->get('archivedQuestions');

            if ($reviewTemplate->getReviewTemplateQuestions()) {
                foreach ($reviewTemplate->getReviewTemplateQuestions() as $reviewTemplateQuestion) { /* @var $reviewTemplateQuestion ReviewTemplateQuestion */
                    $reviewTemplateQuestion->setReviewTemplate($reviewTemplate);
                    if (!empty($archivedQuestions[$reviewTemplateQuestion->getId()]) && !$reviewTemplateQuestion->getArchivedAt()) {
                        $reviewTemplateQuestion->setArchivedAt(new \DateTime());
                    }
                }
            }

            $reviewTemplate->setTypeId(\AppBundle\Entity\Review::TYPE_CUSTOM);
            $this->persistEntity($reviewTemplate)->flush();
            $this->addFlash("success", "Review form saved successfully");
            //return $this->redirect($this->generateUrl('review_template_edit', ['id' => $reviewTemplate->getId()]));

        }

        return [
           'form' => $form->createView(),
           'reviewTemplate' => $reviewTemplate
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/review-template/delete/{id}", name="review_template_delete", options={"expose": true})
     * @Template("AppBundle:ReviewTemplate:delete.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id, Request $request)
    {

        $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($id); /* @var $reviewTemplate ReviewTemplate */

        if (!$reviewTemplate->getFinancialAdviser() || ($reviewTemplate->getFinancialAdviser() && $reviewTemplate->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId())) {
            $this->addFlash('error', 'Access Denied');
            return $this->redirectToRoute('review_templates', ['type' => \AppBundle\Entity\Review::TYPE_CUSTOM]);
        }

        $reviewTemplate->setArchivedAt(new \DateTime());
        $this->persistEntity($reviewTemplate)->flush();

        $this->addFlash('success', 'Review Form "'.$reviewTemplate->getName().'" has been deleted');

        return $this->redirectToRoute('review_templates', ['type' => \AppBundle\Entity\Review::TYPE_CUSTOM]);
    }


    /**
     * @Route("/review-template/add", name="review_template_add")
     * @Template("AppBundle:ReviewTemplate:add.html.twig")
     */
    public function addAction(Request $request)
    {

        $reviewTemplate = new ReviewTemplate();
        $reviewTemplate->setSubject('How Would You Rate My Service?');
        $reviewTemplate->setMessage("{adviserName} has sent a request to for you to review their service.\n\nIt's totally optional but you'd really be helping them out if you {link:click here and give them some feedback!}");
        $reviewTemplate->addReviewTemplateQuestion(new ReviewTemplateQuestion());
        $form = $this->createForm(\AppBundle\Form\Type\ReviewTemplate::class, $reviewTemplate);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $reviewTemplate->setFinancialAdviser($this->getUser()->getFinancialAdviser());
            $reviewTemplate->setTypeId(\AppBundle\Entity\Review::TYPE_CUSTOM);

            if ($reviewTemplate->getReviewTemplateQuestions()) {
                foreach ($reviewTemplate->getReviewTemplateQuestions() as $reviewTemplateQuestion) { /* @var $reviewTemplateQuestion ReviewTemplateQuestion */
                    if (!$reviewTemplateQuestion->getQuestion()) {
                        $form->addError(new FormError("Question cannot be empty"));
                    }
                    $reviewTemplateQuestion->setReviewTemplate($reviewTemplate);
                }
            }
            if ($form->isValid()) {
                $this->persistEntity($reviewTemplate)->flush();


                $this->addFlash("success", "Review form added successfully");

                return $this->redirect(
                   $this->generateUrl('review_template_edit', ['id' => $reviewTemplate->getId()])
                );
            }

        } else {
            // return error
//            $errors = $form->getErrors(true);
//            if ($errors) {
//                foreach ($errors as $error) {
//                    /* @var $error FormError */
//                    $this->addError($error->getMessage());
//                }
//            } else {
//                $this->addError('Sorry, there was a problem saving the event');
//            }
        }

        return [
           'form' => $form->createView()
        ];
    }

    /**
     * @Route("/review/submit/{token}", name="review_submit")
     * @Template("AppBundle:Review:submit.html.twig")
     * @Security("has_role('IS_AUTHENTICATED_ANONYMOUSLY') or has_role('ROLE_USER')")
     *
    public function submitAction($token, Request $request) {


    $review = $this->getRepo('AppBundle:Review')->findOneBy(['token' => $token]);

    $form = $this->createForm(Review::class, $review);

    return [
    'review' => $review,
    'form' => $form->createView()
    ];
    }
     */

}
