<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ReviewTemplate;
use AppBundle\Entity\ReviewTemplateQuestion;
use AppBundle\Form\Type\ReviewTemplateSystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class AdminReviewTemplateController extends BaseController
{
    /**
     * @Route("/admin/review/templates", name="admin_review_templates")
     * @Template("AppBundle:Admin:review-template/index.html.twig")
     */
    public function indexAction(Request $request)
    {

        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $filter = [];

        $filter['page'] = $page;
        $filter['rows'] = $rows;
        $query = $this->get('app.manager.review_template_manager')->getQueryForSystem();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
           $query,
           $page,
           $rows
        );

        $viewData = [
           'pagination' => $pagination,
           'filter' => $filter
        ];

        return $viewData;
    }

    /**
     * @Route("/admin/review-template/view/{id}", name="admin_review_template_view")
     * @Template("AppBundle:Admin:review-template/view.html.twig")
     */
    public function viewAction($id)
    {
        $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($id);

        return [
           'reviewTemplate' => $reviewTemplate
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/review-template/edit/{id}", name="admin_review_template_edit")
     * @Template("AppBundle:Admin:review-template/edit.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction($id, Request $request)
    {

        $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($id); /* @var $reviewTemplate ReviewTemplate */

        $form = $this->createForm(ReviewTemplateSystem::class, $reviewTemplate);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($request->get('archived', 0)) {
                $reviewTemplate->setArchivedAt(new \DateTime());
            } else {
                $reviewTemplate->setArchivedAt(null);
            }

            $this->persistEntity($reviewTemplate)->flush();

            $this->addFlash("success", "Review template updated successfully");
            return $this->redirect($this->generateUrl('admin_review_template_edit', ['id' => $reviewTemplate->getId()]));

        }

        return [
           'form' => $form->createView(),
           'reviewTemplate' => $reviewTemplate
        ];
    }

    /**
     * @Route("/admin/review-template/add", name="admin_review_template_add")
     * @Template("AppBundle:Admin:review-template/add.html.twig")
     */
    public function addAction(Request $request)
    {

        $reviewTemplate = new ReviewTemplate();#
        $reviewTemplate->setSubject('How Would You Rate My Service?');
        $reviewTemplate->setMessage("{adviserName} has sent a request to for you to review their service.\n\nIt's totally optional but you'd really be helping them out if you {link:click here and give them some feedback!}");

        $form = $this->createForm(ReviewTemplateSystem::class, $reviewTemplate);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->persistEntity($reviewTemplate)->flush();

            $this->addFlash("success", "Review template added successfully");
            return $this->redirect($this->generateUrl('admin_review_template_edit', ['id' => $reviewTemplate->getId()]));

        }

        return [
           'form' => $form->createView()
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/review-template-question/edit/{id}", name="admin_review_template_question_edit")
     * @Template("AppBundle:Admin:review-template-question/edit.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editQuestionAction($id, Request $request)
    {

        $reviewTemplateQuestion = $this->getRepo('AppBundle:ReviewTemplateQuestion')->find($id); /* @var $reviewTemplateQuestion ReviewTemplateQuestion */

        $form = $this->createForm(\AppBundle\Form\Type\ReviewTemplateQuestion::class, $reviewTemplateQuestion);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($request->get('archived', 0)) {
                $reviewTemplateQuestion->setArchivedAt(new \DateTime());
            } else {
                $reviewTemplateQuestion->setArchivedAt(null);
            }

            $this->persistEntity($reviewTemplateQuestion)->flush();

            $this->addFlash("success", "Review template question updated successfully");
            return $this->redirect($this->generateUrl('admin_review_template_view', ['id' => $reviewTemplateQuestion->getReviewTemplate()->getId()]));

        }

        return [
           'form' => $form->createView(),
           'reviewTemplateQuestion' => $reviewTemplateQuestion,
           'reviewTemplate' => $reviewTemplateQuestion->getReviewTemplate()
        ];
    }

    /**
     * @param         $id       Review Template ID
     * @param Request $request
     * @Route("/admin/review-template-question/add/{id}", name="admin_review_template_question_add")
     * @Template("AppBundle:Admin:review-template-question/add.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addQuestionAction($id, Request $request)
    {

        $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($id); /* @var $reviewTemplate ReviewTemplate */

        $reviewTemplateQuestion = new ReviewTemplateQuestion();
        $form = $this->createForm(\AppBundle\Form\Type\ReviewTemplateQuestion::class, $reviewTemplateQuestion);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $reviewTemplateQuestion->setReviewTemplate($reviewTemplate);

            $this->persistEntity($reviewTemplateQuestion)->flush();

            $this->addFlash("success", "Review template question added successfully");
            return $this->redirect($this->generateUrl('admin_review_template_view', ['id' => $reviewTemplate->getId()]));

        }

        return [
           'form' => $form->createView(),
           'reviewTemplate' => $reviewTemplate
        ];
    }
}
