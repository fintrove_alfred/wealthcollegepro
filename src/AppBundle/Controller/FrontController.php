<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class FrontController extends BaseController
{

    const OFFLINE = false;


    /**
     * @Route("/", name="home_page")
     * @Template("AppBundle:Front:homepage.html.twig")
     */
    public function homeAction() {
        return $this->redirectToRoute('fos_user_security_login');
        if (self::OFFLINE) {
            return $this->redirectToRoute('under_construction');
        }
        $filterOptions = $this->get('app.manager.financial_adviser')->getFilterOptions();
        return [
            'filterOptions' => $filterOptions
        ];
    }

    /**
     * @Route("/features", name="features")
     * @Template("AppBundle:Front:features.html.twig")
     */
    public function featuresAction() {
        if (self::OFFLINE) {
            return $this->redirectToRoute('under_construction');
        }
        return [];
    }

    /**
     * @Route("/pricing", name="pricing")
     * @Template("AppBundle:Front:pricing.html.twig")
     */
    public function pricingAction() {
        if (self::OFFLINE) {
            return $this->redirectToRoute('under_construction');
        }
        return [];
    }
    /**
     * @Route("/contact", name="contact")
     * @Template("AppBundle:Front:contact.html.twig")
     */
    public function contactAction() {
        if (self::OFFLINE) {
            return $this->redirectToRoute('under_construction');
        }
        return [];
    }

    /**
     * @Route("/under-construction", name="under_construction")
     * @Template("AppBundle:Front:under-construction.html.twig")
     */
    public function underConstructionAction() {

        return [];
    }

    /**
     * @Route("/register-interest", name="register_interest")
     * @Template("AppBundle:Front:register-interest.html.twig")
     */
    public function registerInterestAction(Request $request) {

        $countries = $this->getRepo('AppBundle:Country')->findAll();

        return [
            'countries' => $countries
        ];
    }

    /**
     * @Route("/register/confirmed-inactive", name="register_confirmed_inactive")
     * @Template("UserBundle:Registration:confirmed.html.twig")
     */
    public function confirmedInactiveAction()
    {

        return [
            //'targetUrl' => $this->getTargetUrlFromSession(),
        ];
    }
    
    
}
