<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use AppBundle\Entity\Assessor;
use AppBundle\Entity\AssessorDocument;
use AppBundle\Entity\Payment;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionController extends BaseController
{
    /**
     * @Route("/subscription", name="subscription")
     * @Security("has_role('ROLE_FA')")
     * @Template("AppBundle:Subscription:index.html.twig")
     */
    public function indexAction(Request $request)
    {

        $product = $this->getRepo('AppBundle:Product')->findOneByProductCode('SUBSCRIPTION_1YEAR');

        return [
            'financialAdviser' => $this->getUser()->getFinancialAdviser(),
            'product' => $product
        ];

    }

}
