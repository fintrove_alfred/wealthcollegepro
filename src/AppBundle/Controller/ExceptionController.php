<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController extends BaseController
{


    /**
     * @Route("/access-denied", name="access_denied")
     * @Template("AppBundle:Exception:access-denied.html.twig")
     */
    public function accessDeniedAction() {
        return [];
    }

}
