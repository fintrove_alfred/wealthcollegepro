<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminVerifiableReviewController extends BaseController
{
    /**
     * @Route("/admin/verifiable-reviews", name="admin_verifiable_reviews")
     * @Template("AppBundle:Admin:verifiable-reviews/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 10);

        $search = $request->get('search', '');

        $filter = [];

        $filter['search'] = $search ? $search : '';
        $filter['page'] = $page;
        $filter['rows'] = $rows;
        $filter['role'] = $request->get('role', '');
        $filter['verifiable'] = true;
        $query = $this->get('app.manager.review_manager')->getQuery($filter);


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $page,
            $rows
        );

        $viewData = [
            'pagination' => $pagination,
            'filter' => $filter
        ];

        return $viewData;
    }

    
}
