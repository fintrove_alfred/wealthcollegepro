<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\Webinar;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebinarController extends BaseController
{



    /**
     * @Route("/webinar", name="webinar", options={"expose": true})
     * @Template("AppBundle:Webinar:index.html.twig")
     */
    public function indexAction(Request $request) {

        $financialAdviser = $this->getUser()->getFinancialAdviser();

        $filter = [];
        $filter['past'] = $request->get('past', 0);
        $webinars = $this->get('app.manager.webinar_manager')->getWebinars($financialAdviser, $filter);
        $webinarsNeedingSync = $this->get('app.manager.webinar_manager')->getWebinars($financialAdviser, ['dirty' => 1]);


        $viewData = [
            'financialAdviser' => $financialAdviser,
            'webinars' => $webinars,
            'webinarsNeedingSync' => $webinarsNeedingSync,
            'filter' => $filter
        ];

        $googleAPIClient = $this->get('app.google_api_client');
        if (!$googleAPIClient->getUserAccessToken($this->getUser())) {
            // not authorised with google yet
            $viewData['googleAuthURL'] = $googleAPIClient->getClient($this->getUser())->createAuthUrl();
        }

        return $viewData;
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/webinar/edit/{id}", name="webinar_edit")
     * @Template("AppBundle:Webinar:edit.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction($id, Request $request)
    {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $this->redirectToRoute('webinar');
        }
        
        $webinar = $this->getRepo('AppBundle:Webinar')->find($id); /* @var $webinar Webinar */

        if ($webinar->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            $this->addFlash('error', 'Access Denied');
            return $this->redirectToRoute('webinar');
        }

        $form = $this->createForm(\AppBundle\Form\Type\Webinar::class, $webinar);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if (!$webinar->getCalendarEvent()->getStartAt()) {
                $form->addError(new FormError("Please select a start date"));
            }
            if (!$webinar->getCalendarEvent()->getSummary()) {
                $form->addError(new FormError("Please enter a name for your webinar"));
            }
            if (!$webinar->getCalendarEvent()->getDescription()) {
                $form->addError(new FormError("Please enter a description for your webinar"));
            }

            if ($form->isValid()) {

                $webinar->setFinancialAdviser($this->getUser()->getFinancialAdviser());
                $webinar->getCalendarEvent()->setUser($this->getUser());
                $webinarEventType = $this->getRepo('AppBundle:CalendarEventType')->findOneByHandle('FINTRAIN_SESSIONS');
                $webinar->getCalendarEvent()->setCalendarEventType($webinarEventType);
                $webinar->getCalendarEvent()->setAlertTypeId(CalendarEvent::ALERT_TYPE_DAY_OF_EVENT);
                $alertAt = clone $webinar->getCalendarEvent()->getStartAt();
                $webinar->getCalendarEvent()->setAlertDateTime($alertAt->sub(new \DateInterval('PT1H')));

                $webinar->getCalendarEvent()->setDirty(true);

                $this->persistEntity($webinar);
                $this->persistEntity($webinar->getCalendarEvent());
                $this->flush();

                if ($this->get('app.google_api_client')->getUserAccessToken($this->getUser())) {
                    $this->get('app.manager.calendar_manager')->syncEventsWithGoogleCalendar(
                       $this->getUser(),
                       [$webinar->getCalendarEvent()]
                    );
                }

                if (!$webinar->getCalendarEvent()->isDirty()) {
                    // send invites to attendees if webinar is synced
                    $clientIds = $request->get('client_id', '');
                    $clientIds = explode(',', $clientIds);
                    $clientIds = array_filter($clientIds);
                    $groupIds = $request->get('group_id', '');
                    $groupIds = explode(',', $groupIds);
                    $groupIds = array_filter($groupIds);
                    if ($clientIds || $groupIds) {
                        $calendarEventInvites = $this->get('app.calendar_event_invites');
                        $result = $calendarEventInvites->sendCalendarEventInvites(
                           $webinar->getCalendarEvent(),
                           $clientIds,
                           $groupIds
                        );
                        if ($result === false) {
                            if ($calendarEventInvites->hasErrors()) {
                                $this->addFlash("error", implode('. ',$calendarEventInvites->getErrors()));
                            } else {
                                $this->addFlash("error", "There was a problem inviting attendees");
                            }

                        } else {
                            if (!empty($result['invited']) || !empty($result['alreadyInvited']) || !empty($result['noEmail'])) {
                                $this->addFlash("success", (!empty($result['invited']) ? ' '.$result['invited']." contacts invited." : '').(!empty($result['alreadyInvited']) ? ' '.$result['alreadyInvited']." contacts had already been invited." : '').(!empty($result['noEmail']) ? ' '.$result['noEmail']." contacts have no email address." : ''));
                            }

                        }
                    }
                }

                $this->addFlash("success", "Webinar saved successfully");

                return $this->redirect($this->generateUrl('webinar_edit', ['id' => $webinar->getId()]));
            }

        }

        
        return [
           'form' => $form->createView(),
           'webinar' => $webinar,
           'clientsData' => json_encode($this->get('app.manager.client')->getFAClientsSelectData($this->getUser()->getFinancialAdviser())),
           'groupsData' => json_encode($this->get('app.manager.client')->getFAGroupsSelectData($this->getUser()->getFinancialAdviser()))
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/webinar/save", name="webinar_save")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $this->redirectToRoute('webinar');
        }
        $id = $request->get('id');
        if ($id) {
            $webinar = $this->getRepo('AppBundle:Webinar')->find($id); /* @var $webinar Webinar */
        } else {
            $webinar = new Webinar();
        }

        if ($webinar->getFinancialAdviser() && $webinar->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            $this->addFlash('error', 'Access Denied');
            return $this->redirectToRoute('webinar');
        }

        $form = $this->createForm(\AppBundle\Form\Type\Webinar::class, $webinar);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if (!$webinar->getCalendarEvent()->getStartAt()) {
                $form->addError(new FormError("Please select a start date"));
            }
            if (!$webinar->getCalendarEvent()->getSummary()) {
                $form->addError(new FormError("Please enter a name for your webinar"));
            }
            if (!$webinar->getCalendarEvent()->getDescription()) {
                $form->addError(new FormError("Please enter a description for your webinar"));
            }

            if ($form->isValid()) {

                $webinar->setFinancialAdviser($this->getUser()->getFinancialAdviser());
                $webinar->getCalendarEvent()->setUser($this->getUser());
                $webinarEventType = $this->getRepo('AppBundle:CalendarEventType')->findOneByHandle('FINTRAIN_SESSIONS');
                $webinar->getCalendarEvent()->setCalendarEventType($webinarEventType);
                $webinar->getCalendarEvent()->setAlertTypeId(CalendarEvent::ALERT_TYPE_DAY_OF_EVENT);
                $alertAt = clone $webinar->getCalendarEvent()->getStartAt();
                $webinar->getCalendarEvent()->setAlertDateTime($alertAt->sub(new \DateInterval('PT1H')));

                $webinar->getCalendarEvent()->setDirty(true);

                $this->persistEntity($webinar);
                $this->persistEntity($webinar->getCalendarEvent());
                $this->flush();

                if ($this->get('app.google_api_client')->getUserAccessToken($this->getUser())) {
                    $this->get('app.manager.calendar_manager')->syncEventsWithGoogleCalendar(
                       $this->getUser(),
                       [$webinar->getCalendarEvent()]
                    );
                }

                if (!$webinar->getCalendarEvent()->isDirty()) {
                    // send invites to attendees if webinar is synced
                    $clientIds = $request->get('client_id', '');
                    $clientIds = explode(',', $clientIds);
                    $clientIds = array_filter($clientIds);
                    $groupIds = $request->get('group_id', '');
                    $groupIds = explode(',', $groupIds);
                    $groupIds = array_filter($groupIds);
                    if ($clientIds || $groupIds) {
                        $calendarEventInvites = $this->get('app.calendar_event_invites');
                        $result = $calendarEventInvites->sendCalendarEventInvites(
                           $webinar->getCalendarEvent(),
                           $clientIds,
                           $groupIds
                        );
                        if ($result === false) {
                            if ($calendarEventInvites->hasErrors()) {
                                $this->addFlash("error", implode('. ',$calendarEventInvites->getErrors()));
                            } else {
                                $this->addFlash("error", "There was a problem inviting attendees");
                            }

                        } else {
                            if (!empty($result['invited']) || !empty($result['alreadyInvited']) || !empty($result['noEmail'])) {
                                $this->addFlash("success", (!empty($result['invited']) ? ' '.$result['invited']." contacts invited." : '').(!empty($result['alreadyInvited']) ? ' '.$result['alreadyInvited']." contacts had already been invited." : '').(!empty($result['noEmail']) ? ' '.$result['noEmail']." contacts have no email address." : ''));
                            }

                        }
                    }
                }

                $this->addFlash("success", "Webinar saved successfully");


            } else {
                $this->addFlash("error", "Form is invalid");
            }

        }

        return $this->redirect($this->generateUrl('webinar_edit', ['id' => $webinar->getId()]));
    }

    /**
     * @Route("/webinar/add", name="webinar_add")
     * @Template("AppBundle:Webinar:add.html.twig")
     */
    public function addAction(Request $request)
    {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $this->redirectToRoute('webinar');
        }

        $webinar = new Webinar();
        $form = $this->createForm(\AppBundle\Form\Type\Webinar::class, $webinar);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if (!$webinar->getCalendarEvent()->getStartAt()) {
                $form->addError(new FormError("Please select a start date"));
            }
            if (!$webinar->getCalendarEvent()->getSummary()) {
                $form->addError(new FormError("Please enter a name for your webinar"));
            }
            if (!$webinar->getCalendarEvent()->getDescription()) {
                $form->addError(new FormError("Please enter a description for your webinar"));
            }

            if ($form->isValid()) {

                $webinar->setFinancialAdviser($this->getUser()->getFinancialAdviser());
                $webinar->getCalendarEvent()->setUser($this->getUser());
                $webinarEventType = $this->getRepo('AppBundle:CalendarEventType')->findOneByHandle('FINTRAIN_SESSIONS');
                $webinar->getCalendarEvent()->setCalendarEventType($webinarEventType);
                $webinar->getCalendarEvent()->setAlertTypeId(CalendarEvent::ALERT_TYPE_DAY_OF_EVENT);
                $webinar->getCalendarEvent()->setAlertDateTime($webinar->getCalendarEvent()->getStartAt()->sub(new \DateInterval('PT1H')));

                $this->persistEntity($webinar);
                $this->persistEntity($webinar->getCalendarEvent());
                $this->flush();

                if ($this->get('app.google_api_client')->getUserAccessToken($this->getUser())) {
                    $this->get('app.manager.calendar_manager')->syncEventsWithGoogleCalendar(
                       $this->getUser(),
                       [$webinar->getCalendarEvent()]
                    );
                }

                $this->addFlash("success", "Webinar added successfully");

                return $this->redirect($this->generateUrl('webinar_edit', ['id' => $webinar->getId()]));
            }

        }

        return [
            'form' => $form->createView(),
            'clientsData' => json_encode($this->get('app.manager.client')->getFAClientsSelectData($this->getUser()->getFinancialAdviser())),
            'groupsData' => json_encode($this->get('app.manager.client')->getFAGroupsSelectData($this->getUser()->getFinancialAdviser()))
        ];
    }

    /**
     * @Route("/webinar/detail/{id}", name="webinar_detail", options={"expose"=true})
     * @Template("AppBundle:Webinar:detail.html.twig")
     */
    public function detailAction($id, Request $request) {


        $viewData = [];
        $webinar = $this->getRepo('AppBundle:Webinar')->find($id); /* @var $webinar \AppBundle\Entity\Webinar */
                
        $viewData['webinar'] = $webinar;
        $viewData['financialAdviser'] = $webinar->getFinancialAdviser();

        return $viewData;
    }



}
