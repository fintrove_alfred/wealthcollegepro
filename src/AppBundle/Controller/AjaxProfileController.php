<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\FinancialAdviserPhoto;
use AppBundle\Entity\Upload;
use AppBundle\Service\UploadManager;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxProfileController extends BaseAjaxController
{



   /**
    * Form submit handler for basic info
    *
    * @Route("/ajax/profile/edit-basic-info", name="ajax_profile_edit_basic_info")
    */
    public function editBasicInfoAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $user = $this->getUser();

        $form = $this->createForm('AppBundle\Form\Type\ProfileBasicInfo', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {


            if (!$user->getFinancialAdviser()->getUser()) {
                $user->getFinancialAdviser()->setUser($user);
            }
            // save the event
            $this->persistEntity($user)->flush();

            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * Form submit handler for write up
     *
     * @Route("/ajax/profile/edit-write-up", name="ajax_profile_edit_write_up")
     */
    public function editWriteUpAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $fa = $this->getUser()->getFinancialAdviser();

        $form = $this->createForm('AppBundle\Form\Type\ProfileWriteUp', $fa);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // save the event
            $this->persistEntity($fa)->flush();

            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * Form submit handler for essential details
     *
     * @Route("/ajax/profile/edit-essential-details", name="ajax_profile_edit_essential_details")
     */
    public function editEssentialDetailsAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $user = $this->getUser();

        $form = $this->createForm('AppBundle\Form\Type\ProfileEssentialDetails', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if (!$user->getFinancialAdviser()->getUser()) {
                $user->getFinancialAdviser()->setUser($user);
            }
            // save the event
            $this->persistEntity($user)->flush();

            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * Form submit handler for contact address
     *
     * @Route("/ajax/profile/edit-contact-address", name="ajax_profile_edit_contact_address")
     */
    public function editContactAddressAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $user = $this->getUser();

        $form = $this->createForm('AppBundle\Form\Type\ProfileContactAddress', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {


            // save the event
            $this->persistEntity($user)->flush();

            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * Form submit handler for profile description
     *
     * @Route("/ajax/profile/edit-description", name="ajax_profile_edit_description")
     */
    public function editDescriptionAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $user = $this->getUser();

        $form = $this->createForm('AppBundle\Form\Type\ProfileDescription', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {


            // save the event
            $this->persistEntity($user)->flush();

            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/profile/gallery-image-upload", name="ajax_profile_photo_gallery_upload", options={"expose"=true})
     */
    public function profilePhotoGalleryUploadAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $allowedTypes = ['image/jpg', 'image/jpeg', 'image/png'];
        $uploadedFiles = $request->files->get('image_upload'); // just upload one
        $type = $request->get('type');


        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $photos = $this->get('app.manager.financial_adviser')->getPhotosByType($financialAdviser, $type);
        if (count($photos) >= 5 || count($uploadedFiles) > 5 || (count($photos) + count($uploadedFiles) > 5)) {
            return $this->setFailure("A maximum of 5 photos are allowed per photo gallery")->getResponse();
        }


        $uploads = $this->getUploadManager()->saveUploads($this->getUser(), $uploadedFiles, UploadManager::TYPE_PUBLIC, $allowedTypes);

        $this->flush();

        $responseData = [];
        foreach ($uploads as $i => $upload) { /* @var $upload Upload */

            $financialAdviserPhoto = new FinancialAdviserPhoto();
            $financialAdviserPhoto->setFinancialAdviser($financialAdviser);
            $financialAdviserPhoto->setUpload($upload);
            $financialAdviserPhoto->setType($type);
            $financialAdviserPhoto->setSortOrder(count($photos)+$i+1);

            $this->persistEntity($financialAdviserPhoto)->flush();

            $responseData[] = [
                'id' => $upload->getId(),
                'name' => $upload->getFilename(),
                'type' => $upload->getType(),
                'has_meta_data' => $upload->getMetaData() ? 1 : 0,
                'url' => $upload->getWebPath(),
                'photoHTML' => $this->renderView('AppBundle:Profile:widgets/gallery-photo.html.twig', ['photo' => $financialAdviserPhoto])
            ];

        }
        $this->setData($responseData);
        return $this->getResponse();

    }

    /**
     * @Route("/ajax/profile/gallery-image-delete/{id}", name="ajax_profile_photo_gallery_delete", options={"expose"=true})
     */
    public function profilePhotoGalleryDeleteAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $photo = $this->getEm()->getRepository('AppBundle:FinancialAdviserPhoto')->find($id);

        if (!$photo) {
            return $this->setFailure("Photo could not be found")->getResponse();
        }
        if ($photo->getFinancialAdviser()->getId() != $financialAdviser->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $this->getEm()->remove($photo->getUpload());
        $this->getEm()->remove($photo);
        $this->getEm()->flush();

        return $this->getResponse();

    }

    /**
     * @Route("/ajax/profile/photo-gallery-image-upload", name="ajax_profile_image_upload", options={"expose"=true})
     */
    public function profileImageUploadAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        $allowedTypes = ['image/jpg', 'image/jpeg', 'image/png'];
        $uploadedFile = $request->files->get('profile_image_upload_id'); // just upload one
        $uploads = $this->getUploadManager()->saveUploads($this->getUser(), [$uploadedFile], UploadManager::TYPE_PUBLIC, $allowedTypes);

        $this->flush();

        if (count($uploads) == 1) {
            $upload = array_shift($uploads); /* @var $upload Upload */

            $financialAdviser = $this->getUser()->getFinancialAdviser();
            // remove old profile image
            if ($financialAdviser->getProfileImageUpload()) {
                $this->getEm()->remove($financialAdviser->getProfileImageUpload());
            }
            // set new profile image
            $financialAdviser->setProfileImageUpload($upload);
            $this->persistEntity($financialAdviser)->flush();

            $this->setData([
               'id' => $upload->getId(),
               'name' => $upload->getFilename(),
               'type' => $upload->getType(),
               'has_meta_data' => $upload->getMetaData() ? 1 : 0,
               'url' => $upload->getWebPath()
            ]);
            return $this->getResponse();
        }



        $this->setFailure($this->getUploadManager()->getErrors());
        return $this->getResponse();

    }

    /**
     * @Route("/ajax/profile/profile-background-image-upload", name="ajax_profile_background_image_upload", options={"expose"=true})
     */
    public function profileHeaderImageUploadAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }

        $allowedTypes = ['image/jpg', 'image/jpeg', 'image/png'];
        $uploadedFile = $request->files->get('profile_background_image_upload_id'); // just upload one
        $uploads = $this->getUploadManager()->saveUploads($this->getUser(), [$uploadedFile], UploadManager::TYPE_PUBLIC, $allowedTypes);

        $this->flush();

        if (count($uploads) == 1) {
            $upload = array_shift($uploads); /* @var $upload Upload */

            $financialAdviser = $this->getUser()->getFinancialAdviser();
            // remove old profile image
            if ($financialAdviser->getProfileBackgroundUpload()) {
                $this->getEm()->remove($financialAdviser->getProfileBackgroundUpload());
            }
            // set new profile image
            $financialAdviser->setProfileBackgroundUpload($upload);
            $this->persistEntity($financialAdviser)->flush();

            $this->setData([
               'id' => $upload->getId(),
               'name' => $upload->getFilename(),
               'type' => $upload->getType(),
               'has_meta_data' => $upload->getMetaData() ? 1 : 0,
               'url' => $upload->getWebPath()
            ]);
            return $this->getResponse();
        }



        $this->setFailure($this->getUploadManager()->getErrors());
        return $this->getResponse();

    }


}
