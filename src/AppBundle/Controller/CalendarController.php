<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CalendarEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CalendarController extends BaseController
{


    /**
     * @Route("/calendar/{view}", name="calendar", options={"expose"=true})
     * @Template("AppBundle:Calendar:index.html.twig")
     */
    public function indexAction($view = 'week', Request $request) {

        $calendarManager = $this->get('app.manager.calendar_manager');
        $calendarEventTypes = $calendarManager->getUserCalendarEventTypes($this->getUser());

        $viewData = [
            'view' => $view,
            'calendarEventTypes' => $calendarEventTypes
        ];

        $calendarEventId = $request->get('calendarEventId');
        if ($calendarEventId) {
            $calendarEvent = $this->getRepo('AppBundle:CalendarEvent')->find($calendarEventId); /* @var $calendarEvent CalendarEvent */
            $viewData['startDate'] = $calendarEvent->getStartAt();
        }

        return $viewData;
    }

}
