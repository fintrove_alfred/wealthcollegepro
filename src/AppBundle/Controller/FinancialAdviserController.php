<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FinancialAdviserController extends BaseController
{


    /**
     * @Route("/financial-advisers", name="fa_search")
     * @Template("AppBundle:FinancialAdviser:search.html.twig")
     */
    public function searchAction(Request $request) {

        $filter = $request->get('filter');
        
        $faManager = $this->get('app.manager.financial_adviser');
        $financialAdvisers = $faManager->getSearchResults($filter);

        return [
            'financialAdvisers' => $financialAdvisers,
            'filterOptions' => $faManager->getFilterOptions(),
            'filter' => $filter
        ];
    }

}
