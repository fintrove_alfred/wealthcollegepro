<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use AppBundle\Entity\Assessor;
use AppBundle\Entity\AssessorDocument;
use AppBundle\Entity\Payment;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PaymentController extends BaseController
{
    /**
     * @Route("/payment/approval-redirect", name="payment_approval_redirect")
     */
    public function approvalRedirectAction(Request $request)
    {

        $productId = $request->get('productId');
        $quantity = (int) $request->get('quantity', 1);
        $returnURL = $request->get('returnURL', $this->generateUrl('subscription'));
        $product = $this->getRepo('AppBundle:Product')->find($productId);

        if ($quantity > 0) {
            $order = $this->get('app.manager.order_manager')->createSimpleOrder($this->getUser(), $product, $quantity);
            $paypalManager = $this->get('app.manager.paypal_manager');
            $payment = $paypalManager->createPaymentFromOrder($order);

            if ($payment === false) {
                if ($paypalManager->hasErrors()) {
                    foreach ($paypalManager->getErrors() as $error) {
                        $this->addFlash("error", $error);
                    }
                }
            } else {
                // success, redirect to paypal
                return $this->redirect($payment->getApprovalLink());
            }

        } else {
            $this->addFlash("error", "Quantity is not valid");
        }

        return $this->redirect($returnURL);
    }

    /**
     * @Route("/payment/paypal-confirm", name="payment_paypal_return")
     * @Template("AppBundle:Payment:paypal-confirm.html.twig")
     */
    public function confirmAction(Request $request)
    {

        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerID = $request->get('PayerID');

        $payment = $this->getRepo('AppBundle:Payment')->findOneByReferenceNumber($paymentId); /* @var $payment Payment */
        if (!$payment || ($payment && $payment->getUser()->getId() != $this->getUser()->getId())) {
            return $this->redirectToRoute('payment_failed');
        }

        return [
            'payment' => $payment,
            'order' => $payment->getOrder(),
            'paymentId' => $paymentId,
            'token' => $token,
            'PayerID' => $payerID,
        ];
    }

    /**
     * @Route("/payment/paypal-execute", name="payment_paypal_execute")
     */
    public function paypalExecuteAction(Request $request)
    {

        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerID = $request->get('PayerID');

        $orderManager = $this->get('app.manager.order_manager');
        $paypalManager = $this->get('app.manager.paypal_manager');
        $payment = $this->getRepo('AppBundle:Payment')->findOneByReferenceNumber($paymentId); /* @var $payment Payment */
        if (!$payment || ($payment && $payment->getUser()->getId() != $this->getUser()->getId())) {
            return $this->redirectToRoute('payment_failed');
        }

        
        $result = $paypalManager->executePayment($payment, $payerID);
        if ($result) {
            $orderManager->fulfilOrder($payment->getOrder());
            return $this->redirectToRoute('payment_success');

        } else {
            $orderManager->failOrder($payment->getOrder());
            if ($paypalManager->hasErrors()) {
                $this->addFlash("error", implode('<br/>', $paypalManager->getErrors()));
            }
            return $this->redirectToRoute('payment_failed');
        }

        
    }

    /**
     * @Route("/payment/success", name="payment_success")
     * @Template("AppBundle:Payment:success.html.twig")
     */
    public function successAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/payment/cancel", name="payment_cancel")
     * @Template("AppBundle:Payment:cancelled.html.twig")
     */
    public function cancelAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/payment/failed", name="payment_failed")
     * @Template("AppBundle:Payment:failed.html.twig")
     */
    public function failedAction(Request $request)
    {
        return [];
    }
}
