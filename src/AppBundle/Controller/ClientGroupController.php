<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use AppBundle\Entity\ClientClientGroup;
use AppBundle\Entity\ClientDate;
use AppBundle\Entity\ClientRelation;
use AppBundle\Form\Type\ClientGroup;
use AppBundle\Form\Type\ClientProfile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientGroupController extends BaseController
{

    /**
     * @Route("/client-group", name="client_group")
     * @Template("AppBundle:ClientGroup:index.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function indexAction() {

        $clientGroups = $this->getUser()->getFinancialAdviser()->getClientGroups();
        $webinars = $this->get('app.manager.webinar_manager')->getWebinars($this->getUser()->getFinancialAdviser(), ['dirty' => 0]);
        $clientGroupClientCounts = $this->get('app.manager.client')->getClientGroupClientCounts($this->getUser()->getFinancialAdviser());

        return [
            'clientGroups' => $clientGroups,
            'financialAdviser' => $this->getUser()->getFinancialAdviser(),
            'webinars' => $webinars,
            'clientGroupClientCounts' => $clientGroupClientCounts,
            'client_group_form' => $this->createForm(ClientGroup::class, new \AppBundle\Entity\ClientGroup(), [
                    'action' => $this->generateUrl('ajax_client_group_save')
                ])->createView()
        ];
    }

    /**
     * @Route("/client-group/contacts/{id}", name="client_group_clients")
     * @Template("AppBundle:ClientGroup:clients.html.twig")
     * @Security("has_role('ROLE_FA')")
     */
    public function clientsAction($id) {

        $clientGroup = $this->getRepo('AppBundle:ClientGroup')->find($id); /* @var $clientGroup \AppBundle\Entity\ClientGroup */
        if ($clientGroup->getFinancialAdviser()->getId() != $this->getUser()->getFinancialAdviser()->getId()) {
            return $this->createAccessDeniedException();
        }

        $financialAdviser = $this->getUser()->getFinancialAdviser();
        $availableClients = $this->get('app.manager.client')->getFAClientsSelectData($financialAdviser, null);
        $clients = $this->get('app.manager.client')->getClientsByGroupIds($financialAdviser, [$clientGroup->getId()]);

        return [
            'clientGroup' => $clientGroup,
            'financialAdviser' => $financialAdviser,
            'clients' => $clients,
            'availableClients' => $availableClients
        ];
    }
    
}
