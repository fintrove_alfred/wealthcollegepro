<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseAjaxController extends BaseController
{

    protected $responseData = array('success' => true);

    protected function setData($data = array()) {
        $this->responseData['data'] = $data;
        return $this;
    }

    protected function setSuccess($messages = array()) {

        $this->responseData['success'] = true;
        if ($messages) {
            $this->responseData['messages'] = $messages;
        }
        return $this;
    }

    /**
     * Sets the success notification message, will be picked up by front end and shown as a slide out alert
     *
     * @param $message
     * @return $this
     */
    protected function setSuccessNotificationMessage($message) {
        $this->responseData['notification_message'] = $message;
        return $this;
    }

    protected function setFailure($errors = [], $errorTitle = null) {

        $this->responseData['success'] = false;
        if ($errors) {
            if (is_array($errors)) {
                $this->responseData['errors'] = $errors;
            } else {
                if (!isset($this->responseData['errors'])) {
                    $this->responseData['errors'] = [];
                }
                $this->responseData['errors'][] = $errors;
            }
            if ($errorTitle !== null) {
                $this->responseData['errorTitle'] = $errorTitle;
            }
        }
        return $this;
    }

    /**
     * Return an access denied response with success failure and error message
     * @return JsonResponse
     */
    public function getAccessDeniedResponse() {
        return $this->setFailure("Access Denied")->getResponse();
    }

    /**
     * Add an error message to the response
     * @param $error
     * @return $this
     */
    protected function addError($error) {

        if (!empty($this->responseData['errors'])) {
            if (is_array($error)) {
                $this->responseData['errors'] += $error;
            } else {
                $this->responseData['errors'][] = $error;
            }
        } else {
            $this->responseData['errors'][] = $error;
        }
        return $this;
    }

    /**
     * @return bool
     */
    protected function hasErrors() {
        return !empty($this->responseData['errors']);
    }

    /**
     * Add a message to the response
     * @param $message
     */
    protected function addMessage($message) {

        if (!empty($this->responseData['messages'])) {
            if (is_array($message)) {
                $this->responseData['messages'] += $message;
            } else {
                $this->responseData['messages'][] = $message;
            }
        } else {
            $this->responseData['messages'] = $message;
        }
    }

    /**
     * Get the data as JSON Response
     * @return JsonResponse
     */
    protected function getResponse() {
        $response = new JsonResponse();
        $response->setData($this->responseData);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Check if a subscription is active for this user
     * @return bool
     */
    public function checkIsSubscribed($returnResponseIfFalse = true) {
        $result = parent::checkIsSubscribed();
        if ($result) {
            return true;
        }
        if ($returnResponseIfFalse) {
            return $this->setFailure("Your subscription has expired so you can no longer perform that action", "Subscription Expired")->getResponse();
        }
        return false;
    }
}
