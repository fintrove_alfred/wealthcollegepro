<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ReviewAttribute;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class AdminReviewAttributeController extends BaseController
{
    /**
     * @Route("/admin/review/attributes", name="admin_review_attributes")
     * @Template("AppBundle:Admin:review-attribute/index.html.twig")
     */
    public function indexAction(Request $request)
    {

        $page = $request->query->get('page', 1);
        $rows = $request->query->get('rows', 25);

        $filter = [];

        $filter['page'] = $page;
        $filter['rows'] = $rows;
        $query = $this->getRepo('AppBundle:ReviewAttribute')->createQueryBuilder('ra')
            ->orderBy('ra.archived')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
           $query,
           $page,
           $rows
        );

        $viewData = [
           'pagination' => $pagination,
           'filter' => $filter
        ];

        return $viewData;
    }

    /**
     * @Route("/admin/review-attribute/view/{id}", name="admin_review_attribute_view")
     * @Template("AppBundle:Admin:review-attribute/view.html.twig")
     */
    public function viewAction($id)
    {
        $reviewAttribute = $this->getRepo('AppBundle:ReviewAttribute')->find($id);

        return [
           'reviewAttribute' => $reviewAttribute
        ];
    }

    /**
     * @param         $id
     * @param Request $request
     * @Route("/admin/review-attribute/edit/{id}", name="admin_review_attribute_edit")
     * @Template("AppBundle:Admin:review-attribute/edit.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction($id, Request $request)
    {

        $reviewAttribute = $this->getRepo('AppBundle:ReviewAttribute')->find($id); /* @var $reviewAttribute ReviewAttribute */

        $form = $this->createForm(\AppBundle\Form\Type\ReviewAttribute::class, $reviewAttribute);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->persistEntity($reviewAttribute)->flush();

            $this->addFlash("success", "Review attribute updated successfully");
            return $this->redirect($this->generateUrl('admin_review_attribute_edit', ['id' => $reviewAttribute->getId()]));

        }

        return [
           'form' => $form->createView(),
           'reviewAttribute' => $reviewAttribute
        ];
    }

    /**
     * @Route("/admin/review-attribute/add", name="admin_review_attribute_add")
     * @Template("AppBundle:Admin:review-attribute/add.html.twig")
     */
    public function addAction(Request $request)
    {

        $reviewAttribute = new ReviewAttribute();

        $form = $this->createForm(\AppBundle\Form\Type\ReviewAttribute::class, $reviewAttribute);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->persistEntity($reviewAttribute)->flush();

            $this->addFlash("success", "Review attribute added successfully");
            return $this->redirect($this->generateUrl('admin_review_attribute_edit', ['id' => $reviewAttribute->getId()]));

        }

        return [
           'form' => $form->createView()
        ];
    }

}
