<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ReviewTemplate;
use AppBundle\Entity\ReviewTemplateQuestion;
use AppBundle\Form\Type\Review;
use AppBundle\Form\Type\ReviewTemplateSystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminReviewController extends BaseController
{

    /**
     * @Route("/admin/reviews", name="admin_reviews")
     * @Template("AppBundle:Admin:review/index.html.twig")
     */
    public function indexAction(Request $request) {

        $reviews = $this->get('app.manager.review_manager')->getAllReviewsForFA();


        return [
            'reviews' => $reviews,
        ];
    }


}
