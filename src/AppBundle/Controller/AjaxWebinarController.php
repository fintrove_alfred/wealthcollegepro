<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Webinar;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxWebinarController extends BaseAjaxController
{


    /**
     * Sync webinars with google
     *
     * @Route("/ajax/webinar/google-sync", name="ajax_webinar_google_sync", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function googleSyncAction(Request $request) {

        $webinarManager = $this->get('app.manager.webinar_manager');
        $webinarManager->syncWebinarsWithGoogleCalendar($this->getUser()->getFinancialAdviser());
        if ($webinarManager->hasErrors()) {
            return $this->setFailure($webinarManager->getErrors())->getResponse();
        }

        return $this->setSuccess()->getResponse();
    }

    /**
     * Delete a webinar
     *
     * @Route("/ajax/webinar/delete/{id}", name="ajax_webinar_delete", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function deleteAction($id, Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        
        $webinar = $this->getRepo('AppBundle:Webinar')->find($id); /* @var $webinar Webinar */

        if ($webinar->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->getAccessDeniedResponse();
        }

        $webinarManager = $this->get('app.manager.webinar_manager');
        $webinarManager->deleteWebinar($webinar);
        if ($webinarManager->hasErrors()) {
            return $this->setFailure($webinarManager->getErrors())->getResponse();
        }

        $this->addFlash('success', "Webinar was deleted successfully");

        return $this->setSuccess()->getResponse();
    }
    




}
