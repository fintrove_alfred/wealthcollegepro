<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CalendarEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxCalendarEventController extends BaseAjaxController
{


    /**
     * @Route("/ajax/calendar-event/save", name="ajax_calendar_event_save")
     */
    public function saveAction(Request $request) {

        $calendarEventId = $request->get('id');
        $calendarEvent = $calendarEventId ? $this->getRepo('AppBundle:CalendarEvent')->find($calendarEventId) : new CalendarEvent();

        if ($calendarEvent->getUser() && $this->getUser()->getId() != $calendarEvent->getUser()->getId()) {
            return $this->setFailure("You are not authorised to edit this calendar event")->getResponse();
        }

        $form = $this->createForm('AppBundle\Form\Type\CalendarEvent', $calendarEvent);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if (!$calendarEventId) {
                $calendarEvent->setUser($this->getUser());
            }
            // save the event
            $this->persistEntity($calendarEvent)->flush();
            $this->setData([
                'id' => $calendarEvent->getId()
            ]);
            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();

    }

    /**
     * @Route("/ajax/calendar-event/get-events", name="ajax_calendar_event_get_events", options={"expose"=true})
     */
    public function getEventsAction(Request $request) {

        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $view = $request->get('view', 'week');


        if (!$startDate) {
            $startDate = new \DateTime();
        } else {
            $startDate = new \DateTime($startDate);
        }
        if (!$endDate) {
            $endDate = new \DateTime();
        } else {
            $endDate = new \DateTime($endDate);
        }
        $startDate->sub(new \DateInterval('P30D'));
        $endDate->add(new \DateInterval('P30D'));

        $calendarManager = $this->get('app.manager.calendar_manager');
        $eventsData = $calendarManager->getCalendarEventsData($this->getUser(), $startDate, $endDate, $view);
        $this->setData([
           'events' => $eventsData
        ]);

        return $this->getResponse();
    }

    /**
     * @Route("/ajax/calendar-event/save-types", name="ajax_calendar_event_save_types", options={"expose"=true})
     */
    public function saveTypesAction(Request $request) {

        $eventTypeIDs = $request->get('eventTypeIDs', []);

        $calendarManager = $this->get('app.manager.calendar_manager');
        $calendarManager->saveEventTypes($eventTypeIDs);

        return $this->setSuccess()->getResponse();
    }

    /**
     * @Route("/ajax/calendar-event/delete/{id}", name="ajax_calendar_event_delete", options={"expose"=true})
     */
    public function deleteAction($id) {


        $calendarEvent = $this->getRepo('AppBundle:CalendarEvent')->find($id);
        if (!$calendarEvent) {
            return $this->setFailure('Event not found')->getResponse();
        }
        if ($calendarEvent->getUser() && $this->getUser()->getId() != $calendarEvent->getUser()->getId()) {
            return $this->setFailure("You are not authorised to edit this calendar event")->getResponse();
        }

        $this->get('app.manager.calendar_manager')->deleteCalendarEvent($calendarEvent);

        return $this->getResponse();
    }

    /**
     * @Route("/ajax/calendar-event/invite-contacts", name="ajax_calendar_event_invite_contacts")
     */
    public function inviteContactsAction(Request $request) {

        $calendarEventId = $request->get('calendarEventId');
        $clientIds = $request->get('clientIds');

        $calendarEvent = $this->getRepo('AppBundle:CalendarEvent')->find($calendarEventId); /* @var $calendarEvent CalendarEvent */

        if ($calendarEvent->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }
        
        $calendarEventInvitesService = $this->get('app.calendar_event_invites');
        $result = $calendarEventInvitesService->sendCalendarEventInvites($calendarEvent, $clientIds);
        if ($result === false) {
            return $this->setFailure($calendarEventInvitesService->getErrors())->getResponse();
        }

        return $this->setSuccess()->setData($result)->getResponse();

    }

    /**
     * @Route("/ajax/calendar-event/invite-groups", name="ajax_calendar_event_invite_groups")
     */
    public function inviteGroupsAction(Request $request) {

        $calendarEventId = $request->get('calendarEventId');
        $clientGroupIds = $request->get('clientGroupIds');
        $clientGroupIds = explode(',', $clientGroupIds);

        $calendarEvent = $this->getRepo('AppBundle:CalendarEvent')->find($calendarEventId); /* @var $calendarEvent CalendarEvent */

        if ($calendarEvent->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        $calendarEventInvitesService = $this->get('app.calendar_event_invites');
        $result = $calendarEventInvitesService->sendCalendarEventInvites($calendarEvent, null, $clientGroupIds);
        if ($result === false) {
            return $this->setFailure($calendarEventInvitesService->getErrors())->getResponse();
        }

        return $this->setSuccess()->setData($result)->getResponse();

    }


}
