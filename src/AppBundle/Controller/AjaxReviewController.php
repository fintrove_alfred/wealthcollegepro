<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Review;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxReviewController extends BaseAjaxController
{


    /**
     * Action for generic review request of type feedback
     *
     * @Route("/ajax/review/request", name="ajax_review_request")
     * @Security("has_role('ROLE_FA')")
     */
    public function requestAction(Request $request) {

        if (true !== ($result = $this->checkIsSubscribed())) {
            return $result;
        }
        
        $reviewManager = $this->get('app.manager.review_manager');
        $clientManager = $this->get('app.manager.client');
        $clients = $reviewManager->createAndSendReviewRequest($this->getUser()->getFinancialAdviser(), $clientManager, $request);
        if ($clients === false) {
            return $this->setFailure($reviewManager->hasErrors() ? $reviewManager->getErrors() : 'Sorry there was a problem submitting your request')->getResponse();
        }


        return $this->setData(['recipients' => count($clients)])
           ->setSuccess()
           ->getResponse();
    }

    /**
     * Submit handler for a review submission by client
     *
     * @Route("/ajax/review/submit", name="ajax_review_submit")
     */
    public function submitAction(Request $request) {

        $token = $request->get('token');
        if (!$token) {
            return $this->setFailure('Access denied')->getResponse();
        }
        $review = $this->getRepo('AppBundle:Review')->findOneBy(['token' => $token]); /* @var $review Review */
        if (!$review) {
            return $this->setFailure('Access denied')->getResponse();
        }


        $reviewManager = $this->get('app.manager.review_manager');
        $reviewManager->buildReviewForForm($review);

        $form = $this->createForm(\AppBundle\Form\Type\Review::class, $review);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $recaptcha = $this->get('app.recaptcha');
            $remoteIp = $request->getClientIp();
            $response = $request->get('g-recaptcha-response');
            if (!$recaptcha->verify($response, $remoteIp)) {
                $this->addError("The reCAPTCHA could not be validated. Please complete the reCAPTCHA request before submitting.");
                if ($recaptcha->hasErrors()) {
                    $this->addError($recaptcha->getErrors());
                }
                return $this->setFailure()->getResponse();
            }
            if ($request->get('disagreeTerms', 0)) {
                return $this->setFailure('You cannot submit a review if you disagree to the Terms stipulated in the PDPA Privacy Policy.')->getResponse();
            }
            if (!$request->get('agreeTerms', 0)) {
                return $this->setFailure('Please agree to the Terms stipulated in the PDPA Privacy Policy to submit the review.')->getResponse();
            }
            if ($review->getVerifiable() === null) {
                // ensure rating selected
                return $this->setFailure('Please answer if you would agree for us to email to you to verify this review.')->getResponse();
            }
            if ($review->isRatingFeedbackRequired() && !$review->getRating()) {
                // ensure rating selected
                return $this->setFailure('Please select a star rating for your review')->getResponse();
            }

            if ($review->getStatusId() == Review::STATUS_SENT) {
                if ($review->getTypeId() == Review::TYPE_CUSTOM) {
                    // custom reviews not published by default
                    $review->setStatusId(Review::STATUS_RECEIVED_PENDING);
                } else {
                    $review->setStatusId(Review::STATUS_PUBLISHED);
                    $review->setPublishedAt(new \DateTime());
                }
            }

            $review->setReceivedAt(new \DateTime());

            // save the event
            $this->persistEntity($review)->flush();
            $this->get('app.manager.review_manager')->recalculateAverageFARating($review->getFinancialAdviser());

            return $this->setSuccess()->getResponse();
        }

        // return error
        $errors = $form->getErrors(true);
        if ($errors) {
            foreach ($errors as $error) {
                /* @var $error FormError */
                $this->addError($error->getMessage());
            }
        } else {
            $this->addError('Sorry, there was a problem saving the event');
        }
        return $this->setFailure()->getResponse();
    }

    /**
     * get html for review modal content
     *
     * @Route("/ajax/review/modal/{id}", name="review_view_modal", options={"expose": true})
     */
    public function reviewViewModalAction($id, Request $request) {

        $this->checkAccess(['ROLE_FA', 'ROLE_ADMIN']);

        $review = $this->getRepo('AppBundle:Review')->find($id); /* @var $review Review */
        
        if (!$this->hasAccess('ROLE_ADMIN') && $review->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }

        return new Response($this->renderView('AppBundle:Review/widgets:review-modal-content.html.twig', ['review' => $review]));
    }

    /**
     * get html for review modal content
     *
     * @Route("/ajax/review/toggle-published", name="review_toggle_published", options={"expose": true})
     * @Security("has_role('ROLE_FA')")
     */
    public function reviewTogglePublishedAction(Request $request) {

        $this->checkAccess(['ROLE_FA', 'ROLE_ADMIN']);

        $id = $request->get('id');
        $review = $this->getRepo('AppBundle:Review')->find($id); /* @var $review Review */

        if (!$this->hasAccess('ROLE_ADMIN') && $review->getFinancialAdviser()->getUser()->getId() != $this->getUser()->getId()) {
            return $this->setFailure("Access Denied")->getResponse();
        }
        if ($this->hasAccess('ROLE_FA') && !$this->hasAccess('ROLE_ADMIN') && $review->getTypeId() != Review::TYPE_CUSTOM) {
            return $this->setFailure("Only Custom reviews can be published and unpublished")->getResponse();
        }

        if ($review->getStatusId() == Review::STATUS_PUBLISHED) {
            $review->setStatusId(Review::STATUS_RECEIVED_PENDING);
        } else {
            $review->setStatusId(Review::STATUS_PUBLISHED);
            $review->setPublishedAt(new \DateTime());
        }

        $this->persistEntity($review)->flush();

        return $this->setData(['published' => $review->isPublished()])->getResponse();

    }


}
