<?php
/**
 * ExceptionListener.php
 *
 * @author Gul
 */

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ExceptionListener
{
   protected $router;

   public function __construct(Router $router)
   {
      $this->router = $router;
   }

   public function onKernelException(GetResponseForExceptionEvent $event)
   {
      if ($event->getException() instanceof AccessDeniedException) {
         $url = $this->router->generate('access_denied');
         return new RedirectResponse($url);
      }
   }
}