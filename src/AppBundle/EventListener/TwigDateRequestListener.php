<?php
/**
 * TwigDateRequestListener.php
 *
 * @author Gul
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class TwigDateRequestListener
{
    protected $twig;
    protected $tokenStorage;

    function __construct(\Twig_Environment $twig, TokenStorage $tokenStorage) {
        $this->twig = $twig;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest(GetResponseEvent $event) {

        if ($this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser()) {
            $user = $this->tokenStorage->getToken()->getUser(); /* @var $user User */
            if ($user instanceof User && $user->getPreferences() && $user->getPreferences()->getTimezone()) {
                $this->twig->getExtension('core')->setTimezone($user->getPreferences()->getTimezone());
            }
        }

    }
}