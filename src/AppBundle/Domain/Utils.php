<?php
/**
 * AppBundle\Domain\Utils.php
 *
 * @author: Gul  
 */

namespace AppBundle\Domain;


class Utils {

    public static function outputFileChunked($filename, $retbytes = TRUE) {
        $buffer = "";
        $cnt =0;
        $handle = fopen($filename, "rb");
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, 1024*1024);
            echo $buffer;
            ob_flush();
            flush();
            if ($retbytes) {
                $cnt += strlen($buffer);
            }
        }
        $status = fclose($handle);
        if ($retbytes && $status) {
            return $cnt; // return num. bytes delivered like readfile() does.
        }
        return $status;
    }

    public static function randomHash($length, $keyspace = 'abcdefghijklmnopqrstuvwxyz1234567890')
    {
        $str = '';
        $keysize = strlen($keyspace);
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[mt_rand(0, $keysize-1)];
        }
        return $str;
    }

}