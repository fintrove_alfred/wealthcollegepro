<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 26/02/2015
 * Time: 19:09
 */

namespace AppBundle\Service;

use Doctrine\Entity;
use AppBundle\Entity\AuditLog;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

class BaseManager {


    /**
     * Holds the Doctrine entity manager for database interaction
     * @var EntityManager
     */
    protected $em;

    /**
     * Entity-specific repo, useful for finding entities, for example
     * @var EntityRepository
     */
    protected $repo;

    /**
     * The Fully-Qualified Class Name for our entity
     * @var string
     */
    protected $class;

    public function __construct(EntityManager $em, $class = NULL) {
        $this->em = $em;
        $this->class = $class;
        if ($class) {
            $this->repo = $em->getRepository($class);
        }

    }

    /**
     * @return \Doctrine\ORM\EntityRepository|EntityRepository
     */
    public function getRepo($entityName = NULL) {
        if (NULL === $entityName) {
            if (!empty($this->repo)) {
                return $this->repo;
            } else {
                throw new \Exception("Called BaseManager::getRepo without entity name!");
            }
        } else {
            if (strpos($entityName, ':') === FALSE) {
                // not specified bundle, set default
                $entityName = 'AppBundle:'.$entityName;
            }
            return $this->em->getRepository($entityName);
        }
    }

    /**
     * Create an audit log entry and persist.
     *
     * @param      $entity
     * @param User $user
     * @param      $actionId
     * @param      $meta
     * @return AuditLog
     */
    public function logAction($entity, User $user, $actionId, $meta = '') {

        $classNameParts = explode('\\', get_class($entity));

        $auditLog = new AuditLog();
        $auditLog->setUser($user);
        $auditLog->setActionId($actionId);
        $auditLog->setMeta(is_array($meta) ? json_encode($meta) : $meta);
        $auditLog->setIsMetaJSON(is_array($meta));
        $auditLog->setLink(array_pop($classNameParts));
        $auditLog->setLinkId($entity->getId());
        $auditLog->setCreated(new \DateTime());
        $this->em->persist($auditLog);
        $this->em->flush($auditLog);
        return $auditLog;
    }

    /**
     * @param Entity     $entity
     * @param Integer $auditLogTypeId AuditLog::ACTION_*
     * @return null|AuditLog
     */
    public function getMostRecentAuditLog($entity, $auditLogTypeId = null) {
        $classNameParts = explode('\\', get_class($entity));

        $filter = [
            'link' => array_pop($classNameParts),
            'linkId' => $entity->getId()
        ];
        if ($auditLogTypeId) {
            $filter['typeId'] = $auditLogTypeId;
        }
        return $this->getRepo('AuditLog')->findOneBy($filter, ['created' => 'desc']);
    }


}