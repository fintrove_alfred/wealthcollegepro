<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 23/02/2016
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\CalendarEventType;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\Webinar;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class CalendarManager extends BaseManager {

    use ErrorRetrieval;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var GoogleAPIClient
     */
    protected $googleAPIClient;

    protected $logger;

    /**
     * @var EmailManager
     */
    protected $emailManager;

    /**
     * CalendarManager constructor.
     * @param EntityManager $em
     * @param TokenStorage $tokenStorage
     * @param GoogleAPIClient $googleAPIClient
     * @param Logger $logger
     * @param EmailManager $emailManager
     */
    public function __construct(EntityManager $em, TokenStorage $tokenStorage, GoogleAPIClient $googleAPIClient, Logger $logger, EmailManager $emailManager) {

        parent::__construct($em);

        if ($tokenStorage->getToken()->isAuthenticated()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
        $this->googleAPIClient = $googleAPIClient;
        $this->logger = $logger;
        $this->emailManager = $emailManager;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get calendar event types for a given user.
     *
     * @param User $user
     * @return CalendarEventType[]
     * @throws \Exception
     */
    public function getUserCalendarEventTypes(User $user) {

        $qb = $this->getRepo('AppBundle:CalendarEventType')->createQueryBuilder('cet');
        $qb->where($qb->expr()->isNull('cet.user'));
        $qb->orWhere($qb->expr()->eq('cet.user', $user->getId()));
        $qb->orderBy('cet.user', 'desc');
        $qb->addOrderBy('cet.id', 'asc');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return CalendarEvent[]
     * @throws \Exception
     */
    public function getCalendarEvents(User $user, \DateTime $startDate, \DateTime $endDate) {

        $qb = $this->getRepo('AppBundle:CalendarEvent')->createQueryBuilder('e');
        $qb->where('e.user = :user')->setParameter('user', $user->getId());
        $qb->andWhere('e.endAt >= :startDate')->setParameter('startDate', $startDate->format('Y-m-d H:i:s'));
        $qb->andWhere('e.startAt <= :endDate')->setParameter('endDate', $endDate->format('Y-m-d H:i:s'));
        $qb->orderBy('e.startAt', 'asc');
        $calEvents = $qb->getQuery()->getResult();
        return $calEvents ? $calEvents : [];

    }


    /**
     * @param User $user
     * @param null $startDate
     * @param null $endDate
     * @param null $view
     * @return array
     * @throws \Exception
     */
    public function getCalendarEventsData(User $user, \DateTime $startDate, \DateTime $endDate, $view = null) {

        $userPreferences = $user->getPreferences();

        $qb = $this->getRepo('AppBundle:CalendarEvent')->createQueryBuilder('e');
        $qb->select(['e','et']);
        $qb->join('e.calendarEventType', 'et');
        $qb->where('e.user = :user')->setParameter('user', $user->getId());
        $qb->andWhere('e.endAt >= :startDate')->setParameter('startDate', $startDate->format('Y-m-d H:i:s'));
        $qb->andWhere('e.startAt <= :endDate')->setParameter('endDate', $endDate->format('Y-m-d H:i:s'));

        $selectedCalendarEventTypeIDs = $userPreferences->getSelectedCalendarEventTypeIDs();
        if (!empty($selectedCalendarEventTypeIDs)) {
            $qb->andWhere($qb->expr()->in('e.calendarEventType', ':calendarEventTypes'))->setParameter('calendarEventTypes', $selectedCalendarEventTypeIDs);
        }

        $events = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        $timezone = $user->getPreferences()->getTimezone();
        $timezone = new \DateTimeZone($timezone);

        $data = [];
        if ($events) {
            foreach ($events as $event) {

                $event['startAt']->setTimezone($timezone);
                $event['endAt']->setTimezone($timezone);

                $data[] = [
                    'index' => $event['id'],
                    'title' => $event['summary'],
                    'allDay' => false,
                    'start' => $event['startAt']->format('Y-m-d H:i:s'),
                    'end' => $event['endAt']->format('Y-m-d H:i:s'),
                    'class' => 'event-type-'.$event['calendarEventType']['handle'].' bg',
                    'other' => [
                        'id' => $event['id'],
                        'note' => $event['description'],
                        'location' => $event['location'],
                        'eventType' => $event['calendarEventType']['id'],
                        'eventTypeHandle' => $event['calendarEventType']['handle'],
                        'alertTypeId' => $event['alertTypeId'],
                        'alertTime' => $event['alertTime'] ? $event['alertTime']->format('H:i') : '',
                        'alertDays' => $event['alertDays'],
                    ]
                ];
            }
        }

        return $data;
    }

    /**
     * Save the selected event types in the logged in user's preferences
     * @param array $eventTypeIDs
     * @throws \Exception
     */
    public function saveEventTypes(array $eventTypeIDs) {

        if ($eventTypeIDs) {
            $eventTypes = $this->getRepo('AppBundle:CalendarEventType')->findBy(['id' => $eventTypeIDs]);
        } else {
            $eventTypes = [];
        }

        $userPreferences = $this->getUser()->getPreferences();
        $calendarEventTypeIDs = [];
        foreach ($eventTypes as $eventType) { /* @var $eventType CalendarEventType */
            if (!$eventType->getUser() || $eventType->getUser()->getId() == $this->getUser()->getId()) {
                $calendarEventTypeIDs[] = $eventType->getId();
            }
        }
        $userPreferences->setSelectedCalendarEventTypeIDs($calendarEventTypeIDs);
        $this->getUser()->setPreferences($userPreferences);
        $this->em->persist($this->getUser());
        $this->em->flush($this->getUser());
    }

    /**
     * @param FinancialAdviser $financialAdviser
     * @param array $filter
     * @return CalendarEvent[]
     * @throws \Exception
     */
    public function getWebinarCalendarEvents(FinancialAdviser $financialAdviser, array $filter = []) {

        $calendarEventType = $this->getRepo('AppBundle:CalendarEventType')->findOneByHandle('FINTRAIN_SESSIONS');

        $qb = $this->getRepo('AppBundle:CalendarEvent')->createQueryBuilder('ce');
        $qb->andWhere('ce.user = :user')->setParameter('user', $financialAdviser->getUser()->getId());
        $qb->andWhere('ce.calendarEventType = :calendarEventType')->setParameter('calendarEventType', $calendarEventType->getId());

        if (!empty($filter['past'])) {
            $qb->andWhere('ce.endAt < :now')->setParameter('now', new \DateTime());
        } else {
            $qb->andWhere('ce.endAt >= :now')->setParameter('now', new \DateTime());
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Get attendees data as array
     *
     * @param CalendarEvent $calendarEvent
     * @return array
     * @throws \Exception
     */
    public function getCalendarEventAttendeesData(CalendarEvent $calendarEvent) {

        $qb = $this->getRepo('AppBundle:CalendarEvent')->createQueryBuilder('ce');
        $qb->select(['p.firstName','p.lastName', 'p.email']);
        $qb->join('ce.calendarEventAttendees', 'cea');
        $qb->join('cea.client', 'c');
        $qb->join('c.person', 'p');
        $qb->andWhere($qb->expr()->eq('ce.id', ':id'))->setParameter('id', $calendarEvent->getId());
        $results = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        $attendees = [];
        if ($results) {
            foreach ($results as $result) {
                $attendees[] = [
                    'email' => $result['email'],
                    'displayName' => $result['firstName'].' '.$result['lastName']
                ];
            }
        }

        return $attendees;
    }


    /**
     * Push the event to google calendar
     *
     * @param User $user
     * @param $calendarEvents
     */
    public function syncEventsWithGoogleCalendar(User $user, $calendarEvents) {

        $service = new \Google_Service_Calendar($this->googleAPIClient->getClient($user));

        foreach ($calendarEvents as $calendarEvent) { /* @var $calendarEvent CalendarEvent */
            $calendarEventTypehandle = $calendarEvent->getCalendarEventType()->getHandle();
            $calendarId = $this->googleAPIClient->getCalendarIdByHandle($user, $calendarEventTypehandle);

            $webinar = null;
            if ($calendarEvent->isWebinar()) {
                $webinar = $this->getRepo('AppBundle:Webinar')->findOneBy(['calendarEvent' => $calendarEvent->getId()]); /* @var $webinar Webinar */
                if (!$webinar->getUrl() && $calendarEvent->getExternalId()) {
                    // we have tried to sync before but never got a hangoutURL
                    // so delete the google calendar event and re-create it
                    $service->events->delete($calendarId, $calendarEvent->getExternalId());
                    $calendarEvent->setExternalId(null);
                }
            }


            $event = new \Google_Service_Calendar_Event(
               [
                  'summary' => $calendarEvent->getSummary(),
                  'location' => '',
                  'description' => $calendarEvent->getDescription(),
                  'start' => array(
                     'dateTime' => $calendarEvent->getStartAt()->format(\DateTime:: RFC3339)
                  ),
                  'end' => array(
                     'dateTime' => $calendarEvent->getEndAt()->format(\DateTime:: RFC3339),
                  ),
                  'attendees' => $this->getCalendarEventAttendeesData($calendarEvent),
                  'guestsCanSeeOtherGuests' => false,
                  'guestsCanInviteOthers' => false,
                  'reminders' => array(
                     'useDefault' => $calendarEvent->getAlertTypeId() == CalendarEvent::ALERT_TYPE_NONE,
                     'overrides' => array(
                        array('method' => 'email', 'minutes' => $calendarEvent->getAlertMinutes()),
                        array('method' => 'popup', 'minutes' => $calendarEvent->getAlertMinutes())
                     ),
                  ),
                  'visibility' => 'private'
               ]
            );
            try {
                if ($calendarEvent->getExternalId()) {
                    $event = $service->events->update($calendarId, $calendarEvent->getExternalId(), $event);
                } else {
                    $event = $service->events->insert($calendarId, $event);
                    $calendarEvent->setExternalId($event->getId());
                }
            } catch (\Google_Service_Exception $e) {
                if ($e->getCode() == 404) {
                    // perhaps calendar delted in google calendar, try to create it again
                    $calendarId = $this->googleAPIClient->getCalendarIdByHandle($user, $calendarEventTypehandle, true);
                    if ($calendarEvent->getExternalId()) {
                        $event = $service->events->update($calendarId, $calendarEvent->getExternalId(), $event);
                    } else {
                        $event = $service->events->insert($calendarId, $event);
                        $calendarEvent->setExternalId($event->getId());
                    }
                }
            }

            if ($calendarEvent->isWebinar()) {
                if ($webinar) {
                    $webinar->setUrl($event['hangoutLink']);
                    $this->em->persist($webinar);
                }

                if ($webinar->getUrl()) {
                    // only successful if we got a hangouts URL
                    $calendarEvent->setDirty(false);
                } else {
                    // Setting "Automatically add video calls to events I create" is likely disabled
                    $this->addError("Unable to sync all webinars with google");
                    $this->addError("Please ensure your Google Calendar settings are configured as below:<br/>Go to <strong><a href='https://calendar.google.com' target='_blank'>Google Calendar</a> > Calendar Settings > General</strong> and find the setting <strong>Automatically add video calls to events I create</strong> and ensure it is set to <strong>Yes</strong><br/><br/>Then try to re-sync your webinars with Google.");
                }

            } else {
                $calendarEvent->setDirty(false);
            }

            $calendarEvent->setSyncedAt(new \DateTime());
            $this->em->persist($calendarEvent);

        }

        $this->em->flush();
    }

    /**
     * Delete a calendar event
     *
     * @param CalendarEvent $calendarEvent
     */
    public function deleteCalendarEvent(CalendarEvent $calendarEvent) {


        
        $eventAttendees = $calendarEvent->getCalendarEventAttendees();
        if ($eventAttendees->count() > 0) {
            $this->emailManager->sendEventCancelled($eventAttendees);
        }
        
        if ($calendarEvent->getExternalId()) {
            $this->googleAPIClient->deleteCalendarEvent($calendarEvent);
        }

        $this->em->remove($calendarEvent);
        $this->em->flush($calendarEvent);
    }
}