<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 15/08/2016
 */

namespace AppBundle\Service;

use AppBundle\Entity\Address;
use AppBundle\Entity\Client;
use AppBundle\Entity\ClientDate;
use AppBundle\Entity\ClientGroup;
use AppBundle\Entity\ClientImport;
use AppBundle\Entity\ClientRelation;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\Person;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class ClientExporter extends BaseManager {

    use ErrorRetrieval;

    /**
     * @var User
     */
    protected $user;

    protected $logger;

    /**
     * CalendarEventInvites constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Logger $logger) {

        parent::__construct($em);
        $this->logger = $logger;
    }

    /**
     * Export Client Data as CSV File
     *
     * @param FinancialAdviser $financialAdviser
     * @param $fieldTypes
     * @param $contactsData
     * @return string filename of csv file
     */
    public function exportCSV(FinancialAdviser $financialAdviser) {

        $qb = $this->em->getRepository('AppBundle:Client')->createQueryBuilder('c');
        $clients = $qb->andWhere('c.financialAdviser = '.$financialAdviser->getId())->getQuery()->getResult();

        $filename = tempnam('/tmp', 'client_export');
        if ($filename === false) {
            $this->addError("Could not create temporary file, please contact support");
            return false;
        }

        $fh = fopen($filename, 'r+');

        $header = [
            'First Name',
            'Last Names',
            'Email Address',
            'NRIC No.',
            'Gender',
            'Contact Type',
            'Company',
            'Nationality Country',
            'Religion',
            'Home Tel',
            'Mobile Tel',
            'Office Tel',
            'Race',
            'Marital Status',
            'Date of Birth',
            'Address 1',
            'Address 2',
            'City',
            'Post Code',
            'Country',
            'Website 1',
            'Website 2',
            'Website 3',
            'Facebook',
            'Twitter',
            'LinkedIn',
            'Occupation',
            'Job Title',
            'Work Address 1',
            'Work Address 2',
            'Work City',
            'Work Post Code',
            'Work Country',
            'Referred By',
            'Linked Contacts',
            'Special Dates',
            'Groups',
            'Notes',
            'Created On'
        ];

        fputcsv($fh, $header);

        $relationshipTypes = ClientRelation::getRelationshipTypes();

        foreach ($clients as $client) { /* @var $client Client */

            $data = [];
            $data[] = $client->getPerson()->getFirstName();
            $data[] = $client->getPerson()->getLastName();
            $data[] = $client->getPerson()->getEmail();
            $data[] = $client->getPerson()->getNricPassport();
            $data[] = $client->getPerson()->getGender();
            $data[] = $client->getTypeName();
            $data[] = $client->getPerson()->getBusinessName();
            $data[] = $client->getNationalityCountry() ? $client->getNationalityCountry()->getName() : '';
            $data[] = $client->getReligion();
            $data[] = $client->getPerson()->getTelephone();
            $data[] = $client->getPerson()->getMobile();
            $data[] = $client->getPerson()->getTelephoneWork();
            $data[] = $client->getRace();
            $data[] = $client->getPerson()->getMaritalStatusName();
            $data[] = $client->getPerson()->getDob() ? $client->getPerson()->getDob()->format('Y-m-d') : '';
            $data[] = $client->getAddress() ? $client->getAddress()->getAddress1() : '';
            $data[] = $client->getAddress() ? $client->getAddress()->getAddress2() : '';
            $data[] = $client->getAddress() ? $client->getAddress()->getCity() : '';
            $data[] = $client->getAddress() ? $client->getAddress()->getPostCode() : '';
            $data[] = $client->getAddress() && $client->getAddress()->getCountry() ? $client->getAddress()->getCountry()->getName() : '';
            $data[] = $client->getWebsite1();
            $data[] = $client->getWebsite2();
            $data[] = $client->getWebsite3();
            $data[] = $client->getFacebook();
            $data[] = $client->getTwitter();
            $data[] = $client->getLinkedin();
            $data[] = $client->getOccupation();
            $data[] = $client->getJobTitle();
            $data[] = $client->getCompanyAddress() ? $client->getCompanyAddress()->getAddress1() : '';
            $data[] = $client->getCompanyAddress() ? $client->getCompanyAddress()->getAddress2() : '';
            $data[] = $client->getCompanyAddress() ? $client->getCompanyAddress()->getCity() : '';
            $data[] = $client->getCompanyAddress() ? $client->getCompanyAddress()->getPostCode() : '';
            $data[] = $client->getCompanyAddress() && $client->getCompanyAddress()->getCountry() ? $client->getCompanyAddress()->getCountry()->getName() : '';
            $data[] = $client->getReferredBy() ? $client->getReferredBy()->getPerson()->getFullName() : '';

            $clientRelations = [];
            if ($client->getClientRelations()) {
                foreach ($client->getClientRelations() as $clientRelation) { /* @var $clientRelation ClientRelation */
                    $relationshipTypeName = isset($relationshipTypes[$clientRelation->getRelationshipTypeId()]) ? $relationshipTypes[$clientRelation->getRelationshipTypeId()] : '';
                    $clientRelations[] = $clientRelation->getRelatedClient()->getPerson()->getFullName().':'.$relationshipTypeName;
                }
            }
            $data[] = implode(';', $clientRelations);

            $specialDates = [];
            if ($client->getClientDates()) {
                foreach ($client->getClientDates() as $clientDate) { /* @var $clientDate ClientDate */
                    $specialDates[] = $clientDate->getDateTypeIdName().':'.($clientDate->getDate() ? $clientDate->getDate()->format('Y-m-d') : '');
                }
            }
            $data[] = implode(';', $specialDates);

            $clientGroups = [];
            if ($client->getClientGroups()) {
                foreach ($client->getClientGroups() as $clientGroup) { /* @var $clientGroup ClientGroup */
                    $clientGroups[] = $clientGroup->getName();
                }
            }
            $data[] = implode(';', $clientGroups);
            $data[] = $client->getNotes();
            $data[] = $client->getCreated() ? $client->getCreated()->format('Y-m-d H:i:s') : '';

            fputcsv($fh, $data);
        }

        fclose($fh);

        return $filename;

    }

    /**
     * Get the field types which can be imported
     * @return array
     */
    public function getFieldTypes() {

        $columns = [
            'firstName' => 'First Name',
            'lastName' => 'Last Names',
            'firstNameLastName' => 'First Name + Last Names',
            'lastNameFirstName' => 'Last Names + First Name',
            'email' => 'Email Address',
            'nricNo' => 'NRIC No.',
            'gender' => 'Gender',
            'company' => 'Company',
            'nationality' => 'Nationality Country',
            'religion' => 'Religion',
            'address' => 'Address',
            'postCode' => 'Postal Code',
            'mobileTelephone' => 'Mobile Tel',
            'workTelephone' => 'Office Tel'
        ];


        return $columns;
    }

}