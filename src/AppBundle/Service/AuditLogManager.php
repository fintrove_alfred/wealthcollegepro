<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 02/10/2015
 * Time: 12:58
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

class AuditLogManager extends BaseManager {

    /**
     * Get all audit logs for a particular entity
     *
     * @param $entity
     * @return array
     * @throws \Exception
     */
    public function getAllByEntity($entity, $orderDir = 'desc') {

        $classNameParts = explode('\\', get_class($entity));
        $className = array_pop($classNameParts);
        $auditLogs = $this->getRepo('AppBundle:AuditLog')->findBy([
            'link' => $className,
            'linkId' => $entity->getId(),
        ], [
            'created' => $orderDir
        ]);
        return $auditLogs ? $auditLogs : [];
    }


}