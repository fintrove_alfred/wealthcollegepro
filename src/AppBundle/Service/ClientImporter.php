<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 15/08/2016
 */

namespace AppBundle\Service;

use AppBundle\Entity\Address;
use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\CalendarEventAttendee;
use AppBundle\Entity\CalendarEventType;
use AppBundle\Entity\Client;
use AppBundle\Entity\ClientImport;
use AppBundle\Entity\ClientWPMInsurancePolicy;
use AppBundle\Entity\ClientWPMInvestment;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\Person;
use AppBundle\Form\Type\ClientEventTypes;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class ClientImporter extends BaseManager {

    use ErrorRetrieval;

    /**
     * @var User
     */
    protected $user;

    protected $logger;

    /**
     * CalendarEventInvites constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Logger $logger) {

        parent::__construct($em);
        $this->logger = $logger;
    }

    /**
     * Import Client Data
     *
     * @param FinancialAdviser $financialAdviser
     * @param $fieldTypes
     * @param $contactsData
     * @return ClientImport
     */
    public function import(FinancialAdviser $financialAdviser, $fieldTypes, $contactsData, $contactTypeId = null) {

        $clientImport = new ClientImport();
        $clientImport->setFinancialAdviser($financialAdviser);

        if (!in_array($contactTypeId, [Client::TYPE_CLIENT, Client::TYPE_PROSPECT])) {
            $contactTypeId = null;
        }

        $clients = [];

        foreach ($contactsData as $contactData) {

            $client = new Client();
            $person = new Person();
            $address = new Address();
            $client->setFinancialAdviser($financialAdviser);
            $client->setPerson($person);
            $client->setAddress($address);
            $client->setTypeId($contactTypeId);

            $hasData = false;
            foreach ($fieldTypes as $i => $fieldType) {
                if (isset($contactData[$i])) {
                    if ($this->setFieldData($client, $fieldType, $contactData[$i])) {
                        $hasData = true;
                    }
                } else {
                    $this->logger->debug("Could not set field data for field Type $fieldType, field not present in submitted data");
                }

            }
            if ($hasData) {
                $client->setClientImport($clientImport);
                $this->em->persist($client);

                $clients[] = $client;
            }

        }
        
        $clientImport->setNumClients(count($clients));
        $this->em->persist($clientImport);
        $this->em->flush();
        
        return $clientImport;
    }

    /**
     * Get the field types which can be imported
     * @return array
     */
    public function getFieldTypes() {

        $columns = [
            'firstName' => 'First Name',
            'lastName' => 'Last Names',
            'firstNameLastName' => 'First Name + Last Names',
            'lastNameFirstName' => 'Last Names + First Name',
            'email' => 'Email Address',
            'nricNo' => 'NRIC No.',
            'gender' => 'Gender',
            'company' => 'Company',
            'nationality' => 'Nationality Country',
            'religion' => 'Religion',
            'address' => 'Address',
            'postCode' => 'Postal Code',
            'mobileTelephone' => 'Mobile Tel',
            'workTelephone' => 'Office Tel'
        ];


        return $columns;
    }

    /**
     * Set the client data using the field type and value
     *
     * @param Client $client
     * @param $fieldType
     * @param $value
     * @return boolean      Were we able to set some data on the client object?
     */
    public function setFieldData(Client $client, $fieldType, $value) {

        $person = $client->getPerson();
        switch ($fieldType) {
            case 'firstName':
                $person->setFirstName($value);
                return true;
            case 'lastName':
                $person->setLastName($value);
                return true;
            case 'firstNameLastName':
                $values = explode(' ', $value);
                $person->setFirstName(array_shift($values));
                $person->setLastName(implode(' ', $values));
                return true;
            case 'lastNameFirstName':
                $values = explode(' ', $value);
                $person->setFirstName(array_pop($values));
                $person->setLastName(implode(' ', $values));
                return true;
            case 'email':
                $person->setEmail($value);
                return true;
            case 'nricNo':
                $person->setNricPassport($value);
                return true;
            case 'gender':
                $person->setGender($value);
                return true;
            case 'company':
                $person->setBusinessName($value);
                return true;
            case 'religion':
                $client->setReligion($value);
                return true;
            case 'nationality':
                if ($value) {
                    try {
                        $country = $this->getRepo('Country')->createQueryBuilder('c')
                            ->andWhere("c.name LIKE :country")->setParameter('country', '%' . $value . '%')
                            ->getQuery()
                            ->getOneOrNullResult();

                        $client->setNationalityCountry($country);
                    } catch (\Exception $e) {
                        $this->logger->debug("Error finding country " . $e->getMessage());
                    }
                }
                return true;
            case 'address':
                $values = explode(',', $value);
                if (isset($values[0])) {
                    $client->getAddress()->setAddress1($values[0]);
                }
                if (isset($values[1])) {
                    $client->getAddress()->setAddress2($values[1]);
                }
                if (isset($values[2])) {
                    $client->getAddress()->setCity($values[2]);
                }
                return true;
            case 'postCode':
                $client->getAddress()->setPostCode($value);
                return true;
            case 'mobileTelephone':
                $person->setMobile($value);
                return true;
            case 'workTelephone':
                $person->setTelephoneWork($value);
                return true;
        }

        return false;
    }

}