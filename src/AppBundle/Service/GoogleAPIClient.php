<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 26/04/2016
 * Time: 15:49
 */

namespace AppBundle\Service;


use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\CalendarEventType;
use AppBundle\Entity\User;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class GoogleAPIClient  {

    /**
     * @var User
     */
    protected $user;
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Router
     */
    protected $router;

    protected $clientJsonFile;

    /**
     * GoogleAPIClient constructor.
     * @param EntityManager $em
     * @param TokenStorage $tokenStorage
     * @param Router $router
     * @param string $clientJsonFile
     */
    public function __construct(EntityManager $em, TokenStorage $tokenStorage, Router $router, $clientJsonFile) {
        $this->em = $em;

        if ($tokenStorage->getToken()->isAuthenticated()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
        $this->router = $router;
        $this->clientJsonFile = $clientJsonFile;
    }

    /**
     *
     * @param $authCode
     * @return string
     */
    public function getAccessTokenUsingAuthCode($authCode) {

        $client = $this->getClient();
        $client->authenticate($authCode);
        $accessToken = $client->getAccessToken();
        $refreshToken = $client->getRefreshToken();
        return [
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken,
        ];
    }

    /**
     * Get the google client api
     *
     * @param User $user
     * @return \Google_Client
     */
    public function getClient(User $user = null, $refreshIfExpired = false) {

        $client = new \Google_Client();
        $client->setAuthConfigFile($this->clientJsonFile);
        $client->addScope(\Google_Service_Calendar::CALENDAR);
        $client->setRedirectUri($this->router->generate('google_oauth_response', [], Router::ABSOLUTE_URL));
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        //$client->setAccessType('online');

        if ($user) {
            $preferences = $user->getPreferences();
            $accessToken = $preferences->getGoogleAccessToken();
            $refreshToken = $preferences->getGoogleRefreshToken();
            if ($accessToken && $refreshToken) {
                $client->setAccessToken($accessToken);
                if ($refreshIfExpired && $client->isAccessTokenExpired()) {
                    // get a new token and store it in user preferences
                    $client->refreshToken($refreshToken);
                    $preferences->setGoogleAccessToken($client->getAccessToken());
                    $user->setPreferences($preferences);
                    $this->em->persist($user);
                    $this->em->flush($user);
                }
            }
        }

        return $client;
    }

    /**
     * Get the access token for a user
     *
     * @param User $user
     * @return string
     */
    public function getUserAccessToken(User $user, $refreshIfExpired = false) {

        return $this->getClient($user, $refreshIfExpired)->getAccessToken();

    }

    /**
     * Create a google calendar with the name $title
     * @param User $user
     * @param $summary
     * @return integer the calendar ID
     */
    public function createCalendar(User $user, $title) {
        $service = new \Google_Service_Calendar($this->getClient($user));
        $calendar = new \Google_Service_Calendar_Calendar();
        $calendar->setSummary($title);
        $calendar = $service->calendars->insert($calendar);
        return $calendar->getId();

    }

    /**
     * Get a google calendar id, or if it doesn't exist in google add the calendar to google and get the google id for the calendar
     * @param User $user
     * @param $calendarEventTypeHandle
     */
    public function getCalendarIdByHandle(User $user, $calendarEventTypeHandle, $forceCreate = false) {

        $preferences = $user->getPreferences();
        $calendarIds = $preferences->getGoogleCalendarIds();

        if (empty($calendarIds[$calendarEventTypeHandle]) || $forceCreate) {
            // create calendar
            $calendarEventType = $this->em->getRepository('AppBundle:CalendarEventType')->findOneByHandle($calendarEventTypeHandle); /* @var $calendarEventType CalendarEventType */
            $id = $this->createCalendar($user, $calendarEventType->getNamePlural());
            $calendarIds[$calendarEventTypeHandle] = $id;
            $preferences->setGoogleCalendarIds($calendarIds);
            $user->setPreferences($preferences);
            $this->em->persist($user);
        }
        return $calendarIds[$calendarEventTypeHandle];

    }

    /**
     *
     * @param CalendarEvent $calendarEvent
     */
    public function deleteCalendarEvent(CalendarEvent $calendarEvent) {

        $client = $this->getClient($calendarEvent->getUser(), true);
        $service = new \Google_Service_Calendar($client);

        $calendarEventHandle = $calendarEvent->getCalendarEventType()->getHandle();
        $calendarId = $this->getCalendarIdByHandle($calendarEvent->getUser(), $calendarEventHandle);
        $service->events->delete($calendarId, $calendarEvent->getExternalId());

    }
}