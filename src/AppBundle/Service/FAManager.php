<?php
namespace AppBundle\Service;

use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\FinancialAdviserPhoto;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Bridge\Monolog\Logger;


class FAManager extends BaseManager {

    use ErrorRetrieval;

    /**
     * @var User
     */
    protected $user;

    protected $logger;

    /**
     * CalendarEventInvites constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Logger $logger) {

        parent::__construct($em);
        $this->logger = $logger;
    }

    public function getFilterOptions() {

        $filterOptions = [];

        $qb = $this->getRepo('AppBundle:FinancialAdviser')->createQueryBuilder('fa');
        $qb->distinct(true);
        $qb->join('fa.address', 'a');
        $qb->join('a.country', 'c');
        $qb->select(['c.id', 'c.name']);
        $qb->orderBy('c.name', 'asc');
        $countries = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        if ($countries) {
            foreach ($countries as $country) {
                $filterOptions['countries'][$country['id']] = $country['name'];
            }
        }

        $qb = $this->getRepo('AppBundle:FinancialAdviser')->createQueryBuilder('fa');
        $qb->distinct(true);
        $qb->join('fa.address', 'a');
        $qb->select(['a.city']);
        $qb->orderBy('a.city', 'asc');
        $cities = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);

        if ($cities) {
            foreach ($cities as $city) {
                $filterOptions['cities'][] = $city['city'];
            }
        }


        $filterOptions['orderBy'] = [
            'newest' => 'Newest First',
            'rating' => 'Highest Rated First',
            'recently_active' => 'Recently Active First',
            'alphabetically' => 'Alphabetically, Last Name',
        ];

        return $filterOptions;
    }

    /**
     * Get financial advisers based on the filters
     *
     * @param $filter
     * @return FinancialAdviser[]
     * @throws \Exception
     */
    public function getSearchResults($filter) {

        $qb = $this->getRepo('AppBundle:FinancialAdviser')->createQueryBuilder('fa');
        $qb->leftJoin('fa.address', 'a');
        $qb->leftJoin('fa.user', 'u');

        $qb->andWhere('u.enabled = 1');
        $qb->andWhere('fa.hidden = 0');

        if (!empty($filter['countryId']) && is_array($filter['countryId'])) {
            $qb->andWhere($qb->expr()->in('a.country', ':countryId'))->setParameter('countryId', $filter['countryId']);
        }
        if (!empty($filter['city']) && is_array($filter['city'])) {
            $qb->andWhere($qb->expr()->in('a.city', ':city'))->setParameter('city', $filter['city']);
        }
        if (!empty($filter['name'])) {
            $nameConditions = [];
            $nameConditions[] = $qb->expr()->like('u.firstName', ':name');
            $nameConditions[] = $qb->expr()->like('u.lastName', ':name');
            $qb->setParameter('name', '%'.$filter['name'].'%');

            $nameParts = explode(' ', trim($filter['name']));
            foreach ($nameParts as $i => $namePart) {
                $nameConditions[] = $qb->expr()->like('u.firstName', ':name'.$i);
                $nameConditions[] = $qb->expr()->like('u.lastName', ':name'.$i);
                $qb->setParameter('name'.$i, '%'.$namePart.'%');
            }
            $qb->andWhere(
                implode(' OR ', $nameConditions)
            );
        }

        if (!empty($filter['orderBy'])) {

            switch ($filter['orderBy']) {
                case 'rating':
                    $qb->orderBy('fa.averageRating', 'desc');
                    break;
                case 'recently_active':
                    $qb->orderBy('u.lastLogin', 'desc');
                    break;
                case 'alphabetically':
                    $qb->orderBy('u.lastName', 'asc');
                    break;
                case 'newest':
                default:
                    $qb->orderBy('fa.created', 'desc');
                    break;
            }
        }

        $qb->andWhere($qb->expr()->like("u.roles", ":role"))->setParameter('role', '%ROLE_FA%');

        $query = $qb->getQuery();
        $financialAdvisers = $query->getResult();
        return $financialAdvisers ? $financialAdvisers : [];
    }

    /**
     * Get subscriptions which will expire after a certain number of days
     *
     * @param int $days The days left before the subscription will be expired
     * @return FinancialAdviser[]
     */
    public function getExpiring($days = 30) {

        $rsm = new Query\ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata('AppBundle:FinancialAdviser', 'fa');

        $sql = "
        SELECT fa.*
        FROM financial_adviser AS fa        
        WHERE fa.subscription_ends_at IS NOT NULL
        AND DATEDIFF(fa.subscription_ends_at, NOW()) = ?
        ";

        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $days);

        $financialAdvisers =  $query->getResult();

        return $financialAdvisers ? $financialAdvisers : [];
    }

    /**
     * Get FA Photos by type
     * @return FinancialAdviserPhoto[]
     */
    public function getPhotosByType(FinancialAdviser $financialAdviser, $type) {

        $qb = $this->em->getRepository('AppBundle:FinancialAdviserPhoto')->createQueryBuilder('fap');
        $qb->andWhere('fap.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere('fap.type = :type')->setParameter('type', $type);
        $photos = $qb->getQuery()->getResult();
        $photos = $photos ? $photos : [];
        return $photos;
    }

}