<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 23/03/2016
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\Upload;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;
use Symfony\Component\Form\Exception\BadMethodCallException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadManager extends BaseManager {

    const TYPE_PRIVATE = 1;
    const TYPE_PUBLIC = 2;

    protected $errors = [];
    protected $installDir;

    public function __construct(EntityManager $em, $class = NULL, $installDir) {

        parent::__construct($em, $class);
        $this->installDir = $installDir;

    }

    /**
     * Save uploaded files
     *
     * @param User           $user
     * @param UploadedFile[] $uploadedFiles
     * @param int            $type              UploadManager::TYPE_*
     * @return \AppBundle\Entity\Upload[]
     */
    public function saveUploads(User $user, array $uploadedFiles, $type = self::TYPE_PRIVATE, $validMimeTypes = ['application/pdf']) {

        $this->errors = [];
        $uploads = [];
        foreach ($uploadedFiles as $uploadedFile) { /* @var $uploadedFile UploadedFile */

            if (!$uploadedFile->isValid()) {
                $this->errors[] = "There was a problem uploading the file (Code 1): {$uploadedFile->getClientOriginalName()}";
                error_log(__METHOD__.' '.$uploadedFile->getErrorMessage());
                continue;
            }
            if ($validMimeTypes != '*') {
                if (!in_array($uploadedFile->getMimeType(), $validMimeTypes)) {
                    $this->errors[] = "The file '{$uploadedFile->getClientOriginalName()}' was not recognised as a valid type (".$uploadedFile->getMimeType()."). Allowed types: ".implode(', ',$validMimeTypes);
                    continue;
                }
            }

            $upload = new Upload();
            $upload->setFilename($uploadedFile->getClientOriginalName());
            $upload->setLocation('-');
            $upload->setSize($uploadedFile->getSize());
            $upload->setType($uploadedFile->getMimeType());
            $upload->setUser($user);
            $this->em->persist($upload);
            $this->em->flush();

            if ($type === self::TYPE_PUBLIC) {
                $destDir = $this->getWebFileDir();
            } else {
                $destDir = $this->getFilesDir();
            }
            $filePath = $destDir . '/' . $upload->getId();

            $upload->setLocation($filePath);
            $this->em->persist($upload);
            $this->em->flush();

            try {
                $uploadedFile->move($destDir, $upload->getId());

                $this->loadExifData($upload);

                $uploads[] = $upload;
            } catch (Exception $e) {
                // remove the upload entity
                $this->em->remove($upload);

                $this->errors[] = "There was a problem uploading the file (Code 2): {$uploadedFile->getClientOriginalName()}";
                error_log(__METHOD__.' '.$e->getMessage()."\n".$e->getTraceAsString());
                continue;
            }

        }
        return $uploads;
    }

    /**
     * Load and save exif data if it is found in the file.
     * Image files only
     *
     * @param Upload $upload
     * @return bool     Success or false
     */
    public function loadExifData(Upload $upload) {

        if (strpos($upload->getType(), 'image') === FALSE) {
            return;
        }

        if (!function_exists('exif_read_data')) {
            error_log("exif functions not loaded!");
            return false;
        }


        $data = @exif_read_data($upload->getLocation());
        // remove unnecessary data
        unset($data['MakerNote']);

        $upload->setMetaData($data);
        $this->em->persist($upload);
        $this->em->flush($upload);

        return true;
    }

    public function getErrors() {
        return $this->errors;
    }

    /**
     * Get the directory where files are stored
     * @return string
     */
    public function getFilesDir() {
        return $this->installDir.'/files';
    }

    /**
     * Get the directory where web files are stored
     * @return string
     */
    protected function getWebFileDir() {
        return $this->installDir.'/web/files';
    }

    public function getMetaDataToDisplay(Upload $upload) {

        $data = $upload->getMetaDataArray();

        $allowedFields = [
           'DateTime',
           'GPSVersion',
           'GPSLatitudeRef',
           'GPSLatitude',
           'GPSLongitudeRef',
           'GPSLongitude',
           'GPSAltitudeRef',
           'GPSAltitude',
           'GPSTimeStamp',
           'GPSDateStamp',
           'FileName',
           'FileSize',
           'MimeType',
           'Make',
           'Model',
           'Software',
           'ExifVersion',
        ];

        $displayData = [];
        foreach ($allowedFields as $allowedField) {
            if (!isset($data[$allowedField])) {
                continue;
            }
            $displayData[$allowedField] = $data[$allowedField];
        }
        return $displayData;
    }
}