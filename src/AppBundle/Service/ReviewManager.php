<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 23/02/2016
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Domain\Utils;
use AppBundle\Entity\Client;
use AppBundle\Entity\ClientGroup;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\Review;
use AppBundle\Entity\ReviewAnswer;
use AppBundle\Entity\ReviewTemplate;
use AppBundle\Entity\ReviewTemplateQuestion;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;

class ReviewManager extends BaseManager {

    use ErrorRetrieval;

    protected $router;

    public function __construct(EntityManager $em, Router $router)
    {
        $this->router = $router;
        parent::__construct($em);
    }





    /**
     * Create and send a review request
     * The data is in the request:
     * <code>
     * params:
     *
     * client_id[]
     * subject
     * message
     * review_template_id
     * </code>
     *
     * @param Request $request
     */
    public function createAndSendReviewRequest(FinancialAdviser $financialAdviser, ClientManager $clientManager, Request $request) {
        $this->clearErrors();

        $clientIds = $request->get('client_id', '');
        $clientIds = explode(',', $clientIds);
        $clientIds = array_filter($clientIds);
        $groupIds = $request->get('group_id', '');
        $groupIds = explode(',', $groupIds);
        $groupIds = array_filter($groupIds);

        $subject = $request->get('subject');
        $message = $request->get('message');
        $reviewTemplateId = $request->get('review_template_id');
        $reviewTemplate = null;

        if (!$subject) {
            $this->addError('Please enter a subject for your review request');
            return false;
        }

        if (count($clientIds) == 0 && count($groupIds) == 0) {
            $this->addError('Please select at least one contact or group to send the request to');
            return false;
        }

        $clientsRequested = [];
        if ($clientIds) {
            $clientsRequested = $this->getRepo('AppBundle:Client')->findBy(['id' => $clientIds]);
        }
        if ($groupIds) {
            $clientsRequested += $clientManager->getClientsByGroupIds($financialAdviser, $groupIds);
        }

        if (!$clientsRequested) {
            $this->addError('Invalid clients selected');
            return false;
        }
        $clients = [];
        // validate selected clients belong to financial adviser
        foreach ($clientsRequested as $clientRequested) { /* @var $clientRequested Client */
            if ($clientRequested->getFinancialAdviser()->getId() == $financialAdviser->getId()) {
                $clients[] = $clientRequested;
            }
        }
        if (!$clients) {
            $this->addError('Invalid clients selected');
            return false;
        }
        // validate review template
        if ($reviewTemplateId) {
            $reviewTemplate = $this->getRepo('AppBundle:ReviewTemplate')->find($reviewTemplateId); /* @var $reviewTemplate ReviewTemplate */
            if ($reviewTemplate->getArchivedAt() || ($reviewTemplate->getFinancialAdviser() && $reviewTemplate->getFinancialAdviser()->getId() != $reviewTemplate->getFinancialAdviser()->getId())) {
                $this->addError("Invalid review template selected");
                return false;
            }
        }

        $reviewMessage = '';
        if ($reviewTemplate) {
            $reviewMessage = $reviewTemplate->getMessage();
        } else {

            $reviewMessage = "{adviserName} has sent a request to for you to review their service.\n\nIt's totally optional but you'd really be helping them out if you {link:click here and give them some feedback!}";
        }

        if ($message) {
            $reviewMessage .= "\n\n".' '.nl2br($message);
        }

        foreach ($clients as $client) {
            /* @var $client Client */
            // create the request
            $review = new Review();
            $review->setFinancialAdviser($financialAdviser);
            $review->setClient($client);
            $review->setEmailSubject($subject);
            $review->setTypeId(Review::TYPE_FEEDBACK);
            $review->setStatusId(Review::STATUS_PENDING);
            $review->setToken(Utils::randomHash(32));

            if ($reviewTemplate) {
                $review->setReviewTemplate($reviewTemplate);
                $review->setTypeId($reviewTemplate->getTypeId());
            }

            $reviewLink = $this->router->generate('profile_view', ['id' => $review->getFinancialAdviser()->getId(), 'token' => $review->getToken()], Router::ABSOLUTE_URL);
            $adviserName = $financialAdviser->getUser()->getFullName();

            try {
                $reviewMessage = $this->processPlaceholders($reviewMessage, [
                   'link' => $reviewLink,
                   'adviserName' => $adviserName,
                ]);
            } catch (\Exception $e) {
                $this->addError($e->getMessage());
                return false;
            }


            $review->setEmailMessage($reviewMessage);

            $this->em->persist($review);
            $this->em->flush($review);
        }

        return $clients;
    }


    /**
     * Replace placeholders in $text
     *
     * placeholders:
     * <code>
     * adviserName: The financial adviser's full name
     * link: the link to the review
     * </code>
     *
     * @param $text
     * @param array $replacements
     * @return String
     * @throws \Exception
     */
    public function processPlaceholders($text, array $replacements) {

        $matchedPlaceholders = [];
        preg_match_all("/\{([^}]+)\}/", $text, $matchedPlaceholders);

        if (!empty($matchedPlaceholders[1])) {

            foreach ($matchedPlaceholders[1] as $i => $matchedPlaceholder) {

                $placeholderName = $matchedPlaceholder;
                $value = '';
                if (strpos($matchedPlaceholder, ':') !== false) {
                    list($placeholderName, $value) = explode(':', $matchedPlaceholder);
                }

                if (!isset($replacements[$placeholderName])) {
                    throw new \Exception("Review message placeholder value is missing for: $placeholderName");
                }

                switch ($placeholderName) {
                    case 'link':
                        if ($value) {
                            $text = str_replace('{' . $matchedPlaceholder . '}', '<a href="'.$replacements[$placeholderName].'">'.$value.'</a>', $text);
                        } else {
                            $text =  str_replace('{'.$matchedPlaceholder.'}', $replacements[$placeholderName], $text);
                        }
                        break;
                    default:
                        $text =  str_replace('{'.$matchedPlaceholder.'}', $replacements[$placeholderName], $text);
                      break;
                }


            }

        }

        return $text;
    }


    /**
     * Get all published reviews for a financial adviser. Newest first.
     *
     * @param FinancialAdviser $financialAdviser
     * @param array $filter
     * @return Review[]
     * @throws \Exception
     */
    public function getPublishedReviewsForFA(FinancialAdviser $financialAdviser, $filter = []) {

        $sortField = 'r.publishedAt';
        $sortDir = 'desc';

        if (!empty($filter['reviewsOrder'])) {
            switch ($filter['reviewsOrder']) {
                case 'recent':
                    $sortField = 'r.publishedAt';
                    $sortDir = 'desc';
                    break;
                case 'oldest':
                    $sortField = 'r.publishedAt';
                    $sortDir = 'asc';
                    break;
            }
        }

        $qb = $this->getRepo('AppBundle:Review')->createQueryBuilder('r');
        $qb->leftJoin('r.reviewTemplate', 'rt');
        $qb->andWhere('r.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere('r.statusId = :statusId')->setParameter('statusId', Review::STATUS_PUBLISHED);
        $qb->andWhere($qb->expr()->isNotNull('r.rating').' OR (rt.ratingFeedbackEnabled = 0)');
        $qb->orderBy($sortField, $sortDir);
        $reviews = $qb->getQuery()->getResult();

        return $reviews ? $reviews : [];
    }

    /**
     * Get the most recent review for this financial Adviser
     *
     * @param FinancialAdviser $financialAdviser
     * @return Review
     * @throws \Exception
     */
    public function getMostRecentReview(FinancialAdviser $financialAdviser) {
        
        $qb = $this->getRepo('AppBundle:Review')->createQueryBuilder('r');
        $qb->leftJoin('r.reviewTemplate', 'rt');
        $qb->andWhere('r.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere('r.statusId = :statusId')->setParameter('statusId', Review::STATUS_PUBLISHED);
        $qb->andWhere($qb->expr()->isNotNull('r.rating').' OR (rt.ratingFeedbackEnabled = 0)');
        $qb->orderBy('r.publishedAt', 'desc');
        $qb->setMaxResults(1);
        $review = $qb->getQuery()->getOneOrNullResult();
        return $review;
    }

    /**
     * Get all reviews for a financial adviser. Newest first.
     *
     * @param FinancialAdviser $financialAdviser
     * @return Review[]
     * @throws \Exception
     */
    public function getAllReviewsForFA(FinancialAdviser $financialAdviser = null) {

        $qb = $this->getRepo('AppBundle:Review')->createQueryBuilder('r');
        if ($financialAdviser) {
            $qb->andWhere('r.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        }
        $qb->andWhere($qb->expr()->in('r.statusId',':statusId'))->setParameter('statusId', [Review::STATUS_ARCHIVED, Review::STATUS_PUBLISHED, Review::STATUS_RECEIVED_PENDING]);
        $qb->orderBy('r.receivedAt', 'desc');
        $reviews = $qb->getQuery()->getResult();

        return $reviews ? $reviews : [];
    }



    /**
     * Recalculate average FA rating
     *
     * @param FinancialAdviser $financialAdviser
     * @return FinancialAdviser
     */
    public function recalculateAverageFARating(FinancialAdviser $financialAdviser) {

        $reviews = $this->getPublishedReviewsForFA($financialAdviser);
        $rating = 0;
        $numReviews = count($reviews);

        if ($numReviews > 0) {
            foreach ($reviews as $review) {
                $rating += $review->getRating();
            }
            $rating = round($rating / $numReviews, 1);
        }

        $financialAdviser->setAverageRating($rating);
        $this->em->persist($financialAdviser);
        $this->em->flush($financialAdviser);

        return $financialAdviser;
    }

    /**
     * Setup a review object for use in the Review Form.
     * If the review has not yet been submitted and is using a review template the review template questions will
     * be added to the review as review answers
     *
     */
    public function buildReviewForForm(Review $review) {

        if ($review->getStatusId() != Review::STATUS_SENT || !$review->getReviewTemplate() || $review->getReviewAnswers()->count() > 0) {
            return $review;
        }

        $templateQuestions = $review->getReviewTemplate()->getReviewTemplateQuestions();
        foreach ($templateQuestions as $templateQuestion) { /* @var $templateQuestion ReviewTemplateQuestion */
            if ($templateQuestion->getArchivedAt()) {
                continue;
            }
            $reviewAnswer = new ReviewAnswer();
            $reviewAnswer->setReviewTemplateQuestion($templateQuestion);
            $reviewAnswer->setReview($review);
            $review->addReviewAnswer($reviewAnswer);
        }
        return $review;
    }

    /**
     * Get the query to get Reviews
     *
     * @param Array     $filter
     * @return \Doctrine\ORM\Query
     */
    public function getQuery($filter) {

        $qb = $this->getRepo('AppBundle:Review')->createQueryBuilder('r');

        if (!empty($filter['search'])) {
            $qb->join('r.client', 'c');
            $qb->join('c.person', 'p');
            $qb->join('r.financialAdviser', 'f');
            $qb->join('f.user', 'fu');

            $searchParts = explode(' ', $filter['search']);
            $searchConditions = [];
            foreach ($searchParts as $i => $searchPart) {
                $searchConditions[] = $qb->expr()->like('p.firstName', ':search'.$i);
                $searchConditions[] = $qb->expr()->like('p.lastName', ':search'.$i);
                $searchConditions[] = $qb->expr()->like('p.email', ':search'.$i);
                $searchConditions[] = $qb->expr()->like('f.businessName', ':search'.$i);
                $searchConditions[] = $qb->expr()->like('fu.firstName', ':search'.$i);
                $searchConditions[] = $qb->expr()->like('fu.lastName', ':search'.$i);
                $qb->setParameter('search'.$i, '%'.$searchPart.'%');
            }

            if ($searchConditions) {
                $qb->andWhere(implode(' OR ', $searchConditions));
            }
        }

        if (!empty($filter['verifiable'])) {
            $qb->andWhere($qb->expr()->eq("r.verifiable", 1));
            $qb->andWhere($qb->expr()->isNotNull("r.receivedAt"));
        }

        return $qb->getQuery();
    }

    /**
     *
     * @param Review $review
     */
    public function verifyReview(Review $review) {

        $review->setVerified(true);
        $this->em->persist($review);
        $this->em->flush($review);

    }
}