<?php
/**
 * AppBundle\Service\Recaptcha.php
 *
 * @author: Gul  
 */

namespace AppBundle\Service;


use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

class Recaptcha extends BaseManager {

    use ErrorRetrieval;

    protected $secretKey;

    protected $client;
    protected $verifyURL;

    /**
     * @param EntityManager $em
     * @param null $secretKey
     */
    public function __construct(EntityManager $em, $secretKey) {

        parent::__construct($em);
        $this->secretKey = $secretKey;
        $this->client = new Client();
        $this->verifyURL = 'https://www.google.com/recaptcha/api/siteverify';
    }


    /**
     * Verify recaptcha request
     *
     * @param $recaptchaRequest
     * @param $remoteIp
     * @return boolean
     */
    public function verify($recaptchaRequest, $remoteIp) {

        $this->clearErrors();
        $response = $this->client->post($this->verifyURL, [
            'form_params' => [
                'secret' => $this->secretKey,
                'response' => $recaptchaRequest,
                'remoteip' => $remoteIp
            ]
        ]);

        $content = $response->getBody()->getContents();

        $responseData = \json_decode($content, true);
        if ($responseData['success']) {
            return true;
        } else {
            if (isset($responseData['error-codes'])) {
                foreach ($responseData['error-codes'] as $errorcode) {
                    switch ($errorcode) {
                        case 'missing-input-secret':	$this->addError("The secret parameter is missing."); break;
                        case 'invalid-input-secret': $this->addError("The secret parameter is invalid or malformed."); break;
                        case 'missing-input-response': $this->addError("The response parameter is missing."); break;
                        case 'invalid-input-response': $this->addError("The response parameter is invalid or malformed."); break;
                    }
                }
                
            }
            return false;
        }

        
    }

}