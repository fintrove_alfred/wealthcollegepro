<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 23/02/2016
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\FinancialAdviser;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class ReviewTemplateManager extends BaseManager {


    /**
     * Get the query to get non-user Review templates
     *
     * @param Array     $filter
     * @return \Doctrine\ORM\Query
     */
    public function getQueryForSystem() {

        $qb = $this->getRepo('AppBundle:ReviewTemplate')->createQueryBuilder('rt');
        $qb->where('rt.financialAdviser IS NULL');

        return $qb->getQuery();
    }

    /**
     * Get the query to get user Review templates
     *
     * @param Array     $filter
     * @return \Doctrine\ORM\Query
     */
    public function getQueryForFA(FinancialAdviser $financialAdviser, $filter) {

        $qb = $this->getRepo('AppBundle:ReviewTemplate')->createQueryBuilder('rt');
        $qb->where('rt.financialAdviser = :financialAdviser OR rt.financialAdviser IS NULL')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere($qb->expr()->isNull('rt.archivedAt'));

        if (!empty($filter['type'])) {
            $qb->andWhere('rt.typeId = :typeId')->setParameter('typeId', $filter['type'], \PDO::PARAM_INT);
        }


        return $qb->getQuery();
    }

}