<?php
/**
 * ApplicationSetting.php
 *
 * @author Gul
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

class ApplicationSetting
{

    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }


    /**
     * Get an application setting
     *
     * @param String $handle
     * @return mixed
     * @throws \Exception
     */
    public function get($handle) {
        $applicationSetting = $this->em->getRepository('AppBundle:ApplicationSetting')->findOneBy(['handle' => $handle]);
        if (!$applicationSetting) {
            throw new \Exception('Application Setting not found');
        }
        return $applicationSetting->getValue();
    }

}