<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 26/02/2015
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\FinancialAdviser;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class MessageManager extends BaseManager {

    protected $user;
    /**
     * @var Router
     */
    protected $router;

    public function __construct(EntityManager $em, $class, TokenStorage $tokenStorage, Router $router) {

        parent::__construct($em, $class);

        if ($tokenStorage->getToken() && $tokenStorage->getToken()->isAuthenticated()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }
        $this->router = $router;
    }
  

    public function getUnreadMessageCount(User $user) {
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(m.id)');
        $qb->from('AppBundle:Message','m');
        $qb->where($qb->expr()->eq('m.userTo', $user->getId()));
        $qb->andWhere($qb->expr()->isNull('m.readAt'));
        $qb->andWhere($qb->expr()->isNull('m.trashedAt'));
        return $qb->getQuery()->getSingleScalarResult();
    }


    /**
     * Get the query to get Messages for pagination
     *
     * @param User  $user
     * @param Array $filter
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery(User $user, $filter = []) {

        $qb = $this->getRepo()->createQueryBuilder('m');

        if (!empty($filter['tray'])) {
            switch ($filter['tray']) {
                case 'inbox':
                    $qb->where($qb->expr()->eq('m.userTo', $user->getId()));
                    $qb->andWhere($qb->expr()->isNull('m.trashedAt'));
                    break;
                case 'sent':
                    $qb->where($qb->expr()->eq('m.userFrom', $user->getId()));
                    $qb->andWhere($qb->expr()->isNull('m.trashedAt'));
                    break;
                case 'trash':
                    $qb->where($qb->expr()->eq('m.userTo', $user->getId()));
                    $qb->andWhere($qb->expr()->isNotNull('m.trashedAt'));
                    break;
            }
        } else {
            $qb->where($qb->expr()->eq('m.userTo', $user->getId()));
            $qb->andWhere($qb->expr()->isNull('m.deletedAt'));
        }
        $qb->orderBy('m.created', 'desc');
        return $qb->getQuery();
    }

    /**
     * Mark message read
     * @param Message $message
     */
    public function markRead(Message $message) {

        $message->setReadAt(new \DateTime());
        $this->em->persist($message);
        $this->em->flush($message);
    }

    /**
     * Mark message as trashed (delete)
     * @param Message $message
     */
    public function trashMessage(Message $message) {
        $message->setTrashedAt(new \DateTime());
        $this->em->persist($message);
        $this->em->flush($message);
    }



   

    /**
     * @param $numEmails    The number of records to fetch
     * @return Message[]
     */
    public function getWaitingToEmail($numEmails) {

        $qb = $this->getRepo('Message')->createQueryBuilder('m');
        $qb->where('m.sendEmail = 1');
        $qb->andWhere('m.emailSentAt IS NULL');
        $qb->orderBy('m.created');
        $qb->setMaxResults($numEmails);
        $messages = $qb->getQuery()->getResult();

        return $messages ? $messages : [];
    }

}