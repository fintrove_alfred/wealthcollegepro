<?php
/**
 * CreateReviewRequest.php
 *
 * @author Gul
 */

namespace AppBundle\Service\Review;


use AppBundle\Service\BaseManager;
use AppBundle\Traits\ErrorRetrieval;

class CreateReviewRequest extends BaseManager
{

   use ErrorRetrieval;

   /**
    * Create review request with the given data
    * @param array $data
    */
   public function process(array $data) {
      $this->clearErrors();


   }

}