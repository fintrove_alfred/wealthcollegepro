<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 23/02/2016
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\Webinar;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Repository;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class WebinarManager extends BaseManager {

    use ErrorRetrieval;
    
    /**
     * @var User
     */
    protected $user;
    
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var CalendarManager
     */
    protected $calendarManager;

    public function __construct(EntityManager $em, TokenStorage $tokenStorage, Logger $logger, CalendarManager $calendarManager) {

        parent::__construct($em);

        if ($tokenStorage->getToken()->isAuthenticated()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }

        $this->logger = $logger;
        $this->calendarManager = $calendarManager;
    }

    /**
     * @param FinancialAdviser $financialAdviser
     * @param array $filter
     * @return Query
     * @throws \Exception
     */
    public function getQuery(FinancialAdviser $financialAdviser, array $filter = []) {


        $qb = $this->getRepo('AppBundle:Webinar')->createQueryBuilder('w');
        $qb->andWhere('w.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->join('w.calendarEvent', 'ce');

        if (!empty($filter['past'])) {
            $qb->andWhere('ce.endAt < :now')->setParameter('now', new \DateTime());
            $qb->orderBy('ce.startAt', 'desc');
        } else {
            $qb->andWhere('ce.endAt >= :now')->setParameter('now', new \DateTime());
            $qb->orderBy('ce.startAt', 'asc');
        }

        if (isset($filter['dirty'])) {
            $qb->andWhere('ce.dirty = :dirty')->setParameter('dirty', $filter['dirty'] ? 1 : 0);
        }

        return $qb->getQuery();
    }

    /**
     * @param FinancialAdviser $financialAdviser
     * @param array $filter
     * @return Webinar[]
     * @throws \Exception
     */
    public function getWebinars(FinancialAdviser $financialAdviser, array $filter = []) {
        
        return $this->getQuery($financialAdviser, $filter)->getResult();
    }


    /**
     * Sync webinars for an FA with google calendar
     *
     * @param FinancialAdviser $financialAdviser
     */
    public function syncWebinarsWithGoogleCalendar(FinancialAdviser $financialAdviser) {

        // check if access token is valid
        $user = $financialAdviser->getUser();

        $webinars = $this->getWebinars($financialAdviser, ['dirty' => 1]);

        if ($webinars) {
            try {
                $calendarEvents = [];
                foreach ($webinars as $webinar) {
                    /* @var $webinar Webinar */
                    $calendarEvents[] = $webinar->getCalendarEvent();
                }
                $this->calendarManager->syncEventsWithGoogleCalendar($user, $calendarEvents);
                if ($this->calendarManager->hasErrors()) {
                    $this->setErrors($this->calendarManager->getErrors());
                }
            } catch (Exception $e) {
                $this->logger->log(Logger::CRITICAL, 'Problem syncing webinars with google: '.$e->getMessage());
                $this->addError("Unable to sync webinars with google");
                return false;
            }

        }

    }

    /**
     * @param Webinar $webinar
     * @return bool
     */
    public function deleteWebinar(Webinar $webinar) {

        try {
            
            $this->calendarManager->deleteCalendarEvent($webinar->getCalendarEvent());
            $this->em->remove($webinar);
            $this->em->flush();

        } catch (Exception $e) {
            $this->logger->log(Logger::CRITICAL, 'Problem deleting webinar: '.$e->getMessage());
            $this->addError("Sorry there was a problem deleting the webinar");
            return false;
        }

    }
}