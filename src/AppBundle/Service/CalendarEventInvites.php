<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 23/02/2016
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\CalendarEventAttendee;
use AppBundle\Entity\CalendarEventType;
use AppBundle\Entity\Client;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\Webinar;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class CalendarEventInvites extends BaseManager {

    use ErrorRetrieval;

    /**
     * @var User
     */
    protected $user;


    protected $calendarManager;

    protected $logger;

    protected $emailManager;

    protected $clientManager;

    /**
     * CalendarEventInvites constructor.
     * @param EntityManager $em
     * @param CalendarManager $calendarManager
     * @param Logger $logger
     * @param EmailManager $emailManager
     * @param ClientManager $clientManager
     */
    public function __construct(EntityManager $em, CalendarManager $calendarManager, Logger $logger, EmailManager $emailManager, ClientManager $clientManager) {

        parent::__construct($em);
        $this->calendarManager = $calendarManager;
        $this->logger = $logger;
        $this->emailManager = $emailManager;
        $this->clientManager = $clientManager;
    }


    /**
     * @param CalendarEvent $calendarEvent
     * @param $clientIds
     * @return Array
     * <code>
     * [
     *      'invited' => 0,
     *      'alreadyInvited' => 0,
     *      'noEmail' => 0,
     * ]
     * </code>
     */
    public function sendCalendarEventInvites(CalendarEvent $calendarEvent, $clientIds = null, $clientGroupIds = null) {
        $this->clearErrors();

        if (!$clientIds && !$clientGroupIds) {
            $this->addError("No contacts or groups selected");
            return false;
        }
        if ($calendarEvent->isWebinar() && $calendarEvent->isDirty()) {
            $this->addError('You must sync your webinar with your Google Calendar before inviting attendees');
            return false;
        }
        $financialAdviser = $calendarEvent->getUser()->getFinancialAdviser();

        $clients = [];
        if ($clientIds) {
            $clients = $this->getRepo('AppBundle:Client')->findBy(['id' => $clientIds]);
        }
        if ($clientGroupIds) {
            $clients += $this->clientManager->getClientsByGroupIds($financialAdviser, $clientGroupIds);
        }
        
        $invited = 0;
        $alreadyInvited = 0;
        $noEmail = 0;

        $newAttendees = [];

        foreach ($clients as $client) { /* @var $client Client */
            if ($client->getFinancialAdviser()->getId() != $financialAdviser->getId()) {
                continue;
            }
            if (!$client->getPerson()->getEmail()) {
                $noEmail++;
                continue;
            }
            if ($calendarEvent->hasAttendee($client)) {
                $alreadyInvited++;
                continue;
            }
            $invited++;

            $calendarEventAttendee = new CalendarEventAttendee();
            $calendarEventAttendee->setClient($client);
            $calendarEventAttendee->setCalendarEvent($calendarEvent);

            $calendarEvent->setDirty(true);
            $calendarEvent->addCalendarEventAttendee($calendarEventAttendee);
            $newAttendees[] = $calendarEventAttendee;

        }

        $this->em->persist($calendarEvent);
        $this->em->flush($calendarEvent);

        try {
            $this->calendarManager->syncEventsWithGoogleCalendar($calendarEvent->getUser(), [$calendarEvent]);
            $this->emailManager->sentEventInvites($newAttendees);

        } catch (\Exception $e) {
            $this->logger->log(Logger::CRITICAL, $e->getMessage().': '.$e->getTraceAsString());
            $this->addError("There was a problem inviting your contacts. Please try again or contact support.");
            return false;
        }

        return [
            'invited' => $invited,
            'alreadyInvited' => $alreadyInvited,
            'noEmail' => $noEmail,
        ];
    }

}