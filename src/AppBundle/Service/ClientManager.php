<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 06/05/2016
 * Time: 09:44
 */

namespace AppBundle\Service;

use AppBundle\Entity\CalendarEvent;
use AppBundle\Entity\CalendarEventAttendee;
use AppBundle\Entity\CalendarEventType;
use AppBundle\Entity\Client;
use AppBundle\Entity\ClientImport;
use AppBundle\Entity\ClientWPMInsurancePolicy;
use AppBundle\Entity\ClientWPMInvestment;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\Person;
use AppBundle\Form\Type\ClientEventTypes;
use AppBundle\Traits\ErrorRetrieval;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Repository;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\SecurityContext;


class ClientManager extends BaseManager {

    use ErrorRetrieval;

    /**
     * @var User
     */
    protected $user;

    protected $logger;

    /**
     * CalendarEventInvites constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Logger $logger) {

        parent::__construct($em);
        $this->logger = $logger;
    }


    /**
     * Get all client event types for a financial adviser
     * @param FinancialAdviser $financialAdviser
     * @return ClientEventTypes[]
     */
    public function getFAClientEventTypes(FinancialAdviser $financialAdviser) {

        return $this->em->getRepository('AppBundle:ClientEventType')->createQueryBuilder('cet')
            ->where('cet.financialAdviser = :financialAdviser OR cet.financialAdviser IS NULL')
            ->setParameter('financialAdviser', $financialAdviser->getId())
            ->orderBy('cet.financialAdviser', 'asc')
            ->addOrderBy('cet.id', 'asc')
            ->getQuery()
            ->getResult();

    }

    /**
     * Get all clients that match search string
     * @param FinancialAdviser $financialAdviser
     * @return ClientEventTypes[]
     */
    public function getBySearch(FinancialAdviser $financialAdviser, $search) {

        $qb = $this->em->getRepository('AppBundle:Client')->createQueryBuilder('c')
           ->join('c.person', 'p')
           ->where('c.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());

        $searchParts = explode(' ', $search);
        $searchConditions = [];
        foreach ($searchParts as $i => $searchPart) {
            $searchConditions[] = $qb->expr()->like('p.firstName', ':search'.$i);
            $searchConditions[] = $qb->expr()->like('p.lastName', ':search'.$i);
            $qb->setParameter('search'.$i, '%'.$searchPart.'%');
        }

        if ($searchConditions) {
            $qb->andWhere(implode(' OR ', $searchConditions));
        }

        return $qb->orderBy('p.firstName', 'asc')
           ->getQuery()
           ->getResult();

    }


    /**
     * @param FinancialAdviser $financialAdviser
     * @return Array
     */
    public function getFAClientEventTypeOptions(FinancialAdviser $financialAdviser) {

        $options = [];
        $clientEventTypes = $this->getFAClientEventTypes($financialAdviser);
        if ($clientEventTypes) {
            foreach ($clientEventTypes as $clientEventType) {
                $options[$clientEventType->getId()] = $clientEventType->getName();
            }
        }
        return $options;
    }

    /**
     * Recalculate Wealth PLanning model values using investments / insurance policies / estates
     * @param Client $client
     * @return \AppBundle\Entity\ClientWPM
     */
    public function recalculateWPMValues(Client $client) {

        $clientWPM = $client->getClientWPM();
        $cashflow = $client->getClientWPMCashflow();

        $clientWPM->setActiveIncome(
            $cashflow->getAmountAnnual($cashflow->getSalary(), $cashflow->getSalaryMode())+
            $cashflow->getAmountAnnual($cashflow->getAllowances(), $cashflow->getAllowancesMode())+
            $cashflow->getAmountAnnual($cashflow->getCommissions(), $cashflow->getCommissionsMode())+
            $cashflow->getAmountAnnual($cashflow->getBonuses(), $cashflow->getBonusesMode())+
            $cashflow->getCashflowActiveOthers()
        );
        $clientWPM->setPassiveIncome(
           $cashflow->getAmountAnnual($cashflow->getCoupons(), $cashflow->getCouponsMode())+
           $cashflow->getAmountAnnual($cashflow->getDividends(), $cashflow->getDividendsMode())+
           $cashflow->getAmountAnnual($cashflow->getRentals(), $cashflow->getRentalsMode())+
           $cashflow->getCashflowPassiveOthers()
        );
        $clientWPM->setFixedExpenses(
           $cashflow->getAmountAnnual($cashflow->getHome(), $cashflow->getHomeMode())+
           $cashflow->getAmountAnnual($cashflow->getTransport(), $cashflow->getTransportMode())+
           $cashflow->getAmountAnnual($cashflow->getPersonal(), $cashflow->getPersonalMode())+
           $cashflow->getAmountAnnual($cashflow->getFood(), $cashflow->getFoodMode())+
           $cashflow->getAmountAnnual($cashflow->getMedical(), $cashflow->getMedicalMode())+
           $cashflow->getAmountAnnual($cashflow->getChildren(), $cashflow->getChildrenMode())+
           $cashflow->getAmountAnnual($cashflow->getLoans(), $cashflow->getLoansMode())+
           $cashflow->getCashflowFixedOthers()
        );
        $clientWPM->setVariableExpenses(
           $cashflow->getAmountAnnual($cashflow->getRecreation(), $cashflow->getRecreationMode())+
           $cashflow->getAmountAnnual($cashflow->getMiscellaneous(), $cashflow->getMiscellaneousMode())
        );

        $clientWPM->setRiskInsurancePlanning(
           $cashflow->getAmountAnnual($cashflow->getInsurances(), $cashflow->getInsurancesMode())
        );
        $clientWPM->setInvestmentRetirementPlanning(
           $cashflow->getAmountAnnual($cashflow->getSavings(), $cashflow->getSavingsMode())
        );
        $clientWPM->setGiftEstatePlanning(
           $cashflow->getAmountAnnual($cashflow->getGifts(), $cashflow->getGiftsMode())
        );

        $clientWPM->setAnnualCashFlowBalance(
            floatval($clientWPM->getActiveIncome()) + floatval($clientWPM->getPassiveIncome())
            - $clientWPM->getFixedExpenses()
            - $clientWPM->getVariableExpenses()
            - $clientWPM->getRiskInsurancePlanning()
            - $clientWPM->getInvestmentRetirementPlanning()
            - $clientWPM->getGiftEstatePlanning()
        );

        $death = 0;
        $disabilityRiders = 0;
        $disabilitySup = 0;
        $personalAccidentStandalone = 0;
        $personalAccidentRiders = 0;
        $personalAccidentSup = 0;
        $criticalIllnessStandalone = 0;
        $criticalIllnessRiders = 0;
        $criticalIllnessSup = 0;
        $hospitalisationStandalone = 0;
        $hospitalisationRiders = 0;
        $hospitalisationSup = 0;

        if ($client->getClientWPMInsurancePolicies()->count() > 0) {
            foreach ($client->getClientWPMInsurancePolicies() as $clientWPMInsurancePolicy) {
                /* @var $clientWPMInsurancePolicy ClientWPMInsurancePolicy */
                $death += $clientWPMInsurancePolicy->getSumAssured();

                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_DISABILITY, $clientWPMInsurancePolicy->getRiderBenefit())) {
                    $disabilityRiders += $clientWPMInsurancePolicy->getRiderSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_DISABILITY, $clientWPMInsurancePolicy->getSupBenefit())) {
                    $disabilitySup += $clientWPMInsurancePolicy->getSupSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_PERSONAL_ACCIDENT, $clientWPMInsurancePolicy->getMainBenefit())) {
                    $personalAccidentStandalone += $clientWPMInsurancePolicy->getSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_PERSONAL_ACCIDENT, $clientWPMInsurancePolicy->getRiderBenefit())) {
                    $personalAccidentRiders += $clientWPMInsurancePolicy->getRiderSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_PERSONAL_ACCIDENT, $clientWPMInsurancePolicy->getSupBenefit())) {
                    $personalAccidentSup += $clientWPMInsurancePolicy->getSupSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_CRITICAL_ILLNESS, $clientWPMInsurancePolicy->getMainBenefit())) {
                    $criticalIllnessStandalone += $clientWPMInsurancePolicy->getSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_CRITICAL_ILLNESS, $clientWPMInsurancePolicy->getRiderBenefit())) {
                    $criticalIllnessRiders += $clientWPMInsurancePolicy->getRiderSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_CRITICAL_ILLNESS, $clientWPMInsurancePolicy->getSupBenefit())) {
                    $criticalIllnessSup += $clientWPMInsurancePolicy->getSupSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_HOSPITALISATION, $clientWPMInsurancePolicy->getMainBenefit())) {
                    $hospitalisationStandalone += $clientWPMInsurancePolicy->getSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_HOSPITALISATION, $clientWPMInsurancePolicy->getRiderBenefit())) {
                    $hospitalisationRiders += $clientWPMInsurancePolicy->getRiderSumAssured();
                }
                if (in_array(ClientWPMInsurancePolicy::BENEFIT_TYPE_HOSPITALISATION, $clientWPMInsurancePolicy->getSupBenefit())) {
                    $hospitalisationSup += $clientWPMInsurancePolicy->getSupSumAssured();
                }
            }
        }
        $clientWPM->setDeath($death);
        $clientWPM->setDisabilityRiders($disabilityRiders);
        $clientWPM->setDisabilitySubBenefits($disabilitySup);
        $clientWPM->setPaStdAlone($personalAccidentStandalone);
        $clientWPM->setPaRiders($personalAccidentRiders);
        $clientWPM->setPaSubBenefits($personalAccidentSup);
        $clientWPM->setCiStdAlone($criticalIllnessStandalone);
        $clientWPM->setCiRiders($criticalIllnessRiders);
        $clientWPM->setCiSubBenefits($criticalIllnessSup);
        $clientWPM->setHospStdAlone($hospitalisationStandalone);
        $clientWPM->setHospRiders($hospitalisationRiders);
        $clientWPM->setHospSubBenefits($hospitalisationSup);

        $assetLR = 0;
        $assetMR = 0;
        $assetHR = 0;
        $liabilityLR = 0;
        $liabilityMR = 0;
        $liabilityHR = 0;
        $totalGrossEstate = 0;
        //$totalNetEstate = 0;

        if ($client->getClientWPMInvestments()->count() > 0) {

            foreach ($client->getClientWPMInvestments() as $clientWPMInvestment) { /* @var $clientWPMInvestment ClientWPMInvestment */

                if ($clientWPMInvestment->getLiquidateOnDeath()) {
                    $totalGrossEstate += $clientWPMInvestment->getCurrentMarketValue();
                    //$totalNetEstate += $clientWPMInvestment->getCurrentMarketValue() - $clientWPMInvestment->getCurrentLoanAmount();
                }

                if ($clientWPMInvestment->getRiskClass() == 'Lower Risk') {
                    $assetLR += $clientWPMInvestment->getCurrentMarketValue();
                    $liabilityLR += $clientWPMInvestment->getCurrentLoanAmount();
                }
                if ($clientWPMInvestment->getRiskClass() == 'Medium Risk') {
                    $assetMR += $clientWPMInvestment->getCurrentMarketValue();
                    $liabilityMR += $clientWPMInvestment->getCurrentLoanAmount();
                }
                if ($clientWPMInvestment->getRiskClass() == 'Higher Risk') {
                    $assetHR += $clientWPMInvestment->getCurrentMarketValue();
                    $liabilityHR += $clientWPMInvestment->getCurrentLoanAmount();
                }

            }

        }
        $totalAssets = $assetLR + $assetMR + $assetHR;
        $totalLiabilities = $liabilityLR + $liabilityMR + $liabilityHR;

        $clientWPM->setAssetsLowerRisk($assetLR);
        $clientWPM->setAssetsModerateRisk($assetMR);
        $clientWPM->setAssetsHigherRisk($assetHR);
        $clientWPM->setLiabilitiesLowerRisk($liabilityLR);
        $clientWPM->setLiabilitiesModerateRisk($liabilityMR);
        $clientWPM->setLiabilitiesHigherRisk($liabilityHR);

        $clientWPM->setInvestmentNetWorth($totalAssets - $totalLiabilities);
        $clientWPM->setNetEstate($clientWPM->getInvestmentNetWorth() + $clientWPM->getDeath());

        $this->em->persist($clientWPM);
        $this->em->flush($clientWPM);

        return $clientWPM;
    }

    /**
     * @param $clientGroupIds
     * @return Client[]
     * @throws \Exception
     */
    public function getClientsByGroupIds(FinancialAdviser $financialAdviser, $clientGroupIds) {

        $qb = $this->getRepo('AppBundle:Client')->createQueryBuilder('c');
        $qb->distinct();
        $qb->join('c.clientClientGroups', 'ccg');
        $qb->andWhere($qb->expr()->eq('c.financialAdviser', ':financialAdviser'))->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere($qb->expr()->in('ccg.clientGroup', ':clientGroupIds'))->setParameter('clientGroupIds', $clientGroupIds);

        $clients = $qb->getQuery()->getResult();
        return $clients ? $clients : [];
    }

    /**
     * @param FinancialAdviser $financialAdviser
     * @return array
     * @throws \Exception
     */
    public function getClientGroupClientCounts(FinancialAdviser $financialAdviser) {

        $qb = $this->getRepo('AppBundle:ClientClientGroup')->createQueryBuilder('ccg');
        $qb->select(['cg.id', 'COUNT(ccg.client) as numClients']);
        $qb->join('ccg.clientGroup', 'cg', Query\Expr\Join::WITH, $qb->expr()->eq('cg.financialAdviser', ':financialAdviser'));
        $qb->andWhere('ccg.deletedAt IS NULL');
        $qb->groupBy('ccg.clientGroup');
        $qb->setParameter('financialAdviser', $financialAdviser->getId());

        $results = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $clientGroupCounts = [];
        if ($results) {
            foreach ($results as $result) {
                $clientGroupCounts[$result['id']] = $result['numClients'];
            }
        }

        return $clientGroupCounts;
    }

    /**
     * @param $filter
     * @return Client[]
     * @throws \Exception
     */
    public function getClients($filter) {

        $qb = $this->getRepo('AppBundle:Client')->createQueryBuilder('c');
        $qb->leftJoin('c.person', 'p');
        $qb->leftJoin('c.clientClientGroups', 'ccg');

        if (!empty($filter['financialAdviser'])) {
            $qb->andWhere($qb->expr()->eq('c.financialAdviser', ':financialAdviser'))->setParameter('financialAdviser', $filter['financialAdviser']);
        }
        if (isset($filter['typeId'])) {
            if ($filter['typeId'] == 'all') {
                // show all no filter
            } elseif (!$filter['typeId']) {
                // empty so show others
                $qb->andWhere($qb->expr()->isNull('c.typeId').' OR '.$qb->expr()->eq('c.typeId', "''").' OR '.$qb->expr()->eq('c.typeId', '0'));
            } else {
                // clients and prospects
                $qb->andWhere($qb->expr()->eq('c.typeId', ':typeId'))->setParameter('typeId', $filter['typeId']);
            }
        }
        if (!empty($filter['name'])) {
            $qb->andWhere($qb->expr()->like('p.firstName', ':name').' OR '.$qb->expr()->like('p.lastName', ':name'))
                ->setParameter('name', '%'.$filter['name'].'%');
        }
        if (!empty($filter['notesInclude'])) {
            $qb->andWhere($qb->expr()->like('c.notes', ':notesInclude'))->setParameter('notesInclude', '%'.$filter['notesInclude'].'%');
        }
        if (!empty($filter['groupIds'])) {
            $qb->andWhere($qb->expr()->in('ccg.clientGroup', ':clientGroup'))->setParameter('clientGroup', $filter['groupIds']);
        }
        if (!empty($filter['gender'])) {
            $qb->andWhere($qb->expr()->eq('p.gender', ':gender'))->setParameter('gender', $filter['gender']);
        }
        if (!empty($filter['ageFrom'])) {
            $fromDate  = new \DateTime();
            $fromDate->modify('-'.intval($filter['ageFrom']).' years');
            $qb->andWhere($qb->expr()->lte('p.dob', ':fromDate'))->setParameter('fromDate', $fromDate->format('Y-m-d H:i:s'));
        }
        if (!empty($filter['ageTo'])) {
            $toDate  = new \DateTime();
            $toDate->modify('-'.intval($filter['ageTo']).' years');
            $qb->andWhere($qb->expr()->gte('p.dob', ':toDate'))->setParameter('toDate', $toDate->format('Y-m-d H:i:s'));
        }

        $clients =  $qb->getQuery()->getResult();
        return $clients ? $clients : [];
    }


    /**
     * Get client data for use in a select2 component
     *
     * @param FinancialAdviser $financialAdviser
     * @return array
     */
    public function getFAClientsSelectData(FinancialAdviser $financialAdviser, $typeId = Client::TYPE_CLIENT) {
        $clients = $financialAdviser->getClients();
        $data = [];
        foreach ($clients as $client) { /* @var $client Client */
            if ($client->getPerson()->getEmail() && $client->getPerson()->getFirstName() && ($typeId === null || $client->getTypeId() == $typeId)) {
                $data[] = [
                    'id' => $client->getId(),
                    'text' => $client->getPerson()->getFullName() . ' (' . $client->getPerson()->getEmail() . ')',
                    'type' => 'client'
                ];
            }
        }

        return $data;
    }

    /**
     * Get groups data for use in a select2 component
     *
     * @param FinancialAdviser $financialAdviser
     * @return array
     */
    public function getFAGroupsSelectData(FinancialAdviser $financialAdviser) {

        $clientGroups = $financialAdviser->getClientGroups();
        $data = [];
        foreach ($clientGroups as $clientGroup) { /* @var $clientGroup ClientGroup */
            $data[] = [
                'id' => $clientGroup->getId(),
                'text' => $clientGroup->getName()
            ];

        }
        return $data;
    }

    /**
     * @param User $user
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return array
     * @throws \Exception
     */
    public function getSpecialDatesReminders(FinancialAdviser $financialAdviser, \DateTime $startDate, \DateTime $endDate) {

        $qb = $this->getRepo('AppBundle:ClientDate')->createQueryBuilder('cd');
        $qb->join('cd.client', 'c');
        $qb->where('c.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere('cd.reminder = 1');
        $qb->andWhere('cd.reminderDate >= :startDate')->setParameter('startDate', $startDate->format('Y-m-d H:i:s'));
        $qb->andWhere('cd.reminderDate <= :endDate')->setParameter('endDate', $endDate->format('Y-m-d H:i:s'));
        $qb->orderBy('cd.date', 'asc');

        $clientDates = $qb->getQuery()->getResult();

        return $clientDates ? $clientDates : [];
    }

    /**
     * @param User $user
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return array
     * @throws \Exception
     */
    public function getSpecialDates(FinancialAdviser $financialAdviser, \DateTime $startDate, \DateTime $endDate) {

        $qb = $this->getRepo('AppBundle:ClientDate')->createQueryBuilder('cd');
        $qb->join('cd.client', 'c');
        $qb->where('c.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $financialAdviser->getId());
        $qb->andWhere('cd.date >= :startDate')->setParameter('startDate', $startDate->format('Y-m-d H:i:s'));
        $qb->andWhere('cd.date <= :endDate')->setParameter('endDate', $endDate->format('Y-m-d H:i:s'));
        $qb->orderBy('cd.date', 'asc');
        $clientDates = $qb->getQuery()->getResult();

        return $clientDates ? $clientDates : [];
    }

    /**
     * @param Client $client
     */
    public function deleteClient(Client $client) {

        // delete relations
        $clientRelations = $this->em->getRepository('AppBundle:ClientRelation')
            ->createQueryBuilder('cr')
            ->andWhere('cr.client = :client OR cr.relatedClient = :client')
            ->setParameter('client', $client->getId())
            ->getQuery()
            ->getResult();

        if ($clientRelations) {
            foreach ($clientRelations as $clientRelation) {
                $this->em->remove($clientRelation);
            }
        }

        if ($client->getClientDates()) {
            foreach ($client->getClientDates() as $clientDate) {
                $this->em->remove($clientDate);
            }
        }

        if ($client->getClientEvents()) {
            foreach ($client->getClientEvents() as $clientEvent) {
                $this->em->remove($clientEvent);
            }
        }

        if ($client->getClientClientGroups()) {
            foreach ($client->getClientClientGroups() as $clientClientGroup) {
                $this->em->remove($clientClientGroup);
            }
        }

        $this->em->remove($client);
        $this->em->flush();
    }


}