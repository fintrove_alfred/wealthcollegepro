<?php
/**
 * AppBundle\Service\EmailManager.php
 *
 * @author: Gul  
 */

namespace AppBundle\Service;


use AppBundle\Entity\CalendarEventAttendee;
use AppBundle\Entity\FinancialAdviser;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Communication;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EmailManager extends BaseManager {

    protected $templating;
    protected $mailer;

    protected $logger;

    protected $fromEmail = 'no-reply@fahub.sg';
    protected $fromName = 'Wealth College Pro';

    /**
     * @param EventDispatcherInterface $dispatcher
     * @param EntityManager      $em
     * @param TwigEngine         $templating
     * @param \Swift_Mailer      $mailer
     */
    public function __construct(EntityManager $em, $templating, $mailer, LoggerInterface $logger) {

        parent::__construct($em);

        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->logger = $logger;
    }


    /**
     * @return LoggerInterface
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * Send an email to the user to say they have been invited to an event
     *
     * @param CalendarEventAttendee[] $calendarEventAttendees
     * @return Communication[]|FALSE False if failed to send
     */
    public function sentEventInvites($calendarEventAttendees) {

        $communications = [];

        if (!$calendarEventAttendees) {
            return false;
        }

        $calendarEvent = $calendarEventAttendees[0]->getCalendarEvent();

        foreach ($calendarEventAttendees as $calendarEventAttendee) {

            $templateName = 'event-invite';
            $params = [
               'calendarEventAttendee' => $calendarEventAttendee
            ];
            if ($calendarEvent->isWebinar()) {
                $webinar = $this->getRepo('AppBundle:Webinar')->findOneBy(['calendarEvent' => $calendarEvent->getId()]);
                if ($webinar) {
                    $params['webinar'] = $webinar;
                    $templateName = 'event-invite-webinar';
                }
            }


            $htmlBody = $this->templating->render(
               "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
            );
            $textBody = $this->templating->render(
               "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
            );

            $fromUser = $calendarEventAttendee->getCalendarEvent()->getUser();
            $toPerson = $calendarEventAttendee->getClient()->getPerson();

            $swiftMessage = \Swift_Message::newInstance()
               ->setSubject($fromUser->getFullName().' Has Invited You To Their Webinar: '.$calendarEventAttendee->getCalendarEvent()->getSummary())
               ->setFrom($this->fromEmail, $this->fromName)
               ->setTo($toPerson->getEmail(), $toPerson->getFullName());
            /* @var $swiftMessage \Swift_Message */
            $swiftMessage->setBody($htmlBody, 'text/html');
            $swiftMessage->addPart($textBody, 'text/plain');


            if (!$this->mailer->send($swiftMessage)) {
                $this->getLogger()->warning(
                   'Failed to send event invite email for calendar event ID:'.$calendarEventAttendee->getCalendarEvent()->getId().' to '.$toPerson->getFullName().' '.$toPerson->getEmail()
                );
                continue;
            }

            // create communication
            $communication = new Communication();
            $communication->setTypeId(Communication::TYPE_EMAIL);
            //$communication->setUserTo();
            $communication->setToEmail($toPerson->getEmail());
            $communication->setToName($toPerson->getFullName());
            $communication->setLink('CalendarEvent');
            $communication->setLinkId($calendarEventAttendee->getCalendarEvent()->getId());
            $communication->setSubject($swiftMessage->getSubject());
            $communication->setBody($swiftMessage->getBody());
            $communication->setStatusId(Communication::STATUS_SENT);
            $communication->setSentAt(new \DateTime());

            $this->em->persist($communication);
            $this->em->flush($communication);

            $communications[] = $communication;
        }
        return $communications;
    }

    /**
     * Send an email to the users to say the event has been cancelled
     *
     * @param CalendarEventAttendee[] $calendarEventAttendees
     * @return Communication[]|FALSE False if failed to send
     */
    public function sendEventCancelled($calendarEventAttendees) {

        $communications = [];

        if (!$calendarEventAttendees) {
            return false;
        }

        $calendarEvent = $calendarEventAttendees[0]->getCalendarEvent();

        foreach ($calendarEventAttendees as $calendarEventAttendee) {

            $templateName = 'event-cancelled';
            $params = [
               'calendarEventAttendee' => $calendarEventAttendee
            ];

            $htmlBody = $this->templating->render(
               "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
            );
            $textBody = $this->templating->render(
               "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
            );

            $fromUser = $calendarEventAttendee->getCalendarEvent()->getUser();
            $toPerson = $calendarEventAttendee->getClient()->getPerson();

            $swiftMessage = \Swift_Message::newInstance()
               ->setSubject('The '.($calendarEvent->isWebinar() ? 'Webinar' : 'Event').': '.$calendarEvent->getSummary().' has been cancelled')
               ->setFrom($this->fromEmail, $this->fromName)
               ->setTo($toPerson->getEmail(), $toPerson->getFullName());
            /* @var $swiftMessage \Swift_Message */
            $swiftMessage->setBody($htmlBody, 'text/html');
            $swiftMessage->addPart($textBody, 'text/plain');


            if (!$this->mailer->send($swiftMessage)) {
                $this->getLogger()->warning(
                   'Failed to send event cancelled email for calendar event ID:'.$calendarEventAttendee->getCalendarEvent()->getId().' to '.$toPerson->getFullName().' '.$toPerson->getEmail()
                );
                continue;
            }

            // create communication
            $communication = new Communication();
            $communication->setTypeId(Communication::TYPE_EMAIL);
            //$communication->setUserTo();
            $communication->setToEmail($toPerson->getEmail());
            $communication->setToName($toPerson->getFullName());
            $communication->setLink('CalendarEvent');
            $communication->setLinkId($calendarEventAttendee->getCalendarEvent()->getId());
            $communication->setSubject($swiftMessage->getSubject());
            $communication->setBody($swiftMessage->getBody());
            $communication->setStatusId(Communication::STATUS_SENT);
            $communication->setSentAt(new \DateTime());

            $this->em->persist($communication);
            $this->em->flush($communication);

            $communications[] = $communication;
        }
        return $communications;
    }


    /**
     * Send an email to the users to say the event has been cancelled
     *
     * @param $financialAdviser[] $calendarEventAttendees
     * @return Communication[]|FALSE False if failed to send
     */
    public function sendSubscriptionExpiringMessage($financialAdvisers, $daysToExpiry) {

        $communications = [];

        if (!$financialAdvisers) {
            return false;
        }

        foreach ($financialAdvisers as $financialAdviser) { /* @var $financialAdviser FinancialAdviser */

            $templateName = 'subscription-expiring';
            $params = [
               'financialAdviser' => $financialAdviser
            ];

            $htmlBody = $this->templating->render(
               "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
            );
            $textBody = $this->templating->render(
               "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
            );


            $toUser = $financialAdviser->getUser();

            $swiftMessage = \Swift_Message::newInstance()
               ->setSubject("Wealth College Pro Subscription Expiring in $daysToExpiry days")
               ->setFrom($this->fromEmail, $this->fromName)
               ->setTo($toUser->getEmail(), $toUser->getFullName());
            /* @var $swiftMessage \Swift_Message */
            $swiftMessage->setBody($htmlBody, 'text/html');
            $swiftMessage->addPart($textBody, 'text/plain');


            if (!$this->mailer->send($swiftMessage)) {
                $this->getLogger()->warning(
                   'Failed to send subscription expiring email for FA: '.$financialAdviser->getId().' to '.$toUser->getFullName().' '.$toUser->getEmail()
                );
                continue;
            }

            // create communication
            $communication = new Communication();
            $communication->setTypeId(Communication::TYPE_EMAIL);
            $communication->setUserTo($toUser);
            $communication->setToEmail($toUser->getEmail());
            $communication->setToName($toUser->getFullName());
            $communication->setLink('FinancialAdviser');
            $communication->setLinkId($financialAdviser->getId());
            $communication->setSubject($swiftMessage->getSubject());
            $communication->setBody($swiftMessage->getBody());
            $communication->setStatusId(Communication::STATUS_SENT);
            $communication->setSentAt(new \DateTime());

            $this->em->persist($communication);
            $this->em->flush($communication);

            $communications[] = $communication;
        }
        return $communications;
    }

    /**
     * Send an email to the users to say the event has been cancelled
     *
     * @return Communication[]|FALSE False if failed to send
     */
    public function sendRegisteredInterestWelcomeMessage($name, $email) {

        if (!$email) {
            return false;
        }

        $templateName = 'registered-interest-welcome';
        $params = [
           'name' => $name
        ];

        $htmlBody = $this->templating->render(
           "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
        );
        $textBody = $this->templating->render(
           "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
        );

        $swiftMessage = \Swift_Message::newInstance()
           ->setSubject("Your Interest in Wealth College Pro")
           ->setFrom('admin@fahub.sg', $this->fromName)
           ->setTo($email, $name);
        /* @var $swiftMessage \Swift_Message */
        $swiftMessage->setBody($htmlBody, 'text/html');
        $swiftMessage->addPart($textBody, 'text/plain');


        if (!$this->mailer->send($swiftMessage)) {
            $this->getLogger()->warning(
               'Failed to send registered interest welcome email to '.$email.' '.$name
            );
            return false;
        }

        // create communication
        $communication = new Communication();
        $communication->setTypeId(Communication::TYPE_EMAIL);
        $communication->setToEmail($email);
        $communication->setToName($name);
        $communication->setSubject($swiftMessage->getSubject());
        $communication->setBody($swiftMessage->getBody());
        $communication->setStatusId(Communication::STATUS_SENT);
        $communication->setSentAt(new \DateTime());

        $this->em->persist($communication);
        $this->em->flush($communication);

        return $communication;
    }

    /**
     * Send an email to the users to say their account was activated
     *
     * @return Communication[]|FALSE False if failed to send
     */
    public function sendAccountActivatedEmail(User $user) {

        if (!$user->getEmail()) {
            return false;
        }

        $templateName = 'account-activated';
        $params = [
            'user' => $user,
            'name' => $user->getName()
        ];

        $htmlBody = $this->templating->render(
            "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
        );
        $textBody = $this->templating->render(
            "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
        );

        $swiftMessage = \Swift_Message::newInstance()
            ->setSubject("Your Wealth College Pro account has been activated")
            ->setFrom('admin@fahub.sg', $this->fromName)
            ->setTo($user->getEmail(), $user->getName());
        /* @var $swiftMessage \Swift_Message */
        $swiftMessage->setBody($htmlBody, 'text/html');
        $swiftMessage->addPart($textBody, 'text/plain');


        if (!$this->mailer->send($swiftMessage)) {
            $this->getLogger()->warning(
                'Failed to send account activated email to '.$user->getEmail().' '.$user->getName()
            );
            return false;
        }

        // create communication
        $communication = new Communication();
        $communication->setTypeId(Communication::TYPE_EMAIL);
        $communication->setToEmail($user->getEmail());
        $communication->setToName($user->getName());
        $communication->setSubject($swiftMessage->getSubject());
        $communication->setBody($swiftMessage->getBody());
        $communication->setStatusId(Communication::STATUS_SENT);
        $communication->setSentAt(new \DateTime());

        $this->em->persist($communication);
        $this->em->flush($communication);

        return $communication;
    }

    /**
     * Send new registration notification message to admin email address
     *
     * @param User $user
     * @return \AppBundle\Entity\Communication[]|FALSE False if failed to send
     * @throws \Twig_Error
     */
    public function sendRegistrationNotification(User $user) {


        $toEmail = 'admin@fahub.sg';
        $templateName = 'registration-notification';
        $params = [
           'user' => $user
        ];

        $htmlBody = $this->templating->render(
           "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
        );
        $textBody = $this->templating->render(
           "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
        );

        $swiftMessage = \Swift_Message::newInstance()
           ->setSubject("New registration ".$user->getName(true))
           ->setFrom($this->fromEmail, 'Wealth College Pro Mailer')
           ->setTo($toEmail, 'FAHub');
        /* @var $swiftMessage \Swift_Message */
        $swiftMessage->setBody($htmlBody, 'text/html');
        $swiftMessage->addPart($textBody, 'text/plain');


        if (!$this->mailer->send($swiftMessage)) {
            $this->getLogger()->warning(
               'Failed to send registration notification email to '.$user->getEmail().' '.$user->getName(true)
            );
            return false;
        }

        // create communication
        $communication = new Communication();
        $communication->setTypeId(Communication::TYPE_EMAIL);
        $communication->setToEmail($toEmail);
        $communication->setToName('Wealth College Pro');
        $communication->setSubject($swiftMessage->getSubject());
        $communication->setBody($swiftMessage->getBody());
        $communication->setStatusId(Communication::STATUS_SENT);
        $communication->setSentAt(new \DateTime());

        $this->em->persist($communication);
        $this->em->flush($communication);

        return $communication;
    }

    /**
     * Send the contact form submission to admin email address
     *
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $message
     * @return \AppBundle\Entity\Communication[]|FALSE False if failed to send
     * @throws \Twig_Error
     */
    public function sendContactEmail($firstName, $lastName, $email, $message) {

        if (!$email) {
            return false;
        }

        $toEmail = 'admin@fahub.sg';
        $templateName = 'contact-form-submit';
        $params = [
           'firstName' => $firstName,
           'lastName' => $lastName,
           'email' => $email,
           'message' => $message,
        ];

        $htmlBody = $this->templating->render(
           "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
        );
        $textBody = $this->templating->render(
           "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
        );

        $swiftMessage = \Swift_Message::newInstance()
           ->setSubject("New contact form submission from $firstName $lastName")
           ->setFrom($this->fromEmail, 'Wealth College Pro Mailer')
           ->setTo($toEmail, 'Wealth College Pro');
        /* @var $swiftMessage \Swift_Message */
        $swiftMessage->setBody($htmlBody, 'text/html');
        $swiftMessage->addPart($textBody, 'text/plain');


        if (!$this->mailer->send($swiftMessage)) {
            $this->getLogger()->warning(
               'Failed to contact us welcome email to '.$email.' '.$firstName.' '.$lastName
            );
            return false;
        }

        // create communication
        $communication = new Communication();
        $communication->setTypeId(Communication::TYPE_EMAIL);
        $communication->setToEmail($toEmail);
        $communication->setToName('Wealth College Pro');
        $communication->setSubject($swiftMessage->getSubject());
        $communication->setBody($swiftMessage->getBody());
        $communication->setStatusId(Communication::STATUS_SENT);
        $communication->setSentAt(new \DateTime());

        $this->em->persist($communication);
        $this->em->flush($communication);

        return $communication;
    }
    /**

     * Send the contact form submission to admin email address
     *
     * @param FinancialAdviser $financialAdviser
     * @param $subject
     * @param $subject
     * @return \AppBundle\Entity\Communication[]|FALSE False if failed to send
     * @throws \Twig_Error
     */
    public function sendFarEmail(FinancialAdviser $financialAdviser, $subject, $message) {

        if (!$financialAdviser) {
            return false;
        }
        $email = $financialAdviser->getEmail() ? $financialAdviser->getEmail() : $financialAdviser->getUser()->getEmail();

        $toEmail = $email;
        $templateName = 'far-email';
        $params = [
            'subject' => $subject,
            'message' => $message,
        ];

        $htmlBody = $this->templating->render(
            "AppBundle:CommunicationTemplate:emails/".$templateName.".html.twig", $params
        );
        $textBody = $this->templating->render(
            "AppBundle:CommunicationTemplate:emails/".$templateName.".text.twig", $params
        );

        $swiftMessage = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->fromEmail, 'Wealth College Pro Mailer')
            ->setTo($toEmail, $financialAdviser->getUser()->getName());
        /* @var $swiftMessage \Swift_Message */
        $swiftMessage->setBody($htmlBody, 'text/html');
        $swiftMessage->addPart($textBody, 'text/plain');


        if (!$this->mailer->send($swiftMessage)) {
            $this->getLogger()->warning(
                'Failed to send email to financial adviser '.$financialAdviser->getEmail()
            );
            return false;
        }

        // create communication
        $communication = new Communication();
        $communication->setTypeId(Communication::TYPE_EMAIL);
        $communication->setToEmail($toEmail);
        $communication->setToName('Wealth College Pro');
        $communication->setSubject($swiftMessage->getSubject());
        $communication->setBody($swiftMessage->getBody());
        $communication->setStatusId(Communication::STATUS_SENT);
        $communication->setSentAt(new \DateTime());

        $this->em->persist($communication);
        $this->em->flush($communication);

        return $communication;
    }
}