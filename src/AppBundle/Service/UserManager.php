<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 26/02/2015
 * Time: 19:09
 */

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository;

class UserManager extends BaseManager {


    /**
     * Get the query to get Users
     *
     * @param Array     $filter
     * @return \Doctrine\ORM\Query
     */
    public function getQuery($filter) {

        $qb = $this->getRepo()->createQueryBuilder('u');

        if (!empty($filter['search'])) {
            $search = $filter['search'];
            $qb->orWhere('u.firstName LIKE :search');
            $qb->orWhere('u.lastName LIKE :search');
            $qb->orWhere('u.email LIKE :search');
            $qb->setParameter('search', '%' . $search . '%');
            $qb->setFirstResult(0);
        }

        if (!empty($filter['role'])) {
            $qb->andWhere($qb->expr()->like("u.roles", ":role"))->setParameter('role', '%"'.$filter['role'].'"%');
        }
        $qb->andWhere($qb->expr()->notLike("u.roles", ":role"))->setParameter('role', '%"ROLE_DEVELOPER"%');

        return $qb->getQuery();
    }

}