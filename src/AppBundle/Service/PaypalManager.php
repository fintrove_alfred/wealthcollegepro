<?php
/**
 * AppBundle\Service\PaypalManager.php
 *
 * @author: Gul  
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\AuditLog;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderLine;
use AppBundle\Traits\ErrorRetrieval;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Routing\Router;


class PaypalManager {

    use ErrorRetrieval;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var AuditLogManager
     */
    protected $auditLogManager;

    protected $paypalUsername;
    protected $paypalClientId;
    protected $paypalSecret;

    protected $apiContext = null;

    protected $returnURL = '';
    protected $cancelURL = '';

    protected $paypalLogDir;
    protected $mode;

    /**
     * PaypalManager constructor.
     * @param EntityManager $em
     * @param Logger $logger
     * @param Router $router
     * @param AuditLogManager $auditLogManager
     * @param $paypalClientId
     * @param $paypalSecret
     * @param $paypalLogDir
     */
    public function __construct(EntityManager $em, Logger $logger, Router $router, AuditLogManager $auditLogManager, $paypalClientId, $paypalSecret, $paypalLogDir, $mode) {
        $this->em = $em;
        $this->logger = $logger;
        $this->router = $router;
        $this->auditLogManager = $auditLogManager;

        $this->paypalClientId = $paypalClientId;
        $this->paypalSecret = $paypalSecret;

        $this->paypalLogDir = $paypalLogDir;

        $this->returnURL = $this->router->generate('payment_paypal_return', [], Router::ABSOLUTE_URL);
        $this->cancelURL = $this->router->generate('payment_cancel', [], Router::ABSOLUTE_URL);
        $this->mode = $mode;
    }

    /**
     * Get the API Context
     * @return ApiContext
     */
    public function getApiContext() {

        if ($this->apiContext === null) {
            $this->apiContext = new ApiContext(new OAuthTokenCredential($this->paypalClientId, $this->paypalSecret));
            $config = [
               'log.LogEnabled' => true,
               'log.FileName' => $this->paypalLogDir,
               'log.LogLevel' => 'INFO',
               'mode' => $this->mode,
            ];
            if ($this->mode == 'live') {
                $config['mode'] = 'live';
            }
            $this->apiContext->setConfig($config);
        }
        return $this->apiContext;

    }

    /**
     * Get the return url for paypal
     * @return string
     */
    public function getReturnURL() {
        return $this->returnURL;
    }
    /**
     * Get the cancel url for paypal
     * @return string
     */
    public function getCancelURL() {
        return $this->cancelURL;
    }

    /**
     * Create the payment from an order for Paypal
     *
     * @param Order $order
     * @return bool|Payment false on failure
     */
    public function createPaymentFromOrder(Order $order) {

        $this->clearErrors();
        $apiContext = $this->getApiContext();

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $itemList = new ItemList();
        $description = '';
        foreach ($order->getOrderLines() as $orderLine) { /* @var $orderLine OrderLine */
            $item = new Item();
            $item->setName($orderLine->getProduct()->getTitle())
                ->setCurrency($order->getCurrency()->getCurrencyCode())
                ->setQuantity($orderLine->getQuantity())
                ->setSku($orderLine->getProductCode()) // Similar to `item_number` in Classic API
                ->setPrice($orderLine->getUnitPriceExcTax())
                ->setTax($orderLine->getTax());
            $description .= $orderLine->getProduct()->getTitle(). " x".$orderLine->getQuantity()." \n";
            $itemList->addItem($item);
        }

        $details = new Details();
        $details->setShipping(0)
            ->setTax($order->getVatAmount())
            ->setSubtotal($order->getSubTotal());

        $amount = new Amount();
        $amount->setCurrency($order->getCurrency()->getCurrencyCode())
            ->setTotal($order->getTotal())
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($description)
            ->setInvoiceNumber($order->getId());


        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($this->getReturnURL())
            ->setCancelUrl($this->getCancelURL());

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));


        try {
            $payment->create($apiContext);

            $orderPayment = $order->getPayments()->first(); /* @var $orderPayment \AppBundle\Entity\Payment */
            $orderPayment->setReferenceNumber($payment->getId());

            $this->em->persist($orderPayment);
            $this->em->flush($orderPayment);

            $this->auditLogManager->logAction($orderPayment, $orderPayment->getUser(), AuditLog::ACTION_CREATE, $payment->toArray());

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            $this->addError("Sorry there was a problem processing your request");
            $this->logger->log(Logger::CRITICAL, $ex->getMessage());
            $this->logger->log(Logger::CRITICAL, $ex->getTraceAsString());

            $orderPayment = $order->getPayments()->first(); /* @var $orderPayment \AppBundle\Entity\Payment */
            $orderPayment->setData(json_encode($ex->getData()));

            $this->auditLogManager->logAction($orderPayment, $orderPayment->getUser(), AuditLog::ACTION_CREATE, $payment->toArray());

            $this->em->persist($orderPayment);
            $this->em->flush($orderPayment);

            return false;
        }

        return $payment;
    }

    /**
     * Take the payment
     *
     * @param \AppBundle\Entity\Payment $payment
     * @param $payerID
     * @return bool     was payment successful?
     */
    public function executePayment(\AppBundle\Entity\Payment $payment, $payerID) {

        $this->clearErrors();

        $apiContext = $this->getApiContext();
        $paypalPayment = Payment::get($payment->getReferenceNumber(), $apiContext);

        $paymentExecution = new PaymentExecution();
        try {
            $paymentExecution->setPayerId($payerID);
            $paypalPayment->execute($paymentExecution, $apiContext);

            $this->auditLogManager->logAction($payment, $payment->getUser(), AuditLog::ACTION_UPDATE, $paypalPayment->toArray());

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            $this->auditLogManager->logAction($payment, $payment->getUser(), AuditLog::ACTION_UPDATE, $ex->getData());

            $this->logger->log(Logger::CRITICAL, $ex->getMessage());
            $this->logger->log(Logger::CRITICAL, $ex->getTraceAsString());

            return false;
        }

        if ($paypalPayment->getState() != 'approved') {
            $this->addError("Sorry we were unable to take payment successfully using paypal. Please try again or contact support quoting payment ID: ".$payment->getId());
            $payment->setFailureReason($paypalPayment->getFailureReason());
            $payment->setStatus(\AppBundle\Entity\Payment::STATUS_FAILED);
            $payment->setData(json_encode($paypalPayment->toArray()));
            
            $this->em->persist($payment);
            $this->em->flush($payment);
            return false;
        }

        if ($paypalPayment->getTransactions() && isset($paypalPayment->getTransactions()[0])) {
            $transaction = $paypalPayment->getTransactions()[0];

            $currencyCode = $transaction->getAmount()->getCurrency();
            $currency = $this->em->getRepository('AppBundle:Currency')->findOneByCurrencyCode($currencyCode);
            if (!$currency) {
                $this->logger->crit("Currency Not found in Wealth College Pro DB: $currencyCode");
            } else {
                $payment->setCurrency($currency);
            }
        } else {
            $this->logger->crit("Paypal Payment first transaction not found, could not set payment currency!");
        }

        $payment->setStatus(\AppBundle\Entity\Payment::STATUS_COMPLETE);
        $this->em->persist($payment);
        $this->em->flush($payment);

        return true;
    }



}