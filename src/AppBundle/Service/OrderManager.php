<?php
/**
 * AppBundle\Service\OrderManager.php
 *
 * @author: Gul  
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderLine;
use AppBundle\Entity\Payment;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class OrderManager {

    const VAT_RATE = 7;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(EntityManager $em, Logger $logger) {
        $this->em = $em;
        $this->logger = $logger;

    }

    /**
     * Create an order
     *
     * @param User $user
     * @param Product $product
     * @param $quantity
     * @return Order
     */
    public function createSimpleOrder(User $user, Product $product, $quantity) {

        $order = new Order();
        $orderLine = new OrderLine();

        $order->setUser($user);
        $order->setStatusId(Order::STATUS_PENDING);
        $order->setCurrency($product->getCurrency());
        $order->setVatRate(0);
        $order->setTotal($product->getPrice() * (1 + ($order->getVatRate()/100)) * $quantity);

        $orderLine->setProduct($product);
        $orderLine->setProductCode($product->getProductCode());
        $orderLine->setQuantity($quantity);
        $orderLine->setTypeId(OrderLine::TYPE_PRODUCT);
        $orderLine->setUnitPrice($product->getPrice() * (1 + ($order->getVatRate()/100)));
        $orderLine->setOrder($order);
        $order->addOrderLine($orderLine);

        $payment = new Payment();
        $payment->setOrder($order);
        $payment->setAmount($order->getTotal());
        $payment->setCurrency($product->getCurrency());
        $payment->setStatus(Payment::STATUS_PENDING);
        $payment->setUser($user);

        $order->addPayment($payment);
        $user->addPayment($payment);

        $this->em->persist($order);
        $this->em->persist($orderLine);
        $this->em->persist($payment);
        $this->em->flush($order);
        $this->em->flush($orderLine);
        $this->em->flush($payment);

        return $order;
    }

    /**
     * Fulfil an order and mark it as completed
     *
     * @param Order $order
     */
    public function fulfilOrder(Order $order) {

        if ($order->getStatusId() == Order::STATUS_COMPLETE) {
            $this->logger->log(Logger::WARNING, 'Order '.$order->getReferenceNumber().' is already completed');
            return true;
        }
        $order->setStatusId(Order::STATUS_COMPLETE);
        $this->em->persist($order);
        $this->em->flush($order);

        foreach ($order->getOrderLines() as $orderLine) { /* @var $orderLine OrderLine */
            if ($orderLine->getProduct()->getClass()) {
                $productMethod = 'purchased'.$orderLine->getProduct()->getClass();
                if (method_exists($this, $productMethod)) {
                    $this->logger->info("Executing purchase method $productMethod");
                    $this->$productMethod($orderLine);
                } else {
                    $this->logger->crit("Purchase method not found! #$productMethod# So the order could not be fulfilled");
                }
            }
        }

    }

    /**
     * Cancel an order because customer cancelled
     * @param Order $order
     */
    public function cancelOrder(Order $order) {

        $order->setStatusId(Order::STATUS_CANCELLED);
        $this->em->persist($order);
        $this->em->flush($order);

    }

    /**
     * Fail an order because payment failed
     * @param Order $order
     */
    public function failOrder(Order $order) {

        $order->setStatusId(Order::STATUS_FAILED);
        $this->em->persist($order);
        $this->em->flush($order);

    }


    /**
     * Purchase a subscription for one year and add to the FA's subscription period
     * @param OrderLine $orderLine
     */
    public function purchasedSubscription1Year(OrderLine $orderLine) {

        $user = $orderLine->getOrder()->getUser();
        $financialAdviser = $user->getFinancialAdviser();

        $subEndsat = $financialAdviser->getSubscriptionEndsAt();
        if (!$subEndsat) {
            $subEndsat = new \DateTime();
            $this->logger->info("No subscription currently set");
        } elseif ($subEndsat->getTimestamp() < time()) {
            $subEndsat = new \DateTime();
            $this->logger->info("Subscription has expired");
        } else {
            $this->logger->info("Subscription ends at ". $subEndsat->format('d/m/Y'));
        }
        $subEndsat->modify('+1 year');
        $this->logger->info("New Subscription end date is ". $subEndsat->format('d/m/Y'));
        $financialAdviser->setSubscriptionEndsAt($subEndsat);

        $this->em->persist($financialAdviser);
        $this->em->flush($financialAdviser);
    }

    /**
     * Get the query to get Orders for pagination
     *
     * @param User  $user
     * @param Array $filter
     * @return \Doctrine\ORM\Query
     */
    public function getQuery(User $user, $filter = []) {

        $qb = $this->em->getRepository('AppBundle:Order')->createQueryBuilder('o');

        $qb->andWhere($qb->expr()->eq('o.user', $user->getId()));

        if (!empty($filter['statusId'])) {
            $qb->andWhere($qb->expr()->eq('o.statusId', $filter['statusId']));
        }

        $qb->orderBy('o.created', 'desc');
        return $qb->getQuery();
    }
}