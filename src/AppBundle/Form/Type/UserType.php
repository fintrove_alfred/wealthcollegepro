<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{

    protected $roleNames;

    /**
     * @param $roleNames
     */
    public function __construct()
    {

    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->setMethod('POST')
            ->add('firstName', TextType::class, array(
                'label' => false,
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'placeholder' => 'First Name'
                )
            ))
            ->add('lastName', TextType::class, array(
                'label' => false,
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'placeholder' => 'Last Name'
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => false,
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'icon' => 'mail',
                    'placeholder' => 'Email'
                )
            ))
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => $options['allow_extra_fields']['roles'],
                'attr' => ['class' => 'ui checkbox']
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array(
                    'label' => false,
                    'attr' => array(
                        'icon' => 'lock',
                        'placeholder' => 'Password'
                    )
                ),
                'second_options' => array(
                    'label' => false,
                    'attr' => array(
                        'icon' => 'lock',
                        'placeholder' => 'Confirm Password'
                    )
                ),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('enabled', CheckboxType::class, [
                'value' => 1,
                'label' => 'Account Enabled',
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save',
                'attr' => ['class' => 'ui button']
            ]);


    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => $this->getName(),
        ));
    }

    public function getName()
    {
        return 'user';
    }
}
