<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Address extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add('address1', TextType::class, [
                'label' => 'Address Line 1',
                'attr' => ['class' => 'address-1 address-field']
            ])
            ->add('address2', TextType::class, [
                'label' => 'Address Line 2',
                'attr' => ['class' => 'address-2 address-field'],
                'required' => false,
            ])
            ->add('address3', TextType::class, [
                'attr' => ['class' => 'address-3 address-field'],
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'attr' => ['class' => 'city address-field']
            ])
            ->add('country', EntityType::class, [
                'class' => 'AppBundle:Country',
                'choice_label' => 'name',
                'placeholder' => 'Select country',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.order', 'ASC')
                        ->addOrderBy('c.name', 'ASC');
                },
            ])
            ->add('postCode', TextType::class, [
                'attr' => ['class' => 'postcode address-field']
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
           'data_class' => 'AppBundle\Entity\Address',
           'csrf_protection' => true,
        ));
    }


    public function getName()
    {
        return 'address';
    }
}
