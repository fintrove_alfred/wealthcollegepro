<?php
/**
 * Person.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\FinancialAdviserRegistration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Person extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $maritalStatusOptions = \AppBundle\Entity\Person::getMaritalStatusOptions();

        $builder
            ->add('title', ChoiceType::class, array(
                'label' => '',
                'required' => false,
                'choices' => [
                    'Mr' => 'Mr',
                    'Mrs' => 'Mrs',
                    'Miss' => 'Miss',
                    'Ms' => 'Ms',
                    'Dr' => 'Dr'
                ]
            ))
            ->add('firstName', TextType::class, array(
                'required' => true
            ))
            ->add('lastName', TextType::class, array(
                'required' => false
            ))
            ->add('gender', ChoiceType::class, array(
                'label' => false,
                'required' => false,
                'placeholder' => false,
                'choices' => [
                    'Male' => 'M',
                    'Female' => 'F',
                ],
                'expanded' => true,
                'multiple' => false
            ))
            ->add('maritalStatusId', ChoiceType::class, array(
                'label' => false,
                'required' => false,
                'choices' => array_flip($maritalStatusOptions)
            ))
            ->add('businessName', TextType::class, array(
                'label' => false,
                'required' => false
            ))
            ->add('email', EmailType::class, array(
                'label' => false,
                'required' => false
            ))
            ->add('telephone', TextType::class, array(
                'label' => 'Home',
                'required' => false
            ))
            ->add('telephoneWork', TextType::class, array(
                'label' => 'Work',
                'required' => false
            ))
            ->add('mobile', TextType::class, array(
                'label' => 'Mobile',
                'required' => false
            ))
            ->add('dob', DateType::class, array(
                'label' => 'Date of birth',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false
            ))
            ->add('nricPassport', TextType::class, array(
                'label' => 'NRIC/Passport',
                'required' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Person',
            'csrf_protection' => true,
        ));
    }


    public function getName()
    {
        return 'person';
    }

}