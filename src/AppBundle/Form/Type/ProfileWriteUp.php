<?php
/**
 * ProfileWriteUp.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileWriteUp extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder->add('profileContent', TextareaType::class, array(
         'label' => false,
         'attr' => array(
         )
      ));

   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\FinancialAdviser',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'profile_write_up';
   }

}