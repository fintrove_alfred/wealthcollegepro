<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Transformer\DateTimeSplitTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class DateTimeFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'date', [
                'widget' => 'single_text',
            ])
            ->add('time', 'time', [
                'widget' => 'single_text',
            ])
            ->addViewTransformer(new DateTimeSplitTransformer())
        ;
    }

    public function getName()
    {
        return 'date_time';
    }
}
