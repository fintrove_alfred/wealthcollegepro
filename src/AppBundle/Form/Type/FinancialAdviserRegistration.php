<?php
/**
 * FinancialAdviserRegistration.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FinancialAdviserRegistration extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('businessName', TextType::class, [])
            ->add('licenceNo', TextType::class, [
                'required' => true,
                'label' => 'MAS Representative Number (This number will be verified against MAS Register of Representatives)'
            ])
            ->add('address', Address::class, [
                'data_class' => 'AppBundle\Entity\Address',
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FinancialAdviser',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'financial_adviser_registration';
    }

}