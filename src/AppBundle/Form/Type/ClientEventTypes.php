<?php
/**
 * ClientEvent.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\FinancialAdviserRegistration;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientEventTypes extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
      ->add('clientEventTypes', CollectionType::class, [
         'entry_type'   => ClientEventType::class,
         'entry_options'  => array(
            'required'  => true,
         ),
         'allow_add' => true,
         'allow_delete' => true,
         'by_reference' => false
      ]);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => \AppBundle\Entity\FinancialAdviser::class,
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_event_types';
   }

}