<?php
/**
 * ClientProfile.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientProfile extends AbstractType
{

    /**
     * @var User
     */
    protected $user;

    protected $em;

    /**
     * Client constructor.
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage, EntityManager $em)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // get client client groups for this client
        $cgQB = $this->em->getRepository('AppBundle:ClientGroup')->createQueryBuilder('cg');
        $cgQB->where('cg.financialAdviser = :financialAdviser')->setParameter('financialAdviser', $this->user->getFinancialAdviser()->getId());

        $builder->add('person', Person::class);
        $builder->add('typeId', ChoiceType::class, [
            'label' => 'Type',
            'choices' => [
                'Other' => '',
                'Prospect' => \AppBundle\Entity\Client::TYPE_PROSPECT,
                'Client' => \AppBundle\Entity\Client::TYPE_CLIENT,
            ],
            'multiple' => false,
            'expanded' => true,
            'required' => true
        ]);
        $builder->add('address', Address::class)
            ->add('website1', TextType::class, [
                'label' => 'Website',
                'required' => false,
            ])
            ->add('website2', TextType::class, [
                'label' => 'Website',
                'required' => false,
            ])
            ->add('website3', TextType::class, [
                'label' => 'Website',
                'required' => false,
            ])
            ->add('occupation', TextType::class, [
                'required' => false,
            ])
            ->add('jobTitle', TextType::class, [
                'required' => false,
            ])
            ->add('religion', TextType::class, [
                'required' => false,
            ])
            ->add('race', TextType::class, [
                'required' => false,
            ])
            ->add('nationalityCountry', EntityType::class, [
                'class' => \AppBundle\Entity\Country::class,
                'attr' => ['data-init-plugin' => 'select2', 'class' => "full-width"],
                'placeholder' => 'Select Nationality',
            ])
            ->add('facebook', TextType::class, [
                'required' => false,
            ])
            ->add('twitter', TextType::class, [
                'required' => false,
            ])
            ->add('linkedin', TextType::class, [
                'required' => false,
            ])
            ->add('notes', TextareaType::class, [
                'required' => false,
            ])
            ->add('companyAddress', Address::class)
            ->add('referredBy', EntityType::class, [
                'class' => \AppBundle\Entity\Client::class,
                'attr' => ['data-init-plugin' => 'select2', 'class' => "full-width"],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.financialAdviser = :financialAdviser')
                        ->andWhere('c.deletedAt IS NULL')
                        ->setParameter('financialAdviser', $this->user->getFinancialAdviser()->getId());
                },
            ])
            ->add('clientRelations', CollectionType::class, [
                'entry_type' => ClientRelation::class,
                'entry_options' => array(
                    'required' => true,
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('clientDates', CollectionType::class, [
                'entry_type' => ClientDate::class,
                'entry_options' => array(
                    'required' => true,
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('clientGroups', EntityType::class, [
                'class' => \AppBundle\Entity\ClientGroup::class,
                'query_builder' => $cgQB,
                'multiple' => true,
                'required' => false,
                'attr' => ['data-init-plugin' => 'select2', 'class' => "full-width"],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Client',
            'csrf_protection' => true,
        ));
    }


    public function getName()
    {
        return 'client';
    }

}