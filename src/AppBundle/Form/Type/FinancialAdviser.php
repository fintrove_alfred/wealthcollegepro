<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FinancialAdviser extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
           ->add(
              $builder->create('financialAdviser', FormType::class, [
                 'by_reference' => true,
                 'data_class' => \AppBundle\Entity\FinancialAdviser::class,
              ])
                 ->add('subscriptionEndsAt', DateTimeType::class, [
                    'label' => 'Subscription End Date',
                    'html5' => false,
                    'widget' => 'single_text',
                    'model_timezone' => 'UTC',
                 ])
                 ->add('score', TextType::class, [
                    'label' => 'Score'
                 ])
           );
    }


    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => $this->getName(),
        ));
    }

    public function getName()
    {
        return 'fa';
    }
}
