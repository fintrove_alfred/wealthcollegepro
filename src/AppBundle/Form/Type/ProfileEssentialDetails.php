<?php
/**
 * ProfileEssentialDetails.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\FinancialAdviserRegistration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileEssentialDetails extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
         ->add('gender', ChoiceType::class, array(
            'label' => 'Gender',
            'required' => false,
            'placeholder' => false,
            'choices' => [
               'Male' => 'M',
               'Female' => 'F',
            ],
            'expanded' => true,
            'multiple' => false
         ))
         ->add(
            $builder->create('financialAdviser', FormType::class, [
               'by_reference' => true,
               'data_class' => \AppBundle\Entity\FinancialAdviser::class,
            ])
            ->add('nricNumber', TextType::class, [
               'required' => false
            ])
            ->add('race', TextType::class, [
               'required' => false
            ])
            ->add('nationality', TextType::class, [
               'required' => false
            ])
            ->add('religion', TextType::class, [
               'required' => false
            ])
           ->add('languages', ChoiceType::class, [
              'label' => 'Languages',
              'choices' => [
                 'English' => 'English',
                 'Mandarin' => 'Mandarin',
                 'Malay' => 'Malay',
                 'Tamil' => 'Tamil',
              ],
              'required' => false,
              'expanded' => true,
              'multiple' => true,
           ])
           ->add('languagesOthers', TextareaType::class, [
              'required' => false,
               'label' => 'Other Languages'
           ])
            ->add('yearJoinedIndustry', DateType::class, [
               'widget' => 'single_text',
               'html5' => false,
               'required' => false,

            ])
            ->add('parentCompanyName', TextType::class, [
               'required' => false
            ])
            ->add('companyAddress', Address::class)
            ->add('professionalContactNumber', TextType::class, [
               'label' => 'Contact Number (Mobile)',
               'required' => false
            ])
            ->add('professionalEmailAddress', TextType::class, [
               'label' => 'Email Address',
               'required' => false
            ])
            ->add('academicQualification', ChoiceType::class, [
               'label' => 'Highest Academic Qualification',
               'choices' => [
                  'O-level' => 'O-level',
                  'A-level' => 'A-level',
                  'International Baccalaureate' => 'International Baccalaureate',
                  'Diploma' => 'Diploma',
                  'Graduate Diploma' => 'Graduate Diploma',
                  'Undergraduate Degree' => 'Undergraduate Degree',
                  'Post-Graduate Degree' => 'Post-Graduate Degree',
                  'Others (please state)' => 'Others (please state)'
               ],
               'required' => false,
               'expanded' => false,
               'multiple' => false,
            ])
           ->add('typeOfQualification', TextType::class, [
              'required' => false
           ])
           ->add('institution', TextType::class, [
              'required' => false
           ])
            ->add('academicQualificationOther', TextType::class, [
               'label' => 'Other Academic Qualifications',
               'required' => false
            ])
            ->add('professionalCertifications', ChoiceType::class, [
               'label' => 'Professional Qualifications',
               'choices' => [
                  'AFC' => 'AFC',
                  'AFP' => 'AFP',
                  'AWP' => 'AWP',
                  'ChFC' => 'ChFC',
                  'CFP' => 'CFP',
                  'FChFP' => 'FChFP',
                  'ASEP' => 'ASEP',
                  'CSEP' => 'CSEP',
                  'AFCouns' => 'AFCouns',
                  'ASFE' => 'ASFE',
                  'CSFE' => 'CSFE',
                  'Others (please state)' => 'Others (please state)'
               ],
               'required' => false,
               'expanded' => true,
               'multiple' => true,
            ])
            ->add('professionalCertificationsOthers', TextType::class, [
               'label' => 'Other Professional Qualifications',
               'required' => false
            ])
            ->add('professionalMemberships', ChoiceType::class, [
               'label' => 'Professional Memberships',
               'choices' => [
                  'IFPAS' => 'IFPAS',
                  'FPAS' => 'FPAS',
                  'SFSP' => 'SFSP',
                  'FSMA' => 'FSMA',
                  'AFA(S)' => 'AFA(S)'
               ],
               'required' => false,
               'expanded' => true,
               'multiple' => true,
            ])
            ->add('professionalMembershipsOthers', TextType::class, [
               'label' => 'Other Professional Memberships',
               'required' => false
            ])
           ->add('areasOfCompetencies', ChoiceType::class, [
              'label' => 'Areas of Practice',
              'choices' => [
                 'Financial Counselling' => 'Financial Counselling',
                 'Cash Flow Management' => 'Cash Flow Management',
                 'General Insurance Planning' => 'General Insurance Planning',
                 'Life Insurance Planning' => 'Life Insurance Planning',
                 'Investment Planning' => 'Investment Planning',
                 'Retirement Planning' => 'Retirement Planning',
                 'Tax Planning' => 'Tax Planning',
                 'Gift Planning' => 'Gift Planning',
                 'Estate Planning' => 'Estate Planning',
                 'Corporate General Insurance Planning' => 'Corporate General Insurance Planning',
                 'Employee Benefits Planning' => 'Employee Benefits Planning',
                 'Business Protection Planning' => 'Business Protection Planning',
                 'Business Succession Planning' => 'Business Succession Planning'
              ],
              'required' => false,
              'expanded' => true,
              'multiple' => true,
           ])
           ->add('areasOfCompetenciesOthers', TextareaType::class, [
              'label' => 'Other Areas of Expertise',
              'required' => false
           ])
            ->add('industryAwards', TextareaType::class, [
               'label' => 'Industry Awards',
               'required' => false
            ])
            ->add('communityInvolvement', TextareaType::class, [
               'label' => 'Community Involvement',
               'required' => false
            ])

            ->add('licenceNo', TextType::class, [
               'required' => true,
               'label' => 'MAS Representative Number (This number will be verified against MAS Register of Representatives)'
            ])
            ->add('balanceScoreCard', ChoiceType::class, [
                'choices' => [
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                    'Nill' => 'Nill',
                ],
                'required' => false,
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('bpRate', TextType::class, [
                'label' => 'Business Persistency Rate (average %)',
                'required' => false
            ])
            ->add('specialInterest', TextareaType::class, [
               'label' => 'Expertise',
               'attr' => [
               ],
               'required' => false
            ])
         );
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\User',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'profile_essential_details';
   }

}