<?php
/**
 * Review.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ReviewTemplate;
use AppBundle\Service\ReviewManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The form for a review
 *
 * Class Review
 * @package AppBundle\Form\Type
 */
class Review extends AbstractType
{


   public function buildForm(FormBuilderInterface $builder, array $options)
   {


      $builder->add(
         $builder->create('client', FormType::class, [
            'by_reference' => true,
            'data_class' => \AppBundle\Entity\Client::class,
         ])
         ->add(
            $builder->create('person', FormType::class, [
               'by_reference' => true,
               'data_class' => \AppBundle\Entity\Person::class,
            ])
            ->add('businessName', TextType::class, [
               'required' => false
            ])
            ->add('firstName', TextType::class, [
               'required' => true
            ])
            ->add('lastName', TextType::class, [
               'required' => true
            ])
            ->add('title', TextType::class, [
               'required' => false
            ])
         )
      )
      ->add('title', TextType::class, [
         'required' => true,
         'attr' => [
            'placeholder' => 'example: This adviser is just great'
         ]
      ])
      ->add('feedback', TextareaType::class, [
         'required' => true
      ])
      ->add('comment', TextareaType::class, [
         'required' => true
      ])
      ->add('negativeFeedback', TextareaType::class, [
         'required' => false
      ])
      ->add('rating', HiddenType::class, [
         'required' => true
      ])
      ->add('verifiable', ChoiceType::class, [
         'label' => 'In the event that Wealth College Pro would like to audit the source of this review, would you agree for us to email to you to verify this?',
         'required' => true,
         'choices' => [
            'Yes' => 1,
            'No' => 0,
         ],
         'data' => null,
         'multiple' => false,
         'expanded' => true
      ])
     ->add('attributes', EntityType::class, [
         'class' => \AppBundle\Entity\ReviewAttribute::class,
         'label' => false,
         'query_builder' => function (EntityRepository $er) {
             return $er->createQueryBuilder('ra')->andWhere('ra.archived = 0');
         },
         'multiple' => true,
         'expanded' => true
     ])
      ->add('reviewAnswers', CollectionType::class, [
         'entry_type'   => ReviewAnswer::class,
         'entry_options'  => array(
            'required'  => true,
         ),
         'allow_add' => true,
      ]);

   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\Review',
         'csrf_protection' => true,
      ));
   }

   public function getName()
   {
      return 'review';
   }

}