<?php
/*** ClientDate.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\FinancialAdviserRegistration;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientDate extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dateTypes = \AppBundle\Entity\ClientDate::getDateTypes();

        $builder->add('client', HiddenType::class);
        $builder->add('dateTypeId', ChoiceType::class, [
            'label' => 'Date Type',
            'placeholder' => 'Select Date',
            'choices' => array_flip($dateTypes),
            'attr' => ['class' => 'form-control date-type-id']
        ])
            ->add('dateTypeName', TextType::class, [
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('date', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'form-control date-mask']
            ])
            ->add('reminder', CheckboxType::class, [
                'required' => false,
                'attr' => ['class' => 'date-reminder']
            ])
            ->add('reminderDate', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'form-control date-mask']
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ClientDate',
            'csrf_protection' => true,
        ));
    }


    public function getName()
    {
        return 'client_date';
    }

}