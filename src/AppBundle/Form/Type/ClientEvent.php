<?php
/**
 * ClientEvent.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\FinancialAdviserRegistration;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientEvent extends AbstractType
{

   /**
    * @var User
    */
   protected $user;

   /**
    * Client constructor.
    * @param TokenStorage $tokenStorage
    */
   public function __construct(TokenStorage $tokenStorage)
   {
      $this->user = $tokenStorage->getToken()->getUser();
   }


   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder->add('description', TextareaType::class, [
         'required' => true
      ])
     ->add('date', DateType::class, [
        'required' => false,
        'widget' => 'single_text',
        'attr' => ['class' => 'form-control']
     ])
      ->add('clientEventType', EntityType::class, [
         'class' => \AppBundle\Entity\ClientEventType::class,
         'placeholder' => 'Select Event',
         'label' => false,
         'attr' => ['data-init-plugin' => 'select2', 'class' => "full-width"],
         'query_builder' => function (EntityRepository $er) {
            return $er->createQueryBuilder('cet')
               ->where('cet.financialAdviser = :financialAdviser OR cet.financialAdviser IS NULL')->setParameter('financialAdviser', $this->user->getFinancialAdviser()->getId());
         },
      ]);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => \AppBundle\Entity\ClientEvent::class,
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_event';
   }

}