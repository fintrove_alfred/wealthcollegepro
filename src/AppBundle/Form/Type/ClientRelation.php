<?php
/**
 * ClientRelation.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Form\Type\FinancialAdviserRegistration;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientRelation extends AbstractType
{

   /**
    * @var User
    */
   protected $user;

   /**
    * Client constructor.
    * @param TokenStorage $tokenStorage
    */
   public function __construct(TokenStorage $tokenStorage)
   {
      $this->user = $tokenStorage->getToken()->getUser();
   }


   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $relationshipTypes = \AppBundle\Entity\ClientRelation::getRelationshipTypes();

      $builder->add('client', HiddenType::class);
      $builder->add('relatedClient', EntityType::class, [
         'class' => \AppBundle\Entity\Client::class,
         'placeholder' => 'Select Contact',
         'label' => false,
         'attr' => ['data-init-plugin' => 'select2', 'class' => "full-width"],
         'query_builder' => function (EntityRepository $er) {
            return $er->createQueryBuilder('c')
               ->where('c.financialAdviser = :financialAdviser')
                ->andWhere('c.deletedAt IS NULL')
                ->setParameter('financialAdviser', $this->user->getFinancialAdviser()->getId());
         },
      ]);
      $builder->add('relationshipTypeId', ChoiceType::class, [
         'label' => false,
         'placeholder' => 'Select Relationship Type',
         'choices' => array_flip($relationshipTypes),
         'attr' => ['data-init-plugin' => 'select2', 'class' => "full-width"],
      ]);

   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ClientRelation',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_relation';
   }

}