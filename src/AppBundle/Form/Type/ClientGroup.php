<?php
/**
 * ClientGroup.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\FinancialAdviserRegistration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientGroup extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder->add('name', TextType::class, [
         'required' => true
      ])
      ->add('financialAdviser', HiddenType::class);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => \AppBundle\Entity\ClientGroup::class,
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_group';
   }

}