<?php
/**
 * ClientWPMEstate.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ClientWPM;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientWPMEstate extends AbstractType
{


   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $builder
         ->add('documentType', TextType::class, ['required' => false, 'label' => 'Document Created', 'attr' => ['class' => 'form-control']])
         ->add('documentCreatedDate', DateType::class, ['label' => 'Date Created', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('country', EntityType::class, [
            'class' => 'AppBundle:Country',
            'choice_label' => 'name',
            'placeholder' => 'Country Created',
            'attr' => ['data-init-plugin' => "select2", 'class' => 'full-width'],
            'query_builder' => function (EntityRepository $er) {
               return $er->createQueryBuilder('c')
                  ->orderBy('c.order', 'ASC')
                  ->addOrderBy('c.name', 'ASC');
            },
            'required' => false
         ])
         ->add('location', TextType::class, ['required' => false, 'label' => 'Location Document is Kept', 'attr' => ['class' => 'form-control']])

         ->add('appointedRepresentatives', TextareaType::class, ['required' => false, 'label' => 'Appointed Representative(s)', 'attr' => ['class' => 'form-control']])
         ->add('beneficiaries', TextareaType::class, ['required' => false, 'label' => 'Beneficiaries', 'attr' => ['class' => 'form-control']])
         //->add('keyContacts', TextareaType::class, ['required' => false, 'label' => 'Beneficiaries', 'attr' => ['class' => 'form-control']])
         ->add('remarks', TextareaType::class, ['required' => false, 'label' => 'Remarks', 'attr' => ['class' => 'form-control']])

      ;
      
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ClientWPMEstate',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_wpm_estate';
   }

}