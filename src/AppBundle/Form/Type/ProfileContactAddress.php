<?php
/**
 * ProfileContactAddress.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\FinancialAdviserRegistration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileContactAddress extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
         ->add(
            $builder->create('financialAdviser', FormType::class, [
               'by_reference' => true,
               'data_class' => \AppBundle\Entity\FinancialAdviser::class,
            ])
            ->add('email', EmailType::class)
            ->add('telephone', TextType::class)
            ->add('address', Address::class)
         );
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\User',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'profile_contact_address';
   }

}