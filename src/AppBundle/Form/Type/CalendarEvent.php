<?php
/**
 * CalendarEvent.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarEvent extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $alertTypeOptions = \AppBundle\Entity\CalendarEvent::getAlertTypeOptions();


      $builder->add('calendarEventType', EntityType::class, [
         'class' => 'AppBundle:CalendarEventType',
      ])
      ->add('summary', TextType::class)
      ->add('description', TextType::class, [
         'required' => false
      ])
      ->add('location', TextType::class, [
         'required' => false
      ])
      ->add('startAt', DateTimeType::class, [
         'html5' => false,
         'widget' => 'single_text',
      ])
      ->add('endAt', DateTimeType::class, [
         'html5' => false,
         'widget' => 'single_text',
      ])
      ->add('alertTypeId', ChoiceType::class, [
         'choices' => array_flip($alertTypeOptions),
         'required' => true
      ])
      ->add('alertDays', TextType::class, [
         'required' => false
      ])
      ->add('alertTime', TimeType::class, [
         'required' => false,
         'widget' => 'single_text',
         'html5' => false,
      ])
      ->add('alertDateTime', DateTimeType::class, [
         'widget' => 'single_text',
         'html5' => false,
      ])
      ->add('agreedTerms', CheckboxType::class, array(
         'label' => 'I agree to the terms and conditions',
         'required' => true,
         'mapped' => false
      ));

   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\CalendarEvent',
         'csrf_protection' => false,
      ));
   }

   public function getName()
   {
      return 'calendar_event';
   }

}