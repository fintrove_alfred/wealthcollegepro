<?php
/**
 * ClientWPMInsurancePolicy.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ClientWPM;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientWPMInsurancePolicy extends AbstractType
{


   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $paymentModeOptions = ClientWPM::getPaymentModeOptions();
      $builder
         ->add('policyType', ChoiceType::class, [
            'label' => 'Type of Policy',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Life' => 'Life',
               'Personal Accident' => 'Personal Accident',
               'Critical Illness' => 'Critical Illness',
               'Hospitalisation' => 'Hospitalisation',
               'Others' => 'Others',
            ],
            'required' => false
         ])
         
         ->add('company', TextType::class, ['required' => false, 'label' => 'Company', 'attr' => ['class' => 'form-control']])
         ->add('policyNumber', TextType::class, ['required' => false, 'label' => 'Policy Number', 'attr' => ['class' => 'form-control']])
         ->add('status', ChoiceType::class, [
            'label' => 'Status',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'In-force' => 'In-force',
               'Lapsed' => 'Lapsed',
            ],
            'required' => false
         ])
         ->add('locationCountry', EntityType::class, [
            'class' => 'AppBundle:Country',
            'choice_label' => 'name',
            'placeholder' => 'Select Location',
            'attr' => ['data-init-plugin' => "select2", 'class' => 'full-width'],
            'query_builder' => function (EntityRepository $er) {
               return $er->createQueryBuilder('c')
                  ->orderBy('c.order', 'ASC')
                  ->addOrderBy('c.name', 'ASC');
            },
            'required' => false
         ])
         ->add('ownershipType', ChoiceType::class, [
            'label' => 'Ownership Type',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Personal' => 'Personal',
               'Joint' => 'Joint',
               'Others' => 'Others',
            ],
            'required' => false
         ])
         ->add('owner', TextType::class, ['required' => false, 'label' => 'Owner(s)', 'attr' => ['class' => 'form-control']])
         ->add('mainBenefit', ChoiceType::class, [
            'label' => 'Main Benedfit',
            'attr' => ['class' => 'checkbox'],
            'multiple' => true,
            'expanded' => true,
            'choices' => [
               'Whole' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_WHOLE,
               'Endownment' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_ENDOWMENT,
               'Personal Accident' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_PERSONAL_ACCIDENT,
               'Investment-linked' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_INVESTMENT_LINKED,
               'Critical Illness' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_CRITICAL_ILLNESS,
               'Hospitalisation' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_HOSPITALISATION,
               'Others' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_OTHER
            ],
            'required' => false
         ])
         ->add('sumAssured', TextType::class, ['required' => false, 'label' => 'Sum Assured (Death Benefit)', 'attr' => ['class' => 'number form-control']])
         ->add('startDate', DateType::class, ['label' => 'Start Date', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('endDate', DateType::class, ['label' => 'End Date (e.g. maturity)', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('premium', TextType::class, ['required' => false, 'label' => 'Premium Amount', 'attr' => ['class' => 'number form-control']])
         ->add('premiumMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('premiumDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('riderBenefit', ChoiceType::class, [
            'label' => 'Rider Benefit',
            'attr' => ['class' => 'checkbox'],
            'multiple' => true,
            'expanded' => true,
            'choices' => [
               'Disability' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_DISABILITY,
               'Personal Accident' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_PERSONAL_ACCIDENT,
               'Critical Illness' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_CRITICAL_ILLNESS,
               'Hospitalisation' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_HOSPITALISATION,
               'Others' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_OTHER
            ],
            'required' => false
         ])
         ->add('riderSumAssured', TextType::class, ['required' => false, 'label' => 'Sum Assured', 'attr' => ['class' => 'number form-control']])
         ->add('riderStartDate', DateType::class, ['label' => 'Start Date', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('riderEndDate', DateType::class, ['label' => 'End Date (e.g. maturity)', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('riderPremium', TextType::class, ['required' => false, 'label' => 'Premium Amount', 'attr' => ['class' => 'number form-control']])
         ->add('riderPremiumMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('riderPremiumDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('supBenefit', ChoiceType::class, [
            'label' => 'Rider Benefit',
            'attr' => ['class' => 'checkbox'],
            'multiple' => true,
            'expanded' => true,
            'choices' => [
               'Disability' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_DISABILITY,
               'Personal Accident' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_PERSONAL_ACCIDENT,
               'Critical Illness' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_CRITICAL_ILLNESS,
               'Hospitalisation' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_HOSPITALISATION,
               'Others' => \AppBundle\Entity\ClientWPMInsurancePolicy::BENEFIT_TYPE_OTHER
            ],
            'required' => false
         ])
         ->add('supSumAssured', TextType::class, ['required' => false, 'label' => 'Sum Assured', 'attr' => ['class' => 'number form-control']])
         ->add('supStartDate', DateType::class, ['label' => 'Start Date', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('supEndDate', DateType::class, ['label' => 'End Date (e.g. maturity)', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('supPremium', TextType::class, ['required' => false, 'label' => 'Premium Amount', 'attr' => ['class' => 'number form-control']])
         ->add('supPremiumMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('supPremiumDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('sourceFunds', ChoiceType::class, [
            'label' => 'Source of Funds',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Cash' => 'Cash',
               'Pension (CPF)' => 'Pension (CPF)',
               'Corporate benefits' => 'Corporate benefits',
               'Others' => 'Others'
            ],
            'required' => false
         ])

         ->add('grossCashSurrenderValue', TextType::class, ['required' => false, 'label' => 'Gross Cash Surrender Value', 'attr' => ['class' => 'number form-control']])
         ->add('policyLoan', TextType::class, ['required' => false, 'label' => 'Policy Loan (if any)', 'attr' => ['class' => 'number form-control']])
         ->add('netCashValue', TextType::class, ['required' => false, 'label' => 'Net Cash Value amount', 'attr' => ['class' => 'number form-control']])
         ->add('distribution', ChoiceType::class, [
            'label' => 'Distribution',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'General Law' => 'General Law',
               'Special Law' => 'Special Law',
            ],
            'required' => false
         ])
         ->add('specialLaw', TextType::class, ['required' => false, 'label' => 'If Special Law, please state', 'attr' => ['class' => 'form-control']])
         ->add('beneficiaries', TextareaType::class, ['required' => false, 'label' => 'Beneficiaries', 'attr' => ['class' => 'form-control']])
         ->add('remarks', TextareaType::class, ['required' => false, 'label' => 'Remarks', 'attr' => ['class' => 'form-control']])

      ;
      
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ClientWPMInsurancePolicy',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_wpm_insurance_policy';
   }

}