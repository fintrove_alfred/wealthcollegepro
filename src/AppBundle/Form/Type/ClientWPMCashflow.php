<?php
/**
 * ClientWPMCashflow.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ClientWPM;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientWPMCashflow extends AbstractType
{


   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $paymentModeOptions = ClientWPM::getPaymentModeOptions();
      $builder
         ->add('salary', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('salaryMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('salaryDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('allowances', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('allowancesMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('allowancesDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('commissions', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('commissionsMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('commissionsDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('bonuses', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('bonusesMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('bonusesDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('cashflowActiveOthers', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])

         ->add('coupons', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('couponsMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('couponsDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('dividends', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('dividendsMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('dividendsDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('rentals', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('rentalsMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('rentalsDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('cashflowPassiveOthers', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])

         ->add('home', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('homeMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('homeDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('transport', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('transportMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('transportDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('personal', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('personalMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('personalDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('food', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('foodMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('foodDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('medical', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('medicalMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('medicalDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('children', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('childrenMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('childrenDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('loans', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('loansMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('loansDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('cashflowFixedOthers', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])

         ->add('recreation', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('recreationMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('recreationDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('miscellaneous', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('miscellaneousMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('miscellaneousDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

         ->add('insurances', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('insurancesMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('insurancesDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('savings', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('savingsMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('savingsDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('gifts', TextType::class, ['required' => false, 'label' => 'Amount', 'attr' => ['class' => 'number form-control']])
         ->add('giftsMode', ChoiceType::class, ['label' => 'Payment Mode', 'choices' => array_flip($paymentModeOptions), 'required' => false])
         ->add('giftsDate', DateType::class, ['label' => 'Date Due', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])

      ;
      
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ClientWPMCashflow',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_wpm_cashflow';
   }

}