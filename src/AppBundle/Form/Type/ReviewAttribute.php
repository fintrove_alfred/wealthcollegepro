<?php
/**
 * ReviewAttribute.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Review;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The form for a FA review attribute (custom)
 *
 * Class ReviewAttribute
 * @package AppBundle\Form\Type
 */
class ReviewAttribute extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $typeOptions = [Review::TYPE_CUSTOM => 'Custom'];

      $builder
      ->add('name', TextType::class, [
         'required' => true
      ])
      ->add('score', IntegerType::class, [
         'required' => true
      ])
      ->add('archived', CheckboxType::class, [
          'required' => false
      ])
      ;


   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ReviewAttribute',
         'csrf_protection' => true,
      ));
   }

   public function getName()
   {
      return 'review_attribute';
   }

}