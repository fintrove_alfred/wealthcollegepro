<?php
/**
 * ReviewTemplateSystem.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Review;
use AppBundle\Entity\ReviewTemplate;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The form for a non-FA (system) review template
 *
 * Class ReviewTemplateSystem
 * @package AppBundle\Form\Type
 */
class ReviewTemplateSystem extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $typeOptions = Review::getTypeOptions();
      unset($typeOptions[Review::TYPE_CUSTOM]);
      unset($typeOptions[Review::TYPE_FEEDBACK]);

      $builder->add('typeId', ChoiceType::class, [
         'choices' => array_flip($typeOptions),
         'required' => true
      ])
      ->add('name', TextType::class, [
         'required' => true
      ])
      ->add('subject', TextType::class, [
         'required' => true
      ])
      ->add('message', TextareaType::class, [
         'required' => true
      ]);


   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ReviewTemplate',
         'csrf_protection' => true,
      ));
   }

   public function getName()
   {
      return 'review_template';
   }

}