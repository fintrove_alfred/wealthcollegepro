<?php
/**
 * WebinarProfile.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Webinar extends AbstractType
{

   /**
    * @var User
    */
   protected $user;

   /**
    * Client constructor.
    * @param TokenStorage $tokenStorage
    */
   public function __construct(TokenStorage $tokenStorage)
   {
      $this->user = $tokenStorage->getToken()->getUser();
   }


   public function buildForm(FormBuilderInterface $builder, array $options)
   {
   
      $alertTypeOptions = \AppBundle\Entity\CalendarEvent::getAlertTypeOptions();

      $timezone = 'Europe/London';
      if ($this->user && $this->user->getPreferences() && $this->user->getPreferences()->getTimezone()) {
         $timezone = $this->user->getPreferences()->getTimezone();
      }

      $builder->add(
         $builder->create('calendarEvent', FormType::class, [
            'by_reference' => true,
            'data_class' => \AppBundle\Entity\CalendarEvent::class,
         ])
            ->add('summary', TextType::class)
            ->add('description', TextareaType::class, [
               'required' => true
            ])
            ->add('location', TextType::class, [
               'required' => false
            ])
            ->add('startAt', DateTimeType::class, [
               'html5' => false,
               'widget' => 'single_text',
               'model_timezone' => 'UTC',
               'view_timezone' => $timezone
            ])
            ->add('endAt', DateTimeType::class, [
               'widget' => 'single_text',
            ])
            ->add('alertTypeId', ChoiceType::class, [
               'choices' => array_flip($alertTypeOptions),
               'required' => true
            ])
            ->add('alertDays', TextType::class, [
               'required' => false
            ])
            ->add('alertTime', TimeType::class, [
               'required' => false,
               'widget' => 'single_text',
               'html5' => false,
            ])
            ->add('alertDateTime', DateTimeType::class, [
               'widget' => 'single_text',
               'html5' => false,
            ])
            ->add('agreedTerms', CheckboxType::class, array(
               'label' => 'I agree to the terms and conditions',
               'required' => true,
               'mapped' => false
            ))
      )
         ->add('expectedRunningTime', TimeType::class, [
            'required' => true,
            'widget' => 'single_text',
            'html5' => false,
            'minutes' => range(0,55, 5)
         ]);

      
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\Webinar',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'webinar';
   }

}