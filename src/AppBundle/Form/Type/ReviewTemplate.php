<?php
/**
 * ReviewTemplate.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Review;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The form for a FA review template (custom)
 *
 * Class ReviewTemplate
 * @package AppBundle\Form\Type
 */
class ReviewTemplate extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $typeOptions = [Review::TYPE_CUSTOM => 'Custom'];

      $builder->add('typeId', ChoiceType::class, [
         'choices' => array_flip($typeOptions),
         'required' => true
      ])
      ->add('name', TextType::class, [
         'required' => true
      ])
      ->add('subject', TextType::class, [
         'required' => true
      ])
      ->add('message', TextareaType::class, [
         'required' => true
      ])
      ->add('ratingFeedbackEnabled', ChoiceType::class, [
         'required' => true,
         'multiple' => false,
         'expanded' => true,
         'choices' => [
            'Enabled' => 1,
            'Disabled' => 0,
         ]
      ])
      ->add('commentsEnabled', ChoiceType::class, [
         'required' => true,
         'multiple' => false,
         'expanded' => true,
         'choices' => [
            'Enabled' => 1,
            'Disabled' => 0,
         ]
      ])
      ->add('commentsQuestion', TextareaType::class, [
         'required' => false
      ])
      ->add('reviewTemplateQuestions', CollectionType::class, [
         'entry_type'   => ReviewTemplateQuestion::class,
         'entry_options'  => array(
            'required'  => true,
         ),
         'allow_add' => true,
      ]);


   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ReviewTemplate',
         'csrf_protection' => true,
      ));
   }

   public function getName()
   {
      return 'review_template';
   }

}