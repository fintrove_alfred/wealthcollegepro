<?php
/**
 * ClientWPMInvestment.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ClientWPM;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ClientWPMInvestment extends AbstractType
{


   public function buildForm(FormBuilderInterface $builder, array $options)
   {

      $paymentModeOptions = ClientWPM::getPaymentModeOptions();
      $builder
         ->add('type', ChoiceType::class, [
            'label' => 'Type of Investment',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Real Estate' => 'Real Estate',
               'Bonds' => 'Bonds',
               'Stocks' => 'Stocks',
               'Unit Trusts' => 'Unit Trusts',
               'Deposits' => 'Deposits',
               'Others' => 'Others'
            ],
            'required' => false
         ])
         ->add('riskClass', ChoiceType::class, [
            'label' => 'Risk Classification',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Lower Risk' => 'Lower Risk',
               'Moderate Risk' => 'Moderate Risk',
               'Higher Risk' => 'Higher Risk',
            ],
            'required' => false
         ])
         ->add('liquidateOnDeath', CheckboxType::class, [
            'label' => 'Liquidate on Death',
            'required' => false
         ])
         ->add('country', EntityType::class, [
            'class' => 'AppBundle:Country',
            'choice_label' => 'name',
            'placeholder' => 'Select Location',
            'attr' => ['data-init-plugin' => "select2", 'class' => 'full-width'],
            'query_builder' => function (EntityRepository $er) {
               return $er->createQueryBuilder('c')
                  ->orderBy('c.order', 'ASC')
                  ->addOrderBy('c.name', 'ASC');
            },
            'required' => false
         ])
         ->add('ownershipType', ChoiceType::class, [
            'label' => 'Ownership Type',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Personal' => 'Personal',
               'Joint' => 'Joint',
               'Others' => 'Others',
            ],
            'required' => false
         ])
         ->add('owner', TextType::class, ['required' => false, 'label' => 'Owner(s)', 'attr' => ['class' => 'form-control']])
         ->add('percentOwnership', TextType::class, ['required' => false, 'label' => 'Percentage of Ownership', 'attr' => ['class' => 'number form-control']])
         ->add('originalValue', TextType::class, ['required' => false, 'label' => 'Original Value ', 'attr' => ['class' => 'number form-control']])
         ->add('originalValuationDate', DateType::class, ['label' => 'Date of Valuation', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('originalLoanAmount', TextType::class, ['required' => false, 'label' => 'Original Loan Amount ', 'attr' => ['class' => 'number form-control']])
         ->add('currentLoanAmount', TextType::class, ['required' => false, 'label' => 'Current Loan Amount ', 'attr' => ['class' => 'number form-control']])
         ->add('currentMarketValue', TextType::class, ['required' => false, 'label' => 'Current Market Value ', 'attr' => ['class' => 'number form-control']])
         ->add('currentValuationDate', DateType::class, ['label' => 'Date of Valuation', 'widget' => 'single_text', 'attr' => ['class' => 'form-control date-picker'], 'html5' => false, 'required' => false])
         ->add('investmentMode', TextType::class, ['required' => false, 'label' => 'Mode of Investment', 'attr' => ['class' => 'form-control']])
         ->add('sourceFunds', ChoiceType::class, [
            'label' => 'Source of Funds',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'Cash' => 'Cash',
               'Pension (CPF)' => 'Pension (CPF)',
               'Corporate benefits' => 'Corporate benefits',
               'Others' => 'Others'
            ],
            'required' => false
         ])
         ->add('distribution', ChoiceType::class, [
            'label' => 'Distribution',
            'attr' => ['data-init-plugin' => "cs-select", 'class' => 'cs-select cs-skin-slide full-width'],
            'choices' => [
               'General Law' => 'General Law',
               'Special Law' => 'Special Law',
            ],
            'required' => false
         ])
         ->add('specialLaw', TextType::class, ['required' => false, 'label' => 'If Special Law, please state', 'attr' => ['class' => 'form-control']])
         ->add('beneficiaries', TextareaType::class, ['required' => false, 'label' => 'Beneficiaries', 'attr' => ['class' => 'form-control']])
         ->add('remarks', TextareaType::class, ['required' => false, 'label' => 'Remarks', 'attr' => ['class' => 'form-control']])

      ;
      
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ClientWPMInvestment',
         'csrf_protection' => true,
      ));
   }


   public function getName()
   {
      return 'client_wpm_investment';
   }

}