<?php
/**
 * ReviewAnswer.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ReviewTemplateQuestion;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The form for a review answer
 *
 * Class ReviewAnswer
 * @package AppBundle\Form\Type
 */
class ReviewAnswer extends AbstractType
{

   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder->add('answerScore', ChoiceType::class, [
         'choices' => [
            'Strongly Agree' => 5,
            'Agree' => 4,
            'Neutral' => 3,
            'Disagree' => 2,
            'Strongly Disagree' => 1,
         ],
         'expanded' => true,
         'multiple' => false,
         'required' => true
      ])
      ->add('answerText', TextType::class, [
         'required' => true
      ]);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults(array(
         'data_class' => 'AppBundle\Entity\ReviewAnswer',
         'csrf_protection' => true,
      ));
   }

   public function getName()
   {
      return 'review_answer';
   }

}