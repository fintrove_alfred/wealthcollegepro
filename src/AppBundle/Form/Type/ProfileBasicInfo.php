<?php
/**
 * ProfileBasicInfo.php
 *
 * @author Gul
 */

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\FinancialAdviserRegistration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileBasicInfo extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, array(
            'label' => false,
            'attr' => array(
                'placeholder' => 'First Name'
            ),
            'required' => false
        ))
            ->add('lastName', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Last Name'
                )
            ))
            ->add('title', TextType::class, array(
                'label' => '',
                'attr' => array(
                    'placeholder' => 'Title'
                )
            ))
            ->add('dob', BirthdayType::class, array(
                'label' => 'Date of birth',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => ['class' => 'date-mask']
            ))
            ->add(
                $builder->create('financialAdviser', FormType::class, [
                    'by_reference' => true,
                    'data_class' => \AppBundle\Entity\FinancialAdviser::class,
                ])
                    ->add('businessName', TextType::class)
                    ->add('professionalTitle', TextType::class)
                    ->add('socialFacebook', TextType::class, ['required' => false])
                    ->add('socialTwitter', TextType::class, ['required' => false])
                    ->add('socialLinkedin', TextType::class, ['required' => false])
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'csrf_protection' => true,
        ));
    }


    public function getName()
    {
        return 'profile_basic_info';
    }

}