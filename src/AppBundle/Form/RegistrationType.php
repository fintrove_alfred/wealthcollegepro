<?php
/**
 * RegistrationType.php
 *
 * @author Gul
 */

namespace AppBundle\Form;

use AppBundle\Form\Type\FinancialAdviserRegistration;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'First Name'
                ),
                'required' => false
            ))
            ->add('lastName', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Last Name'
                )
            ))
            ->add('title', TextType::class, array(
                'label' => '',
                'attr' => array(
                    'placeholder' => 'Title'
                )
            ))
//            ->add('dob', BirthdayType::class, array(
//                'label' => 'Date of birth',
//                'widget' => 'single_text',
//                'format' => 'dd/MM/yyyy',
//            ))
//      ->add('financialAdviser', CollectionType::class, [
//         'entry_type' => FinancialAdviserRegistration::class
//      ])
            ->add('financialAdviser', FinancialAdviserRegistration::class, [
                'data_class' => 'AppBundle\Entity\FinancialAdviser',
            ])
            ->add('agreedTerms', CheckboxType::class, array(
                'label' => 'I agree to the terms and conditions',
                'required' => true,
                'mapped' => false
            ));
        $builder->remove('username');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }

}