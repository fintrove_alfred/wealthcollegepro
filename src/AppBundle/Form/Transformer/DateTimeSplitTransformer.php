<?php
/**
 * AppBundle\Form\Transformer\DateTimeSplitTransformer.php
 *
 * @author: Gul  
 */

namespace AppBundle\Form\Transformer;


use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DateTimeSplitTransformer implements DataTransformerInterface {

    public function transform($value)
    {
        if (null === $value) {
            return;
        }

        if ($value instanceof \DateTime) {
            return array(
                'date' => $value,//$value->format('Y-m-d'),
                'time' => $value,//->format('H:i')
            );
        }

        return null;
    }

    public function reverseTransform($value)
    {
        if (null === $value) {
            return null;
        }

        if (is_array($value) && array_key_exists('date', $value) && array_key_exists('time', $value)) {

            if ($value['date'] instanceof \DateTime) {
                $date = $value['date']->format('Y-m-d');
            } else {
                $date = $value['date'];
            }
            if ($value['time'] instanceof \DateTime) {
                $time = $value['time']->format('H:i:s');
            } else {
                $time = $value['time'];
            }

            return new \DateTime($date . ' ' . $time);
        }

        return null;
    }

}