<?php
/**
 * AppBundle\Command\SendReviewRequest.php
 *
 * @author: Gul
 */

namespace AppBundle\Command;

use AppBundle\Command\BaseCommand;
use AppBundle\Entity\Review;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendReviewRequestCommand extends BaseCommand {

    protected function configure()
    {
        $this
           ->setName('review:send')
           ->setDescription('Send any pending review requests')
           ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Set to dry run the command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dryRun = $input->getOption('dry-run');

        $em = $this->getEm();
        $logger = $this->getContainer()->get('logger');

        $reviews = $em->getRepository('AppBundle:Review')->findBy(['statusId' => Review::STATUS_PENDING]);
        $output->writeln("Processing ".count($reviews)." reviews");

        $fromEmail = $this->getContainer()->getParameter('mailer_from_email');
        $fromName = $this->getContainer()->getParameter('app_name');

        foreach ($reviews as $review) {

            if (!$review->getClient()->getPerson()->getEmail()) {
                continue;
            }

            try {
                $swiftMessage = \Swift_Message::newInstance()
                   ->setSubject($review->getEmailSubject())
                   ->setFrom($fromEmail, $fromName)
                   ->setTo(
                      $review->getClient()->getPerson()->getEmail(),
                      $review->getClient()->getPerson()->getFullName()
                   )
                   ->setBody(
                      nl2br($review->getEmailMessage()),
                      'text/html'
                   )//                ->addPart($message->getBody(), 'text/plain')

                ;
                if (!$dryRun) {
                    $this->getContainer()->get('mailer')->send($swiftMessage);

                    $review->setStatusId(Review::STATUS_SENT);
                    $review->setSentAt(new \DateTime());

                    $em->persist($review);
                    $em->flush($review);

                } else {
                    $output->writeln("Sending Email");
                    $output->writeln($swiftMessage->toString());
                }
            } catch (\Exception $exception) {
                $output->writeln($exception->getMessage());
                $output->writeln($exception->getTraceAsString());

                $logger->crit("Could not send review: ".$review->getId()." for FA: ".$review->getFinancialAdviser()->getUser()->getName(). " User ID:".$review->getFinancialAdviser()->getUser()->getId()." TO: <".$review->getClient()->getPerson()->getEmail().'> '.$review->getClient()->getPerson()->getFullName());
                $logger->crit($exception->getMessage().' '.$exception->getTraceAsString());
            }

        }

        $output->writeln("Done.");
    }


    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

}