<?php
/**
 * AppBundle\Command\RefreshWPMCalcsCommand.php
 *
 * @author: Gul  
 */

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use AppBundle\Service\MessageManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshWPMCalcsCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('refresh-wpm')
            ->setDescription('Refresh WPM calcs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getEm();
        $clients = $em->getRepository('AppBundle:Client')->findAll();

        $clientManager = $this->getContainer()->get('app.manager.client');
        foreach ($clients as $client) {

            if ($client->getClientWPM()) {
                $clientManager->recalculateWPMValues($client);
                $output->writeln("Recalculating for ".$client->getPerson()->getFullName());
            }

        }

        $output->writeln("Done.");
    }

    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

}