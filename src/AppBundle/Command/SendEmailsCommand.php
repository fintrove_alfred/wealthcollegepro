<?php
/**
 * AppBundle\Command\SendEmailsCommand.php
 *
 * @author: Gul  
 */

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use AppBundle\Service\MessageManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendEmailsCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('send-emails')
            ->setDescription('Send any messages waiting to be emailed')
            ->addArgument('num-emails', InputArgument::OPTIONAL, 'Number of emails to send', 50)
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Set to dry run the command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dryRun = $input->getOption('dry-run');
        $numEmails = $input->getArgument('num-emails');
        $messages = $this->getMessageManager()->getWaitingToEmail($numEmails);

        $em = $this->getEm();
        $output->writeln("Processing ".count($messages)." messages");

        foreach ($messages as $message) {

            $fromEmail = 'no-reply@fahub.sg';
            $fromName = $message->getUserFrom() ? $message->getUserFrom()->getName() : 'Wealth College Pro';

            $swiftMessage = \Swift_Message::newInstance()
                ->setSubject($message->getSubject())
                ->setFrom($fromEmail, $fromName)
                ->setTo($message->getUserTo()->getEmail(), $message->getUserTo()->getName())
                ->setBody(nl2br($message->getBody()), 'text/html')
//                ->addPart($message->getBody(), 'text/plain')

            ;
            if (!$dryRun) {
                $this->getContainer()->get('mailer')->send($swiftMessage);
            } else {
                $output->writeln("Sending Email");
                $output->writeln($swiftMessage->toString());
            }

            $message->setEmailSentAt(new \DateTime());
            $em->persist($message);
            $em->flush($message);
        }

        $output->writeln("Done.");
    }

    /**
     * @return \AppBundle\Service\MessageManager
     */
    private function getMessageManager() {
        return $this->getContainer()->get('app.manager.message_manager');
    }

    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

}