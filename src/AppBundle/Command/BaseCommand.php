<?php
/**
 * The base comand class to be extended command classes
 */

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityNotFoundException;
use AppBundle\Entity\PurchaseOrder;


abstract class BaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('base-command')
            ->setDescription('Generate WSDL for Elmhurst')
        ;
    }


    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @return string
     */
    protected function getRootDir() {
        return $this->getContainer()->get('kernel')->getRootDir();
    }
}
