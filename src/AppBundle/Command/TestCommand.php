<?php
/**
 * AppBundle\Command\DanceRaceFtpCommand.php
 *
 */

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use AppBundle\Service\MeasureManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityNotFoundException;
use AppBundle\Entity\PurchaseOrder;


class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('test')
            ->setDescription('test command')
            ->addArgument(
                'id'
            )
        ;
    }

}
