<?php
/**
 * AppBundle\Command\SubscriptionExpiryReminderCommand.php
 *
 * @author: Gul  
 */

namespace AppBundle\Command;

use AppBundle\Entity\FinancialAdviser;
use AppBundle\Service\MessageManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SubscriptionExpiryReminderCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('subscription-expiry-reminder')
            ->setDescription('Remind users who\'s subscriptions are expiring')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Set to dry run the command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dryRun = $input->getOption('dry-run');
        $emailManager = $this->getContainer()->get('app.manager.email_manager');
        $expiryDays = [30, 14, 7, 1];
        foreach ($expiryDays as $daysToExpiry) {
            $expiringFAs = $this->getContainer()->get('app.manager.financial_adviser')->getExpiring($daysToExpiry);

            if (!$dryRun) {
                $emailManager->sendSubscriptionExpiringMessage($expiringFAs, $daysToExpiry);
            } else {
                $output->writeln("Sending message to ".count($expiringFAs)." users");
            }
        }

    }

    /**
     * @return \AppBundle\Service\MessageManager
     */
    private function getMessageManager() {
        return $this->getContainer()->get('app.manager.message_manager');
    }
}