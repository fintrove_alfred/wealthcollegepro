<?php
/**
 * AppBundle\Twig\AppExtension.php
 *
 * @author: Gul  
 */

namespace AppBundle\Twig;


class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
            new \Twig_SimpleFilter('units', array($this, 'unitsFilter')),
        );
    }

    public function priceFilter($number, $decimals = 2, $trimZeroDecimals = true)
    {

        $price = number_format($number, $decimals);

        if ($trimZeroDecimals) {
            $price = $this->rtrimZeroDecimals($price);
        }

        return $price;
    }

    public function unitsFilter($number, $decimals = 3, $decPoint = '.', $thousandsSep = ',', $trimZeroDecimals = true)
    {
        $units = number_format($number, $decimals, $decPoint, $thousandsSep);

        if ($trimZeroDecimals) {
            $units = $this->rtrimZeroDecimals($units);
        }

        return $units;
    }

    /**
     * Remove decimal zeroes, '.00'
     * @param $number
     * @return string
     */
    private function rtrimZeroDecimals($number) {
        return strpos($number,'.')!==false ? rtrim(rtrim($number,'0'),'.') : $number;
    }

    public function getName()
    {
        return 'app_extension';
    }
}