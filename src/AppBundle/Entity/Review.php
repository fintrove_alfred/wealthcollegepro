<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Person
 *
 * @ORM\Table(name="review")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Review
{

    const TYPE_FEEDBACK = 1;
    const TYPE_EXPERIENCE = 2;
    const TYPE_CUSTOM = 9;

    const STATUS_PENDING = 1;
    const STATUS_SENT = 2;
    const STATUS_RECEIVED_PENDING = 3;
    const STATUS_PUBLISHED = 4;
    const STATUS_ARCHIVED = 9;



    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\FinancialAdviser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FinancialAdviser", inversedBy="reviews")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financial_adviser_id", referencedColumnName="id")
     * })
     */
    private $financialAdviser;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \AppBundle\Entity\ReviewTemplate
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ReviewTemplate", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="review_template_id", referencedColumnName="id")
     * })
     */
    private $reviewTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="status_id", type="integer", nullable=false)
     */
    private $statusId;

    /**
     * @var string
     *
     * @ORM\Column(name="email_subject", type="string", length=255, nullable=true)
     */
    private $emailSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="email_message", type="text", nullable=true)
     */
    private $emailMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text", nullable=true)
     */
    private $feedback;

    /**
     * @var string
     *
     * @ORM\Column(name="negative_feedback", type="text", nullable=true)
     */
    private $negativeFeedback;

    /**
     * Captures the customer's comment when review template commentsEnabled is true
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToMany(targetEntity="ReviewAttribute")
     * @ORM\JoinTable(name="review_review_attribute",
     *      joinColumns={@ORM\JoinColumn(name="review_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="review_attribute_id", referencedColumnName="id")}
     *      )
     */
    private $attributes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="received_at", type="datetime", nullable=true)
     */
    private $receivedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     */
    private $archivedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @var float
     *
     * @ORM\Column(name="verifiable", type="boolean", nullable=false, options={"default": 0})
     */
    private $verifiable = false;

    /**
     * @var float
     *
     * @ORM\Column(name="verified", type="boolean", nullable=false, options={"default": 0})
     */
    private $verified = false;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ReviewAnswer", mappedBy="review", cascade={"persist"})
     */
    private $reviewAnswers;

    public function __construct()
    {
        $this->reviewAnswers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     *
     * @return Review
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getTypeName()
    {
        return self::getTypeOptions()[$this->typeId];
    }

    public static function getTypeOptions() {
        return [
           self::TYPE_FEEDBACK => 'Feedback',
           self::TYPE_EXPERIENCE => 'Comprehensive',
           self::TYPE_CUSTOM => 'Custom',
        ];
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return Review
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set emailSubject
     *
     * @param string $emailSubject
     *
     * @return Review
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;

        return $this;
    }

    /**
     * Get emailSubject
     *
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * Set emailMessage
     *
     * @param string $emailMessage
     *
     * @return Review
     */
    public function setEmailMessage($emailMessage)
    {
        $this->emailMessage = $emailMessage;

        return $this;
    }

    /**
     * Get emailMessage
     *
     * @return string
     */
    public function getEmailMessage()
    {
        return $this->emailMessage;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Review
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRatingDescription()
    {
        if (!$this->rating) {
            return 'No Rating';
        }

        switch ($this->rating) {
            case 10:
            case 9:
              return 'Excellent'; break;
            case 8:
                return 'Very Good'; break;
            case 7:
            case 6:
                return 'Good'; break;
            case 5:
            case 4:
                return 'Fair'; break;
            case 3:
            case 2:
            case 1:
                return 'Poor'; break;
        }
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Review
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set feedback
     *
     * @param string $feedback
     *
     * @return Review
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * Get feedback
     *
     * @return string
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Review
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Review
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Review
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return Review
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser = null)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return Review
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return Review
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set receivedAt
     *
     * @param \DateTime $receivedAt
     *
     * @return Review
     */
    public function setReceivedAt($receivedAt)
    {
        $this->receivedAt = $receivedAt;

        return $this;
    }

    /**
     * Get receivedAt
     *
     * @return \DateTime
     */
    public function getReceivedAt()
    {
        return $this->receivedAt;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     *
     * @return Review
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set archivedAt
     *
     * @param \DateTime $archivedAt
     *
     * @return Review
     */
    public function setArchivedAt($archivedAt)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * Get archivedAt
     *
     * @return \DateTime
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }


    /**
     * Set token
     *
     * @param string $token
     *
     * @return Review
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Add reviewAnswer
     *
     * @param \AppBundle\Entity\ReviewAnswer $reviewAnswer
     *
     * @return Review
     */
    public function addReviewAnswer(\AppBundle\Entity\ReviewAnswer $reviewAnswer)
    {
        $this->reviewAnswers[] = $reviewAnswer;

        return $this;
    }

    /**
     * Remove reviewAnswer
     *
     * @param \AppBundle\Entity\ReviewAnswer $reviewAnswer
     */
    public function removeReviewAnswer(\AppBundle\Entity\ReviewAnswer $reviewAnswer)
    {
        $this->reviewAnswers->removeElement($reviewAnswer);
    }

    /**
     * Get reviewAnswers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewAnswers()
    {
        return $this->reviewAnswers;
    }

    /**
     * Set reviewTemplate
     *
     * @param \AppBundle\Entity\ReviewTemplate $reviewTemplate
     *
     * @return Review
     */
    public function setReviewTemplate(\AppBundle\Entity\ReviewTemplate $reviewTemplate = null)
    {
        $this->reviewTemplate = $reviewTemplate;

        return $this;
    }

    /**
     * Get reviewTemplate
     *
     * @return \AppBundle\Entity\ReviewTemplate
     */
    public function getReviewTemplate()
    {
        return $this->reviewTemplate;
    }


    /**
     * Get the colour string for the rating
     * green, amber or red
     * @return string
     */
    public function ratingColour($cssColor = false) {
        if ($this->getRating() > 0 && $this->getRating() <= 5) {
            return 'red';
        } elseif ($this->getRating() > 5 && $this->getRating() <= 7) {
            return $cssColor ? 'orange' : 'amber';
        } else {
            return 'green';
        }
    }

    /**
     * Set negativeFeedback
     *
     * @param string $negativeFeedback
     *
     * @return Review
     */
    public function setNegativeFeedback($negativeFeedback)
    {
        $this->negativeFeedback = $negativeFeedback;

        return $this;
    }

    /**
     * Get negativeFeedback
     *
     * @return string
     */
    public function getNegativeFeedback()
    {
        return $this->negativeFeedback;
    }

    /**
     * Does this review feature ratings and feedback
     * @return bool
     */
    public function hasRating() {
        return is_numeric($this->getRating());
    }

    /**
     * Does this review feature ratings and feedback
     * @return bool
     */
    public function isRatingFeedbackRequired() {
        if (!$this->getReviewTemplate()) {
            return true;
        }
        if ($this->getReviewTemplate()->getRatingFeedbackEnabled()) {
            return true;
        }
        return false;
    }

    /**
     * Does this review feature comments
     * @return bool
     */
    public function isCommentRequired() {
        if (!$this->getReviewTemplate()) {
            return false;
        }
        if ($this->getReviewTemplate()->getCommentsEnabled()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isPublished() {
        return $this->getStatusId() == Review::STATUS_PUBLISHED;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Review
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set verifiable
     *
     * @param boolean $verifiable
     *
     * @return Review
     */
    public function setVerifiable($verifiable)
    {
        $this->verifiable = $verifiable;

        return $this;
    }

    /**
     * Get verifiable
     *
     * @return boolean
     */
    public function getVerifiable()
    {
        return $this->verifiable;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     *
     * @return Review
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * Get verified
     *
     * @return boolean
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Get time since this review was published
     *
     * @return string
     */
    public function getTimeSince() {

        $time = $this->getPublishedAt();
        $now = new \DateTime();
        $diff = $time->diff($now, true);
        $timeSince = '';
        if ($diff->days > 0) {
            $timeSince .= $diff->days.' days ';
        }
        if ($diff->days == 0 && $diff->h > 0) {
            $timeSince .= $diff->h.' hours ';
        }
        if ($diff->days == 0 && $diff->i > 0) {
            $timeSince .= $diff->i.' minues ';
        }
        $timeSince .= 'ago';
        return $timeSince;
    }


    /**
     * Add attribute
     *
     * @param \AppBundle\Entity\ReviewAttribute $attribute
     *
     * @return Review
     */
    public function addAttribute(\AppBundle\Entity\ReviewAttribute $attribute)
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    /**
     * Remove attribute
     *
     * @param \AppBundle\Entity\ReviewAttribute $attribute
     */
    public function removeAttribute(\AppBundle\Entity\ReviewAttribute $attribute)
    {
        $this->attributes->removeElement($attribute);
    }

    public function hasAttributeId($attributeId) {

        if ($this->getAttributes() && $this->getAttributes()->count() > 0) {
            foreach ($this->getAttributes() as $attribute) {
                if ($attribute->getId() == $attributeId) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}
