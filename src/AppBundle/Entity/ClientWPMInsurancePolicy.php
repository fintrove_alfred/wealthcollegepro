<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientWPMInsurancePolicy
 *
 * @ORM\Table(name="client_wpm_insurance_policy")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ClientWPMInsurancePolicy
{

    const BENEFIT_TYPE_WHOLE = 1;
    const BENEFIT_TYPE_ENDOWMENT = 2;
    const BENEFIT_TYPE_PERSONAL_ACCIDENT = 3;
    const BENEFIT_TYPE_INVESTMENT_LINKED = 4;
    const BENEFIT_TYPE_CRITICAL_ILLNESS = 5;
    const BENEFIT_TYPE_HOSPITALISATION = 6;
    const BENEFIT_TYPE_DISABILITY = 7;
    const BENEFIT_TYPE_OTHER = 20;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="clientWPMInsurancePolicies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \AppBundle\Entity\Upload
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="document_upload_id", referencedColumnName="id")
     * })
     */
    private $documentUpload;

    /**
     * @var string
     *
     * @ORM\Column(name="policy_type", type="string", length=255, nullable=true)
     */
    private $policyType;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="policy_number", type="string", length=255, nullable=true)
     */
    private $policyNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \AppBundle\Entity\Country
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location_country_id", referencedColumnName="id")
     * })
     */
    private $locationCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="ownership_type", type="string", length=255, nullable=true)
     */
    private $ownershipType;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255, nullable=true)
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="main_benefit", type="simple_array", nullable=true)
     */
    private $mainBenefit;

    /**
     * @var float
     *
     * @ORM\Column(name="sum_assured", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $sumAssured;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var float
     *
     * @ORM\Column(name="premium", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $premium;

    /**
     * @var integer
     *
     * @ORM\Column(name="premium_mode", type="integer", nullable=true)
     */
    private $premiumMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="premium_date", type="datetime", nullable=true)
     */
    private $premiumDate;

    /**
     * @var string
     *
     * @ORM\Column(name="rider_benefit", type="simple_array", nullable=true)
     */
    private $riderBenefit;

    /**
     * @var float
     *
     * @ORM\Column(name="rider_sum_assured", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $riderSumAssured;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rider_start_date", type="datetime", nullable=true)
     */
    private $riderStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rider_end_date", type="datetime", nullable=true)
     */
    private $riderEndDate;

    /**
     * @var float
     *
     * @ORM\Column(name="rider_premium", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $riderPremium;


    /**
     * @var integer
     *
     * @ORM\Column(name="rider_premium_mode", type="integer", nullable=true)
     */
    private $riderPremiumMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rider_premium_date", type="datetime", nullable=true)
     */
    private $riderPremiumDate;

    /**
     * @var string
     *
     * @ORM\Column(name="sup_benefit", type="simple_array", nullable=true)
     */
    private $supBenefit;

    /**
     * @var float
     *
     * @ORM\Column(name="sup_sum_assured", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $supSumAssured;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sup_start_date", type="datetime", nullable=true)
     */
    private $supStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sup_end_date", type="datetime", nullable=true)
     */
    private $supEndDate;

    /**
     * @var float
     *
     * @ORM\Column(name="sup_premium", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $supPremium;

    /**
     * @var integer
     *
     * @ORM\Column(name="sup_premium_mode", type="integer", nullable=true)
     */
    private $supPremiumMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sup_premium_date", type="datetime", nullable=true)
     */
    private $supPremiumDate;

    /**
     * @var string
     *
     * @ORM\Column(name="source_funds", type="string", length=255, nullable=true)
     */
    private $sourceFunds;

    /**
     * @var float
     *
     * @ORM\Column(name="gross_cash_surrender_value", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $grossCashSurrenderValue;

    /**
     * @var float
     *
     * @ORM\Column(name="policy_loan", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $policyLoan;

    /**
     * @var float
     *
     * @ORM\Column(name="net_cash_value", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $netCashValue;

    /**
     * @var string
     *
     * @ORM\Column(name="distribution", type="string", length=255, nullable=true)
     */
    private $distribution;

    /**
     * @var string
     *
     * @ORM\Column(name="special_law", type="string", length=255, nullable=true)
     */
    private $specialLaw;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaries", type="text",  length=1000, nullable=true)
     */
    private $beneficiaries;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks", type="text", length=2000, nullable=true)
     */
    private $remarks;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set policyType
     *
     * @param string $policyType
     *
     * @return string
     */
    public function setPolicyType($policyType)
    {
        $this->policyType = $policyType;

        return $this;
    }

    /**
     * Get policyType
     *
     * @return string
     */
    public function getPolicyType()
    {
        return $this->policyType;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }


    /**
     * Set status
     *
     * @param string $status
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set ownershipType
     *
     * @param string $ownershipType
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setOwnershipType($ownershipType)
    {
        $this->ownershipType = $ownershipType;

        return $this;
    }

    /**
     * Get ownershipType
     *
     * @return string
     */
    public function getOwnershipType()
    {
        return $this->ownershipType;
    }

    /**
     * Set owner
     *
     * @param string $owner
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set sumAssured
     *
     * @param string $sumAssured
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSumAssured($sumAssured)
    {
        $this->sumAssured = $sumAssured;

        return $this;
    }

    /**
     * Get sumAssured
     *
     * @return float
     */
    public function getSumAssured()
    {
        return $this->sumAssured;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set premium
     *
     * @param string $premium
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setPremium($premium)
    {
        $this->premium = $premium;

        return $this;
    }

    /**
     * Get premium
     *
     * @return string
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * Set premiumMode
     *
     * @param integer $premiumMode
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setPremiumMode($premiumMode)
    {
        $this->premiumMode = $premiumMode;

        return $this;
    }

    /**
     * Get premiumMode
     *
     * @return integer
     */
    public function getPremiumMode()
    {
        return $this->premiumMode;
    }

    /**
     * Set premiumDate
     *
     * @param \DateTime $premiumDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setPremiumDate($premiumDate)
    {
        $this->premiumDate = $premiumDate;

        return $this;
    }

    /**
     * Get premiumDate
     *
     * @return \DateTime
     */
    public function getPremiumDate()
    {
        return $this->premiumDate;
    }

    /**
     * Set riderSumAssured
     *
     * @param string $riderSumAssured
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderSumAssured($riderSumAssured)
    {
        $this->riderSumAssured = $riderSumAssured;

        return $this;
    }

    /**
     * Get riderSumAssured
     *
     * @return float
     */
    public function getRiderSumAssured()
    {
        return $this->riderSumAssured;
    }

    /**
     * Set riderStartDate
     *
     * @param \DateTime $riderStartDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderStartDate($riderStartDate)
    {
        $this->riderStartDate = $riderStartDate;

        return $this;
    }

    /**
     * Get riderStartDate
     *
     * @return \DateTime
     */
    public function getRiderStartDate()
    {
        return $this->riderStartDate;
    }

    /**
     * Set riderEndDate
     *
     * @param \DateTime $riderEndDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderEndDate($riderEndDate)
    {
        $this->riderEndDate = $riderEndDate;

        return $this;
    }

    /**
     * Get riderEndDate
     *
     * @return \DateTime
     */
    public function getRiderEndDate()
    {
        return $this->riderEndDate;
    }

    /**
     * Set riderPremium
     *
     * @param string $riderPremium
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderPremium($riderPremium)
    {
        $this->riderPremium = $riderPremium;

        return $this;
    }

    /**
     * Get riderPremium
     *
     * @return string
     */
    public function getRiderPremium()
    {
        return $this->riderPremium;
    }

    /**
     * Set riderPremiumMode
     *
     * @param integer $riderPremiumMode
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderPremiumMode($riderPremiumMode)
    {
        $this->riderPremiumMode = $riderPremiumMode;

        return $this;
    }

    /**
     * Get riderPremiumMode
     *
     * @return integer
     */
    public function getRiderPremiumMode()
    {
        return $this->riderPremiumMode;
    }

    /**
     * Set riderPremiumDate
     *
     * @param \DateTime $riderPremiumDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderPremiumDate($riderPremiumDate)
    {
        $this->riderPremiumDate = $riderPremiumDate;

        return $this;
    }

    /**
     * Get riderPremiumDate
     *
     * @return \DateTime
     */
    public function getRiderPremiumDate()
    {
        return $this->riderPremiumDate;
    }

    /**
     * Set supSumAssured
     *
     * @param string $supSumAssured
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupSumAssured($supSumAssured)
    {
        $this->supSumAssured = $supSumAssured;

        return $this;
    }

    /**
     * Get supSumAssured
     *
     * @return float
     */
    public function getSupSumAssured()
    {
        return $this->supSumAssured;
    }

    /**
     * Set supStartDate
     *
     * @param \DateTime $supStartDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupStartDate($supStartDate)
    {
        $this->supStartDate = $supStartDate;

        return $this;
    }

    /**
     * Get supStartDate
     *
     * @return \DateTime
     */
    public function getSupStartDate()
    {
        return $this->supStartDate;
    }

    /**
     * Set supEndDate
     *
     * @param \DateTime $supEndDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupEndDate($supEndDate)
    {
        $this->supEndDate = $supEndDate;

        return $this;
    }

    /**
     * Get supEndDate
     *
     * @return \DateTime
     */
    public function getSupEndDate()
    {
        return $this->supEndDate;
    }

    /**
     * Set supPremium
     *
     * @param string $supPremium
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupPremium($supPremium)
    {
        $this->supPremium = $supPremium;

        return $this;
    }

    /**
     * Get supPremium
     *
     * @return string
     */
    public function getSupPremium()
    {
        return $this->supPremium;
    }

    /**
     * Set supPremiumMode
     *
     * @param integer $supPremiumMode
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupPremiumMode($supPremiumMode)
    {
        $this->supPremiumMode = $supPremiumMode;

        return $this;
    }

    /**
     * Get supPremiumMode
     *
     * @return integer
     */
    public function getSupPremiumMode()
    {
        return $this->supPremiumMode;
    }

    /**
     * Set supPremiumDate
     *
     * @param \DateTime $supPremiumDate
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupPremiumDate($supPremiumDate)
    {
        $this->supPremiumDate = $supPremiumDate;

        return $this;
    }

    /**
     * Get supPremiumDate
     *
     * @return \DateTime
     */
    public function getSupPremiumDate()
    {
        return $this->supPremiumDate;
    }

    /**
     * Set sourceFunds
     *
     * @param string $sourceFunds
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSourceFunds($sourceFunds)
    {
        $this->sourceFunds = $sourceFunds;

        return $this;
    }

    /**
     * Get sourceFunds
     *
     * @return string
     */
    public function getSourceFunds()
    {
        return $this->sourceFunds;
    }

    /**
     * Set grossCashSurrenderValue
     *
     * @param string $grossCashSurrenderValue
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setGrossCashSurrenderValue($grossCashSurrenderValue)
    {
        $this->grossCashSurrenderValue = $grossCashSurrenderValue;

        return $this;
    }

    /**
     * Get grossCashSurrenderValue
     *
     * @return string
     */
    public function getGrossCashSurrenderValue()
    {
        return $this->grossCashSurrenderValue;
    }

    /**
     * Set policyLoan
     *
     * @param string $policyLoan
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setPolicyLoan($policyLoan)
    {
        $this->policyLoan = $policyLoan;

        return $this;
    }

    /**
     * Get policyLoan
     *
     * @return string
     */
    public function getPolicyLoan()
    {
        return $this->policyLoan;
    }

    /**
     * Set netCashValue
     *
     * @param string $netCashValue
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setNetCashValue($netCashValue)
    {
        $this->netCashValue = $netCashValue;

        return $this;
    }

    /**
     * Get netCashValue
     *
     * @return string
     */
    public function getNetCashValue()
    {
        return $this->netCashValue;
    }

    /**
     * Set distribution
     *
     * @param string $distribution
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setDistribution($distribution)
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Get distribution
     *
     * @return string
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * Set specialLaw
     *
     * @param string $specialLaw
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSpecialLaw($specialLaw)
    {
        $this->specialLaw = $specialLaw;

        return $this;
    }

    /**
     * Get specialLaw
     *
     * @return string
     */
    public function getSpecialLaw()
    {
        return $this->specialLaw;
    }

    /**
     * Set beneficiaries
     *
     * @param string $beneficiaries
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setBeneficiaries($beneficiaries)
    {
        $this->beneficiaries = $beneficiaries;

        return $this;
    }

    /**
     * Get beneficiaries
     *
     * @return string
     */
    public function getBeneficiaries()
    {
        return $this->beneficiaries;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set documentUpload
     *
     * @param \AppBundle\Entity\Upload $documentUpload
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setDocumentUpload(\AppBundle\Entity\Upload $documentUpload = null)
    {
        $this->documentUpload = $documentUpload;

        return $this;
    }

    /**
     * Get documentUpload
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getDocumentUpload()
    {
        return $this->documentUpload;
    }

    /**
     * Set locationCountry
     *
     * @param \AppBundle\Entity\Country $locationCountry
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setLocationCountry(\AppBundle\Entity\Country $locationCountry = null)
    {
        $this->locationCountry = $locationCountry;

        return $this;
    }

    /**
     * Get locationCountry
     *
     * @return \AppBundle\Entity\Country
     */
    public function getLocationCountry()
    {
        return $this->locationCountry;
    }

    /**
     * Set mainBenefit
     *
     * @param string $mainBenefit
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setMainBenefit($mainBenefit)
    {
        $this->mainBenefit = $mainBenefit;

        return $this;
    }

    /**
     * Get mainBenefit
     *
     * @return string
     */
    public function getMainBenefit()
    {
        return $this->mainBenefit;
    }

    /**
     * Set riderBenefit
     *
     * @param string $riderBenefit
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setRiderBenefit($riderBenefit)
    {
        $this->riderBenefit = $riderBenefit;

        return $this;
    }

    /**
     * Get riderBenefit
     *
     * @return string
     */
    public function getRiderBenefit()
    {
        return $this->riderBenefit;
    }

    /**
     * Set supBenefit
     *
     * @param string $supBenefit
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setSupBenefit($supBenefit)
    {
        $this->supBenefit = $supBenefit;

        return $this;
    }

    /**
     * Get supBenefit
     *
     * @return string
     */
    public function getSupBenefit()
    {
        return $this->supBenefit;
    }

    /**
     * Set policyNumber
     *
     * @param string $policyNumber
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setPolicyNumber($policyNumber)
    {
        $this->policyNumber = $policyNumber;

        return $this;
    }

    /**
     * Get policyNumber
     *
     * @return string
     */
    public function getPolicyNumber()
    {
        return $this->policyNumber;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ClientWPMInsurancePolicy
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
    
    
}
