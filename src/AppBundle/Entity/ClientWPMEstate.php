<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientWPMInvestment
 *
 * @ORM\Table(name="client_wpm_estate")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ClientWPMEstate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="clientWPMEstates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="document_type", type="string", length=255, nullable=true)
     */
    private $documentType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="documentCreatedDate", type="datetime", nullable=true)
     */
    private $documentCreatedDate;

    /**
     * @var \AppBundle\Entity\Country
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="appointedRepresentatives", type="text", length=1000, nullable=true)
     */
    private $appointedRepresentatives;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaries", type="text",  length=1000, nullable=true)
     */
    private $beneficiaries;

    /**
     * @var string
     *
     * @ORM\Column(name="key_contacts", type="text", length=5000, nullable=true)
     */
    private $keyContacts;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks", type="text", length=5000, nullable=true)
     */
    private $remarks;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentType
     *
     * @param string $documentType
     *
     * @return ClientWPMEstate
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get documentType
     *
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * Set documentCreatedDate
     *
     * @param \DateTime $documentCreatedDate
     *
     * @return ClientWPMEstate
     */
    public function setDocumentCreatedDate($documentCreatedDate)
    {
        $this->documentCreatedDate = $documentCreatedDate;

        return $this;
    }

    /**
     * Get documentCreatedDate
     *
     * @return \DateTime
     */
    public function getDocumentCreatedDate()
    {
        return $this->documentCreatedDate;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return ClientWPMEstate
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set appointedRepresentatives
     *
     * @param string $appointedRepresentatives
     *
     * @return ClientWPMEstate
     */
    public function setAppointedRepresentatives($appointedRepresentatives)
    {
        $this->appointedRepresentatives = $appointedRepresentatives;

        return $this;
    }

    /**
     * Get appointedRepresentatives
     *
     * @return string
     */
    public function getAppointedRepresentatives()
    {
        return $this->appointedRepresentatives;
    }

    /**
     * Set beneficiaries
     *
     * @param string $beneficiaries
     *
     * @return ClientWPMEstate
     */
    public function setBeneficiaries($beneficiaries)
    {
        $this->beneficiaries = $beneficiaries;

        return $this;
    }

    /**
     * Get beneficiaries
     *
     * @return string
     */
    public function getBeneficiaries()
    {
        return $this->beneficiaries;
    }

    /**
     * Set keyContacts
     *
     * @param string $keyContacts
     *
     * @return ClientWPMEstate
     */
    public function setKeyContacts($keyContacts)
    {
        $this->keyContacts = $keyContacts;

        return $this;
    }

    /**
     * Get keyContacts
     *
     * @return string
     */
    public function getKeyContacts()
    {
        return $this->keyContacts;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return ClientWPMEstate
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientWPMEstate
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientWPMEstate
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientWPMEstate
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return ClientWPMEstate
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ClientWPMEstate
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
