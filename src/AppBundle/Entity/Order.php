<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Order
 *
 * @ORM\Table(name="orders", indexes={@ORM\Index(name="fk_order_product_id", columns={"product_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{
	
	const STATUS_PENDING = 1;
	const STATUS_COMPLETE = 2;
	const STATUS_FAILED = 3;
	const STATUS_CANCELLED = 4;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"}, inversedBy="orders")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

	/**
     * @var \AppBundle\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;
	
	/**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title;
	
    /**
     * Total is always inc tax
     * @var decimal
     *
     * @ORM\Column(name="total", type="decimal", nullable=false, precision=12, scale=2, nullable=false, options={"default":0})
     */
    private $total;

    /**
     * @var \AppBundle\Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Currency", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     * })
     */
    private $currency;

    /**
     * @var decimal
     *
     * @ORM\Column(name="vat_rate", type="decimal", nullable=false, precision=4, scale=2, nullable=false, options={"default":0})
     */
    private $vatRate;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="status_id", type="integer", nullable=false)
     */
    private $statusId = self::STATUS_PENDING;
	
	/**
     * @var string
     *
     * @ORM\Column(name="payment_reference", type="string", length=100, nullable=true)
     */
    private $paymentReference;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderLine", mappedBy="order")
     **/
    private $orderLines;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment", mappedBy="order")
     **/
    private $payments;

    public function __construct()
    {
        $this->orderLines = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
	/**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return Product
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
	
	/**
     * Set title
     *
     * @param string title
     * @return Order
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set total inclusive of VAT
     *
     * @param decimal total
     * @return Installer
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return decimal 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set vatRate
     *
     * @param decimal vatRate
     * @return Installer
     */
    public function setVatRate($vatRate)
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * Get vatRate
     *
     * @return decimal 
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }
	
	/**
     * Set statusId
     *
     * @param integer $statusId
     * @return Installer
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }
	
	/**
     * Set paymentReference
     *
     * @param string $paymentReference
     * @return Installer
     */
    public function setPaymentReference($paymentReference)
    {
        $this->paymentReference = $paymentReference;

        return $this;
    }
	
	/**
     * Get paymentReference
     *
     * @return string 
     */
    public function getPaymentReference()
    {
        return $this->paymentReference;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Installer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Installer
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get order reference
     *
     * @return  string
     */
    public function orderReference()
    {
        return 'DR'.str_pad($this->getId(), 8, '0', STR_PAD_LEFT);
    }

    public function getReferenceNumber() {
        return $this->orderReference();
    }
	
	/**
     * Get vat amount
     *
     * @return  decimal
     */
	public function getVatAmount()
	{
		return $this->total - $this->getSubTotal();
	}
	
	/**
     * Get sub total
     *
     * @return  decimal
     */
	public function getSubTotal()
	{
		return $this->total / (1 + ($this->vatRate/100));
	}


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Order
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add orderLines
     *
     * @param \AppBundle\Entity\OrderLine $orderLines
     * @return Order
     */
    public function addOrderLine(\AppBundle\Entity\OrderLine $orderLines)
    {
        $this->orderLines[] = $orderLines;

        return $this;
    }

    /**
     * Remove orderLines
     *
     * @param \AppBundle\Entity\OrderLine $orderLines
     */
    public function removeOrderLine(\AppBundle\Entity\OrderLine $orderLines)
    {
        $this->orderLines->removeElement($orderLines);
    }

    /**
     * Get orderLines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderLines()
    {
        return $this->orderLines;
    }

    /**
     * Add payments
     *
     * @param \AppBundle\Entity\Payment $payments
     * @return Order
     */
    public function addPayment(\AppBundle\Entity\Payment $payments)
    {
        $this->payments[] = $payments;

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \AppBundle\Entity\Payment $payments
     */
    public function removePayment(\AppBundle\Entity\Payment $payments)
    {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * Set currency
     *
     * @param \AppBundle\Entity\Currency $currency
     *
     * @return Order
     */
    public function setCurrency(\AppBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
