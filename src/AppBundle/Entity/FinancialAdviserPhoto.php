<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * FA Photo
 *
 * @ORM\Table(name="financial_adviser_photo")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class FinancialAdviserPhoto
{

    const TYPE_PHOTOS = 'photos';
    const TYPE_LOGOS = 'logos';

    const TYPE_PROFESSIONAL_QUALIFICATIONS = 'professional-qualifications';
    const TYPE_PROFESSIONAL_MEMBERSHIPS = 'professional-memberships';
    const TYPE_INDUSTRY_AWARDS = 'industry-awards';
    const TYPE_COMMUNITY_INVOLVEMENT = 'community-involvement';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\FinancialAdviser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FinancialAdviser", inversedBy="financialAdviserPhotos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financial_adviser_id", referencedColumnName="id")
     * })
     */
    private $financialAdviser;

    /**
     * @var string
     *
     * @ORM\Column(name="type_id", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var \AppBundle\Entity\Upload
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="upload_id", referencedColumnName="id")
     * })
     */
    private $upload;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return FinancialAdviserPhoto
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return FinancialAdviserPhoto
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return FinancialAdviserPhoto
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return FinancialAdviserPhoto
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser = null)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }

    /**
     * Set upload
     *
     * @param \AppBundle\Entity\Upload $upload
     *
     * @return FinancialAdviserPhoto
     */
    public function setUpload(\AppBundle\Entity\Upload $upload = null)
    {
        $this->upload = $upload;

        return $this;
    }

    /**
     * Get upload
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getUpload()
    {
        return $this->upload;
    }
}
