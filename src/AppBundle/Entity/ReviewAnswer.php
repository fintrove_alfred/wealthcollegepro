<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Person
 *
 * @ORM\Table(name="review_answer")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ReviewAnswer
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Review
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Review", inversedBy="reviewAnswers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="review_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $review;

    /**
     * @var \AppBundle\Entity\ReviewTemplateQuestion
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ReviewTemplateQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="review_template_question_id", referencedColumnName="id")
     * })
     */
    private $reviewTemplateQuestion;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_score", type="integer", nullable=false)
     */
    private $answerScore;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_text", type="text", nullable=true)
     */
    private $answerText;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answerScore
     *
     * @param integer $answerScore
     *
     * @return ReviewAnswer
     */
    public function setAnswerScore($answerScore)
    {
        $this->answerScore = $answerScore;

        return $this;
    }

    /**
     * Get answerScore
     *
     * @return integer
     */
    public function getAnswerScore()
    {
        return $this->answerScore;
    }

    /**
     * Get answerScore
     *
     * @return integer
     */
    public function getAnswerScoreName()
    {
        switch ($this->answerScore) {
            case 5: return 'Strongly Agree';
            case 4: return 'Agree';
            case 3: return 'Neutral';
            case 2: return 'Disagree';
            case 1: return 'Strongly Disagree';
            default: return '';
        }
    }

    /**
     * Set answerText
     *
     * @param string $answerText
     *
     * @return ReviewAnswer
     */
    public function setAnswerText($answerText)
    {
        $this->answerText = $answerText;

        return $this;
    }

    /**
     * Get answerText
     *
     * @return string
     */
    public function getAnswerText()
    {
        return $this->answerText;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ReviewAnswer
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ReviewAnswer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set review
     *
     * @param \AppBundle\Entity\Review $review
     *
     * @return ReviewAnswer
     */
    public function setReview(\AppBundle\Entity\Review $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return \AppBundle\Entity\Review
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set reviewTemplateQuestion
     *
     * @param \AppBundle\Entity\ReviewTemplateQuestion $reviewTemplateQuestion
     *
     * @return ReviewAnswer
     */
    public function setReviewTemplateQuestion(\AppBundle\Entity\ReviewTemplateQuestion $reviewTemplateQuestion = null)
    {
        $this->reviewTemplateQuestion = $reviewTemplateQuestion;

        return $this;
    }

    /**
     * Get reviewTemplateQuestion
     *
     * @return \AppBundle\Entity\ReviewTemplateQuestion
     */
    public function getReviewTemplateQuestion()
    {
        return $this->reviewTemplateQuestion;
    }
}
