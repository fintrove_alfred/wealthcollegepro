<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientDate
 *
 * @ORM\Table(name="client_date")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ClientDate
{

    const DATE_TYPE_BIRTHDAY = 1;
    const DATE_TYPE_ANNIVERSARY = 2;
    const DATE_TYPE_OTHER = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="clientDates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var integer
     *
     * @ORM\Column(name="date_type_id", type="integer", nullable=false)
     */
    private $dateTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="date_type_name", type="string", nullable=true)
     */
    private $dateTypeName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reminder", type="boolean", nullable=false, options={"default":0})
     */
    private $reminder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reminder_date", type="date", nullable=true)
     */
    private $reminderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;


    /**
     * @return array
     */
    public static function getDateTypes() {
        return [
            self::DATE_TYPE_BIRTHDAY => 'Birthday',
            self::DATE_TYPE_ANNIVERSARY => 'Anniversary',
            self::DATE_TYPE_OTHER => 'Other',
        ];
    }

    /**
     * Return the date type name
     * @return mixed
     */
    public function getDateTypeIdName() {
        if ($this->getDateTypeId() == self::DATE_TYPE_OTHER) {
            return $this->getDateTypeName();
        } else {
            return $this->getDateTypes()[$this->getDateTypeId()];
        }
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTypeId
     *
     * @param integer $dateTypeId
     *
     * @return ClientDate
     */
    public function setDateTypeId($dateTypeId)
    {
        $this->dateTypeId = $dateTypeId;

        return $this;
    }

    /**
     * Get dateTypeId
     *
     * @return integer
     */
    public function getDateTypeId()
    {
        return $this->dateTypeId;
    }

    /**
     * Set dateTypeName
     *
     * @param string $dateTypeName
     *
     * @return ClientDate
     */
    public function setDateTypeName($dateTypeName)
    {
        $this->dateTypeName = $dateTypeName;

        return $this;
    }

    /**
     * Get dateTypeName
     *
     * @return string
     */
    public function getDateTypeName()
    {
        return $this->dateTypeName;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ClientDate
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set reminder
     *
     * @param boolean $reminder
     *
     * @return ClientDate
     */
    public function setReminder($reminder)
    {
        $this->reminder = $reminder;

        return $this;
    }

    /**
     * Get reminder
     *
     * @return boolean
     */
    public function getReminder()
    {
        return $this->reminder;
    }

    /**
     * Set reminderDate
     *
     * @param \DateTime $reminderDate
     *
     * @return ClientDate
     */
    public function setReminderDate($reminderDate)
    {
        $this->reminderDate = $reminderDate;

        return $this;
    }

    /**
     * Get reminderDate
     *
     * @return \DateTime
     */
    public function getReminderDate()
    {
        return $this->reminderDate;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ClientDate
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientDate
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientDate
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientDate
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
