<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientWPMInvestment
 *
 * @ORM\Table(name="client_wpm_investment")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ClientWPMInvestment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="clientWPMInvestments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="risk_class", type="string", length=255, nullable=true)
     */
    private $riskClass;

    /**
     * @var boolean
     *
     * @ORM\Column(name="liquidate_on_death", type="boolean", nullable=true)
     */
    private $liquidateOnDeath;

    /**
     * @var \AppBundle\Entity\Country
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="ownership_type", type="string", length=255, nullable=true)
     */
    private $ownershipType;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=1000, nullable=true)
     */
    private $owner;

    /**
     * @var float
     *
     * @ORM\Column(name="percent_ownership", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $percentOwnership;

    /**
     * @var float
     *
     * @ORM\Column(name="original_value", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $originalValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="original_valuation_date", type="datetime", nullable=true)
     */
    private $originalValuationDate;

    /**
     * @var float
     *
     * @ORM\Column(name="original_loan_amount", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $originalLoanAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="current_loan_amount", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $currentLoanAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="current_market_value", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $currentMarketValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="current_valuation_date", type="datetime", nullable=true)
     */
    private $currentValuationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="investment_mode", type="string", length=255, nullable=true)
     */
    private $investmentMode;

    /**
     * @var string
     *
     * @ORM\Column(name="source_funds", type="string", length=255, nullable=true)
     */
    private $sourceFunds;


    /**
     * @var string
     *
     * @ORM\Column(name="distribution", type="string", length=255, nullable=true)
     */
    private $distribution;

    /**
     * @var string
     *
     * @ORM\Column(name="special_law", type="string", length=255, nullable=true)
     */
    private $specialLaw;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiaries", type="text",  length=1000, nullable=true)
     */
    private $beneficiaries;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks", type="text", length=2000, nullable=true)
     */
    private $remarks;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ClientWPMInvestment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set riskClass
     *
     * @param string $riskClass
     *
     * @return ClientWPMInvestment
     */
    public function setRiskClass($riskClass)
    {
        $this->riskClass = $riskClass;

        return $this;
    }

    /**
     * Get riskClass
     *
     * @return string
     */
    public function getRiskClass()
    {
        return $this->riskClass;
    }

    /**
     * Set ownershipType
     *
     * @param string $ownershipType
     *
     * @return ClientWPMInvestment
     */
    public function setOwnershipType($ownershipType)
    {
        $this->ownershipType = $ownershipType;

        return $this;
    }

    /**
     * Get ownershipType
     *
     * @return string
     */
    public function getOwnershipType()
    {
        return $this->ownershipType;
    }

    /**
     * Set owner
     *
     * @param string $owner
     *
     * @return ClientWPMInvestment
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set percentOwnership
     *
     * @param string $percentOwnership
     *
     * @return ClientWPMInvestment
     */
    public function setPercentOwnership($percentOwnership)
    {
        $this->percentOwnership = $percentOwnership;

        return $this;
    }

    /**
     * Get percentOwnership
     *
     * @return string
     */
    public function getPercentOwnership()
    {
        return $this->percentOwnership;
    }

    /**
     * Set originalValue
     *
     * @param string $originalValue
     *
     * @return ClientWPMInvestment
     */
    public function setOriginalValue($originalValue)
    {
        $this->originalValue = $originalValue;

        return $this;
    }

    /**
     * Get originalValue
     *
     * @return string
     */
    public function getOriginalValue()
    {
        return $this->originalValue;
    }

    /**
     * Set originalValuationDate
     *
     * @param \DateTime $originalValuationDate
     *
     * @return ClientWPMInvestment
     */
    public function setOriginalValuationDate($originalValuationDate)
    {
        $this->originalValuationDate = $originalValuationDate;

        return $this;
    }

    /**
     * Get originalValuationDate
     *
     * @return \DateTime
     */
    public function getOriginalValuationDate()
    {
        return $this->originalValuationDate;
    }

    /**
     * Set originalLoanAmount
     *
     * @param string $originalLoanAmount
     *
     * @return ClientWPMInvestment
     */
    public function setOriginalLoanAmount($originalLoanAmount)
    {
        $this->originalLoanAmount = $originalLoanAmount;

        return $this;
    }

    /**
     * Get originalLoanAmount
     *
     * @return string
     */
    public function getOriginalLoanAmount()
    {
        return $this->originalLoanAmount;
    }

    /**
     * Set currentLoanAmount
     *
     * @param string $currentLoanAmount
     *
     * @return ClientWPMInvestment
     */
    public function setCurrentLoanAmount($currentLoanAmount)
    {
        $this->currentLoanAmount = $currentLoanAmount;

        return $this;
    }

    /**
     * Get currentLoanAmount
     *
     * @return float
     */
    public function getCurrentLoanAmount()
    {
        return $this->currentLoanAmount;
    }

    /**
     * Set currentMarketValue
     *
     * @param string $currentMarketValue
     *
     * @return ClientWPMInvestment
     */
    public function setCurrentMarketValue($currentMarketValue)
    {
        $this->currentMarketValue = $currentMarketValue;

        return $this;
    }

    /**
     * Get currentMarketValue
     *
     * @return float
     */
    public function getCurrentMarketValue()
    {
        return $this->currentMarketValue;
    }

    /**
     * Set currentValuationDate
     *
     * @param \DateTime $currentValuationDate
     *
     * @return ClientWPMInvestment
     */
    public function setCurrentValuationDate($currentValuationDate)
    {
        $this->currentValuationDate = $currentValuationDate;

        return $this;
    }

    /**
     * Get currentValuationDate
     *
     * @return \DateTime
     */
    public function getCurrentValuationDate()
    {
        return $this->currentValuationDate;
    }

    /**
     * Set investmentMode
     *
     * @param string $investmentMode
     *
     * @return ClientWPMInvestment
     */
    public function setInvestmentMode($investmentMode)
    {
        $this->investmentMode = $investmentMode;

        return $this;
    }

    /**
     * Get investmentMode
     *
     * @return string
     */
    public function getInvestmentMode()
    {
        return $this->investmentMode;
    }

    /**
     * Set sourceFunds
     *
     * @param string $sourceFunds
     *
     * @return ClientWPMInvestment
     */
    public function setSourceFunds($sourceFunds)
    {
        $this->sourceFunds = $sourceFunds;

        return $this;
    }

    /**
     * Get sourceFunds
     *
     * @return string
     */
    public function getSourceFunds()
    {
        return $this->sourceFunds;
    }

    /**
     * Set distribution
     *
     * @param string $distribution
     *
     * @return ClientWPMInvestment
     */
    public function setDistribution($distribution)
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Get distribution
     *
     * @return string
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * Set specialLaw
     *
     * @param string $specialLaw
     *
     * @return ClientWPMInvestment
     */
    public function setSpecialLaw($specialLaw)
    {
        $this->specialLaw = $specialLaw;

        return $this;
    }

    /**
     * Get specialLaw
     *
     * @return string
     */
    public function getSpecialLaw()
    {
        return $this->specialLaw;
    }

    /**
     * Set beneficiaries
     *
     * @param string $beneficiaries
     *
     * @return ClientWPMInvestment
     */
    public function setBeneficiaries($beneficiaries)
    {
        $this->beneficiaries = $beneficiaries;

        return $this;
    }

    /**
     * Get beneficiaries
     *
     * @return string
     */
    public function getBeneficiaries()
    {
        return $this->beneficiaries;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return ClientWPMInvestment
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientWPMInvestment
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientWPMInvestment
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientWPMInvestment
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return ClientWPMInvestment
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ClientWPMInvestment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set liquidateOnDeath
     *
     * @param boolean $liquidateOnDeath
     *
     * @return ClientWPMInvestment
     */
    public function setLiquidateOnDeath($liquidateOnDeath)
    {
        $this->liquidateOnDeath = $liquidateOnDeath;

        return $this;
    }

    /**
     * Get liquidateOnDeath
     *
     * @return boolean
     */
    public function getLiquidateOnDeath()
    {
        return $this->liquidateOnDeath;
    }
}
