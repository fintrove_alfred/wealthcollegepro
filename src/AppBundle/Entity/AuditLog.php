<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AuditLog
 *
 * @ORM\Table(name="audit_log", indexes={@ORM\Index(name="idx_link", columns={"link", "link_id"}), @ORM\Index(name="fk_audit_log_user_id", columns={"user_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class AuditLog
{

    const ACTION_CREATE = 1;
    const ACTION_READ = 2;
    const ACTION_UPDATE = 3;
    const ACTION_DELETE = 4;

    const ACTION_UPDATE_STATUS = 5;
    const ACTION_UPDATE_MEASURE_UNITS = 6;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     **/
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var integer
     *
     * @ORM\Column(name="link_id", type="integer", nullable=true)
     */
    private $linkId;

    /**
     * @var integer
     *
     * @ORM\Column(name="action_id", type="integer", nullable=false)
     */
    private $actionId;

    /**
     * @var text
     *
     * @ORM\Column(name="meta", type="text", length=65535, nullable=true)
     */
    private $meta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_meta_json", type="boolean", nullable=false, options={"default": 0})
     */
    private $isMetaJSON = 0;

    /**
     * Stores the decoded meta data
     *
     * @var Array
     */
    protected $metaDecoded;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return AuditLog
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set linkId
     *
     * @param integer $linkId
     * @return AuditLog
     */
    public function setLinkId($linkId)
    {
        $this->linkId = $linkId;

        return $this;
    }

    /**
     * Get linkId
     *
     * @return integer 
     */
    public function getLinkId()
    {
        return $this->linkId;
    }

    /**
     * Set actionId
     *
     * @param boolean $actionId
     * @return AuditLog
     */
    public function setActionId($actionId)
    {
        $this->actionId = $actionId;

        return $this;
    }

    /**
     * Get actionId
     *
     * @return boolean 
     */
    public function getActionId()
    {
        return $this->actionId;
    }

    /**
     * Set meta
     *
     * @param string $meta
     * @return AuditLog
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }



    /**
     * Get meta
     *
     * @return string 
     */
    public function getMeta()
    {
        if ($this->getIsMetaJSON()) {
            if (!isset($this->metaDecoded)) {
                $this->metaDecoded = json_decode($this->meta, true);
            }
            return $this->metaDecoded;
        }
        return $this->meta;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return AuditLog
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return AuditLog
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get the user's name and time
     * @return string
     */
    public function getUserAndTime() {
        return $this->getUser()->getName().' on '.$this->getCreated()->format('Y-m-d H:i');
    }


    /**
     * Set isMetaJSON
     *
     * @param boolean $isMetaJSON
     * @return AuditLog
     */
    public function setIsMetaJSON($isMetaJSON)
    {
        $this->isMetaJSON = $isMetaJSON;

        return $this;
    }

    /**
     * Get isMetaJSON
     *
     * @return boolean 
     */
    public function getIsMetaJSON()
    {
        return $this->isMetaJSON;
    }

    /**
     * @return string
     */
    public function getActionName() {
        switch ($this->getActionId()) {
            case self::ACTION_CREATE: return 'created';
            case self::ACTION_DELETE: return 'deleted';
            case self::ACTION_READ: return 'viewed';
            case self::ACTION_UPDATE: return 'updated';
            case self::ACTION_UPDATE_MEASURE_UNITS: return 'measure units updated';
            case self::ACTION_UPDATE_STATUS: return 'status updated';
        }
    }
}
