<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Person
 *
 * @ORM\Table(name="client_relation")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ClientRelation
{

    const RELATIONSHIP_TYPE_PARENT = 1;
    const RELATIONSHIP_TYPE_CHILD = 2;
    const RELATIONSHIP_TYPE_SIBLING = 3;
    const RELATIONSHIP_TYPE_IN_LAW = 4;
    const RELATIONSHIP_TYPE_PARTNER = 5;
    const RELATIONSHIP_TYPE_COUSIN = 6;
    const RELATIONSHIP_TYPE_NEPHEW = 7;
    const RELATIONSHIP_TYPE_NIECE = 8;
    const RELATIONSHIP_TYPE_FRIEND = 9;
    const RELATIONSHIP_TYPE_BUSINESS_PARTNER = 10;
    const RELATIONSHIP_TYPE_BUSINESS_CONTACT = 11;
    const RELATIONSHIP_TYPE_FATHER = 12;
    const RELATIONSHIP_TYPE_MOTHER = 13;
    const RELATIONSHIP_TYPE_SON = 14;
    const RELATIONSHIP_TYPE_DAUGHTER = 15;
    const RELATIONSHIP_TYPE_BROTHER = 16;
    const RELATIONSHIP_TYPE_SISTER = 17;
    const RELATIONSHIP_TYPE_HUSBAND = 18;
    const RELATIONSHIP_TYPE_WIFE = 19;
    const RELATIONSHIP_TYPE_UNCLE = 20;
    const RELATIONSHIP_TYPE_AUNTIE = 21;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="clientRelations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $client;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="related_client_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $relatedClient;

    /**
     * @var integer
     *
     * @ORM\Column(name="relationship_type_id", type="integer", nullable=false)
     */
    private $relationshipTypeId;

    /**
     * This field is used to join two client relations together
     *
     * @var \AppBundle\Entity\ClientRelation
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ClientRelation", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_relation_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $clientRelation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;


    /**
     * @return array
     */
    public static function getRelationshipTypes() {

        return [
            self::RELATIONSHIP_TYPE_FATHER => 'Father',
            self::RELATIONSHIP_TYPE_MOTHER => 'Mother',
            self::RELATIONSHIP_TYPE_SON => 'Son',
            self::RELATIONSHIP_TYPE_DAUGHTER => 'Daughter',
            self::RELATIONSHIP_TYPE_BROTHER => 'Brother',
            self::RELATIONSHIP_TYPE_SISTER => 'Sister',
            self::RELATIONSHIP_TYPE_IN_LAW => 'In-Law',
            self::RELATIONSHIP_TYPE_HUSBAND => 'Husband',
            self::RELATIONSHIP_TYPE_WIFE => 'Wife',
            self::RELATIONSHIP_TYPE_COUSIN => 'Cousin',
            self::RELATIONSHIP_TYPE_UNCLE => 'Uncle',
            self::RELATIONSHIP_TYPE_AUNTIE => 'Auntie',
            self::RELATIONSHIP_TYPE_NEPHEW => 'Nephew',
            self::RELATIONSHIP_TYPE_NIECE => 'Niece',
            self::RELATIONSHIP_TYPE_FRIEND => 'Friend',
            self::RELATIONSHIP_TYPE_BUSINESS_PARTNER => 'Business Partner',
            self::RELATIONSHIP_TYPE_BUSINESS_CONTACT => 'Business Contact',
        ];

    }

    /**
     * @return int
     */
    public static function getOppositeRelationshipType(ClientRelation $client) {

        switch ($client->getRelationshipTypeId()) {
            case self::RELATIONSHIP_TYPE_FATHER:
            case self::RELATIONSHIP_TYPE_MOTHER:
                if ($client->getRelatedClient()->getPerson()->getGender() == 'F') {
                    return self::RELATIONSHIP_TYPE_DAUGHTER;
                } else {
                    return self::RELATIONSHIP_TYPE_SON;
                }
            case self::RELATIONSHIP_TYPE_SON:
            case self::RELATIONSHIP_TYPE_DAUGHTER:
                if ($client->getRelatedClient()->getPerson()->getGender() == 'F') {
                    return self::RELATIONSHIP_TYPE_MOTHER;
                } else {
                    return self::RELATIONSHIP_TYPE_FATHER;
                }
            case self::RELATIONSHIP_TYPE_BROTHER:
            case self::RELATIONSHIP_TYPE_SISTER:
                if ($client->getRelatedClient()->getPerson()->getGender() == 'F') {
                    return self::RELATIONSHIP_TYPE_SISTER;
                } else {
                    return self::RELATIONSHIP_TYPE_BROTHER;
                }
            case self::RELATIONSHIP_TYPE_IN_LAW:
                return self::RELATIONSHIP_TYPE_IN_LAW;
            case self::RELATIONSHIP_TYPE_HUSBAND:
            case self::RELATIONSHIP_TYPE_WIFE:
                if ($client->getRelatedClient()->getPerson()->getGender() == 'F') {
                    return self::RELATIONSHIP_TYPE_WIFE;
                } else {
                    return self::RELATIONSHIP_TYPE_HUSBAND;
                }
            case self::RELATIONSHIP_TYPE_COUSIN:
                return self::RELATIONSHIP_TYPE_COUSIN;
            case self::RELATIONSHIP_TYPE_UNCLE:
            case self::RELATIONSHIP_TYPE_AUNTIE:
                if ($client->getRelatedClient()->getPerson()->getGender() == 'F') {
                    return self::RELATIONSHIP_TYPE_NIECE;
                } else {
                    return self::RELATIONSHIP_TYPE_NEPHEW;
                }
            case self::RELATIONSHIP_TYPE_NEPHEW:
            case self::RELATIONSHIP_TYPE_NIECE:
                if ($client->getRelatedClient()->getPerson()->getGender() == 'F') {
                    return self::RELATIONSHIP_TYPE_AUNTIE;
                } else {
                    return self::RELATIONSHIP_TYPE_UNCLE;
                }
            case self::RELATIONSHIP_TYPE_FRIEND:
                return self::RELATIONSHIP_TYPE_FRIEND;
            case self::RELATIONSHIP_TYPE_BUSINESS_PARTNER:
                return self::RELATIONSHIP_TYPE_BUSINESS_PARTNER;
            case self::RELATIONSHIP_TYPE_BUSINESS_CONTACT:
                return self::RELATIONSHIP_TYPE_BUSINESS_CONTACT;
        };

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set relationshipTypeId
     *
     * @param integer $relationshipTypeId
     *
     * @return ClientRelation
     */
    public function setRelationshipTypeId($relationshipTypeId, $updateLinked = true)
    {
        $this->relationshipTypeId = $relationshipTypeId;
        //if ($updateLinked && $this->getClientRelation()) {
        //    $oppositeRelationshipTypeId = self::getOppositeRelationshipType($this);
        //    $this->getClientRelation()->setRelationshipTypeId($oppositeRelationshipTypeId ? $oppositeRelationshipTypeId : $relationshipTypeId, false);
        //}
        return $this;
    }

    /**
     * Get relationshipTypeId
     *
     * @return integer
     */
    public function getRelationshipTypeId()
    {
        return $this->relationshipTypeId;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ClientRelation
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientRelation
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientRelation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientRelation
     */
    public function setClient(\AppBundle\Entity\Client $client = null, $updateLinked = true)
    {
        $this->client = $client;
        //if ($updateLinked && $client && $this->getClientRelation()) {
        //    $this->getClientRelation()->setRelatedClient($client, false);
        //}
        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set relatedClient
     *
     * @param \AppBundle\Entity\Client $relatedClient
     *
     * @return ClientRelation
     */
    public function setRelatedClient(\AppBundle\Entity\Client $relatedClient = null, $updateLinked = true)
    {
        $this->relatedClient = $relatedClient;
        //if ($updateLinked && $relatedClient && $this->getClientRelation()) {
        //    $this->getClientRelation()->setClient($relatedClient, false);
        //}
        return $this;
    }

    /**
     * Get relatedClient
     *
     * @return \AppBundle\Entity\Client
     */
    public function getRelatedClient()
    {
        return $this->relatedClient;
    }


    /**
     * Set clientRelation
     *
     * @param \AppBundle\Entity\ClientRelation $clientRelation
     *
     * @return ClientRelation
     */
    public function setClientRelation(\AppBundle\Entity\ClientRelation $clientRelation)
    {
        $this->clientRelation = $clientRelation;

        return $this;
    }

    /**
     * Get clientRelation
     *
     * @return \AppBundle\Entity\ClientRelation
     */
    public function getClientRelation()
    {
        return $this->clientRelation;
    }

    
}
