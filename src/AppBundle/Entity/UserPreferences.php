<?php
/**
 * UserPreferences.php
 *
 * Class to represent user preferences. Should use this class to get and set user preferences on the User entity
 *
 * @author Gul
 */

namespace AppBundle\Entity;


class UserPreferences
{

   protected $preferences;

   public function __construct($preferences = [])
   {
      $this->preferences = (array)$preferences;
   }

   public function getPreferences() {
      return $this->preferences;
   }

   /**
    * Get the calendar event types the user has selected to appear on their calendar
    *
    * @return array
    */
   public function getSelectedCalendarEventTypeIDs() {
      return !empty($this->preferences['calendar-event-type-ids']) ? $this->preferences['calendar-event-type-ids'] : [];
   }

   /**
    * Set the calendar event types the user has selected to appear on their calendar
    *
    * @param array $calendarEventTypeIDs
    * @return array
    */
   public function setSelectedCalendarEventTypeIDs(array $calendarEventTypeIDs) {
      $this->preferences['calendar-event-type-ids'] = $calendarEventTypeIDs;
   }

   /**
    * Get the google access token for this user
    *
    * @return array
    */
   public function getGoogleAccessToken() {
      return !empty($this->preferences['google-access-token']) ? $this->preferences['google-access-token'] : null;
   }

   /**
    * Set the access token
    *
    * @param String $token
    * @return array
    */
   public function setGoogleAccessToken($token) {
      $this->preferences['google-access-token'] = $token;
   }

   /**
    * Get the google refresh token for this user
    *
    * @return array
    */
   public function getGoogleRefreshToken() {
      return !empty($this->preferences['google-refresh-token']) ? $this->preferences['google-refresh-token'] : null;
   }

   /**
    * Set the refresh token
    *
    * @param String $token
    * @return array
    */
   public function setGoogleRefreshToken($token) {
      $this->preferences['google-refresh-token'] = $token;
   }

   /**
    * Get the google calendar ids
    *
    * @return array
    */
   public function getGoogleCalendarIds() {
      return !empty($this->preferences['google-calendar-ids']) ? $this->preferences['google-calendar-ids'] : null;
   }

   /**
    * Set the calendar event types the user has selected to appear on their calendar
    *
    * @param String $token
    * @return array
    */
   public function setGoogleCalendarIds(array $calendarIds) {
      $this->preferences['google-calendar-ids'] = $calendarIds;
   }


   /**
    * Get the user's timezone
    *
    * @return array
    */
   public function getTimezone() {
      return !empty($this->preferences['timezone']) ? $this->preferences['timezone'] : null;
   }

   /**
    * Set user's timezone
    *
    * @param String $token
    * @return array
    */
   public function setTimezone($timezone) {
      $this->preferences['timezone'] = $timezone;
   }

}