<?php
/**
 * AppBundle\Entity\Webinar.php
 *
 * @author: Gul  
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Webinar
 *
 * @ORM\Table(name="webinar")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Webinar {


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\CalendarEvent
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CalendarEvent", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calendar_event_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $calendarEvent;

    /**
     * @var \AppBundle\Entity\FinancialAdviser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FinancialAdviser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financial_adviser_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $financialAdviser;

    /**
     * @var String
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Webinar
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Webinar
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Webinar
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Webinar
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set calendarEvent
     *
     * @param \AppBundle\Entity\CalendarEvent $calendarEvent
     *
     * @return Webinar
     */
    public function setCalendarEvent(\AppBundle\Entity\CalendarEvent $calendarEvent)
    {
        $this->calendarEvent = $calendarEvent;

        return $this;
    }

    /**
     * Get calendarEvent
     *
     * @return \AppBundle\Entity\CalendarEvent
     */
    public function getCalendarEvent()
    {
        return $this->calendarEvent;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return Webinar
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }

    public function setExpectedRunningTIme(\DateTime $time) {

        if ($this->getCalendarEvent() && $this->getCalendarEvent()->getStartAt()) {
            $startAt = $this->getCalendarEvent()->getStartAt();
            $endAt = new \DateTime();
            $endAt->setTimestamp($startAt->getTimestamp());
            $dateInterval = new \DateInterval('PT'.$time->format('G').'H'.intval($time->format('i')).'M');
            $endAt->add($dateInterval);
            $this->getCalendarEvent()->setEndAt($endAt);
        }

    }

    public function getExpectedRunningTIme() {

        $datetime = new \DateTime();
        if ($this->getCalendarEvent() && $this->getCalendarEvent()->getStartAt() && $this->getCalendarEvent()->getEndAt()) {
            $runningTime = $this->getCalendarEvent()->getStartAt()->diff($this->getCalendarEvent()->getEndAt());
            $datetime->setTime($runningTime->h, $runningTime->i, 0);
        } else {
            $datetime->setTime(1, 0, 0);
        }
        return $datetime;
    }


}
