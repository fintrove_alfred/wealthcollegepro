<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Person
 *
 * @ORM\Table(name="review_template_question")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ReviewTemplateQuestion
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\ReviewTemplate
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ReviewTemplate", inversedBy="reviewTemplateQuestions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="review_template_id", referencedColumnName="id")
     * })
     */
    private $reviewTemplate;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text", nullable=false)
     * @Assert\NotNull(message="Question cannot be blank")
     */
    private $question;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     */
    private $archivedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return ReviewTemplateQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set archivedAt
     *
     * @param \DateTime $archivedAt
     *
     * @return ReviewTemplateQuestion
     */
    public function setArchivedAt($archivedAt)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * Get archivedAt
     *
     * @return \DateTime
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ReviewTemplateQuestion
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ReviewTemplateQuestion
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set reviewTemplate
     *
     * @param \AppBundle\Entity\ReviewTemplate $reviewTemplate
     *
     * @return ReviewTemplateQuestion
     */
    public function setReviewTemplate(\AppBundle\Entity\ReviewTemplate $reviewTemplate = null)
    {
        $this->reviewTemplate = $reviewTemplate;

        return $this;
    }

    /**
     * Get reviewTemplate
     *
     * @return \AppBundle\Entity\ReviewTemplate
     */
    public function getReviewTemplate()
    {
        return $this->reviewTemplate;
    }
}
