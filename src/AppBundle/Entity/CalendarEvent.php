<?php
/**
 * AppBundle\Entity\CalendarEvent.php
 *
 * @author: Gul  
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * CalendarEvent
 *
 * @ORM\Table(name="calendar_event", indexes={
 *    @ORM\Index(name="idx_type_user", columns={"user_id","calendar_event_type_id"}),
 *    @ORM\Index(name="idx_date_range", columns={"start_at", "end_at"}),
 *    @ORM\Index(name="idx_alert_time", columns={"alert_type_id", "alert_date_time"})
 * })
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class CalendarEvent {

    const ALERT_TYPE_NONE = 1;
    const ALERT_TYPE_DAY_OF_EVENT = 2;
    const ALERT_TYPE_DAYS_BEFORE = 3;
    const ALERT_TYPE_DAYS_AFTER = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var CalendarEventType
     * @ORM\ManyToOne(targetEntity="CalendarEventType")
     * @ORM\JoinColumn(name="calendar_event_type_id", referencedColumnName="id")
     **/
    private $calendarEventType;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=255, nullable=true)
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=5000, nullable=true)
     */
    private $description;



    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_at", type="datetime", nullable=false)
     * @Assert\Time()
     * @Assert\NotBlank(message="Start time must be set")
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_at", type="datetime", nullable=false)
     * @Assert\Time()
     * @Assert\NotBlank(message="End time must be set")
     */
    private $endAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="alert_type_id", type="integer", nullable=false)
     * @Assert\NotBlank(message="An alert option must be selected")
     */
    private $alertTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="alert_days", type="integer", nullable=true)
     */
    private $alertDays;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alert_time", type="time", nullable=true)
     */
    private $alertTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alert_date_time", type="datetime", nullable=true)
     */
    private $alertDateTime;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CalendarEventAttendee", mappedBy="calendarEvent", cascade={"persist"})
     */
    private $calendarEventAttendees;

    /**
     * true if the event needs to be synced with google
     * @var boolean
     *
     * @ORM\Column(name="dirty", type="boolean", nullable=false, options={"default": 1})
     */
    private $dirty = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="synced_at", type="datetime", nullable=true)
     */
    private $syncedAt;

    
    
    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->calendarEventAttendees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return CalendarEvent
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CalendarEvent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return CalendarEvent
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return CalendarEvent
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return CalendarEvent
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Has this webinar ended
     * @return bool
     */
    public function hasEnded() {
        if ($this->getEndAt()) {
            return $this->getEndAt()->getTimestamp() < time();
        }
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return CalendarEvent
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return CalendarEvent
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return CalendarEvent
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return CalendarEvent
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set calendarEventType
     *
     * @param \AppBundle\Entity\CalendarEventType $calendarEventType
     *
     * @return CalendarEvent
     */
    public function setCalendarEventType(\AppBundle\Entity\CalendarEventType $calendarEventType = null)
    {
        $this->calendarEventType = $calendarEventType;

        return $this;
    }

    /**
     * Get calendarEventType
     *
     * @return \AppBundle\Entity\CalendarEventType
     */
    public function getCalendarEventType()
    {
        return $this->calendarEventType;
    }

    /**
     * Set alertTypeId
     *
     * @param integer $alertTypeId
     *
     * @return CalendarEvent
     */
    public function setAlertTypeId($alertTypeId)
    {
        $this->alertTypeId = $alertTypeId;

        return $this;
    }

    /**
     * Get alertTypeId
     *
     * @return integer
     */
    public function getAlertTypeId()
    {
        return $this->alertTypeId;
    }

    /**
     * Set alertDays
     *
     * @param integer $alertDays
     *
     * @return CalendarEvent
     */
    public function setAlertDays($alertDays)
    {
        $this->alertDays = $alertDays;

        return $this;
    }

    /**
     * Get alertDays
     *
     * @return integer
     */
    public function getAlertDays()
    {
        return $this->alertDays;
    }

    /**
     * Set alertTime
     *
     * @param \DateTime $alertTime
     *
     * @return CalendarEvent
     */
    public function setAlertTime($alertTime)
    {
        $this->alertTime = $alertTime;

        return $this;
    }

    /**
     * Get alertTime
     *
     * @return \DateTime
     */
    public function getAlertTime()
    {
        return $this->alertTime;
    }

    /**
     * Get number of munites before the event to alert the user
     *
     * @return \DateTime
     */
    public function getAlertMinutes()
    {
        if ($this->getAlertTypeId() == self::ALERT_TYPE_DAYS_BEFORE) {
            return 0;
        }
        if ($this->getAlertDateTime()) {
            $diff = $this->getStartAt()->getTimestamp() - $this->getAlertDateTime()->getTimestamp();

            return intval($diff / 60);
        }

    }

    /**
     * Set alertDateTime
     *
     * @param \DateTime $alertDateTime
     *
     * @return CalendarEvent
     */
    public function setAlertDateTime($alertDateTime)
    {
        $this->alertDateTime = $alertDateTime;

        return $this;
    }

    /**
     * Get alertDateTime
     *
     * @return \DateTime
     */
    public function getAlertDateTime()
    {
        return $this->alertDateTime;
    }

    public static function getAlertTypeOptions() {
        return [
            self::ALERT_TYPE_NONE => 'No alert',
            self::ALERT_TYPE_DAY_OF_EVENT => 'Day of event',
            self::ALERT_TYPE_DAYS_BEFORE => 'Number of days before',
            self::ALERT_TYPE_DAYS_AFTER => 'Number of days after',
        ];
    }

    /**
     * Set the alert datetime depending on the selected alert type and selected number of days and time of the alert
     *
     * @ORM\PrePersist
     */
    public function setDerivedAlertDateTime()
    {
        if ($this->getAlertTypeId() != self::ALERT_TYPE_NONE && $this->getAlertTime()) {
            switch ($this->getAlertTypeId()) {
                case self::ALERT_TYPE_DAY_OF_EVENT:
                    $time = new \DateTime($this->getStartAt()->format('d-m-Y') . ' '.$this->getAlertTime()->format('H:i'));
                    $this->setAlertDateTime($time);
                  break;
                case self::ALERT_TYPE_DAYS_BEFORE:
                    $startDate = clone $this->getStartAt();
                    $alertDateTime = $startDate->sub(new \DateInterval('P'.((int)$this->getAlertDays()).'D'));
                    $this->setAlertDateTime($alertDateTime);
                    break;
                case self::ALERT_TYPE_DAYS_AFTER:
                    $endDate = clone $this->getEndAt();
                    $alertDateTime = $endDate->add(new \DateInterval('P'.((int)$this->getAlertDays()).'D'));
                    $this->setAlertDateTime($alertDateTime);
                    break;
            }
        }
    }

    /**
     * Custom validation
     *
     * @param ExecutionContextInterface $context
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {

        // times are valid
        if ($this->getStartAt()->getTimestamp() >= $this->getEndAt()->getTimestamp()) {
            $context->buildViolation($this->getCalendarEventType()->getName(). ' start time must be before the end time')
               ->atPath('startAt')
               ->addViolation();
        }
        if (!$this->getAlertTypeId()) {
            $context->buildViolation($this->getCalendarEventType()->getName(). ' alert type was not selected')
               ->atPath('alertTypeId')
               ->addViolation();
        }
        if (in_array($this->getAlertTypeId(), [self::ALERT_TYPE_DAYS_AFTER, self::ALERT_TYPE_DAYS_BEFORE])) {
            if (!is_numeric($this->getAlertDays())) {
                $context->buildViolation($this->getCalendarEventType()->getName(). ' alert days must be a number')
                   ->atPath('alertDays')
                   ->addViolation();
            }
        }
        if ($this->getAlertTypeId() != self::ALERT_TYPE_NONE && !$this->getAlertTime()) {
            $context->buildViolation($this->getCalendarEventType()->getName(). ' alert time must be set')
               ->atPath('alertTime')
               ->addViolation();
        }

    }

    /**
     * Add calendarEventAttendee
     *
     * @param \AppBundle\Entity\CalendarEventAttendee $calendarEventAttendee
     *
     * @return CalendarEvent
     */
    public function addCalendarEventAttendee(\AppBundle\Entity\CalendarEventAttendee $calendarEventAttendee)
    {
        $this->calendarEventAttendees[] = $calendarEventAttendee;

        return $this;
    }

    /**
     * Remove calendarEventAttendee
     *
     * @param \AppBundle\Entity\CalendarEventAttendee $calendarEventAttendee
     */
    public function removeCalendarEventAttendee(\AppBundle\Entity\CalendarEventAttendee $calendarEventAttendee)
    {
        $this->calendarEventAttendees->removeElement($calendarEventAttendee);
    }

    /**
     * Get calendarEventAttendees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalendarEventAttendees()
    {
        return $this->calendarEventAttendees;
    }

    /**
     * Is this client an attendee of this event
     * @param Client $client
     * @return bool
     */
    public function hasAttendee(Client $client)
    {
        if ($this->getCalendarEventAttendees()) {
            foreach ($this->getCalendarEventAttendees() as $calendarEventAttendee) {
                if ($calendarEventAttendee->getClient()->getId() == $client->getId()) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Set externalId
     *
     * @param string $externalId
     *
     * @return CalendarEvent
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set dirty
     *
     * @param boolean $dirty
     *
     * @return CalendarEvent
     */
    public function setDirty($dirty)
    {
        $this->dirty = $dirty;

        return $this;
    }

    /**
     * Is dirty, if true has not yet been synced with google
     *
     * @return boolean
     */
    public function getDirty()
    {
        return $this->dirty;
    }
    /**
     * Is dirty, if true has not yet been synced with google
     *
     * @return boolean
     */
    public function isDirty()
    {
        return $this->dirty;
    }

    /**
     * Set syncedAt
     *
     * @param \DateTime $syncedAt
     *
     * @return CalendarEvent
     */
    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;

        return $this;
    }

    /**
     * Get syncedAt
     *
     * @return \DateTime
     */
    public function getSyncedAt()
    {
        return $this->syncedAt;
    }

    /**
     * Is this a webinar calendar event
     * @return bool
     */
    public function isWebinar() {
        return $this->getCalendarEventType() && $this->getCalendarEventType()->getHandle() == 'FINTRAIN_SESSIONS';
    }


    /**
     * Get the event duration
     * @return \DateTime
     */
    public function getDuration() {

        $duration = new \DateInterval('PT1H');
        if ($this->getStartAt() && $this->getEndAt()) {
            $duration = $this->getStartAt()->diff($this->getEndAt());
        }
        return $duration;
    }

    /**
     * Get the eta to the event
     * @return \DateTime
     */
    public function getEta() {

        $eta = new \DateInterval('P1D');
        if ($this->getStartAt()) {
            $now = new \DateTime();
            $eta = $now->diff($this->getStartAt());

        }
        return $eta;
    }

}
