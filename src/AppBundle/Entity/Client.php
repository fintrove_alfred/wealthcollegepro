<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Client
{

    const TYPE_PROSPECT = 1;
    const TYPE_CLIENT = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\FinancialAdviser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FinancialAdviser", inversedBy="clients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financial_adviser_id", referencedColumnName="id")
     * })
     */
    private $financialAdviser;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=true)
     */
    private $typeId;

    /**
     * @var \AppBundle\Entity\Person
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Person", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * })
     */
    private $person;

    /**
     * @var \AppBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * })
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="website1", type="string", length=255, nullable=true)
     */
    private $website1;

    /**
     * @var string
     *
     * @ORM\Column(name="website2", type="string", length=255, nullable=true)
     */
    private $website2;

    /**
     * @var string
     *
     * @ORM\Column(name="website3", type="string", length=255, nullable=true)
     */
    private $website3;

    /**
     * @var string
     *
     * @ORM\Column(name="occupation", type="string", length=255, nullable=true)
     */
    private $occupation;

    /**
     * @var string
     *
     * @ORM\Column(name="job_title", type="string", length=255, nullable=true)
     */
    private $jobTitle;


    /**
     * @var \AppBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_address", referencedColumnName="id")
     * })
     */
    private $companyAddress;

    /**
     * @var \AppBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="referred_by", referencedColumnName="id")
     * })
     */
    private $referredBy;

    /**
     * @var \AppBundle\Entity\Upload
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="photo_upload_id", referencedColumnName="id")
     * })
     */
    private $photoUpload;

    /**
     * @var \AppBundle\Entity\ClientImport
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ClientImport", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_import_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $clientImport;

    /**
     * @var string
     *
     * @ORM\Column(name="race", type="string", length=255, nullable=true)
     */
    private $race;

    /**
     * @var \AppBundle\Entity\Country
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nationality_country_id", referencedColumnName="id")
     * })
     */
    private $nationalityCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="religion", type="string", length=255, nullable=true)
     */
    private $religion;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin", type="string", length=255, nullable=true)
     */
    private $linkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientDate", mappedBy="client", cascade={"persist"})
     */
    private $clientDates;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection     
     */
    private $clientGroups;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientClientGroup", mappedBy="client", cascade={"persist"})
     */
    private $clientClientGroups;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientRelation", mappedBy="client", cascade={"persist"})
     */
    private $clientRelations;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientEvent", mappedBy="client", cascade={"persist"})
     */
    private $clientEvents;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientWPMInsurancePolicy", mappedBy="client", cascade={"persist"})
     */
    private $clientWPMInsurancePolicies;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientWPMInvestment", mappedBy="client", cascade={"persist"})
     */
    private $clientWPMInvestments;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientWPMEstate", mappedBy="client", cascade={"persist"})
     */
    private $clientWPMEstates;

    /**
     * @ORM\OneToOne(targetEntity="ClientWPM", mappedBy="client")
     */
    private $clientWPM;

    /**
     * @ORM\OneToOne(targetEntity="ClientWPMCashflow", mappedBy="client")
     */
    private $clientWPMCashflow;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->clientDates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientClientGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientRelations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientEvents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientWPMInsurancePolicies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientWPMInvestments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientWPMEstates = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Client
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Client
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Client
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return Client
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser = null)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     *
     * @return Client
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return Client
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set website1
     *
     * @param string $website1
     *
     * @return Client
     */
    public function setWebsite1($website1)
    {
        $this->website1 = $website1;

        return $this;
    }

    /**
     * Get website1
     *
     * @return string
     */
    public function getWebsite1()
    {
        return $this->website1;
    }

    /**
     * Set website2
     *
     * @param string $website2
     *
     * @return Client
     */
    public function setWebsite2($website2)
    {
        $this->website2 = $website2;

        return $this;
    }

    /**
     * Get website2
     *
     * @return string
     */
    public function getWebsite2()
    {
        return $this->website2;
    }

    /**
     * Set website3
     *
     * @param string $website3
     *
     * @return Client
     */
    public function setWebsite3($website3)
    {
        $this->website3 = $website3;

        return $this;
    }

    /**
     * Get website3
     *
     * @return string
     */
    public function getWebsite3()
    {
        return $this->website3;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     *
     * @return Client
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     *
     * @return Client
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set companyAddress
     *
     * @param \AppBundle\Entity\Address $companyAddress
     *
     * @return Client
     */
    public function setCompanyAddress(\AppBundle\Entity\Address $companyAddress = null)
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    /**
     * Get companyAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * Set referredBy
     *
     * @param \AppBundle\Entity\Client $referredBy
     *
     * @return Client
     */
    public function setReferredBy(\AppBundle\Entity\Client $referredBy = null)
    {
        $this->referredBy = $referredBy;

        return $this;
    }

    /**
     * Get referredBy
     *
     * @return \AppBundle\Entity\Client
     */
    public function getReferredBy()
    {
        return $this->referredBy;
    }

    public function __toString()
    {
        if ($this->getPerson()) {
            return $this->getPerson()->getFullName();
        }
    }

    /**
     * Add clientDate
     *
     * @param \AppBundle\Entity\ClientDate $clientDate
     *
     * @return Client
     */
    public function addClientDate(\AppBundle\Entity\ClientDate $clientDate)
    {
        $clientDate->setClient($this);
        $this->clientDates[] = $clientDate;

        return $this;
    }

    /**
     * Remove clientDate
     *
     * @param \AppBundle\Entity\ClientDate $clientDate
     */
    public function removeClientDate(\AppBundle\Entity\ClientDate $clientDate)
    {
        $clientDate->setDeletedAt(new \DateTime());
        $this->clientDates->removeElement($clientDate);
    }

    /**
     * Get clientDates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientDates()
    {
        return $this->clientDates;
    }


    /**
     * Add clientRelation
     *
     * @param \AppBundle\Entity\ClientRelation $clientRelation
     *
     * @param bool $addRelated  if true will add the relation to the related client object as well
     * @return Client
     */
    public function addClientRelation(\AppBundle\Entity\ClientRelation $clientRelation, $addRelated = true)
    {
        $clientRelation->setClient($this);

        foreach ($this->getClientRelations() as $existingClientRelation) { /* @var $existingClientRelation ClientRelation */
            if ($existingClientRelation->getRelatedClient()->getId() == $clientRelation->getRelatedClient()->getId()) {
                return;
            }
        }

        $this->clientRelations[] = $clientRelation;

//        if ($addRelated && $clientRelation->getRelatedClient()) {
//            $relatedClientClientRelation = clone $clientRelation;
//            $relatedClientClientRelation->setClient($clientRelation->getRelatedClient());
//            $relatedClientClientRelation->setRelatedClient($clientRelation->getClient());
//            // link them together
//            $relatedClientClientRelation->setClientRelation($clientRelation);
//            $clientRelation->setClientRelation($relatedClientClientRelation);
//
//            $clientRelation->getRelatedClient()->addClientRelation($relatedClientClientRelation, false);
//        }

        return $this;
    }

    /**
     * Remove clientRelation
     *
     * @param \AppBundle\Entity\ClientRelation $clientRelation
     * @param bool $removeRelated       Iftrue will remove the client relation from the related client object as well
     */
    public function removeClientRelation(\AppBundle\Entity\ClientRelation $clientRelation, $removeRelated = true)
    {
        $clientRelation->setDeletedAt(new \DateTime());
        $this->getClientRelations()->removeElement($clientRelation);

        if (!$removeRelated) {
            return;
        }
//        foreach ($clientRelation->getRelatedClient()->getClientRelations() as $relatedClientClientRelation) { /* @var $relatedClientClientRelation ClientRelation  */
//            if ($relatedClientClientRelation->getRelatedClient()->getId() == $this->getId()) {
//                $clientRelation->getRelatedClient()->removeClientRelation($relatedClientClientRelation, false);
//            }
//        }
    }

    /**
     * Get clientRelations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientRelations()
    {
        return $this->clientRelations;
    }

    /**
     * Add clientClientGroup
     *
     * @param \AppBundle\Entity\ClientClientGroup $clientClientGroup
     *
     * @return Client
     */
    public function addClientClientGroup(\AppBundle\Entity\ClientClientGroup $clientClientGroup)
    {
        // check if client group has already been added
        foreach ($this->getClientClientGroups() as $existingClientClientGroup) {
            if ($clientClientGroup->getClientGroup()->getId() == $existingClientClientGroup->getClientGroup()->getId()) {
                return;
            }
        }

        $this->clientClientGroups[] = $clientClientGroup;

        return $this;
    }

    /**
     * Remove clientClientGroup
     *
     * @param \AppBundle\Entity\ClientClientGroup $clientClientGroup
     */
    public function removeClientClientGroup(\AppBundle\Entity\ClientClientGroup $clientClientGroup)
    {
        $clientClientGroup->setDeletedAt(new \DateTime());
        $this->clientClientGroups->removeElement($clientClientGroup);
    }

    /**
     * Get clientClientGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientClientGroups()
    {
        return $this->clientClientGroups;
    }

    /**
     * Add clientGroup
     *
     * @param \AppBundle\Entity\ClientGroup $clientGroup
     *
     * @return Client
     */
    public function addClientGroup(\AppBundle\Entity\ClientGroup $clientGroup)
    {
        $this->clientGroups[] = $clientGroup;

        return $this;
    }

    /**
     * Check if the client has this client group.
     *
     * @param ClientGroup $clientGroup
     * @return bool
     */
    public function inGroup(ClientGroup $clientGroup) {

        if ($this->getClientGroups()) {
            foreach ($this->getClientGroups() as $currentClientGroup) {
                if ($clientGroup->getId() == $currentClientGroup->getId()) {
                    return true;
                }
            }
        }
        return false;

    }

    /**
     * Remove clientGroup
     *
     * @param \AppBundle\Entity\ClientGroup $clientGroup
     */
    public function removeClientGroup(\AppBundle\Entity\ClientGroup $clientGroup)
    {
        $this->clientGroups->removeElement($clientGroup);
    }

    /**
     * Get clientGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientGroups()
    {
        if (!$this->clientGroups && $this->getClientClientGroups()->count() > 0) {
            $this->clientGroups = new \Doctrine\Common\Collections\ArrayCollection();
            foreach ($this->getClientClientGroups() as $clientClientGroup) {
                $this->clientGroups->add($clientClientGroup->getClientGroup());
            }
        }

        return $this->clientGroups;
    }

    /**
     * Set religion
     *
     * @param string $religion
     *
     * @return Client
     */
    public function setReligion($religion)
    {
        $this->religion = $religion;

        return $this;
    }

    /**
     * Get religion
     *
     * @return string
     */
    public function getReligion()
    {
        return $this->religion;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return Client
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return Client
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     *
     * @return Client
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Client
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set photoUpload
     *
     * @param \AppBundle\Entity\Upload $photoUpload
     *
     * @return Client
     */
    public function setPhotoUpload(\AppBundle\Entity\Upload $photoUpload = null)
    {
        $this->photoUpload = $photoUpload;

        return $this;
    }

    /**
     * Get photoUpload
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getPhotoUpload()
    {
        return $this->photoUpload;
    }

    /**
     * Add clientEvent
     *
     * @param \AppBundle\Entity\ClientEvent $clientEvent
     *
     * @return Client
     */
    public function addClientEvent(\AppBundle\Entity\ClientEvent $clientEvent)
    {
        $this->clientEvents[] = $clientEvent;

        return $this;
    }

    /**
     * Remove clientEvent
     *
     * @param \AppBundle\Entity\ClientEvent $clientEvent
     */
    public function removeClientEvent(\AppBundle\Entity\ClientEvent $clientEvent)
    {
        $this->clientEvents->removeElement($clientEvent);
    }

    /**
     * Get clientEvents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientEvents()
    {
        return $this->clientEvents;
    }

    /**
     * Set clientWPM
     *
     * @param \AppBundle\Entity\ClientWPM $clientWPM
     *
     * @return Client
     */
    public function setClientWPM(\AppBundle\Entity\ClientWPM $clientWPM = null)
    {
        $this->clientWPM = $clientWPM;

        return $this;
    }

    /**
     * Get clientWPM
     *
     * @return \AppBundle\Entity\ClientWPM
     */
    public function getClientWPM()
    {
        return $this->clientWPM;
    }

    /**
     * Set clientWPMCashflow
     *
     * @param \AppBundle\Entity\ClientWPMCashflow $clientWPMCashflow
     *
     * @return Client
     */
    public function setClientWPMCashflow(\AppBundle\Entity\ClientWPMCashflow $clientWPMCashflow = null)
    {
        $this->clientWPMCashflow = $clientWPMCashflow;

        return $this;
    }

    /**
     * Get clientWPMCashflow
     *
     * @return \AppBundle\Entity\ClientWPMCashflow
     */
    public function getClientWPMCashflow()
    {
        return $this->clientWPMCashflow;
    }

    /**
     * Add clientWPMInsurancePolicy
     *
     * @param \AppBundle\Entity\ClientWPMInsurancePolicy $clientWPMInsurancePolicy
     *
     * @return Client
     */
    public function addClientWPMInsurancePolicy(\AppBundle\Entity\ClientWPMInsurancePolicy $clientWPMInsurancePolicy)
    {
        $this->clientWPMInsurancePolicies[] = $clientWPMInsurancePolicy;

        return $this;
    }

    /**
     * Remove clientWPMInsurancePolicy
     *
     * @param \AppBundle\Entity\ClientWPMInsurancePolicy $clientWPMInsurancePolicy
     */
    public function removeClientWPMInsurancePolicy(\AppBundle\Entity\ClientWPMInsurancePolicy $clientWPMInsurancePolicy)
    {
        $this->clientWPMInsurancePolicies->removeElement($clientWPMInsurancePolicy);
    }

    /**
     * Get clientWPMInsurancePolicies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientWPMInsurancePolicies()
    {
        return $this->clientWPMInsurancePolicies;
    }

    /**
     * Add clientWPMInvestment
     *
     * @param \AppBundle\Entity\ClientWPMInvestment $clientWPMInvestment
     *
     * @return Client
     */
    public function addClientWPMInvestment(\AppBundle\Entity\ClientWPMInvestment $clientWPMInvestment)
    {
        $this->clientWPMInvestments[] = $clientWPMInvestment;

        return $this;
    }

    /**
     * Remove clientWPMInvestment
     *
     * @param \AppBundle\Entity\ClientWPMInvestment $clientWPMInvestment
     */
    public function removeClientWPMInvestment(\AppBundle\Entity\ClientWPMInvestment $clientWPMInvestment)
    {
        $this->clientWPMInvestments->removeElement($clientWPMInvestment);
    }

    /**
     * Get clientWPMInvestments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientWPMInvestments()
    {
        return $this->clientWPMInvestments;
    }

    /**
     * Add clientWPMEstate
     *
     * @param \AppBundle\Entity\ClientWPMEstate $clientWPMEstate
     *
     * @return Client
     */
    public function addClientWPMEstate(\AppBundle\Entity\ClientWPMEstate $clientWPMEstate)
    {
        $this->clientWPMEstates[] = $clientWPMEstate;

        return $this;
    }

    /**
     * Remove clientWPMEstate
     *
     * @param \AppBundle\Entity\ClientWPMEstate $clientWPMEstate
     */
    public function removeClientWPMEstate(\AppBundle\Entity\ClientWPMEstate $clientWPMEstate)
    {
        $this->clientWPMEstates->removeElement($clientWPMEstate);
    }

    /**
     * Get clientWPMEstates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientWPMEstates()
    {
        return $this->clientWPMEstates;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     *
     * @return Client
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Get the type name
     * @return string
     */
    public function getTypeName()
    {
        switch ($this->typeId) {
            case self::TYPE_CLIENT: return 'Client';
            case self::TYPE_PROSPECT: return 'Prospect';
            case false: return 'Other';
            default: return 'Other';
        }
    }


    /**
     * Set clientImport
     *
     * @param \AppBundle\Entity\ClientImport $clientImport
     *
     * @return Client
     */
    public function setClientImport(\AppBundle\Entity\ClientImport $clientImport = null)
    {
        $this->clientImport = $clientImport;

        return $this;
    }

    /**
     * Get clientImport
     *
     * @return \AppBundle\Entity\ClientImport
     */
    public function getClientImport()
    {
        return $this->clientImport;
    }

    /**
     * Set race
     *
     * @param string $race
     *
     * @return Client
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Set nationalityCountry
     *
     * @param \AppBundle\Entity\Country $nationalityCountry
     *
     * @return Client
     */
    public function setNationalityCountry(\AppBundle\Entity\Country $nationalityCountry = null)
    {
        $this->nationalityCountry = $nationalityCountry;

        return $this;
    }

    /**
     * Get nationalityCountry
     *
     * @return \AppBundle\Entity\Country
     */
    public function getNationalityCountry()
    {
        return $this->nationalityCountry;
    }
}
