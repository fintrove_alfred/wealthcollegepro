<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users",
 *      indexes={
 *          @ORM\Index(name="fk_image_upload_id", columns={"image_upload_id"}),
 *          @ORM\Index(name="fk_parent_id", columns={"parent_id"}),
 *          @ORM\Index(name="idx_name", columns={"first_name", "last_name"}),
 *      }
 * )
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="parent")
     **/
    private $children;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var FinancialAdviser
     *
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\FinancialAdviser", mappedBy="user", cascade={"persist", "merge"})
     */
    private $financialAdviser;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Please enter your last name.", groups={"Registration", "Profile"})
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=1, nullable=true)
     */
    private $gender;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var \AppBundle\Entity\Upload
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image_upload_id", referencedColumnName="id")
     * })
     */
    private $imageUpload;

    /**
     * @var Array
     *
     * @ORM\Column(name="preferences", type="array", nullable=true)
     */
    private $preferences;

    /**
     * @var UserPreferences
     */
    private $userPreferences;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment", mappedBy="user")
     **/
    private $payments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="user")
     **/
    private $orders;

    public function __construct($id = null)
    {
        parent::__construct();
        // your own logic
        parent::setUsername("empty");

        $this->id = $id;
        $this->roles = array('ROLE_USER');
        $this->children = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }


    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Users
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }


    /**
     * Set deleted
     *
     * @param \DateTime $deletedAt
     * @return DocumentType
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Person
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Person
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get full name
     * @return string
     */
    public function getName($incTitle = false) {
        $fullName = $this->getFirstName().' '.$this->getLastName();
        if ($incTitle && $this->getTitle()) {
            $fullName = $this->getTitle().' '.$fullName;
        }
        return $fullName;
    }

    /**
     * alias to getName
     *
     * @return string
     */
    public function getFullName($incTitle = false) {
        return $this->getName($incTitle);
    }

    /**
     * Get formattedEmail
     * @return string
     */
    public function getFormattedEmail() {
    	$limit = 30;
        return strlen($this->getEmail()) > $limit ? substr($this->getEmail(), 0, $limit) . '...' : $this->getEmail();
    }

    /**
     * Set upload
     *
     * @param \AppBundle\Entity\Upload $upload
     * @return User
     */
    public function setImageUpload($upload = null)
    {
        if ($upload instanceof \AppBundle\Entity\Upload) {
            $this->imageUpload = $upload;
        }

        return $this;
    }

    /**
     * Get upload
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getImageUpload()
    {
        return $this->imageUpload;
    }

    /**
     * Get initials
     *
     * @return string
     */
    public function getInitials()
    {
        return substr($this->getFirstName(), 0, 1) . substr($this->getLastName(), 0, 1);
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\User $parent
     * @return User
     */
    public function setParent(\AppBundle\Entity\User $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parentUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Add children
     *
     * @param \AppBundle\Entity\User $users
     * @return User
     */
    public function addChildren(\AppBundle\Entity\User $users)
    {
        $this->children[] = $users;

        return $this;
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }
    

    /**
     * Add children
     *
     * @param \AppBundle\Entity\User $children
     * @return User
     */
    public function addChild(\AppBundle\Entity\User $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AppBundle\Entity\User $children
     */
    public function removeChild(\AppBundle\Entity\User $children)
    {
        $this->children->removeElement($children);
    }

    public function __toString() {
        return $this->getName();
    }




    public function isUser(UserInterface $user = null)
    {
        return null !== $user && $this->getId() === $user->getId();
    }



    /**
     * Set preferences
     *
     * @param UserPreferences $userPpreferences
     *
     * @return User
     */
    public function setPreferences(UserPreferences $userPreferences)
    {
        $this->userPreferences = $userPreferences;
        $this->preferences = $userPreferences->getPreferences();

        return $this;
    }

    /**
     * Get preferences
     *
     * @return UserPreferences
     */
    public function getPreferences()
    {
        if (!isset($this->userPreferences)) {
            $this->userPreferences = new UserPreferences($this->preferences);
        }
        return $this->userPreferences;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     *
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return User
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser = null)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }

    /**
     * Get role names
     * @return array
     */
    public static function getRoleNames() {
        return ['ROLE_ADMIN'];
    }


    /**
     * Add payment
     *
     * @param \AppBundle\Entity\Payment $payment
     *
     * @return User
     */
    public function addPayment(\AppBundle\Entity\Payment $payment)
    {
        $this->payments[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param \AppBundle\Entity\Payment $payment
     */
    public function removePayment(\AppBundle\Entity\Payment $payment)
    {
        $this->payments->removeElement($payment);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\Order $order
     *
     * @return User
     */
    public function addOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\Order $order
     */
    public function removeOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }
}
