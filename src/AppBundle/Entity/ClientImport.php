<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Client Import
 *
 * @ORM\Table(name="client_import")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ClientImport
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\FinancialAdviser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FinancialAdviser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financial_adviser_id", referencedColumnName="id")
     * })
     */
    private $financialAdviser;

    /**
     * @var string
     *
     * @ORM\Column(name="num_clients", type="integer", nullable=false, options={"default": 0})
     */
    private $numClients;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numClients
     *
     * @param integer $numClients
     *
     * @return ClientImport
     */
    public function setNumClients($numClients)
    {
        $this->numClients = $numClients;

        return $this;
    }

    /**
     * Get numClients
     *
     * @return integer
     */
    public function getNumClients()
    {
        return $this->numClients;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ClientImport
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientImport
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return ClientImport
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser = null)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }
}
