<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Person
 *
 * @ORM\Table(name="review_template")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ReviewTemplate
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\FinancialAdviser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FinancialAdviser", inversedBy="reviewTemplates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financial_adviser_id", referencedColumnName="id")
     * })
     */
    private $financialAdviser;

    /**
     * @var string
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rating_feedback_enabled", type="boolean", nullable=false, options={"default": 1})
     */
    private $ratingFeedbackEnabled = 1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comments_enabled", type="boolean", nullable=false, options={"default": 0})
     */
    private $commentsEnabled = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_question", type="text", nullable=true)
     */
    private $commentsQuestion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     */
    private $archivedAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ReviewTemplateQuestion", mappedBy="reviewTemplate", cascade={"persist"})
     */
    private $reviewTemplateQuestions;

    /**
     * ReviewTemplate constructor.
     */
    public function __construct()
    {
        $this->reviewTemplateQuestions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeId
     *
     * @param integer $typeId
     *
     * @return ReviewTemplate
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ReviewTemplate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getTypeName()
    {
        return Review::getTypeOptions()[$this->typeId];
    }


    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return ReviewTemplate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ReviewTemplate
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set archivedAt
     *
     * @param \DateTime $archivedAt
     *
     * @return ReviewTemplate
     */
    public function setArchivedAt($archivedAt)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * Get archivedAt
     *
     * @return \DateTime
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ReviewTemplate
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ReviewTemplate
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set financialAdviser
     *
     * @param \AppBundle\Entity\FinancialAdviser $financialAdviser
     *
     * @return ReviewTemplate
     */
    public function setFinancialAdviser(\AppBundle\Entity\FinancialAdviser $financialAdviser = null)
    {
        $this->financialAdviser = $financialAdviser;

        return $this;
    }

    /**
     * Get financialAdviser
     *
     * @return \AppBundle\Entity\FinancialAdviser
     */
    public function getFinancialAdviser()
    {
        return $this->financialAdviser;
    }
    

    /**
     * Add reviewTemplateQuestion
     *
     * @param \AppBundle\Entity\ReviewTemplateQuestion $reviewTemplateQuestion
     *
     * @return ReviewTemplate
     */
    public function addReviewTemplateQuestion(\AppBundle\Entity\ReviewTemplateQuestion $reviewTemplateQuestion)
    {
        $this->reviewTemplateQuestions[] = $reviewTemplateQuestion;

        return $this;
    }

    /**
     * Remove reviewTemplateQuestion
     *
     * @param \AppBundle\Entity\ReviewTemplateQuestion $reviewTemplateQuestion
     */
    public function removeReviewTemplateQuestion(\AppBundle\Entity\ReviewTemplateQuestion $reviewTemplateQuestion)
    {

        $this->reviewTemplateQuestions->removeElement($reviewTemplateQuestion);
    }

    /**
     * Get reviewTemplateQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewTemplateQuestions()
    {
        return $this->reviewTemplateQuestions;
    }

    /**
     * Set ratingFeedbackEnabled
     *
     * @param boolean $ratingFeedbackEnabled
     *
     * @return ReviewTemplate
     */
    public function setRatingFeedbackEnabled($ratingFeedbackEnabled)
    {
        $this->ratingFeedbackEnabled = $ratingFeedbackEnabled;

        return $this;
    }

    /**
     * Get ratingFeedbackEnabled
     *
     * @return boolean
     */
    public function getRatingFeedbackEnabled()
    {
        return $this->ratingFeedbackEnabled;
    }

    /**
     * Set commentsEnabled
     *
     * @param boolean $commentsEnabled
     *
     * @return ReviewTemplate
     */
    public function setCommentsEnabled($commentsEnabled)
    {
        $this->commentsEnabled = $commentsEnabled;

        return $this;
    }

    /**
     * Get commentsEnabled
     *
     * @return boolean
     */
    public function getCommentsEnabled()
    {
        return $this->commentsEnabled;
    }

    /**
     * Set commentsQuestion
     *
     * @param string $commentsQuestion
     *
     * @return ReviewTemplate
     */
    public function setCommentsQuestion($commentsQuestion)
    {
        $this->commentsQuestion = $commentsQuestion;

        return $this;
    }

    /**
     * Get commentsQuestion
     *
     * @return string
     */
    public function getCommentsQuestion()
    {
        return $this->commentsQuestion;
    }
}
