<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientWPMCashflow
 *
 * @ORM\Table(name="client_wpm_cashflow")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ClientWPMCashflow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Client", inversedBy="clientWPMCashflow")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var float
     *
     * @ORM\Column(name="salary", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $salary;

    /**
     * @var integer
     *
     * @ORM\Column(name="salary_mode", type="integer", nullable=true)
     */
    private $salaryMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="salary_date", type="datetime", nullable=true)
     */
    private $salaryDate;

    /**
     * @var float
     *
     * @ORM\Column(name="allowances", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $allowances;

    /**
     * @var integer
     *
     * @ORM\Column(name="allowances_mode", type="integer", nullable=true)
     */
    private $allowancesMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="allowances_date", type="datetime", nullable=true)
     */
    private $allowancesDate;

    /**
     * @var float
     *
     * @ORM\Column(name="commissions", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $commissions;

    /**
     * @var integer
     *
     * @ORM\Column(name="commissions_mode", type="integer", nullable=true)
     */
    private $commissionsMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="commissions_date", type="datetime", nullable=true)
     */
    private $commissionsDate;

    /**
     * @var float
     *
     * @ORM\Column(name="bonuses", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $bonuses;

    /**
     * @var integer
     *
     * @ORM\Column(name="bonuses_mode", type="integer", nullable=true)
     */
    private $bonusesMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bonuses_date", type="datetime", nullable=true)
     */
    private $bonusesDate;

    /**
     * @var float
     *
     * @ORM\Column(name="cashflow_active_others", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $cashflowActiveOthers;

    /**
     * @var float
     *
     * @ORM\Column(name="coupons", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $coupons;

    /**
     * @var integer
     *
     * @ORM\Column(name="coupons_mode", type="integer", nullable=true)
     */
    private $couponsMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="coupons_date", type="datetime", nullable=true)
     */
    private $couponsDate;

    /**
     * @var float
     *
     * @ORM\Column(name="dividends", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $dividends;

    /**
     * @var integer
     *
     * @ORM\Column(name="dividends_mode", type="integer", nullable=true)
     */
    private $dividendsMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dividends_date", type="datetime", nullable=true)
     */
    private $dividendsDate;


    /**
     * @var float
     *
     * @ORM\Column(name="rentals", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $rentals;

    /**
     * @var integer
     *
     * @ORM\Column(name="rentals_mode", type="integer", nullable=true)
     */
    private $rentalsMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="rentals_date", type="datetime", nullable=true)
     */
    private $rentalsDate;

    /**
     * @var float
     *
     * @ORM\Column(name="cashflow_passive_others", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $cashflowPassiveOthers;

    /**
     * @var float
     *
     * @ORM\Column(name="home", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $home;

    /**
     * @var integer
     *
     * @ORM\Column(name="home_mode", type="integer", nullable=true)
     */
    private $homeMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="home_date", type="datetime", nullable=true)
     */
    private $homeDate;

    /**
     * @var float
     *
     * @ORM\Column(name="transport", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $transport;

    /**
     * @var integer
     *
     * @ORM\Column(name="transport_mode", type="integer", nullable=true)
     */
    private $transportMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="transport_date", type="datetime", nullable=true)
     */
    private $transportDate;

    /**
     * @var float
     *
     * @ORM\Column(name="personal", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $personal;

    /**
     * @var integer
     *
     * @ORM\Column(name="personal_mode", type="integer", nullable=true)
     */
    private $personalMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="personal_date", type="datetime", nullable=true)
     */
    private $personalDate;


    /**
     * @var float
     *
     * @ORM\Column(name="food", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $food;

    /**
     * @var integer
     *
     * @ORM\Column(name="food_mode", type="integer", nullable=true)
     */
    private $foodMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="food_date", type="datetime", nullable=true)
     */
    private $foodDate;

    /**
     * @var float
     *
     * @ORM\Column(name="medical", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $medical;

    /**
     * @var integer
     *
     * @ORM\Column(name="medical_mode", type="integer", nullable=true)
     */
    private $medicalMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="medical_date", type="datetime", nullable=true)
     */
    private $medicalDate;

    /**
     * @var float
     *
     * @ORM\Column(name="children", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $children;

    /**
     * @var integer
     *
     * @ORM\Column(name="children_mode", type="integer", nullable=true)
     */
    private $childrenMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="children_date", type="datetime", nullable=true)
     */
    private $childrenDate;

    /**
     * @var float
     *
     * @ORM\Column(name="loans", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $loans;

    /**
     * @var integer
     *
     * @ORM\Column(name="loans_mode", type="integer", nullable=true)
     */
    private $loansMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="loans_date", type="datetime", nullable=true)
     */
    private $loansDate;

    /**
     * @var float
     *
     * @ORM\Column(name="cashflow_fixed_others", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $cashflowFixedOthers;

    /**
     * @var integer
     *
     * @ORM\Column(name="cashflow_fixed_others_mode", type="integer", nullable=true)
     */
    private $cashflowFixedOthersMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cashflow_fixed_others_date", type="datetime", nullable=true)
     */
    private $cashflowFixedOthersDate;

    /**
     * @var float
     *
     * @ORM\Column(name="recreation", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $recreation;

    /**
     * @var integer
     *
     * @ORM\Column(name="recreation_mode", type="integer", nullable=true)
     */
    private $recreationMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="recreation_date", type="datetime", nullable=true)
     */
    private $recreationDate;

    /**
     * @var float
     *
     * @ORM\Column(name="miscellaneous", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $miscellaneous;

    /**
     * @var integer
     *
     * @ORM\Column(name="miscellaneous_mode", type="integer", nullable=true)
     */
    private $miscellaneousMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="miscellaneous_date", type="datetime", nullable=true)
     */
    private $miscellaneousDate;

    /**
     * @var float
     *
     * @ORM\Column(name="insurances", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $insurances;

    /**
     * @var integer
     *
     * @ORM\Column(name="insurances_mode", type="integer", nullable=true)
     */
    private $insurancesMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insurances_date", type="datetime", nullable=true)
     */
    private $insurancesDate;

    /**
     * @var float
     *
     * @ORM\Column(name="savings", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $savings;

    /**
     * @var integer
     *
     * @ORM\Column(name="savings_mode", type="integer", nullable=true)
     */
    private $savingsMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="savings_date", type="datetime", nullable=true)
     */
    private $savingsDate;

    /**
     * @var float
     *
     * @ORM\Column(name="gifts", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $gifts;

    /**
     * @var integer
     *
     * @ORM\Column(name="gifts_mode", type="integer", nullable=true)
     */
    private $giftsMode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="gifts_date", type="datetime", nullable=true)
     */
    private $giftsDate;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set salary
     *
     * @param string $salary
     *
     * @return ClientWPMCashflow
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return float
     */
    public function getSalary()
    {
        return $this->salary;
    }



    /**
     * Set salaryMode
     *
     * @param integer $salaryMode
     *
     * @return ClientWPMCashflow
     */
    public function setSalaryMode($salaryMode)
    {
        $this->salaryMode = $salaryMode;

        return $this;
    }

    /**
     * Get salaryMode
     *
     * @return integer
     */
    public function getSalaryMode()
    {
        return $this->salaryMode;
    }

    /**
     * Set salaryDate
     *
     * @param \DateTime $salaryDate
     *
     * @return ClientWPMCashflow
     */
    public function setSalaryDate($salaryDate)
    {
        $this->salaryDate = $salaryDate;

        return $this;
    }

    /**
     * Get salaryDate
     *
     * @return \DateTime
     */
    public function getSalaryDate()
    {
        return $this->salaryDate;
    }

    /**
     * Set allowances
     *
     * @param string $allowances
     *
     * @return ClientWPMCashflow
     */
    public function setAllowances($allowances)
    {
        $this->allowances = $allowances;

        return $this;
    }

    /**
     * Get allowances
     *
     * @return float
     */
    public function getAllowances()
    {
        return $this->allowances;
    }

    /**
     * Set allowancesMode
     *
     * @param integer $allowancesMode
     *
     * @return ClientWPMCashflow
     */
    public function setAllowancesMode($allowancesMode)
    {
        $this->allowancesMode = $allowancesMode;

        return $this;
    }

    /**
     * Get allowancesMode
     *
     * @return integer
     */
    public function getAllowancesMode()
    {
        return $this->allowancesMode;
    }

    /**
     * Set allowancesDate
     *
     * @param \DateTime $allowancesDate
     *
     * @return ClientWPMCashflow
     */
    public function setAllowancesDate($allowancesDate)
    {
        $this->allowancesDate = $allowancesDate;

        return $this;
    }

    /**
     * Get allowancesDate
     *
     * @return \DateTime
     */
    public function getAllowancesDate()
    {
        return $this->allowancesDate;
    }

    /**
     * Set commissions
     *
     * @param string $commissions
     *
     * @return ClientWPMCashflow
     */
    public function setCommissions($commissions)
    {
        $this->commissions = $commissions;

        return $this;
    }

    /**
     * Get commissions
     *
     * @return float
     */
    public function getCommissions()
    {
        return $this->commissions;
    }

    /**
     * Set commissionsMode
     *
     * @param integer $commissionsMode
     *
     * @return ClientWPMCashflow
     */
    public function setCommissionsMode($commissionsMode)
    {
        $this->commissionsMode = $commissionsMode;

        return $this;
    }

    /**
     * Get commissionsMode
     *
     * @return integer
     */
    public function getCommissionsMode()
    {
        return $this->commissionsMode;
    }

    /**
     * Set commissionsDate
     *
     * @param \DateTime $commissionsDate
     *
     * @return ClientWPMCashflow
     */
    public function setCommissionsDate($commissionsDate)
    {
        $this->commissionsDate = $commissionsDate;

        return $this;
    }

    /**
     * Get commissionsDate
     *
     * @return \DateTime
     */
    public function getCommissionsDate()
    {
        return $this->commissionsDate;
    }

    /**
     * Set bonuses
     *
     * @param string $bonuses
     *
     * @return ClientWPMCashflow
     */
    public function setBonuses($bonuses)
    {
        $this->bonuses = $bonuses;

        return $this;
    }

    /**
     * Get bonuses
     *
     * @return float
     */
    public function getBonuses()
    {
        return $this->bonuses;
    }

    /**
     * Set bonusesMode
     *
     * @param integer $bonusesMode
     *
     * @return ClientWPMCashflow
     */
    public function setBonusesMode($bonusesMode)
    {
        $this->bonusesMode = $bonusesMode;

        return $this;
    }

    /**
     * Get bonusesMode
     *
     * @return integer
     */
    public function getBonusesMode()
    {
        return $this->bonusesMode;
    }

    /**
     * Set bonusesDate
     *
     * @param \DateTime $bonusesDate
     *
     * @return ClientWPMCashflow
     */
    public function setBonusesDate($bonusesDate)
    {
        $this->bonusesDate = $bonusesDate;

        return $this;
    }

    /**
     * Get bonusesDate
     *
     * @return \DateTime
     */
    public function getBonusesDate()
    {
        return $this->bonusesDate;
    }

    /**
     * Set cashflowActiveOthers
     *
     * @param string $cashflowActiveOthers
     *
     * @return ClientWPMCashflow
     */
    public function setCashflowActiveOthers($cashflowActiveOthers)
    {
        $this->cashflowActiveOthers = $cashflowActiveOthers;

        return $this;
    }

    /**
     * Get cashflowActiveOthers
     *
     * @return float
     */
    public function getCashflowActiveOthers()
    {
        return $this->cashflowActiveOthers;
    }

    /**
     * Set coupons
     *
     * @param string $coupons
     *
     * @return ClientWPMCashflow
     */
    public function setCoupons($coupons)
    {
        $this->coupons = $coupons;

        return $this;
    }

    /**
     * Get coupons
     *
     * @return float
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * Set couponsMode
     *
     * @param integer $couponsMode
     *
     * @return ClientWPMCashflow
     */
    public function setCouponsMode($couponsMode)
    {
        $this->couponsMode = $couponsMode;

        return $this;
    }

    /**
     * Get couponsMode
     *
     * @return integer
     */
    public function getCouponsMode()
    {
        return $this->couponsMode;
    }

    /**
     * Set couponsDate
     *
     * @param \DateTime $couponsDate
     *
     * @return ClientWPMCashflow
     */
    public function setCouponsDate($couponsDate)
    {
        $this->couponsDate = $couponsDate;

        return $this;
    }

    /**
     * Get couponsDate
     *
     * @return \DateTime
     */
    public function getCouponsDate()
    {
        return $this->couponsDate;
    }

    /**
     * Set dividends
     *
     * @param string $dividends
     *
     * @return ClientWPMCashflow
     */
    public function setDividends($dividends)
    {
        $this->dividends = $dividends;

        return $this;
    }

    /**
     * Get dividends
     *
     * @return float
     */
    public function getDividends()
    {
        return $this->dividends;
    }

    /**
     * Set dividendsMode
     *
     * @param integer $dividendsMode
     *
     * @return ClientWPMCashflow
     */
    public function setDividendsMode($dividendsMode)
    {
        $this->dividendsMode = $dividendsMode;

        return $this;
    }

    /**
     * Get dividendsMode
     *
     * @return integer
     */
    public function getDividendsMode()
    {
        return $this->dividendsMode;
    }

    /**
     * Set dividendsDate
     *
     * @param \DateTime $dividendsDate
     *
     * @return ClientWPMCashflow
     */
    public function setDividendsDate($dividendsDate)
    {
        $this->dividendsDate = $dividendsDate;

        return $this;
    }

    /**
     * Get dividendsDate
     *
     * @return \DateTime
     */
    public function getDividendsDate()
    {
        return $this->dividendsDate;
    }

    /**
     * Set rentals
     *
     * @param string $rentals
     *
     * @return ClientWPMCashflow
     */
    public function setRentals($rentals)
    {
        $this->rentals = $rentals;

        return $this;
    }

    /**
     * Get rentals
     *
     * @return float
     */
    public function getRentals()
    {
        return $this->rentals;
    }

    /**
     * Set rentalsMode
     *
     * @param integer $rentalsMode
     *
     * @return ClientWPMCashflow
     */
    public function setRentalsMode($rentalsMode)
    {
        $this->rentalsMode = $rentalsMode;

        return $this;
    }

    /**
     * Get rentalsMode
     *
     * @return integer
     */
    public function getRentalsMode()
    {
        return $this->rentalsMode;
    }

    /**
     * Set rentalsDate
     *
     * @param \DateTime $rentalsDate
     *
     * @return ClientWPMCashflow
     */
    public function setRentalsDate($rentalsDate)
    {
        $this->rentalsDate = $rentalsDate;

        return $this;
    }

    /**
     * Get rentalsDate
     *
     * @return \DateTime
     */
    public function getRentalsDate()
    {
        return $this->rentalsDate;
    }

    /**
     * Set cashflowPassiveOthers
     *
     * @param string $cashflowPassiveOthers
     *
     * @return ClientWPMCashflow
     */
    public function setCashflowPassiveOthers($cashflowPassiveOthers)
    {
        $this->cashflowPassiveOthers = $cashflowPassiveOthers;

        return $this;
    }

    /**
     * Get cashflowPassiveOthers
     *
     * @return float
     */
    public function getCashflowPassiveOthers()
    {
        return $this->cashflowPassiveOthers;
    }

    /**
     * Set home
     *
     * @param string $home
     *
     * @return ClientWPMCashflow
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return float
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set homeMode
     *
     * @param integer $homeMode
     *
     * @return ClientWPMCashflow
     */
    public function setHomeMode($homeMode)
    {
        $this->homeMode = $homeMode;

        return $this;
    }

    /**
     * Get homeMode
     *
     * @return integer
     */
    public function getHomeMode()
    {
        return $this->homeMode;
    }

    /**
     * Set homeDate
     *
     * @param \DateTime $homeDate
     *
     * @return ClientWPMCashflow
     */
    public function setHomeDate($homeDate)
    {
        $this->homeDate = $homeDate;

        return $this;
    }

    /**
     * Get homeDate
     *
     * @return \DateTime
     */
    public function getHomeDate()
    {
        return $this->homeDate;
    }

    /**
     * Set transport
     *
     * @param string $transport
     *
     * @return ClientWPMCashflow
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Get transport
     *
     * @return float
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set transportMode
     *
     * @param integer $transportMode
     *
     * @return ClientWPMCashflow
     */
    public function setTransportMode($transportMode)
    {
        $this->transportMode = $transportMode;

        return $this;
    }

    /**
     * Get transportMode
     *
     * @return integer
     */
    public function getTransportMode()
    {
        return $this->transportMode;
    }

    /**
     * Set transportDate
     *
     * @param \DateTime $transportDate
     *
     * @return ClientWPMCashflow
     */
    public function setTransportDate($transportDate)
    {
        $this->transportDate = $transportDate;

        return $this;
    }

    /**
     * Get transportDate
     *
     * @return \DateTime
     */
    public function getTransportDate()
    {
        return $this->transportDate;
    }

    /**
     * Set personal
     *
     * @param string $personal
     *
     * @return ClientWPMCashflow
     */
    public function setPersonal($personal)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return float
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * Set personalMode
     *
     * @param integer $personalMode
     *
     * @return ClientWPMCashflow
     */
    public function setPersonalMode($personalMode)
    {
        $this->personalMode = $personalMode;

        return $this;
    }

    /**
     * Get personalMode
     *
     * @return integer
     */
    public function getPersonalMode()
    {
        return $this->personalMode;
    }

    /**
     * Set personalDate
     *
     * @param \DateTime $personalDate
     *
     * @return ClientWPMCashflow
     */
    public function setPersonalDate($personalDate)
    {
        $this->personalDate = $personalDate;

        return $this;
    }

    /**
     * Get personalDate
     *
     * @return \DateTime
     */
    public function getPersonalDate()
    {
        return $this->personalDate;
    }

    /**
     * Set food
     *
     * @param string $food
     *
     * @return ClientWPMCashflow
     */
    public function setFood($food)
    {
        $this->food = $food;

        return $this;
    }

    /**
     * Get food
     *
     * @return float
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * Set foodMode
     *
     * @param integer $foodMode
     *
     * @return ClientWPMCashflow
     */
    public function setFoodMode($foodMode)
    {
        $this->foodMode = $foodMode;

        return $this;
    }

    /**
     * Get foodMode
     *
     * @return integer
     */
    public function getFoodMode()
    {
        return $this->foodMode;
    }

    /**
     * Set foodDate
     *
     * @param \DateTime $foodDate
     *
     * @return ClientWPMCashflow
     */
    public function setFoodDate($foodDate)
    {
        $this->foodDate = $foodDate;

        return $this;
    }

    /**
     * Get foodDate
     *
     * @return \DateTime
     */
    public function getFoodDate()
    {
        return $this->foodDate;
    }

    /**
     * Set medical
     *
     * @param string $medical
     *
     * @return ClientWPMCashflow
     */
    public function setMedical($medical)
    {
        $this->medical = $medical;

        return $this;
    }

    /**
     * Get medical
     *
     * @return float
     */
    public function getMedical()
    {
        return $this->medical;
    }

    /**
     * Set medicalMode
     *
     * @param integer $medicalMode
     *
     * @return ClientWPMCashflow
     */
    public function setMedicalMode($medicalMode)
    {
        $this->medicalMode = $medicalMode;

        return $this;
    }

    /**
     * Get medicalMode
     *
     * @return integer
     */
    public function getMedicalMode()
    {
        return $this->medicalMode;
    }

    /**
     * Set medicalDate
     *
     * @param \DateTime $medicalDate
     *
     * @return ClientWPMCashflow
     */
    public function setMedicalDate($medicalDate)
    {
        $this->medicalDate = $medicalDate;

        return $this;
    }

    /**
     * Get medicalDate
     *
     * @return \DateTime
     */
    public function getMedicalDate()
    {
        return $this->medicalDate;
    }

    /**
     * Set children
     *
     * @param string $children
     *
     * @return ClientWPMCashflow
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return float
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set childrenMode
     *
     * @param integer $childrenMode
     *
     * @return ClientWPMCashflow
     */
    public function setChildrenMode($childrenMode)
    {
        $this->childrenMode = $childrenMode;

        return $this;
    }

    /**
     * Get childrenMode
     *
     * @return integer
     */
    public function getChildrenMode()
    {
        return $this->childrenMode;
    }

    /**
     * Set childrenDate
     *
     * @param \DateTime $childrenDate
     *
     * @return ClientWPMCashflow
     */
    public function setChildrenDate($childrenDate)
    {
        $this->childrenDate = $childrenDate;

        return $this;
    }

    /**
     * Get childrenDate
     *
     * @return \DateTime
     */
    public function getChildrenDate()
    {
        return $this->childrenDate;
    }

    /**
     * Set loans
     *
     * @param string $loans
     *
     * @return ClientWPMCashflow
     */
    public function setLoans($loans)
    {
        $this->loans = $loans;

        return $this;
    }

    /**
     * Get loans
     *
     * @return string
     */
    public function getLoans()
    {
        return $this->loans;
    }

    /**
     * Set loansMode
     *
     * @param integer $loansMode
     *
     * @return ClientWPMCashflow
     */
    public function setLoansMode($loansMode)
    {
        $this->loansMode = $loansMode;

        return $this;
    }

    /**
     * Get loansMode
     *
     * @return integer
     */
    public function getLoansMode()
    {
        return $this->loansMode;
    }

    /**
     * Set loansDate
     *
     * @param \DateTime $loansDate
     *
     * @return ClientWPMCashflow
     */
    public function setLoansDate($loansDate)
    {
        $this->loansDate = $loansDate;

        return $this;
    }

    /**
     * Get loansDate
     *
     * @return \DateTime
     */
    public function getLoansDate()
    {
        return $this->loansDate;
    }

    /**
     * Set cashflowFixedOthers
     *
     * @param string $cashflowFixedOthers
     *
     * @return ClientWPMCashflow
     */
    public function setCashflowFixedOthers($cashflowFixedOthers)
    {
        $this->cashflowFixedOthers = $cashflowFixedOthers;

        return $this;
    }

    /**
     * Get cashflowFixedOthers
     *
     * @return float
     */
    public function getCashflowFixedOthers()
    {
        return $this->cashflowFixedOthers;
    }

    /**
     * Set cashflowFixedOthersMode
     *
     * @param integer $cashflowFixedOthersMode
     *
     * @return ClientWPMCashflow
     */
    public function setCashflowFixedOthersMode($cashflowFixedOthersMode)
    {
        $this->cashflowFixedOthersMode = $cashflowFixedOthersMode;

        return $this;
    }

    /**
     * Get cashflowFixedOthersMode
     *
     * @return integer
     */
    public function getCashflowFixedOthersMode()
    {
        return $this->cashflowFixedOthersMode;
    }

    /**
     * Set cashflowFixedOthersDate
     *
     * @param \DateTime $cashflowFixedOthersDate
     *
     * @return ClientWPMCashflow
     */
    public function setCashflowFixedOthersDate($cashflowFixedOthersDate)
    {
        $this->cashflowFixedOthersDate = $cashflowFixedOthersDate;

        return $this;
    }

    /**
     * Get cashflowFixedOthersDate
     *
     * @return \DateTime
     */
    public function getCashflowFixedOthersDate()
    {
        return $this->cashflowFixedOthersDate;
    }

    /**
     * Set recreation
     *
     * @param string $recreation
     *
     * @return ClientWPMCashflow
     */
    public function setRecreation($recreation)
    {
        $this->recreation = $recreation;

        return $this;
    }

    /**
     * Get recreation
     *
     * @return float
     */
    public function getRecreation()
    {
        return $this->recreation;
    }

    /**
     * Set recreationMode
     *
     * @param integer $recreationMode
     *
     * @return ClientWPMCashflow
     */
    public function setRecreationMode($recreationMode)
    {
        $this->recreationMode = $recreationMode;

        return $this;
    }

    /**
     * Get recreationMode
     *
     * @return integer
     */
    public function getRecreationMode()
    {
        return $this->recreationMode;
    }

    /**
     * Set recreationDate
     *
     * @param \DateTime $recreationDate
     *
     * @return ClientWPMCashflow
     */
    public function setRecreationDate($recreationDate)
    {
        $this->recreationDate = $recreationDate;

        return $this;
    }

    /**
     * Get recreationDate
     *
     * @return \DateTime
     */
    public function getRecreationDate()
    {
        return $this->recreationDate;
    }

    /**
     * Set miscellaneous
     *
     * @param string $miscellaneous
     *
     * @return ClientWPMCashflow
     */
    public function setMiscellaneous($miscellaneous)
    {
        $this->miscellaneous = $miscellaneous;

        return $this;
    }

    /**
     * Get miscellaneous
     *
     * @return float
     */
    public function getMiscellaneous()
    {
        return $this->miscellaneous;
    }

    /**
     * Set miscellaneousMode
     *
     * @param integer $miscellaneousMode
     *
     * @return ClientWPMCashflow
     */
    public function setMiscellaneousMode($miscellaneousMode)
    {
        $this->miscellaneousMode = $miscellaneousMode;

        return $this;
    }

    /**
     * Get miscellaneousMode
     *
     * @return integer
     */
    public function getMiscellaneousMode()
    {
        return $this->miscellaneousMode;
    }

    /**
     * Set miscellaneousDate
     *
     * @param \DateTime $miscellaneousDate
     *
     * @return ClientWPMCashflow
     */
    public function setMiscellaneousDate($miscellaneousDate)
    {
        $this->miscellaneousDate = $miscellaneousDate;

        return $this;
    }

    /**
     * Get miscellaneousDate
     *
     * @return \DateTime
     */
    public function getMiscellaneousDate()
    {
        return $this->miscellaneousDate;
    }

    /**
     * Set insurances
     *
     * @param string $insurances
     *
     * @return ClientWPMCashflow
     */
    public function setInsurances($insurances)
    {
        $this->insurances = $insurances;

        return $this;
    }

    /**
     * Get insurances
     *
     * @return string
     */
    public function getInsurances()
    {
        return $this->insurances;
    }

    /**
     * Set insurancesMode
     *
     * @param integer $insurancesMode
     *
     * @return ClientWPMCashflow
     */
    public function setInsurancesMode($insurancesMode)
    {
        $this->insurancesMode = $insurancesMode;

        return $this;
    }

    /**
     * Get insurancesMode
     *
     * @return integer
     */
    public function getInsurancesMode()
    {
        return $this->insurancesMode;
    }

    /**
     * Set insurancesDate
     *
     * @param \DateTime $insurancesDate
     *
     * @return ClientWPMCashflow
     */
    public function setInsurancesDate($insurancesDate)
    {
        $this->insurancesDate = $insurancesDate;

        return $this;
    }

    /**
     * Get insurancesDate
     *
     * @return \DateTime
     */
    public function getInsurancesDate()
    {
        return $this->insurancesDate;
    }

    /**
     * Set savings
     *
     * @param string $savings
     *
     * @return ClientWPMCashflow
     */
    public function setSavings($savings)
    {
        $this->savings = $savings;

        return $this;
    }

    /**
     * Get savings
     *
     * @return string
     */
    public function getSavings()
    {
        return $this->savings;
    }

    /**
     * Set savingsMode
     *
     * @param integer $savingsMode
     *
     * @return ClientWPMCashflow
     */
    public function setSavingsMode($savingsMode)
    {
        $this->savingsMode = $savingsMode;

        return $this;
    }

    /**
     * Get savingsMode
     *
     * @return integer
     */
    public function getSavingsMode()
    {
        return $this->savingsMode;
    }

    /**
     * Set savingsDate
     *
     * @param \DateTime $savingsDate
     *
     * @return ClientWPMCashflow
     */
    public function setSavingsDate($savingsDate)
    {
        $this->savingsDate = $savingsDate;

        return $this;
    }

    /**
     * Get savingsDate
     *
     * @return \DateTime
     */
    public function getSavingsDate()
    {
        return $this->savingsDate;
    }

    /**
     * Set gifts
     *
     * @param string $gifts
     *
     * @return ClientWPMCashflow
     */
    public function setGifts($gifts)
    {
        $this->gifts = $gifts;

        return $this;
    }

    /**
     * Get gifts
     *
     * @return string
     */
    public function getGifts()
    {
        return $this->gifts;
    }

    /**
     * Set giftsMode
     *
     * @param integer $giftsMode
     *
     * @return ClientWPMCashflow
     */
    public function setGiftsMode($giftsMode)
    {
        $this->giftsMode = $giftsMode;

        return $this;
    }

    /**
     * Get giftsMode
     *
     * @return integer
     */
    public function getGiftsMode()
    {
        return $this->giftsMode;
    }

    /**
     * Set giftsDate
     *
     * @param \DateTime $giftsDate
     *
     * @return ClientWPMCashflow
     */
    public function setGiftsDate($giftsDate)
    {
        $this->giftsDate = $giftsDate;

        return $this;
    }

    /**
     * Get giftsDate
     *
     * @return \DateTime
     */
    public function getGiftsDate()
    {
        return $this->giftsDate;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientWPMCashflow
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientWPMCashflow
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientWPMCashflow
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Get salary
     *
     * @return float
     */
    public function getAmountAnnual($amount, $paymentMode)
    {
        switch ($paymentMode) {
            case ClientWPM::PAYMENT_MODE_ANNUAL:
                return $amount;
            case ClientWPM::PAYMENT_MODE_BIANNUAL:
                return $amount * 2;
            case ClientWPM::PAYMENT_MODE_QUARTERLY:
                return $amount * 4;
            case ClientWPM::PAYMENT_MODE_MONTHLY:
                return $amount * 12;
        }
        return $amount;
    }

}
