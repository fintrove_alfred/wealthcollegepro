<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ClientWPM
 *
 * @ORM\Table(name="client_wpm")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ClientWPM
{

    const PAYMENT_MODE_MONTHLY = 1;
    const PAYMENT_MODE_QUARTERLY = 3;
    const PAYMENT_MODE_BIANNUAL = 6;
    const PAYMENT_MODE_ANNUAL = 12;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Client", inversedBy="clientWPM")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var float
     *
     * @ORM\Column(name="net_estate", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $netEstate;

    /**
     * @var float
     *
     * @ORM\Column(name="annual_cashflow_balance", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $annualCashFlowBalance;

    /**
     * @var float
     *
     * @ORM\Column(name="gift_estate_planning", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $giftEstatePlanning;

    /**
     * @var float
     *
     * @ORM\Column(name="active_income", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $activeIncome;

    /**
     * @var float
     *
     * @ORM\Column(name="passive_income", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $passiveIncome;

    /**
     * @var float
     *
     * @ORM\Column(name="fixed_expenses", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $fixedExpenses;

    /**
     * @var float
     *
     * @ORM\Column(name="variable_expenses", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $variableExpenses;

    /**
     * @var float
     *
     * @ORM\Column(name="investment_retirement_planning", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $investmentRetirementPlanning;

    /**
     * @var float
     *
     * @ORM\Column(name="assets_lower_risk", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $assetsLowerRisk;

    /**
     * @var float
     *
     * @ORM\Column(name="assets_moderate_risk", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $assetsModerateRisk;

    /**
     * @var float
     *
     * @ORM\Column(name="assets_higher_risk", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $assetsHigherRisk;

    /**
     * @var float
     *
     * @ORM\Column(name="liabilities_lower_risk", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $liabilitiesLowerRisk;

    /**
     * @var float
     *
     * @ORM\Column(name="liabilities_moderate_risk", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $liabilitiesModerateRisk;

    /**
     * @var float
     *
     * @ORM\Column(name="liabilities_higher_risk", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $liabilitiesHigherRisk;

    /**
     * @var float
     *
     * @ORM\Column(name="investment_net_worth", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $investmentNetWorth;

    /**
     * @var float
     *
     * @ORM\Column(name="risk_insurance_planning", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $riskInsurancePlanning;

    /**
     * @var float
     *
     * @ORM\Column(name="death", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $death;

    /**
     * @var float
     *
     * @ORM\Column(name="disability_riders", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $disabilityRiders;

    /**
     * @var float
     *
     * @ORM\Column(name="disability_sup_benefits", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $disabilitySubBenefits;


    /**
     * @var float
     *
     * @ORM\Column(name="pa_std_alone", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $paStdAlone;

    /**
     * @var float
     *
     * @ORM\Column(name="pa_riders", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $paRiders;

    /**
     * @var float
     *
     * @ORM\Column(name="pa_sup_benefits", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $paSubBenefits;

    /**
     * @var float
     *
     * @ORM\Column(name="ci_std_alone", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $ciStdAlone;

    /**
     * @var float
     *
     * @ORM\Column(name="ci_riders", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $ciRiders;

    /**
     * @var float
     *
     * @ORM\Column(name="ci_sup_benefits", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $ciSubBenefits;

    /**
     * @var float
     *
     * @ORM\Column(name="hosp_std_alone", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $hospStdAlone;

    /**
     * @var float
     *
     * @ORM\Column(name="hosp_riders", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $hospRiders;

    /**
     * @var float
     *
     * @ORM\Column(name="hosp_sup_benefits", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $hospSubBenefits;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set netEstate
     *
     * @param string $netEstate
     *
     * @return ClientWPM
     */
    public function setNetEstate($netEstate)
    {
        $this->netEstate = $netEstate;

        return $this;
    }

    /**
     * Get netEstate
     *
     * @return string
     */
    public function getNetEstate()
    {
        return $this->netEstate;
    }

    /**
     * Set giftEstatePlanning
     *
     * @param string $giftEstatePlanning
     *
     * @return ClientWPM
     */
    public function setGiftEstatePlanning($giftEstatePlanning)
    {
        $this->giftEstatePlanning = $giftEstatePlanning;

        return $this;
    }

    /**
     * Get giftEstatePlanning
     *
     * @return string
     */
    public function getGiftEstatePlanning()
    {
        return $this->giftEstatePlanning;
    }


    /**
     * Set activeIncome
     *
     * @param string $activeIncome
     *
     * @return ClientWPM
     */
    public function setActiveIncome($activeIncome)
    {
        $this->activeIncome = $activeIncome;

        return $this;
    }

    /**
     * Get activeIncome
     *
     * @return string
     */
    public function getActiveIncome()
    {
        return $this->activeIncome;
    }

    /**
     * Set passiveIncome
     *
     * @param string $passiveIncome
     *
     * @return ClientWPM
     */
    public function setPassiveIncome($passiveIncome)
    {
        $this->passiveIncome = $passiveIncome;

        return $this;
    }

    /**
     * Get passiveIncome
     *
     * @return string
     */
    public function getPassiveIncome()
    {
        return $this->passiveIncome;
    }

    /**
     * Set fixedExpenses
     *
     * @param string $fixedExpenses
     *
     * @return ClientWPM
     */
    public function setFixedExpenses($fixedExpenses)
    {
        $this->fixedExpenses = $fixedExpenses;

        return $this;
    }

    /**
     * Get fixedExpenses
     *
     * @return string
     */
    public function getFixedExpenses()
    {
        return $this->fixedExpenses;
    }

    /**
     * Set variableExpenses
     *
     * @param string $variableExpenses
     *
     * @return ClientWPM
     */
    public function setVariableExpenses($variableExpenses)
    {
        $this->variableExpenses = $variableExpenses;

        return $this;
    }

    /**
     * Get variableExpenses
     *
     * @return string
     */
    public function getVariableExpenses()
    {
        return $this->variableExpenses;
    }

    /**
     * Set investmentRetirementPlanning
     *
     * @param string $investmentRetirementPlanning
     *
     * @return ClientWPM
     */
    public function setInvestmentRetirementPlanning($investmentRetirementPlanning)
    {
        $this->investmentRetirementPlanning = $investmentRetirementPlanning;

        return $this;
    }

    /**
     * Get investmentRetirementPlanning
     *
     * @return string
     */
    public function getInvestmentRetirementPlanning()
    {
        return $this->investmentRetirementPlanning;
    }

    /**
     * Set assetsLowerRisk
     *
     * @param string $assetsLowerRisk
     *
     * @return ClientWPM
     */
    public function setAssetsLowerRisk($assetsLowerRisk)
    {
        $this->assetsLowerRisk = $assetsLowerRisk;

        return $this;
    }

    /**
     * Get assetsLowerRisk
     *
     * @return string
     */
    public function getAssetsLowerRisk()
    {
        return $this->assetsLowerRisk;
    }

    /**
     * Set assetsModerateRisk
     *
     * @param string $assetsModerateRisk
     *
     * @return ClientWPM
     */
    public function setAssetsModerateRisk($assetsModerateRisk)
    {
        $this->assetsModerateRisk = $assetsModerateRisk;

        return $this;
    }

    /**
     * Get assetsModerateRisk
     *
     * @return string
     */
    public function getAssetsModerateRisk()
    {
        return $this->assetsModerateRisk;
    }

    /**
     * Set assetsHigherRisk
     *
     * @param string $assetsHigherRisk
     *
     * @return ClientWPM
     */
    public function setAssetsHigherRisk($assetsHigherRisk)
    {
        $this->assetsHigherRisk = $assetsHigherRisk;

        return $this;
    }

    /**
     * Get assetsHigherRisk
     *
     * @return string
     */
    public function getAssetsHigherRisk()
    {
        return $this->assetsHigherRisk;
    }

    /**
     * Set liabilitiesLowerRisk
     *
     * @param string $liabilitiesLowerRisk
     *
     * @return ClientWPM
     */
    public function setLiabilitiesLowerRisk($liabilitiesLowerRisk)
    {
        $this->liabilitiesLowerRisk = $liabilitiesLowerRisk;

        return $this;
    }

    /**
     * Get liabilitiesLowerRisk
     *
     * @return string
     */
    public function getLiabilitiesLowerRisk()
    {
        return $this->liabilitiesLowerRisk;
    }

    /**
     * Set liabilitiesModerateRisk
     *
     * @param string $liabilitiesModerateRisk
     *
     * @return ClientWPM
     */
    public function setLiabilitiesModerateRisk($liabilitiesModerateRisk)
    {
        $this->liabilitiesModerateRisk = $liabilitiesModerateRisk;

        return $this;
    }

    /**
     * Get liabilitiesModerateRisk
     *
     * @return string
     */
    public function getLiabilitiesModerateRisk()
    {
        return $this->liabilitiesModerateRisk;
    }

    /**
     * Set liabilitiesHigherRisk
     *
     * @param string $liabilitiesHigherRisk
     *
     * @return ClientWPM
     */
    public function setLiabilitiesHigherRisk($liabilitiesHigherRisk)
    {
        $this->liabilitiesHigherRisk = $liabilitiesHigherRisk;

        return $this;
    }

    /**
     * Get liabilitiesHigherRisk
     *
     * @return string
     */
    public function getLiabilitiesHigherRisk()
    {
        return $this->liabilitiesHigherRisk;
    }

    /**
     * Set investmentNetWorth
     *
     * @param string $investmentNetWorth
     *
     * @return ClientWPM
     */
    public function setInvestmentNetWorth($investmentNetWorth)
    {
        $this->investmentNetWorth = $investmentNetWorth;

        return $this;
    }

    /**
     * Get investmentNetWorth
     *
     * @return string
     */
    public function getInvestmentNetWorth()
    {
        return $this->investmentNetWorth;
    }

    /**
     * Set riskInsurancePlanning
     *
     * @param string $riskInsurancePlanning
     *
     * @return ClientWPM
     */
    public function setRiskInsurancePlanning($riskInsurancePlanning)
    {
        $this->riskInsurancePlanning = $riskInsurancePlanning;

        return $this;
    }

    /**
     * Get riskInsurancePlanning
     *
     * @return string
     */
    public function getRiskInsurancePlanning()
    {
        return $this->riskInsurancePlanning;
    }

    /**
     * Set death
     *
     * @param string $death
     *
     * @return ClientWPM
     */
    public function setDeath($death)
    {
        $this->death = $death;

        return $this;
    }

    /**
     * Get death
     *
     * @return string
     */
    public function getDeath()
    {
        return $this->death;
    }

    /**
     * Set disabilityRiders
     *
     * @param string $disabilityRiders
     *
     * @return ClientWPM
     */
    public function setDisabilityRiders($disabilityRiders)
    {
        $this->disabilityRiders = $disabilityRiders;

        return $this;
    }

    /**
     * Get disabilityRiders
     *
     * @return string
     */
    public function getDisabilityRiders()
    {
        return $this->disabilityRiders;
    }

    /**
     * Set disabilitySubBenefits
     *
     * @param string $disabilitySubBenefits
     *
     * @return ClientWPM
     */
    public function setDisabilitySubBenefits($disabilitySubBenefits)
    {
        $this->disabilitySubBenefits = $disabilitySubBenefits;

        return $this;
    }

    /**
     * Get disabilitySubBenefits
     *
     * @return string
     */
    public function getDisabilitySubBenefits()
    {
        return $this->disabilitySubBenefits;
    }

    /**
     * Set paStdAlone
     *
     * @param string $paStdAlone
     *
     * @return ClientWPM
     */
    public function setPaStdAlone($paStdAlone)
    {
        $this->paStdAlone = $paStdAlone;

        return $this;
    }

    /**
     * Get paStdAlone
     *
     * @return string
     */
    public function getPaStdAlone()
    {
        return $this->paStdAlone;
    }

    /**
     * Set paRiders
     *
     * @param string $paRiders
     *
     * @return ClientWPM
     */
    public function setPaRiders($paRiders)
    {
        $this->paRiders = $paRiders;

        return $this;
    }

    /**
     * Get paRiders
     *
     * @return string
     */
    public function getPaRiders()
    {
        return $this->paRiders;
    }

    /**
     * Set paSubBenefits
     *
     * @param string $paSubBenefits
     *
     * @return ClientWPM
     */
    public function setPaSubBenefits($paSubBenefits)
    {
        $this->paSubBenefits = $paSubBenefits;

        return $this;
    }

    /**
     * Get paSubBenefits
     *
     * @return string
     */
    public function getPaSubBenefits()
    {
        return $this->paSubBenefits;
    }

    /**
     * Set ciStdAlone
     *
     * @param string $ciStdAlone
     *
     * @return ClientWPM
     */
    public function setCiStdAlone($ciStdAlone)
    {
        $this->ciStdAlone = $ciStdAlone;

        return $this;
    }

    /**
     * Get ciStdAlone
     *
     * @return string
     */
    public function getCiStdAlone()
    {
        return $this->ciStdAlone;
    }

    /**
     * Set ciRiders
     *
     * @param string $ciRiders
     *
     * @return ClientWPM
     */
    public function setCiRiders($ciRiders)
    {
        $this->ciRiders = $ciRiders;

        return $this;
    }

    /**
     * Get ciRiders
     *
     * @return string
     */
    public function getCiRiders()
    {
        return $this->ciRiders;
    }

    /**
     * Set ciSubBenefits
     *
     * @param string $ciSubBenefits
     *
     * @return ClientWPM
     */
    public function setCiSubBenefits($ciSubBenefits)
    {
        $this->ciSubBenefits = $ciSubBenefits;

        return $this;
    }

    /**
     * Get ciSubBenefits
     *
     * @return string
     */
    public function getCiSubBenefits()
    {
        return $this->ciSubBenefits;
    }

    /**
     * Set hospStdAlone
     *
     * @param string $hospStdAlone
     *
     * @return ClientWPM
     */
    public function setHospStdAlone($hospStdAlone)
    {
        $this->hospStdAlone = $hospStdAlone;

        return $this;
    }

    /**
     * Get hospStdAlone
     *
     * @return string
     */
    public function getHospStdAlone()
    {
        return $this->hospStdAlone;
    }

    /**
     * Set hospRiders
     *
     * @param string $hospRiders
     *
     * @return ClientWPM
     */
    public function setHospRiders($hospRiders)
    {
        $this->hospRiders = $hospRiders;

        return $this;
    }

    /**
     * Get hospRiders
     *
     * @return string
     */
    public function getHospRiders()
    {
        return $this->hospRiders;
    }

    /**
     * Set hospSubBenefits
     *
     * @param string $hospSubBenefits
     *
     * @return ClientWPM
     */
    public function setHospSubBenefits($hospSubBenefits)
    {
        $this->hospSubBenefits = $hospSubBenefits;

        return $this;
    }

    /**
     * Get hospSubBenefits
     *
     * @return string
     */
    public function getHospSubBenefits()
    {
        return $this->hospSubBenefits;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return ClientWPM
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return ClientWPM
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return ClientWPM
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public static function getPaymentModeOptions() {
        return [
           self::PAYMENT_MODE_MONTHLY => 'Monthly',
           self::PAYMENT_MODE_QUARTERLY => 'Quarterly',
           self::PAYMENT_MODE_BIANNUAL => 'Half-Yearly',
           self::PAYMENT_MODE_ANNUAL => 'Yearly'
        ];
    }

    /**
     * Set annualCashFlowBalance
     *
     * @param string $annualCashFlowBalance
     *
     * @return ClientWPM
     */
    public function setAnnualCashFlowBalance($annualCashFlowBalance)
    {
        $this->annualCashFlowBalance = $annualCashFlowBalance;

        return $this;
    }

    /**
     * Get annualCashFlowBalance
     *
     * @return string
     */
    public function getAnnualCashFlowBalance()
    {
        return $this->annualCashFlowBalance;
    }
}
