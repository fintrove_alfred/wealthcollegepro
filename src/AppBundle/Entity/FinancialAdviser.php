<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Person
 *
 * @ORM\Table(name="financial_adviser")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class FinancialAdviser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="financialAdviser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     **/
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="professional_title", type="string", length=255, nullable=true)
     */
    private $professionalTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="business_name", type="string", length=255, nullable=true)
     */
    private $businessName;

    /**
     * @var \AppBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * })
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=32, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=32, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=32, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="nric_number", type="string", length=255, nullable=true)
     */
    private $nricNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="race", type="string", length=255, nullable=true)
     */
    private $race;

    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=255, nullable=true)
     */
    private $nationality;

    /**
     * @var string
     *
     * @ORM\Column(name="religion", type="string", length=255, nullable=true)
     */
    private $religion;

    /**
     * @var array
     *
     * @ORM\Column(name="languages", type="simple_array", nullable=true)
     */
    private $languages;

    /**
     * @var string
     *
     * @ORM\Column(name="languages_others", type="string", length=1000, nullable=true)
     */
    private $languagesOthers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="year_joined_industry", type="date", nullable=true)
     */
    private $yearJoinedIndustry;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_company", type="string", length=255, nullable=true)
     */
    private $parentCompanyName;

    /**
     * @var \AppBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_address_id", referencedColumnName="id")
     * })
     */
    private $companyAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="professional_contact_number", type="string", length=255, nullable=true)
     */
    private $professionalContactNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="professional_email_address", type="string", length=255, nullable=true)
     */
    private $professionalEmailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="academic_qualification", type="string", length=255, nullable=true)
     */
    private $academicQualification;

    /**
     * @var string
     *
     * @ORM\Column(name="type_if_qualification", type="string", length=255, nullable=true)
     */
    private $typeOfQualification;

    /**
     * @var string
     *
     * @ORM\Column(name="institution", type="string", length=255, nullable=true)
     */
    private $institution;

    /**
     * @var string
     *
     * @ORM\Column(name="balance_score_card", type="string", length=255, nullable=true)
     */
    private $balanceScoreCard;

    /**
     * @var string
     *
     *
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      minMessage = "Business Persistency Rate must be between 0 and 100",
     *      maxMessage = "Business Persistency Rate must be between 0 and 100"
     * )
     *
     * @ORM\Column(name="bp_rate", type="decimal", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $bpRate;

    /**
     * @var string
     *
     * @ORM\Column(name="academic_qualification_other", type="string", length=255, nullable=true)
     */
    private $academicQualificationOther;

    /**
     * @var array
     *
     * @ORM\Column(name="professional_certifications", type="simple_array", nullable=true)
     */
    private $professionalCertifications;

    /**
     * @var string
     *
     * @ORM\Column(name="professional_certifications_others", type="string", length=1000, nullable=true)
     */
    private $professionalCertificationsOthers;

    /**
     * @var array
     *
     * @ORM\Column(name="professional_memberships", type="simple_array", nullable=true)
     */
    private $professionalMemberships;

    /**
     * @var string
     *
     * @ORM\Column(name="professional_memberships_others", type="string", length=1000, nullable=true)
     */
    private $professionalMembershipsOthers;

    /**
     * @var string
     *
     * @ORM\Column(name="industry_awards", type="text", length=10000, nullable=true)
     */
    private $industryAwards;

    /**
     * @var string
     *
     * @ORM\Column(name="community_involvement", type="text", length=10000, nullable=true)
     */
    private $communityInvolvement;

    /**
     * @var array
     *
     * @ORM\Column(name="areas_of_competencies", type="simple_array", nullable=true)
     */
    private $areasOfCompetencies;

    /**
     * @var string
     *
     * @ORM\Column(name="areas_of_competencies_others", type="text", length=1000, nullable=true)
     */
    private $areasOfCompetenciesOthers;

    /**
     * @var string
     *
     * @ORM\Column(name="social_facebook", type="string", length=255, nullable=true)
     */
    private $socialFacebook;

    /**
     * @var string
     *
     * @ORM\Column(name="social_twitter", type="string", length=255, nullable=true)
     */
    private $socialTwitter;

    /**
     * @var string
     *
     * @ORM\Column(name="social_linkedin", type="string", length=255, nullable=true)
     */
    private $socialLinkedIn;

    /**
     * @var string
     *
     * @ORM\Column(name="licence_no", type="string", length=255, nullable=true)
     */
    private $licenceNo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_content", type="text", length=10000, nullable=true)
     */
    private $profileContent;

   /**
    * @var string
    *
    * @ORM\Column(name="special_interest", type="text", length=10000, nullable=true)
    */
   private $specialInterest;

    /**
     * @var \AppBundle\Entity\Upload
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="profile_image_upload_id", referencedColumnName="id")
     * })
     */
    private $profileImageUpload;

    /**
     * @var \AppBundle\Entity\Upload
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="profile_background_upload_id", referencedColumnName="id")
     * })
     */
    private $profileBackgroundUpload;

    /**
     * @var float
     *
     * @ORM\Column(name="average_rating", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $averageRating;

    /**
     * @var float
     *
     * @ORM\Column(name="score", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $score;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscription_ends_at", type="datetime", nullable=true)
     */
    private $subscriptionEndsAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false, options={"default": 0})
     */
    private $hidden = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Client", mappedBy="financialAdviser")
     */
    private $clients;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ReviewTemplate", mappedBy="financialAdviser")
     */
    private $reviewTemplates;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientEventType", mappedBy="financialAdviser", cascade={"persist"})
     */
    private $clientEventTypes;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Review", mappedBy="financialAdviser")
     */
    private $reviews;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ClientGroup", mappedBy="financialAdviser", cascade={"persist"})
     */
    private $clientGroups;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FinancialAdviserPhoto", mappedBy="financialAdviser")
     */
    private $financialAdviserPhotos;

    /**
     * FinancialAdviser constructor.
     */
    public function __construct()
    {
        $this->clients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reviewTemplates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientEventTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->financialAdviserPhotos = new \Doctrine\Common\Collections\ArrayCollection();
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set businessName
     *
     * @param string $businessName
     *
     * @return FinancialAdviser
     */
    public function setBusinessName($businessName)
    {
        $this->businessName = $businessName;

        return $this;
    }

    /**
     * Get businessName
     *
     * @return string
     */
    public function getBusinessName()
    {
        return $this->businessName;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return FinancialAdviser
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return FinancialAdviser
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return FinancialAdviser
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FinancialAdviser
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return FinancialAdviser
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return FinancialAdviser
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return FinancialAdviser
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     *
     * @return FinancialAdviser
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return FinancialAdviser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FinancialAdviser
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return FinancialAdviser
     */
    public function addClient(\AppBundle\Entity\Client $client)
    {
        $this->clients[] = $client;

        return $this;
    }

    /**
     * Remove client
     *
     * @param \AppBundle\Entity\Client $client
     */
    public function removeClient(\AppBundle\Entity\Client $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set profileImageUpload
     *
     * @param \AppBundle\Entity\Upload $profileImageUpload
     *
     * @return FinancialAdviser
     */
    public function setProfileImageUpload(\AppBundle\Entity\Upload $profileImageUpload = null)
    {
        $this->profileImageUpload = $profileImageUpload;

        return $this;
    }

    /**
     * Get profileImageUpload
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getProfileImageUpload()
    {
        return $this->profileImageUpload;
    }

    /**
     * Set profileBackgroundUpload
     *
     * @param \AppBundle\Entity\Upload $profileBackgroundUpload
     *
     * @return FinancialAdviser
     */
    public function setProfileBackgroundUpload(\AppBundle\Entity\Upload $profileBackgroundUpload = null)
    {
        $this->profileBackgroundUpload = $profileBackgroundUpload;

        return $this;
    }

    /**
     * Get profileBackgroundUpload
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getProfileBackgroundUpload()
    {
        return $this->profileBackgroundUpload;
    }

    /**
     * Add reviewTemplate
     *
     * @param \AppBundle\Entity\ReviewTemplate $reviewTemplate
     *
     * @return FinancialAdviser
     */
    public function addReviewTemplate(\AppBundle\Entity\ReviewTemplate $reviewTemplate)
    {
        $this->reviewTemplates[] = $reviewTemplate;

        return $this;
    }

    /**
     * Remove reviewTemplate
     *
     * @param \AppBundle\Entity\ReviewTemplate $reviewTemplate
     */
    public function removeReviewTemplate(\AppBundle\Entity\ReviewTemplate $reviewTemplate)
    {
        $this->reviewTemplates->removeElement($reviewTemplate);
    }

    /**
     * Get reviewTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewTemplates()
    {
        return $this->reviewTemplates;
    }

    /**
     * Add review
     *
     * @param \AppBundle\Entity\Review $review
     *
     * @return FinancialAdviser
     */
    public function addReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AppBundle\Entity\Review $review
     */
    public function removeReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set averageRating
     *
     * @param float $averageRating
     *
     * @return FinancialAdviser
     */
    public function setAverageRating($averageRating)
    {
        $this->averageRating = $averageRating;

        return $this;
    }

    /**
     * Get averageRating
     *
     * @return float
     */
    public function getAverageRating()
    {
        return $this->averageRating;
    }

    public function averageRatingDescription() {

        if (!isset($this->averageRating)) {
            return 'No Rating';
        } else if ($this->averageRating >= 0 && $this->averageRating < 4) {
            return 'Poor';
        } else if ($this->averageRating >= 4 && $this->averageRating < 6) {
            return 'Fair';
        } else if ($this->averageRating >= 6 && $this->averageRating < 8) {
            return 'Good';
        } else if ($this->averageRating >= 8 && $this->averageRating < 9) {
            return 'Very Good';
        } else if ($this->averageRating >= 9) {
            return 'Excellent';
        } else {
            return 'No Rating';
        }

    }

    /**
     * Get averageRating out of 5
     *
     * @return float
     */
    public function getAverageRatingStars()
    {
        return round($this->averageRating / 2, 1);
    }

    /**
     * Get the average rating colour name
     * green, amber or red
     *
     * @return string
     */
    public function getAverageRatingColour() {
        if (!$this->getAverageRating()) {
            return 'gray';
        } elseif ($this->getAverageRating() > 0 && $this->getAverageRating() <= 5) {
            return 'red';
        } elseif ($this->getAverageRating() > 5 && $this->getAverageRating() <= 7) {
            return 'amber';
        } else {
            return 'green';
        }
    }

    /**
     * Add clientGroup
     *
     * @param \AppBundle\Entity\ClientGroup $clientGroup
     *
     * @return FinancialAdviser
     */
    public function addClientGroup(\AppBundle\Entity\ClientGroup $clientGroup)
    {
        $this->clientGroups[] = $clientGroup;

        return $this;
    }

    /**
     * Remove clientGroup
     *
     * @param \AppBundle\Entity\ClientGroup $clientGroup
     */
    public function removeClientGroup(\AppBundle\Entity\ClientGroup $clientGroup)
    {
        $this->clientGroups->removeElement($clientGroup);
    }

    /**
     * Get clientGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientGroups()
    {
        return $this->clientGroups;
    }

    /**
     * Add clientEventType
     *
     * @param \AppBundle\Entity\ClientEventType $clientEventType
     *
     * @return FinancialAdviser
     */
    public function addClientEventType(\AppBundle\Entity\ClientEventType $clientEventType)
    {
        $this->clientEventTypes[] = $clientEventType;

        return $this;
    }

    /**
     * Remove clientEventType
     *
     * @param \AppBundle\Entity\ClientEventType $clientEventType
     */
    public function removeClientEventType(\AppBundle\Entity\ClientEventType $clientEventType)
    {
        $this->clientEventTypes->removeElement($clientEventType);
    }

    /**
     * Get clientEventTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientEventTypes()
    {
        return $this->clientEventTypes;
    }

    /**
     * Set subscriptionEndsAt
     *
     * @param \DateTime $subscriptionEndsAt
     *
     * @return FinancialAdviser
     */
    public function setSubscriptionEndsAt($subscriptionEndsAt)
    {
        $this->subscriptionEndsAt = $subscriptionEndsAt;

        return $this;
    }

    /**
     * Get subscriptionEndsAt
     *
     * @return \DateTime
     */
    public function getSubscriptionEndsAt()
    {
        return $this->subscriptionEndsAt;
    }

    /**
     * Does the FA have a valid subscription
     *
     * @return boolean
     */
    public function isSubscribed()
    {
        return $this->subscriptionEndsAt && $this->subscriptionEndsAt->getTimestamp() > time();
    }

    /**
     * How many days of subscription does the user have left
     *
     * @return integer
     */
    public function getNumSubscriptionDaysRemaining()
    {
        if ($this->isSubscribed()) {
            $days = ceil(abs($this->subscriptionEndsAt->getTimestamp() - time()) / (60 * 60 * 24));
            return $days;
        } else {
            return 0;
        }
    }


    /**
     * Set nricNumber
     *
     * @param string $nricNumber
     *
     * @return FinancialAdviser
     */
    public function setNricNumber($nricNumber)
    {
        $this->nricNumber = $nricNumber;

        return $this;
    }

    /**
     * Get nricNumber
     *
     * @return string
     */
    public function getNricNumber()
    {
        return $this->nricNumber;
    }

    /**
     * Set race
     *
     * @param string $race
     *
     * @return FinancialAdviser
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Set nationality
     *
     * @param string $nationality
     *
     * @return FinancialAdviser
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get nationality
     *
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set yearJoinedIndustry
     *
     * @param \DateTime $yearJoinedIndustry
     *
     * @return FinancialAdviser
     */
    public function setYearJoinedIndustry($yearJoinedIndustry)
    {
        $this->yearJoinedIndustry = $yearJoinedIndustry;

        return $this;
    }

    /**
     * Get yearJoinedIndustry
     *
     * @return \DateTime
     */
    public function getYearJoinedIndustry()
    {
        return $this->yearJoinedIndustry;
    }

    /**
     * Get years in industry
     *
     * @return integer
     */
    public function getYearsInIndustry()
    {
        if ($this->getYearJoinedIndustry()) {
            return date('Y') - $this->getYearJoinedIndustry()->format('Y');
        }
        return '';
    }

    /**
     * Set parentCompanyName
     *
     * @param string $parentCompanyName
     *
     * @return FinancialAdviser
     */
    public function setParentCompanyName($parentCompanyName)
    {
        $this->parentCompanyName = $parentCompanyName;

        return $this;
    }

    /**
     * Get parentCompanyName
     *
     * @return string
     */
    public function getParentCompanyName()
    {
        return $this->parentCompanyName;
    }

    /**
     * Set professionalContactNumber
     *
     * @param string $professionalContactNumber
     *
     * @return FinancialAdviser
     */
    public function setProfessionalContactNumber($professionalContactNumber)
    {
        $this->professionalContactNumber = $professionalContactNumber;

        return $this;
    }

    /**
     * Get professionalContactNumber
     *
     * @return string
     */
    public function getProfessionalContactNumber()
    {
        return $this->professionalContactNumber;
    }

    /**
     * Set professionalEmailAddress
     *
     * @param string $professionalEmailAddress
     *
     * @return FinancialAdviser
     */
    public function setProfessionalEmailAddress($professionalEmailAddress)
    {
        $this->professionalEmailAddress = $professionalEmailAddress;

        return $this;
    }

    /**
     * Get professionalEmailAddress
     *
     * @return string
     */
    public function getProfessionalEmailAddress()
    {
        return $this->professionalEmailAddress;
    }

    /**
     * Set academicQualification
     *
     * @param string $academicQualification
     *
     * @return FinancialAdviser
     */
    public function setAcademicQualification($academicQualification)
    {
        $this->academicQualification = $academicQualification;

        return $this;
    }

    /**
     * Get academicQualification
     *
     * @return string
     */
    public function getAcademicQualification()
    {
        return $this->academicQualification;
    }

    /**
     * Set academicQualificationOther
     *
     * @param string $academicQualificationOther
     *
     * @return FinancialAdviser
     */
    public function setAcademicQualificationOther($academicQualificationOther)
    {
        $this->academicQualificationOther = $academicQualificationOther;

        return $this;
    }

    /**
     * Get academicQualificationOther
     *
     * @return string
     */
    public function getAcademicQualificationOther()
    {
        return $this->academicQualificationOther;
    }

    /**
     * Set professionalCertifications
     *
     * @param array $professionalCertifications
     *
     * @return FinancialAdviser
     */
    public function setProfessionalCertifications($professionalCertifications)
    {
        $this->professionalCertifications = $professionalCertifications;

        return $this;
    }

    /**
     * Get professionalCertifications
     *
     * @return array
     */
    public function getProfessionalCertifications()
    {
        return $this->professionalCertifications;
    }

    /**
     * Set professionalCertificationsOthers
     *
     * @param string $professionalCertificationsOthers
     *
     * @return FinancialAdviser
     */
    public function setProfessionalCertificationsOthers($professionalCertificationsOthers)
    {
        $this->professionalCertificationsOthers = $professionalCertificationsOthers;

        return $this;
    }

    /**
     * Get professionalCertificationsOthers
     *
     * @return string
     */
    public function getProfessionalCertificationsOthers()
    {
        return $this->professionalCertificationsOthers;
    }

    /**
     * Set professionalMemberships
     *
     * @param array $professionalMemberships
     *
     * @return FinancialAdviser
     */
    public function setProfessionalMemberships($professionalMemberships)
    {
        $this->professionalMemberships = $professionalMemberships;

        return $this;
    }

    /**
     * Get professionalMemberships
     *
     * @return array
     */
    public function getProfessionalMemberships()
    {
        return $this->professionalMemberships;
    }

    /**
     * Set professionalMembershipsOthers
     *
     * @param string $professionalMembershipsOthers
     *
     * @return FinancialAdviser
     */
    public function setProfessionalMembershipsOthers($professionalMembershipsOthers)
    {
        $this->professionalMembershipsOthers = $professionalMembershipsOthers;

        return $this;
    }

    /**
     * Get professionalMembershipsOthers
     *
     * @return string
     */
    public function getProfessionalMembershipsOthers()
    {
        return $this->professionalMembershipsOthers;
    }

    /**
     * Set industryAwards
     *
     * @param string $industryAwards
     *
     * @return FinancialAdviser
     */
    public function setIndustryAwards($industryAwards)
    {
        $this->industryAwards = $industryAwards;

        return $this;
    }

    /**
     * Get industryAwards
     *
     * @return string
     */
    public function getIndustryAwards()
    {
        return $this->industryAwards;
    }

    /**
     * Set communityInvolvement
     *
     * @param string $communityInvolvement
     *
     * @return FinancialAdviser
     */
    public function setCommunityInvolvement($communityInvolvement)
    {
        $this->communityInvolvement = $communityInvolvement;

        return $this;
    }

    /**
     * Get communityInvolvement
     *
     * @return string
     */
    public function getCommunityInvolvement()
    {
        return $this->communityInvolvement;
    }

    /**
     * Set areasOfCompetencies
     *
     * @param string $areasOfCompetencies
     *
     * @return FinancialAdviser
     */
    public function setAreasOfCompetencies($areasOfCompetencies)
    {
        $this->areasOfCompetencies = $areasOfCompetencies;

        return $this;
    }

    /**
     * Get areasOfCompetencies
     *
     * @return string
     */
    public function getAreasOfCompetencies()
    {
        return $this->areasOfCompetencies;
    }

    /**
     * Set companyAddress
     *
     * @param \AppBundle\Entity\Address $companyAddress
     *
     * @return FinancialAdviser
     */
    public function setCompanyAddress(\AppBundle\Entity\Address $companyAddress = null)
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    /**
     * Get companyAddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * Set socialFacebook
     *
     * @param string $socialFacebook
     *
     * @return FinancialAdviser
     */
    public function setSocialFacebook($socialFacebook)
    {
        $this->socialFacebook = $socialFacebook;

        return $this;
    }

    /**
     * Get socialFacebook
     *
     * @return string
     */
    public function getSocialFacebook()
    {
        return $this->socialFacebook;
    }

    /**
     * Set socialTwitter
     *
     * @param string $socialTwitter
     *
     * @return FinancialAdviser
     */
    public function setSocialTwitter($socialTwitter)
    {
        $this->socialTwitter = $socialTwitter;

        return $this;
    }

    /**
     * Get socialTwitter
     *
     * @return string
     */
    public function getSocialTwitter()
    {
        return $this->socialTwitter;
    }

    /**
     * Set socialLinkedIn
     *
     * @param string $socialLinkedIn
     *
     * @return FinancialAdviser
     */
    public function setSocialLinkedIn($socialLinkedIn)
    {
        $this->socialLinkedIn = $socialLinkedIn;

        return $this;
    }

    /**
     * Get socialLinkedIn
     *
     * @return string
     */
    public function getSocialLinkedIn()
    {
        return $this->socialLinkedIn;
    }

    /**
     * Set score
     *
     * @param string $score
     *
     * @return FinancialAdviser
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Add financialAdviserPhoto
     *
     * @param \AppBundle\Entity\FinancialAdviserPhoto $financialAdviserPhoto
     *
     * @return FinancialAdviser
     */
    public function addFinancialAdviserPhoto(\AppBundle\Entity\FinancialAdviserPhoto $financialAdviserPhoto)
    {
        $this->financialAdviserPhotos[] = $financialAdviserPhoto;

        return $this;
    }

    /**
     * Remove financialAdviserPhoto
     *
     * @param \AppBundle\Entity\FinancialAdviserPhoto $financialAdviserPhoto
     */
    public function removeFinancialAdviserPhoto(\AppBundle\Entity\FinancialAdviserPhoto $financialAdviserPhoto)
    {
        $this->financialAdviserPhotos->removeElement($financialAdviserPhoto);
    }

    /**
     * Get financialAdviserPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinancialAdviserPhotos()
    {
        return $this->financialAdviserPhotos;
    }

    /**
     * Get financialAdviserPhotos keyed by type
     *
     * @return array
     */
    public function getFinancialAdviserPhotosKeyedByType()
    {
        $photos = [];
        foreach ($this->getFinancialAdviserPhotos() as $photo) { /* @var $photo FinancialAdviserPhoto */
            $photos[$photo->getType()][] = $photo;
        }
        return $photos;
    }


    /**
     * Set religion
     *
     * @param string $religion
     *
     * @return FinancialAdviser
     */
    public function setReligion($religion)
    {
        $this->religion = $religion;

        return $this;
    }

    /**
     * Get religion
     *
     * @return string
     */
    public function getReligion()
    {
        return $this->religion;
    }

    /**
     * Set profileContent
     *
     * @param string $profileContent
     *
     * @return FinancialAdviser
     */
    public function setProfileContent($profileContent)
    {
        $this->profileContent = $profileContent;

        return $this;
    }

    /**
     * Get profileContent
     *
     * @return string
     */
    public function getProfileContent()
    {
        return $this->profileContent;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return FinancialAdviser
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set licenceNo
     *
     * @param string $licenceNo
     *
     * @return FinancialAdviser
     */
    public function setLicenceNo($licenceNo)
    {
        $this->licenceNo = $licenceNo;

        return $this;
    }

    /**
     * Get licenceNo
     *
     * @return string
     */
    public function getLicenceNo()
    {
        return $this->licenceNo;
    }

    /**
     * Set areasOfCompetenciesOthers
     *
     * @param string $areasOfCompetenciesOthers
     *
     * @return FinancialAdviser
     */
    public function setAreasOfCompetenciesOthers($areasOfCompetenciesOthers)
    {
        $this->areasOfCompetenciesOthers = $areasOfCompetenciesOthers;

        return $this;
    }

    /**
     * Get areasOfCompetenciesOthers
     *
     * @return string
     */
    public function getAreasOfCompetenciesOthers()
    {
        return $this->areasOfCompetenciesOthers;
    }

    /**
     * Set languages
     *
     * @param array $languages
     *
     * @return FinancialAdviser
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Get languages
     *
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set languagesOthers
     *
     * @param string $languagesOthers
     *
     * @return FinancialAdviser
     */
    public function setLanguagesOthers($languagesOthers)
    {
        $this->languagesOthers = $languagesOthers;

        return $this;
    }

    /**
     * Get languagesOthers
     *
     * @return string
     */
    public function getLanguagesOthers()
    {
        return $this->languagesOthers;
    }

    /**
     * Set institution
     *
     * @param string $institution
     *
     * @return FinancialAdviser
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Set balanceScoreCard
     *
     * @param string $balanceScoreCard
     *
     * @return FinancialAdviser
     */
    public function setBalanceScoreCard($balanceScoreCard)
    {
        $this->balanceScoreCard = $balanceScoreCard;

        return $this;
    }

    /**
     * Get balanceScoreCard
     *
     * @return string
     */
    public function getBalanceScoreCard()
    {
        return $this->balanceScoreCard;
    }

    /**
     * Set bpRate
     *
     * @param string $bpRate
     *
     * @return FinancialAdviser
     */
    public function setBpRate($bpRate)
    {
        $this->bpRate = $bpRate;

        return $this;
    }

    /**
     * Get bpRate
     *
     * @return string
     */
    public function getBpRate()
    {
        return $this->bpRate;
    }

    /**
     * Set professionalTitle
     *
     * @param string $professionalTitle
     *
     * @return FinancialAdviser
     */
    public function setProfessionalTitle($professionalTitle)
    {
        $this->professionalTitle = $professionalTitle;

        return $this;
    }

    /**
     * Get professionalTitle
     *
     * @return string
     */
    public function getProfessionalTitle()
    {
        return $this->professionalTitle;
    }

    /**
     * Set typeOfQualification
     *
     * @param string $typeOfQualification
     *
     * @return FinancialAdviser
     */
    public function setTypeOfQualification($typeOfQualification)
    {
        $this->typeOfQualification = $typeOfQualification;

        return $this;
    }

    /**
     * Get typeOfQualification
     *
     * @return string
     */
    public function getTypeOfQualification()
    {
        return $this->typeOfQualification;
    }

    /**
     * Set specialInterest
     *
     * @param string $specialInterest
     *
     * @return FinancialAdviser
     */
    public function setSpecialInterest($specialInterest)
    {
        $this->specialInterest = $specialInterest;

        return $this;
    }

    /**
     * Get specialInterest
     *
     * @return string
     */
    public function getSpecialInterest()
    {
        return $this->specialInterest;
    }
}
