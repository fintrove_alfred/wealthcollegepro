<?php
/**
 * ErrorRetrieval.php
 *
 * @author Gul
 */

namespace AppBundle\Traits;


trait ErrorRetrieval
{

   protected $errors = [];

   public function getErrors() {
      return $this->errors;
   }

   public function hasErrors() {
      return count($this->errors) > 0;
   }

   public function addError($error) {
      $this->errors[] = $error;
   }

   public function setErrors(array $errors) {
      $this->errors = $errors;
   }

   public function clearErrors() {
      $this->errors = [];
   }

}