<?php

namespace AppBundle\Tests;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class BaseWebTestCase extends TestCase
{


    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContiner() {
        return $this->getClient()->getContainer();
    }

    /**
     * @param $entityName
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo($entityName) {
        if (strpos($entityName, ':') === FALSE) {
            // not specified bundle, set default
            $entityName = 'AppBundle:'.$entityName;
        }
        return $this->getEm()->getRepository($entityName);
    }

    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getContiner()->get('doctrine')->getManager();
    }


}