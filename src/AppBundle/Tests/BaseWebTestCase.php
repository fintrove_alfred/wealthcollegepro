<?php
/**
 * Created by PhpStorm.
 * User: Gul
 * Date: 14/09/2015
 * Time: 11:25
 */

namespace AppBundle\Tests;


use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class BaseWebTestCase extends WebTestCase
{

    /**
     * @param array $options
     * @param array $server
     * @return Client
     */
    public function getClient($options = [], $server = []) {
        $defaultOptions = [
           'environment' => 'test',
           'debug' => true
        ];
        $defaultServer = [
           'HTTP_HOST' => 'app-test'
        ];
        $options = array_merge($defaultOptions, $options);
        $server = array_merge($defaultServer, $server);
        
        return static::createClient($options, $server);
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContiner() {
        return $this->getClient()->getContainer();
    }

    /**
     * @param $entityName
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepo($entityName) {
        if (strpos($entityName, ':') === FALSE) {
            // not specified bundle, set default
            $entityName = 'AppBundle:'.$entityName;
        }
        return $this->getEm()->getRepository($entityName);
    }

    /**
     * Get the Entity Manager
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getContiner()->get('doctrine')->getManager();
    }


}