<?php
namespace AppBundle\Tests;

use AppBundle\Tests\BaseWebTestCase;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class AdminPageLoadTest extends BaseWebTestCase
{
    public function testLinkReplacement()
    {

        $text = '{link:Hello World!} random text {link:Hello World!}';
        $url = 'http://www.google.com';

        $reviewManager = $this->getContiner()->get('app.manager.review_manager');
        $text = $reviewManager->processPlaceholders($text, ['link' => $url]);

        $this->assertTrue(strpos($text, '<a href="http://www.google.com">Hello World!</a>') !== false, 'Unexpected value: '.$text);

    }

    public function testAdviserNameReplacement()
    {

        $text = 'test {adviserName} text';
        $value = 'Gul Khan';

        $reviewManager = $this->getContiner()->get('app.manager.review_manager');
        $text = $reviewManager->processPlaceholders($text, ['adviserName' => $value]);

        $this->assertTrue(strpos($text, 'Gul Khan') !== false, 'Unexpected value: '.$text);

    }

    public function testAdviserNameLinkReplacement()
    {

        $text = 'test {link:Hello World!} test {adviserName} test';

        $reviewManager = $this->getContiner()->get('app.manager.review_manager');
        $text = $reviewManager->processPlaceholders($text, ['adviserName' => 'Gul Khan', 'link' => 'http://www.google.com']);

        $this->assertTrue(strpos($text, '<a href="http://www.google.com">Hello World!</a>') !== false, 'Unexpected value: '.$text);
        $this->assertTrue(strpos($text, 'Gul Khan') !== false, 'Unexpected value: '.$text);

    }
}
