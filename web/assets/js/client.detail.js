/**
 * Created by Gul on 22/03/2016.
 */

if (!App.Client) {
    App.Client = {};
}

App.Client.numRelations = 0;


App.Client.init_profile_form = function () {

    App.Client.numRelations = $('.client-relations .client-relation').length;

    $('.add-client-relation').on('click', function () {


        var prototype = $('.client-relations').data('prototype');

        prototype = prototype.replace(/__name__/g, App.Client.numRelations);

        $('.client-relations').append(
            $('<div class="form-group client-relation" id="client-relation-' + App.Client.numRelations + '"></div>')
                .append(
                    $('<div class="col-sm-4"></div>').append($(prototype).find('.relatedClient').html())
                ).append(
                    $('<div class="col-sm-2"><strong>is '+$('#client_profile_person_firstName').val()+'\'s</strong></div>')
                ).append(
                $('<div class="col-sm-4"></div>').append($(prototype).find('.relationshipTypeId').html())
            ).append(
                $('<div class="col-sm-2"></div>').append('<button class="btn btn-default remove-client-relation" type="button"><i class="fa fa-remove"></i> Remove</button>')
            )
        );

        $('#client-relation-' + App.Client.numRelations + ' select').select2();

        App.Client.numRelations++;
    });

    $('.client-relations').on('click', '.remove-client-relation', function () {
        $(this).parents('.client-relation').remove();
        $(this).parent('form').submit();
        App.Client.numRelations--;
    });

    App.Client.numDates = $('.client-dates .client-date').length;

    $('.add-client-date').on('click', function () {

        var prototype = $('.client-dates').data('prototype');

        prototype = prototype.replace(/__name__/g, App.Client.numDates);

        $('.client-dates').append(
            $('<div class="form-group client-date" id="client-date-' + App.Client.numDates + '"></div>')
                .append(
                    $('<div class="col-sm-3"></div>')
                        .append(
                            $('<div class="form-group-default"></div>').html(
                                $(prototype).find('.dateTypeId').html()
                                )
                                .append(
                                    $('<div class="date-type-name" style="display:none"></div>')
                                        .append($(prototype).find('.dateTypeName').html())
                                )
                        )
                ).append(
                $('<div class="col-sm-4"></div>').append(
                    $('<div class="form-group-default"></div>').html($(prototype).find('.date').html())
                )
            ).append(
                $('<div class="col-sm-3"></div>')
                    .append(
                        $('<div class="form-group-default"></div>').html($(prototype).find('.reminder').html())
                            .append(
                                $('<div class="date-reminder" style="display:none"></div>')
                                    .append($(prototype).find('.reminderDate').html())
                            )
                    )
            ).append(
                $('<div class="col-sm-2"></div>').append('<button class="btn btn-default remove-client-date" type="button"><i class="fa fa-remove"></i> Remove</button>')
            )
        );

        //$('#client-date-' + App.Client.numDates + ' input[type=date]').datepicker({format: 'yyyy-mm-dd'});
        // $('#client-date-' + App.Client.numDates + ' input[type=date]').inputmask("99/99/9999");
        // console.log($('#client-date-' + App.Client.numDates + ' input[type=date]').length);
        App.init_date_mask();

        App.Client.numDates++;
    });

    $('.client-dates').on('click', '.remove-client-date', function () {
        $(this).parents('.client-date').remove();
        $(this).parent('form').submit();
        App.Client.numDates--;
    });

    $('.client-dates').on('change', '.date-type-id', function () {
        if ($(this).val() == 3) {
            // selected OTHER, show date type name field
            $(this).next().show();
        } else {
            // hide date type name field
            $(this).next().hide();
        }
    });

    $('.client-dates').on('change', 'input.date-reminder', function () {
        if ($(this).is(':checked')) {
            // selected OTHER, show date type name field
            $(this).next().show();
        } else {
            // hide date type name field
            $(this).next().hide();
        }
    });

    $('#profile-form').on('submit', function (e) {
        e.preventDefault();

        var form = $('#profile-form');

        App.set_progress('#profile-form', true);

        $.post(form.attr('action'), form.serializeArray(), function (response) {

            App.set_progress('#profile-form', false);
            if (!response.success) {
                return App.handle_error_response(response);
            }

            App.notify('Client Saved', 'Client saved successfully', 'success');
            App.Client.update_client_date_indices();
            App.Client.update_client_relation_indices();
            form.trigger('saved');

        }).error(function () {
            App.set_progress('#profile-form', false);
        })
    });



    $('#photo-upload').fileupload({
        url: $('#photo-upload-id').data('url'),
        dataType: 'json',
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        //disableImageResize: /Android(?!.*Chrome)|Opera/
        //   .test(window.navigator && navigator.userAgent),
        //imageMaxWidth: 800,
        //imageMaxHeight: 800,
        imageCrop: false, // Force cropped images
        singleFileUploads: true,
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#photo-upload-progress').show();
            $('#photo-upload-progress .progress-bar').css('width', progress + '%');
        },
        done: function (e, data) {
            if (!data.result.success) {
                return App.handle_error_response(data.result);
            }
            $('#photo-upload-progress').hide();
            $('#photo-upload img').attr('src', data.result.data.url);
            App.notify('Photo', 'Upload Successful', 'success');
        }
    });


    /**
     * confirm navigate away if forms have changed
     */
    $('form.confirm-exit').each(function () {
        $(this).data('serialize',$(this).serialize());
        $(this).on('saved', function () {
            console.log($(this).serialize());
            $(this).data('serialize',$(this).serialize());

        })
    });
    // On load save form current state
    $(window).bind('beforeunload', function(e){
        var changed = false;
        $('form.confirm-exit').each(function () {

            if($(this).serialize() != $(this).data('serialize')) {
                changed = true;
            }
            // i.e; if form state change show box not.
        });

        if (changed) {
            return true;
        } else {
            e = null;
        }


    });

    App.init_date_mask();


}

App.Client.update_client_date_indices= function () {

    $('.client-date').each(function (i, cr) {
        $(cr).find('select,input').each(function (j, el) {
            var name = $(el).attr('name');
            name = name.replace(/\[clientDates\]\[(\d+)\]/gi, '[clientDates]['+i+']');
            $(el).attr('name', name);

        })
    });

}

App.Client.init_client_event_form = function () {

    $('body').on('submit', '#client-event-form', function (e) {
        e.preventDefault();

        var form = $('#client-event-form');

        App.set_progress('#pipeline', true);

        $.post(form.attr('action'), form.serializeArray(), function (response) {

            App.set_progress('#pipeline', false);
            if (!response.success) {
                return App.handle_error_response(response);
            }

            App.notify('Pipeline Event Added', 'Pipeline event added successfully', 'success');

            $('#client-event-form')[0].reset();
            $('#client_event_clientEventType').trigger('change');
            // add to pipeline
            $('.timeline-container .timeline').append(response.data.eventHTML);

        }).error(function () {
            App.set_progress('#pipeline', false);
        })
    });

    $('body').on('saved', '#client-event-types-form', function (e) {
        App.notify("Event Types", "Updated successfully", 'success');

        $('#client-event-types-modal').modal('hide');

        // recreate event types select field
        $('#client_event_clientEventType').select2('destroy');
        var response = $(this).data('last-response');
        $('#client_event_clientEventType').html('');
        for (id in response.data.clientEventTypeOptions) {
            $('#client_event_clientEventType').append('<option value="'+id+'">'+response.data.clientEventTypeOptions[id]+'</option>');
        }
        $('#client_event_clientEventType').select2();
    });

    $('#client-event-types-modal').on('show.bs.modal', function () {
        // load form
        App.set_progress('#client-event-types-modal .modal-body', true);

        $.get(Routing.generate('ajax_client_event_types_form', {id: $('#client-id').val()}), {}, function (response) {
            $('#client-event-types-modal .modal-body').html(response);
            App.set_progress('#client-event-types-modal  .modal-body', false);
        });
        
    });

    var $timeline_block = $('.timeline-block');

    //hide timeline blocks which are outside the viewport
    $timeline_block.each(function(){
        if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
            $(this).find('.timeline-point, .timeline-content').addClass('is-hidden');
        }
    });

    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function(){
        $timeline_block.each(function(){
            if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.timeline-point').hasClass('is-hidden') ) {
                $(this).find('.timeline-point, .timeline-content').removeClass('is-hidden').addClass('bounce-in');
            }
        });
    });

    $('body').on('click', '.client-event .remove', function () {
        var id = $(this).data('id');


        App.confirm('Confirm Removal', 'Are you sure you want to remove this event from the pipeline?', function () {

            App.set_progress('#pipeline', true);
            $.post(Routing.generate('ajax_client_event_remove', {id: id}), {}, function (response) {
                App.set_progress('#pipeline', false);
                if (!response.success) {
                    return App.handle_error_response(response);
                }

                $('#client-event-'+id).remove();
                App.notify('Event Removed', 'Event removed successfully', 'success');
            }).error(function () {
                App.set_progress('#pipeline', false);
            })
        });

    });
}

App.Client.update_client_relation_indices  = function () {

    $('.client-relation').each(function (i, cr) {
        $(cr).find('select,input').each(function (j, el) {
            var name = $(el).attr('name');
            name = name.replace(/\[clientRelations\]\[(\d+)\]/gi, '[clientRelations]['+i+']');
            $(el).attr('name', name);

        })
    });

}



$(document).ready(function () {

    App.Client.init_profile_form();
    App.Client.init_client_event_form();

    if (window.location.hash) {
        var tab = $('.nav.nav-tabs a[href="'+window.location.hash+'"]');
        if (tab.length) {
            tab.click();
        }
    }



});