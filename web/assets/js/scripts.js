App = {};

/**
 *
 * @param title
 * @param message
 * @param type      One of 'success', 'error', 'info'
 */
App.notify = function (title, message, type) {
    $('body').pgNotification({
        style: 'circle',
        title: title,
        message: message,
        position: 'top-right',
        timeout: 5000,
        type: type,
        thumbnail: '<img width="40" height="40" style="display: inline-block;" src="/assets/img/logo-icon.png" data-src="/assets/img/logo-icon.png" data-src-retina="/assets/img/logo-icon.png" alt="">'
    }).show();
}

App.alert = function (title, message) {
    $('#message-modal .title').html(title);
    $('#message-modal .content').html('<p>'+message+'</p>');
    $('#message-modal').modal('show');
};

App.confirm = function (title, message, success_handler, options) {

    var defaultOptions = {
        'confirmText': 'Yes',
        'cancelText': 'No'
    }

    if (typeof options == 'undefined') {
        options = defaultOptions;
    } else {
        $.extend(defaultOptions, options);
        options = defaultOptions;
    }


    $('#confirm-modal .title').html(title);
    $('#confirm-modal .content').html('<p>'+message+'</p>');
    $('#confirm-modal button.confirm').unbind('click');
    $('#confirm-modal button.confirm').on('click', success_handler);
    $('#confirm-modal .btn.btn-danger').html(options.cancelText);
    $('#confirm-modal .btn.btn-success').html(options.confirmText);
    $('#confirm-modal').modal('show');

};

App.handle_error_response = function (response, title) {

    var errors = typeof response.errors == 'undefined' ? '' : response.errors;

    if (typeof title == 'undefined') {
        if (typeof response.errorTitle !== 'undefined') {
            title = response.errorTitle;
        } else {
            title = 'Error';
        }

    }

    var html = '';
    if ($.isArray(errors)) {
        if (errors.length == 1) {
            html = '<p>'+errors[0]+'</p>';
        } else {
            html = '<ul><li>' + errors.join('</li><li>') + '</li>';
        }
    } else if (typeof errors == 'string') {
        html = errors;
    } else if (Object.keys(errors).length) {
        html = '<ul>';
        for (error in errors) {
            html += '<li>' + errors[error] + '</li>';
        }
    } else {
        html = '<p>'+errors+'</p>';
    }

    if (!errors) {
        html = '<p>Sorry, there was a problem carrying out your request</p>';
    }

    $('#message-modal .title').html(title);
    $('#message-modal .content').html(html);
    $('#message-modal').modal('show');
};

App.set_progress = function (elementId, show) {
    if (show) {
        $(elementId).append('<div class="progress-circle-indeterminate center"></div>');
        $(elementId).append('<div class="progress-overlay"></div>');
    } else {
        $(elementId+ ' .progress-circle-indeterminate').remove();
        $(elementId+ ' .progress-overlay').remove();
    }
};

App.live_form_submit_handler = function(e, form) {

    e.preventDefault();
    if (typeof form == 'undefined') {
        form = $(this);
    }
    var form_id = '#'+form.attr('id');

    App.set_progress(form_id, true);

    $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: form.serializeArray(),
        dataType: "json",
        success: function (response, status) {
            App.set_progress(form_id, false);

            if (!response.success) {
                return App.handle_error_response(response);
            }

            form.data('last-response', response);
            form.trigger('saved');

            if (typeof response.notification_message != 'undefined') {
                App.alert('Success', response.notification_message);
            }
        },
        error: function (xhr, status, error) {
            App.set_progress(form_id, false);
            App.handle_error_response({errors: error}, 'Error submitting request');
        },
        complete: function () {
            App.set_progress(form_id, false);
        }
    })

};

App.init_form_elements = function (parentElementId) {
    
    parentElementId = typeof parentElementId == 'undefined' ? '' : parentElementId+' ';
    $(parentElementId+'.date-time-picker').each(function () {

        var options = {
            norange: true, // use only one value
            cells: [1, 1], // show only one month

            resizeButton: false, // deny resize picker
            fullsizeButton: false,
            fullsizeOnDblClick: false,

            formatDateTime: 'YYYY-MM-DDTHH:mm:ssZ',
            formatDecoreDateTimeWithYear: 'DD/MM/YYYY HH:mm',
            formatDecoreDateTime: 'DD/MM/YYYY HH:mm',

            timepicker: true, // use timepicker
            timepickerOptions: {
                hours: true,
                minutes: true,
                seconds: false,
                ampm: true
            }
        };

        if ($(this).data('min-date')) {
            options.minDate = $(this).data('min-date');
        }
        if ($(this).data('max-date')) {
            options.maxDate = $(this).data('max-date');
        }
        $(this).periodpicker(options);

    });

    $(parentElementId+'.date-picker').each(function () {

        var options = {
            norange: true, // use only one value
            cells: [1, 1], // show only one month

            resizeButton: false, // deny resize picker
            fullsizeButton: false,
            fullsizeOnDblClick: false,

            formatDate: 'YYYY-MM-DD',

            timepicker: false // use timepicker
        };

        if ($(this).data('min-date')) {
            options.minDate = $(this).data('min-date');
        }
        if ($(this).data('max-date')) {
            options.maxDate = $(this).data('max-date');
        }

        $(this).periodpicker(options);

    });

    $(parentElementId+'.time-picker').each(function () {

        var options = {
            hours: true,
            minutes: true,
            seconds: false,
            ampm: true
        };


        $(this).timepickeralone(options);

    });

    if (parentElementId != '') {
        $(parentElementId + ' .cs-select').each(function () {
            new SelectFx($(this)[0]);
        });
        $(parentElementId + ' select[data-init-plugin="select2"]').each(function () {
            $(this).select2();
        });
    }

}

App.init_date_mask = function () {
    $('.date-mask').inputmask("99/99/9999");
};

$(document).ready(function() {
    // Initializes search overlay plugin.
    // Replace onSearchSubmit() and onKeyEnter() with
    // your logic to perform a search and display results
    $('[data-pages="search"]').search({
        searchField: '#overlay-search',
        closeButton: '.overlay-close',
        suggestions: '#overlay-suggestions',
        brand: '.brand',
        onSearchSubmit: function(searchString) {

            $.get(Routing.generate('site_search', {search: searchString}), function (response) {

                if (!response.success) {
                    App.handle_error_response(response);
                    return false;
                }

                $('#search-results').html('');
                if (response.data.contacts.length > 0) {
                    for (var i in response.data.contacts) {
                        if( !response.data.contacts.hasOwnProperty(i) ) {
                            continue;
                        }
                        var contact = response.data.contacts[i];
                        $('#search-results').append("<div class=\"col-md-4\"><div class=\"\">" +
                                "<div class=\"thumbnail-wrapper d48 circular bg-success text-white inline m-t-10\">" +
                                    "<div>"+contact.initials+"</div>" +
                                "</div>" +
                                "<div class=\"p-l-10 inline p-t-5\">" +
                                    "<h5 class=\"m-b-5\"><a href='"+Routing.generate('client_detail', {id: contact.id})+"'><span class=\"semi-bold result-name\">"+contact.firstName+" "+contact.lastName+"</span></a></h5>" +
                                "</div>" +
                            "</div></div>"
                        );
                    }
                    $('.search-results').fadeIn("fast");
                } else {
                    $('#search-results').html("<p>No contacts found matching that name</p>");
                }




            })

        },
        onKeyEnter: function(searchString) {
            //console.log("Live search for: " + searchString);
            // var searchField = $('#overlay-search');
            // var searchResults = $('.search-results');
            // clearTimeout($.data(this, 'timer'));
            // searchResults.fadeOut("fast");
            // var wait = setTimeout(function() {
            //     searchResults.find('.result-name').each(function() {
            //         if (searchField.val().length != 0) {
            //             $(this).html(searchField.val());
            //             searchResults.fadeIn("fast");
            //         }
            //     });
            // }, 500);
            // $(this).data('timer', wait);
        }
    });

    $('body').on('submit', 'form.live-form', App.live_form_submit_handler);

    App.init_form_elements();

});