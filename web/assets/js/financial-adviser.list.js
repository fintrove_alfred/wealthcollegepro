
'use strict';

App.FinancialAdviser = {};

var init_datatable = function (tableId) {
    var table = $(tableId);

    var settings = {
        "sDom": "<'table-responsive't><'row'<p i>>",
        "destroy": true,
        "scrollCollapse": true,
        "ordering": false,
        "oLanguage": {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
        }

    };

    App.FinancialAdviser.table = table.DataTable(settings);

    // search box for table
    $('#search-table').on( 'keyup', function () {
        App.FinancialAdviser.table.search( $('#search-table').val() ).draw();
    });

    $('#fa-table').on('click', 'tr', function () {
       if ($(this).data('id')) {
           location.href = Routing.generate('profile_view', {id: $(this).data('id')});
       }
    });
};

$(document).ready(function() {

    init_datatable('#fa-table');

    $("#filter-countryId").select2({placeholder: 'All Countries'});
    $("#filter-city").select2({placeholder: 'All Cities'});
    $("#filter-orderBy").select2();

})