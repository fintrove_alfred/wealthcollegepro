/**
 * Created by Gul on 22/03/2016.
 */

if (!App.Client) {
   App.Client = {};
}

App.Client.all_selected = false;
App.Client.all_selected_in_effect = false;

App.Client.client_saved_handler = function (response) {

   $('#add-client-modal').modal('hide');
   App.set_progress('#clients-container', true);
   location.reload();

};

App.Client.edit_client = function () {

   var id = $(this).data('id');
   var form = $('#save-client-form');

   $('#add-client-modal').modal('show');
   form[0].reset();
   App.set_progress('#save-client-form', true);

   $.get(Routing.generate('ajax_client_data', {id: id}), function (response) {

      App.set_progress('#save-client-form', false);

      if (!response.success) {
         return App.handle_error_response(response);
      }

      // update the client form with the new data
      form.find('.client-id').val(response.data.id);
      form.find('#client_person_title').val(response.data.title);
      form.find('#client_person_firstName').val(response.data.firstName);
      form.find('#client_person_lastName').val(response.data.lastName);
      form.find('#client_person_dob').val(response.data.dob);
      form.find('#client_person_email').val(response.data.email);
      form.find('#client_person_telephone').val(response.data.telephone);
      form.find('#client_person_mobile').val(response.data.mobile);
      form.find('#client_address_address1').val(response.data.address1);
      form.find('#client_address_address2').val(response.data.address2);
      form.find('#client_address_city').val(response.data.city);
      form.find('#client_address_postCode').val(response.data.postCode);
      form.find('#client_address_country').val(response.data.country);
      

   });

};

App.Client.delete_client = function () {

   var id = $(this).data('id');

   App.set_progress('#save-client-form', true);

   App.confirm('Delete Client', 'Are you sure you want to delete this client?', function () {

      App.set_progress('#clients-table_wrapper', true);

      $.post(Routing.generate('ajax_client_delete'), {id: id}, function (response) {

         App.set_progress('#clients-table_wrapper', false);

         if (!response.success) {
            return App.handle_error_response(response);
         }

         App.notify('Client Deleted', 'Client deleted successfully');


         $('#clients-table').DataTable().rows('#client-'+id+'-row').remove().draw();

      })
      .always(function () {
         App.set_progress('#clients-table_wrapper', false);
      });

   });
};

var init_datatable = function (tableId) {
   var table = $(tableId);

   var settings = {
      "sDom": "<'exportOptions'T><'table-responsive't><'row'<p i>>",
      "destroy": true,
      "scrollCollapse": true,
      "oLanguage": {
         "sLengthMenu": "_MENU_ ",
         "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
      },

      "order": [[ 1, "asc" ]],
      "columnDefs": [
         { "width": "5%", "targets": 0 },
         { "width": "20%", "targets": 1 },
         { "width": "20%", "targets": 2 },
         //{ "width": "20%", "targets": 3 },
         //{ "width": "10%", "targets": 4 },
         { "width": "10%", "targets": 3 },
         { "width": "15%", "targets": 4 },
         { "orderable": false, "targets": [0,4] }
      ],
      "iDisplayLength": 5,
      "oTableTools": {
         "sRowSelect": "multi",
         "sRowSelector": 'td:first-child',
         "sSwfPath": "/assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
         "aButtons": [{
            "sExtends": "csv",
            "sButtonText": "<i class='pg-grid'></i>",
            "sToolTip": "Download as CSV"
         }, {
            "sExtends": "xls",
            "sButtonText": "<i class='fa fa-file-excel-o'></i>",
            "sToolTip": "Download as XLS"
         }, {
            "sExtends": "pdf",
            "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
            "sToolTip": "Download as PDF"
         }, {
            "sExtends": "copy",
            "sButtonText": "<i class='fa fa-copy'></i>",
            "sToolTip": "Copy Data to Clipboard"
         },
         "select_all",
         "select_none",
         {
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-send'></i> Invite to Webinar",
            "fnClick": App.Client.invite_to_webinar
         }]
      },
      fnDrawCallback: function(oSettings) {
         $('.export-options-container').append($('.exportOptions'));

         $('#ToolTables_'+tableId+'_0').tooltip({
            title: 'Export as CSV',
            container: 'body'
         });

         $('#ToolTables_'+tableId+'_1').tooltip({
            title: 'Export as Excel',
            container: 'body'
         });

         $('#ToolTables_'+tableId+'_2').tooltip({
            title: 'Export as PDF',
            container: 'body'
         });

         $('#ToolTables_'+tableId+'_3').tooltip({
            title: 'Copy data',
            container: 'body'
         });
      }

   };

   App.Client.clients_table = table.DataTable(settings);

   // search box for table
   $('#search-table').on( 'keyup', function () {
      App.Client.clients_table.search( $('#search-table').val() ).draw();
   });


};

App.Client.invite_to_webinar = function () {

   var count = App.Client.clients_table.rows('.active').count();
   if (count == 0) {
      return App.alert('Select a contact', 'Please first select a contact to invite to your webinar');
   }

   App.Client.inviteClientIDs = [];
   App.Client.clients_table.rows('.active').every(function (rowIdx, tableLoop, rowLoop) {
      App.Client.inviteClientIDs.push($(this.node()).data('id'));
   });
   
   $('#invite-to-webinar-modal').modal('show');

   $('#invite-to-webinar-modal .clients-selected').html(App.Client.inviteClientIDs.length);



};

$(document).ready(function () {

   $('#save-client-form').on('saved', App.Client.client_saved_handler);
   $('#clients-table').on('click', 'button.edit-client', App.Client.edit_client);
   $('.delete-client').on('click', App.Client.delete_client);

   init_datatable('#clients-table');

   $('#invite-to-webinar-form').on('submit', function (e) {
      e.preventDefault();

      var form = $('#invite-to-webinar-form');
      var calendarEventId = $('#invite-event-id').val();

      if (!calendarEventId) {
         App.alert('Select Webinar', 'Please select the webinar you want to invite your contacts to');
         return false;
      }
      if (App.Client.inviteClientIDs.length == 0) {
         App.alert('Select Contacts', 'Please select the contacts you want to invite to the webinar');
         return false;
      }

      App.set_progress('#invite-to-webinar-form', true);

      $.post(form.attr('action'), {calendarEventId: calendarEventId, clientIds: App.Client.inviteClientIDs}, function (response) {

         if (!response.success) {
            return App.handle_error_response(response);
         }

         form[0].reset();
         $('#invite-to-webinar-modal').modal('hide');

         var result = $('<p></p>');
         result.html('<span><strong>'+response.data.invited+'</strong> contacts invited to the webinar</span><br/>');
         if (response.data.alreadyInvited) {
            result.append('<span><strong>'+response.data.alreadyInvited+'</strong> contacts had already been invited to the webinar</span><br/>');
         }
         if (response.data.noEmail) {
            result.append('<span><strong>'+response.data.noEmail+'</strong> contacts did not have an email address so could not be invited</span><br/>');
         }

         App.alert('Invite to Webinar', result.html());


      }).error(function () {
         App.set_progress('#invite-to-webinar-form', false);
      }).always(function () {
         App.set_progress('#invite-to-webinar-form', false);
      });

   });

   $('#search-tools').nestable();
   $('#show-search-tools').on('click', function () {
      $('#search-tools-container').toggle();
   });
   $('#search-tools-container').hide();

   $('form#clients-filter input[name="filter[typeId]"]').on('change', function () {
      $('form#clients-filter').submit();
   })
});