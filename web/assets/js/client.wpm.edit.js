/**
 * Created by Gul on 09/05/2016.
 */

if (!App.ClientWPM) {
    App.ClientWPM = {};
}

App.ClientWPM.init_insurance_document_upload = function (elementId, insurancePolicyId) {


    $(elementId).fileupload({
        url: Routing.generate('ajax_client_insurance_policy_upload', {id: insurancePolicyId}),
        dataType: 'json',
        singleFileUploads: true,
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#insurance-policy-progress-'+insurancePolicyId).show();
            $('#insurance-policy-progress-'+insurancePolicyId+' .progress-bar').css('width', progress + '%');
        },
        done: function (e, data) {
            $('#insurance-policy-progress-'+insurancePolicyId).hide();

            if (!data.result.success) {
                return App.handle_error_response(data.result);
            }
            $('#insurance-document-'+insurancePolicyId).html('<a href="'+Routing.generate('client_insurance_policy_download', {id: insurancePolicyId})+'" target="_blank" class="btn btn-warning m-t-5"><i class="fa fa-download"></i> Download: '+data.result.data.name+'</a>');
            $('#insurance-document-'+insurancePolicyId).show();
            App.notify('Insurance Policy Document', 'Upload Successful', 'success');
        }
    });

}
App.ClientWPM.update_insurance_indices  = function () {

    $('.insurance-policy').each(function (i, ip) {
        $(ip).find('select,input,textarea').each(function (j, el) {
            var name = $(el).attr('name');
            name = name.replace(/\[clientWPMInsurancePolicies\]\[(\d+)\]/gi, '[clientWPMInsurancePolicies]['+i+']');
            $(el).attr('name', name);

        })
    });

}
App.ClientWPM.update_investment_indices  = function () {

    $('.investment').each(function (i, ip) {
        $(ip).find('select,input,textarea').each(function (j, el) {
            var name = $(el).attr('name');
            name = name.replace(/\[clientWPMInvestments\]\[(\d+)\]/gi, '[clientWPMInvestments]['+i+']');
            $(el).attr('name', name);

        })
    });

}
App.ClientWPM.update_estate_indices  = function () {

    $('.estate').each(function (i, ip) {
        $(ip).find('select,input,textarea').each(function (j, el) {
            var name = $(el).attr('name');
            name = name.replace(/\[clientWPMEstates\]\[(\d+)\]/gi, '[clientWPMEstates]['+i+']');
            $(el).attr('name', name);

        })
    });

}

$(document).ready(function () {


    $('#cashflow-form').validate();
    $('#cashflow-form').on('saved', function () {
        App.notify('Cashflow', 'Saved successfully', 'success');
    });

    $('#insurance-form').validate();
    $('#insurance-form').on('saved', function () {
        App.notify('Insurance Planning', 'Saved successfully', 'success');
    });

    $('#investment-form').validate();
    $('#investment-form').on('saved', function () {
        App.notify('Investment Planning', 'Saved successfully', 'success');
    });

    $('#estate-form').validate();
    $('#estate-form').on('saved', function () {
        App.notify('Estate Planning', 'Saved successfully', 'success');
    });

    $('#add-insurance-policy').on('click', function () {

        App.set_progress('#insurance-form', true);

        var clientId = $(this).data('client-id');
        $.get(Routing.generate('ajax_client_add_client_wpm_insurance_policy', {id: clientId, numPolicies: $('.insurance-policy').length}), function (response) {

            App.set_progress('#insurance-form', false);
            if (!response.success) {
                return App.handle_error_response(response);
            }

            $('#insurance-policies').append(response.data.formHTML);
            $('#save-insurance-policies').show();

            // init file upload
            var elementId = '#insurance-document-upload-'+response.data.id;
            App.ClientWPM.init_insurance_document_upload(elementId, response.data.id);

            App.init_form_elements('#insurancePolicy-'+response.data.id);
            

        });

    })

    $('body').on('click', '.remove-insurance-policy', function () {

        var button = $(this);
        var id = button.data('id');

        App.confirm('Confirm Removal', 'Are you sure you want to remove this insurance policy?', function () {

            App.set_progress('#insurance-form', true);
            $.post(Routing.generate('ajax_client_wpm_remove_insurance_policy', {id: id}), {}, function (response) {

                App.set_progress('#insurance-form', false);
                if (!response.success) {
                    return App.handle_error_response(response);
                }


                button.parents('.insurance-policy').remove();
                App.notify('Insurance Policy', 'Removed successfully', 'success');

                if ($('.insurance-policy').length == 0) {
                    $('#save-insurance-policies').hide();
                }

                // ensure input names indcides are in sequence
                App.ClientWPM.update_insurance_indices();
            });

        })

    });

    $('.insurance-document-upload').each(function () {

        var insurancePolicyId = $(this).data('id');
        var elementId = '#'+$(this).attr('id');

        App.ClientWPM.init_insurance_document_upload(elementId, insurancePolicyId);

    });

    $('#add-investment').on('click', function () {

        App.set_progress('#investment-form', true);

        var clientId = $(this).data('client-id');
        $.get(Routing.generate('ajax_client_add_client_wpm_investment', {id: clientId, numInvestments: $('.investment').length}), function (response) {

            App.set_progress('#investment-form', false);
            if (!response.success) {
                return App.handle_error_response(response);
            }

            $('#investments').append(response.data.formHTML);
            $('#save-investments').show();

            App.init_form_elements('#investment-'+response.data.id);

        });

    })

    $('body').on('click', '.remove-investment', function () {


        var button = $(this);
        var id = button.data('id');

        App.confirm('Confirm Removal', 'Are you sure you want to remove this investment?', function () {

            App.set_progress('#investment-form', true);
            $.post(Routing.generate('ajax_client_wpm_remove_investment', {id: id}), {}, function (response) {

                App.set_progress('#investment-form', false);
                if (!response.success) {
                    return App.handle_error_response(response);
                }


                button.parents('.investment').remove();
                App.notify('Investment', 'Removed successfully', 'success');

                if ($('.investment').length == 0) {
                    $('#save-investments').hide();
                }
                // ensure input names indcides are in sequence
                App.ClientWPM.update_investment_indices();
            });

        })

    });

    $('#add-estate').on('click', function () {

        App.set_progress('#estate-form', true);

        var clientId = $(this).data('client-id');
        $.get(Routing.generate('ajax_client_add_client_wpm_estate', {id: clientId, numEstates: $('.estate').length}), function (response) {

            App.set_progress('#estate-form', false);
            if (!response.success) {
                return App.handle_error_response(response);
            }

            $('#estates').append(response.data.formHTML);
            $('#save-estates').show();

            App.init_form_elements('#estate-'+response.data.id);

        });

    });

    $('body').on('click', '.remove-estate', function () {

        var button = $(this);
        var id = button.data('id');

        App.confirm('Confirm Removal', 'Are you sure you want to remove this estate?', function () {

            App.set_progress('#estate-form', true);
            $.post(Routing.generate('ajax_client_wpm_remove_estate', {id: id}), {}, function (response) {

                App.set_progress('#estate-form', false);
                if (!response.success) {
                    return App.handle_error_response(response);
                }

                button.parents('.estate').remove();
                App.notify('Estate', 'Removed successfully', 'success');

                if ($('.estate').length == 0) {
                    $('#save-estates').hide();
                }
                // ensure input names indcides are in sequence
                App.ClientWPM.update_estate_indices();
            });
        })
    });

    /**
     * confirm navigate away if forms have changed
     */
    $('form.live-form').each(function () {
        $(this).data('serialize',$(this).serialize());
        $(this).on('saved', function () {
            console.log($(this).serialize());
            $(this).data('serialize',$(this).serialize());

        })
    });
    // On load save form current state
    $(window).bind('beforeunload', function(e){
        var changed = false;
        $('form.live-form').each(function () {

            if($(this).serialize() != $(this).data('serialize')) {
                changed = true;
            }
            // i.e; if form state change show box not.
        });

        if (changed) {
            return true;
        } else {
            e = null;
        }


    });
});