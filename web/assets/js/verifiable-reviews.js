
$(document).ready(function () {

   $('.verify-review').on('click', function () {

      var button = $(this);
      button.attr('disabled', 'disabled');
      button.find('i').removeClass('fa-check').addClass('fa-spinner');

      $.post(Routing.generate('admin_verify_review', {id: button.data('id')}), function (response) {

         if (!response.success) {
            App.handle_error_response(response);
            button.attr('disabled', false);
            button.find('i').removeClass('fa-spinner').addClass('fa-check');
            return false;
         }

         App.notify('Review Verified', '', 'success');
         button.siblings('strong').css('color', 'green');
         button.siblings('strong').html('Verified');
         button.remove();

      });

   });
   
});