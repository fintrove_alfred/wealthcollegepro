
$(document).ready(function () {

    $('#process-file-form-submit').on('click', function (e) {
        e.preventDefault();

        App.confirm("Confirm Consent", "It's against Data Protection laws to share someone's personal details with others without first obtaining permission. Before you proceed, make sure you have obtained consent to use data from each person in your list. Can you confirm that you have obtained each person's permission?", function () {

            $('#process-file-form').submit();

        }, {
            confirmText: 'Confirm',
            cancelText: 'Cancel'
        });


    });

    $('#client-import-form').on('saved', function () {

        var response = $(this).data('last-response');

        App.notify('Import Successful', response.data.count+' contacts imported', 'success');

        setTimeout(function () {
            location.href = Routing.generate('clients');
        }, 3000);

    });

    $('.remove-row').on('click', function () {
        $(this).parents('tr').remove();
    });



});