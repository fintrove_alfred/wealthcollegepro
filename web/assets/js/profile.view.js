/**
 * Created by Gul on 22/03/2016.
 */

App.ProfileView = {}

App.ProfileView.reviewFormInitialised = false;
App.ProfileView.reviewSubmitRecaptcha = null;

App.ProfileView.init_review_form = function () {

   // only run this function once
   if (App.ProfileView.reviewFormInitialised) {
      return;
   }
   App.ProfileView.reviewFormInitialised = true;


   var width = $("#star-rating-stars").width();
   var step = width / 10;

   var setStarRating = function (rating) {
      rating = Math.ceil(rating);
      var starRating = Math.ceil(rating / 2);

      for (var i = 1; i <= 5; i++) {
         $('#star-'+i).removeClass('fa-star-o fa-star fa-star-half-o star-o');
         if (i <= starRating) {
            if (rating % 2 == 1 && starRating == i) {
               // half star
               $('#star-'+i).addClass('fa-star-half-o');
            } else {
               // full star
               $('#star-'+i).addClass('fa-star');
            }
         } else {
            $('#star-'+i).addClass('fa-star-o');
         }
      }
      $('#star-rating').data('rating', rating);

      if (rating > 0 && rating <= 5) {
         $('#star-rating').addClass('bg-red').removeClass('bg-amber bg-green');
      } else if (rating > 5 && rating <= 7) {
         $('#star-rating').addClass('bg-amber').removeClass('bg-red bg-green');
      } else {
         $('#star-rating').addClass('bg-green').removeClass('bg-amber bg-red');
      }

   }

   $('#star-rating-stars').mousemove(function (e) {
      var offset = $("#star-rating-stars").offset();
      var position = e.pageX - offset.left;
      var rating = position / step;

      // offset.left
      setStarRating(rating);
   });
   $('#star-rating-stars').mouseleave(function () {
      setStarRating($('#review_rating').val());
   });

   $('#star-rating-stars').on('click', function () {
      $('#review_rating').val($('#star-rating').data('rating'));
      $('#star-rating-stars').addClass('value-set');
   });

   setStarRating($('#review_rating').val());

   if ($('#review-form').data('star-rating-required')) {
      $('#review-form').on('submit', function (e) {
         e.preventDefault();

         if ($('#review-form input[name="agreeTerms"]:checked').val() != 1) {
            App.alert("Agree Terms", "You must agree and consent to the terms before you can continue");
            return;
         }

         var stars = $('#review_rating').val();

         var confirmContent = '' +
             '<p>You are about to rate this person with ' + (stars/2) + ' stars. Please confirm the star rating or go back and change it and we\'ll send on the feedback!</p>'+
             '<div class="row">'+
               '<div class="col-sm-12">'+
                  '<div class="form-group required">'+
                     '<label>IN THE EVENT THAT FAHUB&reg; WOULD LIKE TO AUDIT THE SOURCE OF THIS REVIEW,<br/>WOULD YOU AGREE FOR US TO EMAIL TO YOU TO VERIFY THIS?</label>'+

                     '<div class="radio radio-success">'+
                        '<input type="radio" id="review_verifiable_1" name="review[verifiable]" required="required" value="1">' +
                        '<label for="review_verifiable_1" class="required">Yes</label>' +
                        '<input type="radio" id="review_verifiable_0" name="review[verifiable]" required="required" value="0">' +
                        '<label for="review_verifiable_0" class="required">No</label>'+
                     '</div>'+

                  '</div>'+
               '</div>'+
             '</div>';


         App.confirm('Confirm Submission',
             confirmContent,
            function () {
               App.live_form_submit_handler(e, $('#review-form'));
               grecaptcha.reset(App.ProfileView.reviewSubmitRecaptchaId);
            },
            {
               confirmText: 'Send',
               cancelText: 'Go Back'
            }
         );
         $('#review_verifiable_'+$('#review-form-modal input[name="review[verifiable]"]').val()).click();

      });
   } else {
      $('#review-form').on('submit', App.live_form_submit_handler);
   }

   // review form submit success handler
   $('#review-form').on('saved', function (e) {
      App.notify('Thank you!', 'Review has been received', 'success');
      $('#review-form-modal').modal('hide');
      setTimeout(function () {
         location.href = Routing.generate('profile_view', {id: $('#review-form').data('fa-id')});
      }, 2000);

   });

   $('#confirm-modal').on('change', 'input[name="review[verifiable]"]', function () {
      $('#review-form-modal input[name="review[verifiable]"]').val($(this).val());
   })

   App.ProfileView.reviewSubmitRecaptchaId = grecaptcha.render('review_submit_recaptcha', {
      'sitekey' : '6LdYPScTAAAAAK9A7N0L8UTgyQRFTiu3KsiDiaZA',
      'theme' : 'light'
   });

};

$(document).ready(function () {


   if ($('#review-form-modal').length) {

      // init review form
      $('#review-form-modal').modal({
         backdrop: 'static',
         keyboard: false
      })
      .modal('show')
      .on('shown.bs.modal', App.ProfileView.init_review_form);
   }

   $('#reviews-order').on('change', function (e) {
      setTimeout(function () {
         $('#reviews-filter-form')[0].submit();
      }, 500);

   });

   $('.gallery-image-thumb').on('click', function () {

      $('#gallery-image-modal .image-container').css('textAlign', 'center');
      $('#gallery-image-modal .image-container').html('<img src="'+$(this).data('url')+'" style="max-width: 75vw; text" />')
      $('#gallery-image-modal').modal('show');

   })

   if (document.getElementById('links')) {
      document.getElementById('links').onclick = function (event) {
         event = event || window.event;
         var target = event.target || event.srcElement,
             link = target.src ? target.parentNode : target,
             options = {index: link, event: event},
             links = this.getElementsByTagName('a');
         blueimp.Gallery(links, options);
      };
   }

   $(document).on('#stOverlay', 'click', function () {
      stWidget.closeWidget();
   });

});