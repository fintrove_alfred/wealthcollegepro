/**
 * Created by Gul on 26/03/2016.
 */

$(document).ready(function () {
   $('#rows-select').on('change', function () {
      $('#filter-rows').val($(this).val());
      $('#filter-page').val(1);
      $('#table-filter').submit();
   });
   $('#role-filter').on('change', function () {
      $('#filter-role').val($(this).val());
      $('#filter-page').val(1);
      $('#table-filter').submit();
   });
})