/**
 * Created by Gul on 24/04/2016.
 */


$(document).ready(function () {

    $('#webinar_calendarEvent_startAt_time').timepicker({
        showMeridian: false,
        defaultTime: '01:00'
    }).on('show.timepicker',function(e){
        var widget = $('.bootstrap-timepicker-widget');
        widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
        widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
    });

    $('#webinar_expectedRunningTime').timepicker({
        showMeridian: false,
        defaultTime: '01:00'
    }).on('show.timepicker',function(e){
        var widget = $('.bootstrap-timepicker-widget');
        widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
        widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
    });


    $('#webinar-form').on('submit', function (e) {
        App.set_progress('#webinar-form', true);
    });

    $('.delete-webinar').on('click', function () {
        var id = $(this).data('id');
        App.confirm('Confirm Deletion', 'Are you sure you want to delete this webinar, this action cannot be reversed. Any attendees will be notified that the webinar has been cancelled.', function () {
            App.set_progress('#webinar-form', true);

            var url = Routing.generate('ajax_webinar_delete', {id: id});
            $.post(url, {}, function (response) {
                if (!response.success) {
                    App.set_progress('#webinar-form', true);
                    App.handle_error_response(response);
                    return;
                }
                location.href = Routing.generate('webinar');
            });

        });

    });

    var clientsData = $('#webinar-client-select').data('clients-data');
    var groupsData = $('#webinar-group-select').data('groups-data');

    $('#webinar-client-select').select2({
        placeholder: "Select recipients",
        data: clientsData
    });
    $('#webinar-client-select').next().css('width', '90%');
    $('#webinar-group-select').select2({
        placeholder: "Select recipients",
        data: groupsData
    });
    $('#webinar-group-select').next().css('width', '90%');
    $('.attendees-select .select2-search__field').css('width', '90%');


});