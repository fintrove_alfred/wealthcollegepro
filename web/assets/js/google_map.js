function initMap() {
    var mapDiv = document.getElementById('google-map');
    var map = new google.maps.Map(mapDiv, {
        center: {lat: 1.3508955, lng: 103.8485},
        zoom: 12
    });
}