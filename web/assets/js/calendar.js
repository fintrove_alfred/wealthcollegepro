/* ============================================================
 * Calendar
 * ============================================================ */

(function($) {

    'use strict';

    var selectedEvent;
   /**
    * Used to track new events added to the calendar
    * @type {number}
    */
    var eventIndex = 1;

    var on_action_selected = function (e) {
        var value = $(this).val();
        if (!value) {
            return;
        }
        switch (value) {
            case 'month':
                location.href = Routing.generate('calendar', {view: 'month'});
                break;
            case 'week':
                location.href = Routing.generate('calendar', {view: 'week'});
                break;
            case 'event-types':
                $('#event-types-modal').modal('show');
                break;
        }
        var select = $('#calendar-action').select2();
        select.val("").trigger("change");

    };

    var day_names = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    var month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    function formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    var add_event = function (date) {

        $('#calendar-event').removeClass('open');
        //Adding a new Event on Slot Double Click
        var newEvent = {
            index: ""+(eventIndex++),
            title: 'my new event',
            class: 'bg-success-lighter',
            start: moment(date).format(),
            end: moment(date).add(1, 'hour').format(),
            allDay: false,
            other: {
                //You can have your custom list of attributes here
                id: '',
                note: '',
                location: '',
                eventType: 1,
                alertTypeId: 2,
                alertDays: '',
                alertTime: moment(date).hour(moment(date).utc().hour()-1).format('HH:mm')
            }
        };
        selectedEvent = newEvent;
        $('#myCalendar').pagescalendar('addEvent', newEvent);
        setEventDetailsToForm(selectedEvent);


        if ($('#'+selectedEvent.dataID).length) {
            $('#' + selectedEvent.dataID)[0].scrollIntoView({
                behavior: "smooth", // or "auto" or "instant"
                block: "start" // or "end"
            });
        }
        //$('#calendar .grid').animate({
        //    scrollTop: $('#'+selectedEvent.dataID).offset().top - 500
        //}, 2000);

    };

    var init_event_types_form = function () {

        $('#event-types-form').on('change', function (e) {

            $.post($(this).attr('action'), $(this).serializeArray(), function (response) {
                if (!response.success) {
                    return App.handle_error_response(response);
                }
                reload_events()
            });

        })

    };

    var init_event_form = function () {

        $('#eventForm').on('submit', function (e) {
           e.preventDefault();
        });

        $('#eventForm').validate();

        $('#event-date-range').daterangepicker({
               timePicker:true,
               timePickerIncrement:5,
               format:'DD/MM/YYYY h:mm A',
               opens: 'left'
           },
           function(start,end,label){

           }
        );
        $('#event-alert-time').timepicker({
            showMeridian: false
        }).on('show.timepicker',function(e){
            var widget = $('.bootstrap-timepicker-widget');
            widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
            widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
        });

        $('#event-date-range').on('apply.daterangepicker', function () {
            var drp_data = $('#event-date-range').data('daterangepicker');

            var startDate = new Date(drp_data.startDate);
            var endDate = new Date(drp_data.endDate);

            var startDateLabel = month_names[startDate.getMonth()]+', '+startDate.getDate()+' '+day_names[startDate.getDay()];
            var endDateLabel = month_names[endDate.getMonth()]+', '+endDate.getDate()+' '+day_names[endDate.getDay()];

            $('#event-date').html(startDateLabel);
            if (startDateLabel != endDateLabel) {
                $('#event-date').append(' to ' +endDateLabel);
            }

            $('#lblfromTime').html(formatAMPM(startDate));
            $('#lbltoTime').html(formatAMPM(endDate));

            $('#event-date-range').data('start', moment(drp_data.startDate).format());
            $('#event-date-range').data('end', moment(drp_data.endDate).format());
            $('#calendar-event-start-at').val(moment(drp_data.startDate).utc().format('YYYY-MM-DD HH:mm:ss'));
            $('#calendar-event-end-at').val(moment(drp_data.endDate).utc().format('YYYY-MM-DD HH:mm:ss'));
        });

        $('input[name="calendar_event[alertTypeId]"]').on('change', function () {
            $('.alert-time-container').hide();
            $('.alert-time-container input').attr('disabled', 'disabled');
            $('.event-alert-days-container').hide();
            $('.event-alert-days-container input').attr('disabled', 'disabled');

            if ($('.alert-time-container.alert-type-'+$(this).val()).length > 0) {
                $('.alert-time-container.alert-type-'+$(this).val()).show();
                $('.alert-time-container.alert-type-'+$(this).val()+' input').attr('disabled', false);
            }
            if ($('.event-alert-days-container.alert-type-'+$(this).val()).length > 0) {
                $('.event-alert-days-container.alert-type-'+$(this).val()).show();
                $('.event-alert-days-container.alert-type-'+$(this).val()+' input').attr('disabled', false);
            }
        });

    };

    // Some Other Public Methods That can be Use are below \
    //console.log($('body').pagescalendar('getEvents'))
    //get the value of a property
    //console.log($('body').pagescalendar('getDate','MMMM'));

    function setEventDetailsToForm(event) {
        console.log(event);
        //Show Event date
        $('#event-date').html(moment(event.start).format('MMM, D dddd'));
        if (moment(event.start).format('MMM, D dddd') != moment(event.end).format('MMM, D dddd')) {
            $('#event-date').append(' to ' + moment(event.end).format('MMM, D dddd'));
        }

        $('#lblfromTime').html(moment(event.start).format('h:mm A'));
        $('#lbltoTime').html(moment(event.end).format('H:mm A'));

        $('#event-date-range').data('start', moment(event.start).format());
        $('#event-date-range').data('end', moment(event.end).format());
        $('#calendar-event-start-at').val(moment(event.start).utc().format('YYYY-MM-DD HH:mm:ss'));
        $('#calendar-event-end-at').val(moment(event.end).utc().format('YYYY-MM-DD HH:mm:ss'));

        $('#event-date-range').val($('#event-date-range').data('start') + ' ' + $('#event-date-range').data('end'));
        $('#event-date-range').data('daterangepicker').setStartDate(moment(event.start));
        $('#event-date-range').data('daterangepicker').setEndDate(moment(event.end));

        //Load Event Data To Text Field
        $('#calendar-event-id').val(event.other.id);
        $('#txtEventName').val(event.title);
        $('#txtEventLocation').val(event.other.location);
        $('#txtEventDesc').val(event.other.note);

        $('#calendar-event-type').val(event.other.eventType);
        $('#calendar-event-type').trigger('change');

        if (event.other.alertTypeId) {
            $('input[name="calendar_event[alertTypeId]"]').attr('checked', false);
            $('#alert-type-' + event.other.alertTypeId).click();
        }
        $('#event-alert-days').val(event.other.alertDays);
        $('#event-alert-time').val(event.other.alertTime);
    }

    var reload_events = function () {

        var range = Calendar._getDateRangeInView();
        var url = Routing.generate('ajax_calendar_event_get_events');
        $.get(url, {
            startDate: $('#myCalendar').data('range-start').utc().format(),
            endDate: $('#myCalendar').data('range-end').utc().format()
        }, function (response) {
            if (response.success) {
                $('#myCalendar').pagescalendar('removeAllEvents');
                if (response.data.events && response.data.events.length > 0) {
                    $('#myCalendar').pagescalendar('addEvents', response.data.events);
                }
                $('#myCalendar').pagescalendar('rebuild');
            }
        });
    };

    $(document).ready(function() {

        $('#myCalendar').pagescalendar({
            ui: {
                week: {
                    visible: $('#myCalendar').data('view') == 'week',
                    startOfTheWeek: '0',
                    endOfTheWeek:'6'
                }
            },
            //Web Service
            events: [],
            view: $('#myCalendar').data('view'),
            onViewRenderComplete: function(range) {

                $('#myCalendar').data('range-start', range.start);
                $('#myCalendar').data('range-end', range.end);

                // get calendar data
                var url = Routing.generate('ajax_calendar_event_get_events', {view: $('#myCalendar').data('view')});
                $.get(url, function (response) {
                    if (response.success && response.data.events && response.data.events.length > 0) {
                        $('#myCalendar').pagescalendar('removeAllEvents');
                        $('#myCalendar').pagescalendar('addEvents', response.data.events);
                    }

                });
            },

            onEventClick: function(event) {
                if (event.other.eventTypeHandle == "FINTRAIN_SESSIONS") {
                    App.alert('Webinar', "Webinars cannot be edited from the calendar.<br/>You can edit the Webinar from the Webinars section.");
                    return false;
                }

                //Open Pages Custom Quick View
                if (!$('#calendar-event').hasClass('open'))
                    $('#calendar-event').addClass('open');

                selectedEvent = event;

                setEventDetailsToForm(selectedEvent);
            },
            onEventDragComplete: function(event) {
                selectedEvent = event;
                setEventDetailsToForm(selectedEvent);
            },
            onEventResizeComplete: function(event) {
                selectedEvent = event;
                setEventDetailsToForm(selectedEvent);
            },
            onTimeSlotDblClick: function(timeSlot) {

                //add_event(timeSlot.date);

            },
            onDateChange: function(range) {

                $('#myCalendar').data('range-start', range.start);
                $('#myCalendar').data('range-end', range.end);

                var url = Routing.generate('ajax_calendar_event_get_events');
                $.get(url, {
                    startDate: moment(range.start).utc().format(),
                    endDate: moment(range.end).utc().format()
                }, function (response) {
                    if (response.success && response.data.events && response.data.events.length > 0) {
                        $('#myCalendar').pagescalendar('removeAllEvents');
                        $('#myCalendar').pagescalendar('addEvents', response.data.events);
                    }
                });
            }
        });

        if ($('#myCalendar').data('start-date')) {
            $('#myCalendar').pagescalendar('setDate', $('#myCalendar').data('start-date'));
        }

        $('#eventSave').on('click', function() {

            var url = $('#eventForm').attr('action');

            App.set_progress('#eventForm', true);

            $.post(url, $('#eventForm').serializeArray(), function (response) {

                App.set_progress('#eventForm', false);

                if (response.success) {

                    // save the event data to the calendar
                    selectedEvent.other.id = response.data.id;
                    selectedEvent.title = $('#txtEventName').val();
                    selectedEvent.start = moment($('#event-date-range').data('start')).format();
                    selectedEvent.end = moment($('#event-date-range').data('end')).format();
                    //You can add Any thing inside "other" object and it will get save inside the plugin.
                    //Refer it back using the same name other.your_custom_attribute


                    selectedEvent.other.eventType = $('#calendar-event-type').val();
                    selectedEvent.class = 'event-type-'+$('#calendar-event-type option[value='+selectedEvent.other.eventType+']').data('handle')+' bg'
                    selectedEvent.other.alertTypeId = $('input[name="calendar_event[alertTypeId]"]:checked').val();
                    selectedEvent.other.alertDays = $('#event-alert-days').val();
                    selectedEvent.other.alertTime = $('#event-alert-time').val();
                    selectedEvent.other.location = $('#txtEventLocation').val();


                    //$('#myCalendar').pagescalendar('updateEvent',selectedEvent);
                    reload_events();
                    $('#calendar-event').removeClass('open');

                    App.notify('Success', 'Your event has been saved', 'success');

                } else {
                    // show error
                    App.handle_error_response(response, 'Event Error');
                }
            });


        });

        $('#confirm-delete-event').on('click', function() {

            if (!selectedEvent.other.id) {
                // not persisted yet so just delete from calendar
                $('#myCalendar').pagescalendar('removeEvent', selectedEvent.index);
                $('#calendar-event').removeClass('open');
                $('#confirm-delete-event-modal').modal('hide');
                return;
            }

            App.set_progress('#confirm-delete-event-modal .modal-content', true);

            var url = Routing.generate('ajax_calendar_event_delete', {id: selectedEvent.other.id});
            $.post(url, {}, function (response) {

                App.set_progress('#confirm-delete-event-modal .modal-content', false);

                if (response.success) {
                    $('#myCalendar').pagescalendar('removeEvent', selectedEvent.index);
                    $('#calendar-event').removeClass('open');

                    $('#confirm-delete-event-modal').modal('hide');
                    App.notify('Success', 'Your event has been deleted', 'success');
                } else {
                    App.handle_error_response(response);
                }
            });


        });

        $('#add-event').on('click', function () {
            var today = new Date();
            today.setHours(9);
            today.setMinutes(0);
            today.setSeconds(0);
            add_event(today);

            if (!$('#calendar-event').hasClass('open'))
                $('#calendar-event').addClass('open');
        });

        $('#calendar-action').on('change', on_action_selected);

        init_event_form();
        init_event_types_form();
    });

})(window.jQuery);