/**
 * Created by Gul on 06/10/2016.
 */

$(document).ready(function () {

   $('#add-client-group-client-form').on('saved', function () {
      App.notify("Contact added", "Contact added successfully", "success");
      location.reload();
   });

   $('#clients-table').on('click', '.remove-client-group-client', function () {

      var clientId = $(this).data('client-id');
      var clientGroupId = $(this).data('client-group-id');

      App.set_progress('#clients-table', true);
      $.post(Routing.generate('ajax_client_group_client_remove', {clientGroupId: clientGroupId}), {clientId: clientId}, function (response) {

         App.set_progress('#clients-table', false);
         if (response.success) {
            App.notify("Removed Contact", "Contact successfully removed", "success");
         }

         $('#clients-table').DataTable().rows('#client-'+clientId+'-row').remove().draw();

      });

   });

});