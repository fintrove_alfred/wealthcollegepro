/**
 * Created by Gul on 22/03/2016.
 */

App.sendEmailFARRecaptchaId = null;

$(document).ready(function () {

   $('.send-far-email').on('click', function () {

      if (App.sendEmailFARRecaptchaId !== null) {
         grecaptcha.reset(App.sendEmailFARRecaptchaId);
      } else {
         App.sendEmailFARRecaptchaId = grecaptcha.render('send-far-email-recaptcha', {
            'sitekey' : '6LdYPScTAAAAAK9A7N0L8UTgyQRFTiu3KsiDiaZA',
            'theme' : 'light'
         });
      }


      $('#send-far-email-modal').modal('show');
   });



   $('#send-far-email-form').on('saved', function () {
      $('#send-far-email-subject').val('');
      $('#send-far-email-message').val('');
      $('#send-far-email-message').html('');

      $('#send-far-email-modal').modal('hide');


      App.notify("Message Sent", "Your message has been sent", "success");

   });

});