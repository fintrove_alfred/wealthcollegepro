/**
 * Created by Gul on 24/03/2016.
 */


$(document).ready(function () {

  
   $('#reviews-request-modal').on('shown.bs.modal', function () {
      var clientsData = $('#review-client-select').data('clients-data');
      var groupsData = $('#review-group-select').data('groups-data');

      $('#review-client-select').select2({
            placeholder: "Select recipients",
            data: clientsData
      });

      $('#review-group-select').select2({
         placeholder: "Select recipients",
         data: groupsData
      });

   });

   $('#review-request-subject').on('click', function() {
      $(this).select();
   });

   $('#reviews-request-modal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);

      $('#reviews-request-form .review-template-id').val(button.data('review-template-id') ? button.data('review-template-id') : '');
      $('#review-request-subject').val(button.data('subject'));
      $('#review-request-message').html(button.data('message'));

   });

   $('#reviews-request-form').on('saved', function () {
      var response = $(this).data('last-response');
      App.notify('Review Request', 'Request sent to '+response.data.recipients+' recipients', 'success');
      $('#reviews-request-modal').modal('hide');
      $('#reviews-request-form')[0].reset();
      $('#review-client-select').val('');
      $('#review-group-select').val('');
   });


   $('button.show-preview').on('click', function () {
      var params = $('#reviews-request-form').serialize();
      window.open(Routing.generate('review_preview')+'?'+params);
   })

});