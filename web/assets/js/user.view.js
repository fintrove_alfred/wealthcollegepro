/**
 * Created by Gul on 24/04/2016.
 */


$(document).ready(function () {

    
    $('.delete-user').on('click', function () {
       
        var url = $(this).data('url');
        App.confirm('Permanently Delete User', "Please confirm that you are sure you want to permanently delete this user. This action cannot be reversed.", function () {
            location.href = url;
        });
        
    });
    

});