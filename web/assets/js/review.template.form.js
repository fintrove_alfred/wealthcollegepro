/**
 * Created by Gul on 24/03/2016.
 */


$(document).ready(function () {

   $('.add-question').on('click', function () {

      var numQuestions = $('#review-template-questions .question').length;

      $('#review-template-questions').append(
         $('<div class="form-group question"></div>')
            .append('<label class="col-sm-3 control-label">Question '+(numQuestions+1)+'</label>')
            .append(
               $('<div class="col-sm-9"></div>')
                  .append('<input type="text" name="review_template[reviewTemplateQuestions]['+numQuestions+'][question]" class="form-control"/>')
            )
      );

   })


});