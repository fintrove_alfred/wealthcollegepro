/* ============================================================
 * Plugin Core Init
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
$(document).ready(function() {
	'use strict';
    $('#contact-form').validate({
        // Override to submit the form via ajax
        submitHandler: function(form) {
            $('#contact-panel').portlet({
				refresh:true
			});
            $.ajax({
			     type:$(form).attr('method'),
			     url: $(form).attr('action'),
			     data: $(form).serialize(),
			     dataType: 'json',
			     success: function(response){

					 $('#contact-panel').portlet({
						 refresh:false
					 });

			     	if (response.success) {
						clearForm("Thank you for Contacting Us! We will be in touch");
						return;
					}
					App.handle_error_response(response);
			       	
			     },
			     error: function(err){
					 $('#contact-panel').portlet({
						 refresh:false
					 });
			     }
		    });
		    return false; // required to block normal submit since you used ajax
        }
    });
    function clearForm(msg){
    	$('#contact-panel').html('<div class="alert alert-success" role="alert">'+msg+'</div>');
    }
    
    $('#contact-panel').portlet({
        onRefresh: function() {
        }
    });
});