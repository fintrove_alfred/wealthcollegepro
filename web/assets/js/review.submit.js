/**
 * Created by Gul on 27/03/2016.
 */

$(document).ready(function () {

   var width = ($("#star-rating span").width() + 3) * 5;
   var step = width / 10;

   var setStarRating = function (rating) {
      rating = Math.ceil(rating);
      var starRating = Math.ceil(rating / 2);

      for (var i = 1; i <= 5; i++) {
         $('#star-'+i).removeClass('fa-star-o fa-star fa-star-half-o star-o');
         if (i <= starRating) {
            if (rating % 2 == 1 && starRating == i) {
               // half star
               $('#star-'+i).addClass('fa-star-half-o');
            } else {
               // full star
               $('#star-'+i).addClass('fa-star');
            }
         } else {
            $('#star-'+i).addClass('fa-star-o');
         }
      }
      $('#star-rating').data('rating', rating);
   }

   $('#star-rating-stars span').mousemove(function (e) {
      var offset = $("#star-rating-stars").offset();
      var position = e.pageX - offset.left;
      var rating = position / step;

      // offset.left
      setStarRating(rating);
   });
   $('#star-rating-stars').mouseleave(function () {
      setStarRating($('#review_rating').val());
   });

   $('#star-rating-stars').on('click', function () {
      $('#review_rating').val($('#star-rating').data('rating'));
   });


   setStarRating($('#review_rating').val());
});