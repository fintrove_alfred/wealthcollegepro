/**
 * Created by Gul on 22/03/2016.
 */

if (!App.Review) {
   App.Review = {};
}

var init_datatable = function (tableId) {
   var table = $(tableId);

   var settings = {
      "sDom": "<'exportOptions'T><'table-responsive't><'row'<p i>>",
      "destroy": true,
      "scrollCollapse": true,
      "oLanguage": {
         "sLengthMenu": "_MENU_ ",
         "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
      },
      "order": [[ 3, "desc" ]],
      "columnDefs": [
         { "orderable": false, "targets": [4] }
      ],
      // buttons: {
      //    buttons: [
      //       {extend: 'csv', text: "<i class='pg-grid'></i>"},
      //       {extend: 'xls', text: "<i class='fa fa-file-excel-o'></i>"},
      //       {extend: 'pdf', text: "<i class='fa fa-file-pdf-o'></i>"},
      //       {extend: 'copy', text: "<i class='fa fa-copy'></i>"}
      //    ]
      // },
      "oTableTools": {
         "sRowSelect": "multi",
         "sSwfPath": "/assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
         "aButtons": [{
            "sExtends": "csv",
            "sButtonText": "<i class='pg-grid'></i>",
            "sToolTip": "Download as CSV"
         }, {
            "sExtends": "xls",
            "sButtonText": "<i class='fa fa-file-excel-o'></i>",
            "sToolTip": "Download as XLS"
         }, {
            "sExtends": "pdf",
            "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
            "sToolTip": "Download as PDF"
         }, {
            "sExtends": "copy",
            "sButtonText": "<i class='fa fa-copy'></i>",
            "sToolTip": "Copy Data to Clipboard"
         }]
      },
      fnDrawCallback: function(oSettings) {
         $('.export-options-container').append($('.exportOptions'));

         $('#ToolTables_'+tableId+'_0').tooltip({
            title: 'Export as CSV',
            container: tableId
         });

         $('#ToolTables_'+tableId+'_1').tooltip({
            title: 'Export as Excel',
            container: tableId
         });

         $('#ToolTables_'+tableId+'_2').tooltip({
            title: 'Export as PDF',
            container: tableId
         });

         $('#ToolTables_'+tableId+'_3').tooltip({
            title: 'Copy data',
            container: tableId
         });
      }

   };

   App.Review.reviews_table = table.DataTable(settings);

   // search box for table
   $('#search-table').on( 'keyup', function () {
      App.Review.reviews_table.search( $('#search-table').val() ).draw();
   });



};

App.Review.show_review = function (event) {

   var id = $(event.target).data('id');

   App.set_progress('#review-modal .modal-content', true);
   $('#review-modal').modal('show');

   $.get(Routing.generate('review_view_modal', {id: id}), function (response) {

      $('#review-modal .modal-content').html(response);

   });



};

App.Review.publish_review = function (id) {

   App.set_progress('#reviews-table', true);

   $.post(Routing.generate('review_toggle_published', {id: id}), function (response) {

      App.set_progress('#reviews-table', false);

      var row = $('#review-'+id+'-row');
      if (response.data.published) {
         row.find('.published-state').removeClass('unpublished').addClass('published');
         row.find('.published-state').html('<i class="fa fa-eye "></i> Published');
      } else {
         row.find('.published-state').removeClass('published').addClass('unpublished');
         row.find('.published-state').html('<i class="fa fa-eye-slash"></i> Unpublished');
      }



   });



};

$(document).ready(function () {

   $('#reviews-table').on('click', 'button.view-review', App.Review.show_review);
   init_datatable('#reviews-table');

   $('#reviews-table').on('click', 'button.publish-review', function () {
      App.Review.publish_review($(this).data('id'));
   });
   
});