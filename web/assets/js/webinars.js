/**
 * Created by Gul on 24/04/2016.
 */


$(document).ready(function () {

    /* GRID
     -------------------------------------------------------------*/

    /*
     Wait for the images to be loaded before applying
     Isotope plugin.
     */
    var $gallery = $('.gallery');
    $gallery.imagesLoaded(function() {
        applyIsotope();
        $('.gallery-item').css('visibility', 'visible');
    });

    /*  Apply Isotope plugin
     isotope.metafizzy.co
     */
    var applyIsotope = function() {
        $gallery.isotope({
            itemSelector: '.gallery-item',
            masonry: {
                columnWidth: 280,
                gutter: 10,
                isFitWidth: true
            }
        });
        $gallery.on('arrangeComplete', function () {
            $('.gallery-item').show();
        });
    }

    var $widgetFilter = $('#widget-filter');
    $widgetFilter.keyup(debounce(function(){
        qsRegex = new RegExp($widgetFilter.val(),'gi');
        $gallery.isotope({
            filter:function(){
                return qsRegex ? $(this).text().match(qsRegex):true;
            }
        });
        if ($widgetFilter.val()){
            $("html, body").stop().animate({scrollTop:"320px"});
        } else {
            $("html, body").stop().animate({scrollTop:"0px"});
        }
    },200));
    function debounce(fn,threshold){
        var timeout;
        return function debounced(){
            if(timeout){
                clearTimeout(timeout);
            }
            function delayed(){
                fn();
                timeout=null;
            }
            timeout=setTimeout(delayed,threshold||100);
        }
    }


    $('.sync-with-google').on('click', function () {

        App.set_progress('#sync-alert', true);
        
        $.post(Routing.generate('ajax_webinar_google_sync'), {}, function (response) {

            if (!response.success) {
                App.handle_error_response(response);
                return false;
            }

            $('#sync-alert').remove();
            App.notify('Google Sync', 'Sync Completed Successfully', 'success');

        }).error(function () {
            App.set_progress('#sync-alert', false);
            App.handle_error_response();
        }).always(function () {
            App.set_progress('#sync-alert', false);
        });
        

    });

});