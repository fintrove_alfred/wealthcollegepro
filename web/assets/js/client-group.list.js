/**
 * Created by Gul on 22/03/2016.
 */

if (!App.ClientGroup) {
   App.ClientGroup = {};
}

App.ClientGroup.client_group_saved_handler = function (response) {

   $('#add-client-group-modal').modal('hide');
   App.set_progress('#client-groups-container', true);
   location.reload();

};

App.ClientGroup.edit_client_group = function () {

   var id = $(this).data('id');
   var form = $('#save-client-group-form');

   $('#add-client-group-modal').modal('show');
   form[0].reset();
   App.set_progress('#save-client-group-form', true);

   $.get(Routing.generate('ajax_client_group_data', {id: id}), function (response) {

      App.set_progress('#save-client-group-form', false);

      if (!response.success) {
         return App.handle_error_response(response);
      }

      // update the client-group form with the new data
      form.find('.client-group-id').val(response.data.id);
      form.find('#client_group_name').val(response.data.name);
      

   });

};

App.ClientGroup.delete_client_group = function () {

   var id = $(this).data('id');

   App.confirm('Delete Group', 'Are you sure you want to delete this group?', function () {

      App.set_progress('#client-groups-table_wrapper', true);

      $.post(Routing.generate('ajax_client_group_delete'), {id: id}, function (response) {

         App.set_progress('#client-groups-table_wrapper', false);

         if (!response.success) {
            return App.handle_error_response(response);
         }

         App.notify('Group Deleted', 'Group deleted successfully');
         $('#client-groups-table').DataTable().rows('#client-group-'+id+'-row').remove().draw();

      })
      .always(function () {
         App.set_progress('#client-groups-table_wrapper', false);
      });

   });
};

var init_datatable = function (tableId) {
   var table = $(tableId);

   var settings = {
      "sDom": "<'exportOptions'T><'table-responsive't><'row'<p i>>",
      "destroy": true,
      "scrollCollapse": true,
      "oLanguage": {
         "sLengthMenu": "_MENU_ ",
         "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
      },
      "oTableTools": {
         "sRowSelect": "multi",
         "sSwfPath": "/assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
         "aButtons": [{
            "sExtends": "csv",
            "sButtonText": "<i class='pg-grid'></i>",
            "sToolTip": "Download as CSV"
         }, {
            "sExtends": "xls",
            "sButtonText": "<i class='fa fa-file-excel-o'></i>",
            "sToolTip": "Download as XLS"
         }, {
            "sExtends": "pdf",
            "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
            "sToolTip": "Download as PDF"
         }, {
            "sExtends": "copy",
            "sButtonText": "<i class='fa fa-copy'></i>",
            "sToolTip": "Copy Data to Clipboard"
         },
         "select_all",
         "select_none",
         {
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-send'></i> Invite to Webinar",
            "fnClick": App.ClientGroup.invite_to_webinar
         }]
      },
      fnDrawCallback: function(oSettings) {
         $('.export-options-container').append($('.exportOptions'));

         $('#ToolTables_'+tableId+'_0').tooltip({
            title: 'Export as CSV',
            container: 'body'
         });

         $('#ToolTables_'+tableId+'_1').tooltip({
            title: 'Export as Excel',
            container: 'body'
         });

         $('#ToolTables_'+tableId+'_2').tooltip({
            title: 'Export as PDF',
            container: 'body'
         });

         $('#ToolTables_'+tableId+'_3').tooltip({
            title: 'Copy data',
            container: 'body'
         });
      }

   };

   App.ClientGroup.client_groups_table = table.DataTable(settings);

   // search box for table
   $('#search-table').on( 'keyup', function () {
      App.ClientGroup.client_groups_table.search( $('#search-table').val() ).draw();
   });

   //App.ClientGroup.client_groups_table = table;

   $('#invite-to-webinar-form').on('submit', function (e) {
      e.preventDefault();

      var form = $('#invite-to-webinar-form');
      var calendarEventId = $('#invite-event-id').val();

      if (!calendarEventId) {
         App.alert('Select Webinar', 'Please select the webinar you want to invite your groups to');
         return false;
      }
      if (App.ClientGroup.inviteGroupIDs.length == 0) {
         App.alert('Select Groups', 'Please select the groups you want to invite to the webinar');
         return false;
      }

      App.set_progress('#invite-to-webinar-form', true);

      $.post(form.attr('action'), {calendarEventId: calendarEventId, clientGroupIds: App.ClientGroup.inviteGroupIDs.join(',')}, function (response) {

         if (!response.success) {
            return App.handle_error_response(response);
         }

         form[0].reset();
         $('#invite-to-webinar-modal').modal('hide');

         var result = $('<p></p>');
         result.html('<span><strong>'+response.data.invited+'</strong> contacts invited to the webinar</span><br/>');
         if (response.data.alreadyInvited) {
            result.append('<span><strong>'+response.data.alreadyInvited+'</strong> contacts had already been invited to the webinar</span><br/>');
         }
         if (response.data.noEmail) {
            result.append('<span><strong>'+response.data.noEmail+'</strong> contacts did not have an email address so could not be invited</span><br/>');
         }

         App.alert('Invite to Webinar', result.html());


      }).error(function () {
         App.set_progress('#invite-to-webinar-form', false);
      }).always(function () {
         App.set_progress('#invite-to-webinar-form', false);
      });

   })
};

App.ClientGroup.invite_to_webinar = function () {

   var count = App.ClientGroup.client_groups_table.rows('.active').count();
   if (count == 0) {
      return App.alert('Select a group', 'Please first select a group to invite to your webinar');
   }

   App.ClientGroup.inviteGroupIDs = [];
   App.ClientGroup.client_groups_table.rows('.active').every(function (rowIdx, tableLoop, rowLoop) {
      App.ClientGroup.inviteGroupIDs.push($(this.node()).data('id'));
   });

   $('#invite-to-webinar-modal').modal('show');

   $('#invite-to-webinar-modal .groups-selected').html(App.ClientGroup.inviteGroupIDs.length);



};

$(document).ready(function () {

   $('#save-client-group-form').on('saved', App.ClientGroup.client_group_saved_handler);
   $('#client-groups-table').on('click', 'button.edit-client-group', App.ClientGroup.edit_client_group);
   $('.delete-client-group').on('click', App.ClientGroup.delete_client_group);

   init_datatable('#client-groups-table');


});