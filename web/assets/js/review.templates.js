/**
 * Created by Gul on 24/03/2016.
 */


$(document).ready(function () {

   /* GRID
    -------------------------------------------------------------*/

   /*
    Wait for the images to be loaded before applying
    Isotope plugin.
    */
   var $gallery = $('.gallery');
   $gallery.imagesLoaded(function() {
      applyIsotope();
   });

   /*  Apply Isotope plugin
    isotope.metafizzy.co
    */
   var applyIsotope = function() {
      $gallery.isotope({
         itemSelector: '.gallery-item',
         masonry: {
            columnWidth: 280,
            gutter: 10,
            isFitWidth: true
         }
      });
   }

   /*
    Show a sliding item using MetroJS
    http://www.drewgreenwell.com/projects/metrojs
    */
   $(".live-tile,.flip-list").liveTile();


   /* DETAIL VIEW
    -------------------------------------------------------------*/

   /*
    Look for data-image attribute and apply those
    images as CSS background-image
    */
   $('.item-slideshow > div').each(function() {
      var img = $(this).data('image');
      $(this).css({
         'background-image': 'url(' + img + ')',
         'background-size': 'cover'
      })
   });

   $('.delete-review-template').on('click', function (e) {
      e.preventDefault();
      var url = $(this).attr('href');

      App.confirm('Confirm', 'Are you sure you want to delete this review form?', function () {
         location.href = url;
      })
   })
});