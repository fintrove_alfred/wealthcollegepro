/**
 * Created by Gul on 22/03/2016.
 */

$(document).ready(function () {


   // max length for profile description textarea
   $('#profile_description_financialAdviser_description').on('keyup blur', function () {

      var value = $(this).val();
      var max = parseInt($(this).attr('maxlength'));
      if (value.length > max) {
         value = value.substr(0, 160);
         $(this).val(value);
      }
      $('#edit-description-form .character-limit .remaining').html(max - value.length);
   });


   $('#profile-image-upload-id').fileupload({
      url: Routing.generate('ajax_profile_image_upload'),
      dataType: 'json',
      // Enable image resizing, except for Android and Opera,
      // which actually support image resizing, but fail to
      // send Blob objects via XHR requests:
      //disableImageResize: /Android(?!.*Chrome)|Opera/
      //   .test(window.navigator && navigator.userAgent),
      //imageMaxWidth: 800,
      //imageMaxHeight: 800,
      imageCrop: false, // Force cropped images
      singleFileUploads: true,
      progressall: function (e, data) {
         var progress = parseInt(data.loaded / data.total * 100, 10);
         $('#profile-image-upload-progress').show();
         $('#profile-image-upload-progress .progress-bar').css('width', progress + '%');
      },
      done: function (e, data) {
         if (!data.result.success) {
            return App.handle_error_response(data.result);
         }
         $('#profile-image-upload-progress').hide();
         $('#profile-image').html('<img src="'+data.result.data.url+'" alt="Profile Image" height="150" />');
         App.notify('Profile Image', 'Upload Successful', 'success');
      }
   });

   $('#profile-background-image-upload-id').fileupload({
      url: Routing.generate('ajax_profile_background_image_upload'),
      dataType: 'json',
      // Enable image resizing, except for Android and Opera,
      // which actually support image resizing, but fail to
      // send Blob objects via XHR requests:
      //disableImageResize: /Android(?!.*Chrome)|Opera/
      //   .test(window.navigator && navigator.userAgent),
      //imageMaxWidth: 800,
      //imageMaxHeight: 800,
      imageCrop: false, // Force cropped images
      singleFileUploads: true,
      progressall: function (e, data) {
         var progress = parseInt(data.loaded / data.total * 100, 10);
         $('#profile-background-image-upload-progress').show();
         $('#profile-background-image-upload-progress .progress-bar').css('width', progress + '%');
      },
      done: function (e, data) {
         if (!data.result.success) {
            return App.handle_error_response(data.result);
         }
         $('#profile-background-image-upload-progress').hide();
         $('#profile-background-image').html('<img src="'+data.result.data.url+'" alt="Profile Header Image" height="150" />');
         App.notify('Profile Header Image', 'Upload Successful', 'success');
      }
   });

   $('#edit-essential-details-form').on('saved', function () {
      App.notify('Essential Details', 'Saved successfully', 'success');
   });
   $('#edit-basic-info-form').on('saved', function () {
      App.notify('Basic Info', 'Saved successfully', 'success');
   });
   $('#edit-description-form').on('saved', function () {
      App.notify('Description', 'Saved successfully', 'success');
   });
   $('#edit-contact-address-form').on('saved', function () {
      App.notify('Contact Address', 'Saved successfully', 'success');
   });
   $('#edit-write-up-form').on('saved', function () {
      App.notify('Write Up', 'Saved successfully', 'success');
   });


   $('.photo-gallery-upload-input').each(function () {

      var id = $(this).attr('id');
      var type = $(this).data('type');
      $('#'+type+'-upload').fileupload({
         url: Routing.generate('ajax_profile_photo_gallery_upload'),
         dataType: 'json',
         formData: {
            type: type
         },
         // Enable image resizing, except for Android and Opera,
         // which actually support image resizing, but fail to
         // send Blob objects via XHR requests:
         //disableImageResize: /Android(?!.*Chrome)|Opera/
         //   .test(window.navigator && navigator.userAgent),
         //imageMaxWidth: 800,
         //imageMaxHeight: 800,
         imageCrop: false, // Force cropped images
         maxNumberOfFiles: 5,
         singleFileUploads: false,
         multipart: true,
         progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#'+type+'-upload-progress').show();
            $('#'+type+'-upload-progress .progress-bar').css('width', progress + '%');
         },
         done: function (e, data) {
            if (!data.result.success) {
               return App.handle_error_response(data.result);
            }
            $('#'+type+'-upload-progress').hide();
            var photos = data.result.data;
            for (var i in photos) {
               $('#'+type+'-images').append(photos[i].photoHTML);
            }

            App.notify('Photo Gallery Image', 'Upload Successful', 'success');
         }
      });
   });

   $('.uploaded-photos').on('click', '.delete-gallery-photo', function () {

      var button = $(this);
      App.confirm("Confirm Delete", "Are you sure you want to delete this photo? This action cannot be undone.", function () {

         var photoId = button.data('id');
         var type = button.data('type');

         App.set_progress('#'+type+'-images', true);

         $.post(Routing.generate('ajax_profile_photo_gallery_delete', {id: photoId}), {}, function (response) {

            if (!response.success) {
               App.handle_error_response(response);
               return false;
            }

            $('#fa-photo-'+photoId).remove();
            App.notify('Deleted Photo', 'Success', 'success');

         }).error(function () {
            App.set_progress('#'+type+'-images', false);
            App.handle_error_response();
         }).always(function () {
            App.set_progress('#'+type+'-images', false);
         });


      })
   });

   tinymce.init({
      selector:'#profile_write_up_profileContent,#profile_essential_details_financialAdviser_specialInterest',
      height: 500,
      plugins: [
         'advlist autolink lists link image charmap print preview anchor',
         'searchreplace visualblocks code fullscreen',
         'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
   });


   /**
    * confirm navigate away if forms have changed
    */
   $('form.live-form').each(function () {
      $(this).data('serialize',$(this).serialize());
      $(this).on('saved', function () {
         $(this).data('serialize',$(this).serialize());

      })
   });
   // On load save form current state
   $(window).bind('beforeunload', function(e){
      var changed = false;
      $('form.live-form').each(function () {

         if($(this).serialize() != $(this).data('serialize')) {
            changed = true;
         }
         // i.e; if form state change show box not.
      });

      if (changed) {
         return true;
      } else {
         e = null;
      }


   });

   App.init_date_mask();
});