<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160715121036 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser ADD company_address_id INT DEFAULT NULL, ADD nric_number VARCHAR(255) DEFAULT NULL, ADD race VARCHAR(255) DEFAULT NULL, ADD nationality VARCHAR(255) DEFAULT NULL, ADD year_joined_industry DATE DEFAULT NULL, ADD parent_company VARCHAR(255) DEFAULT NULL, ADD professional_contact_number VARCHAR(255) DEFAULT NULL, ADD professional_email_address VARCHAR(255) DEFAULT NULL, ADD academic_qualification VARCHAR(255) DEFAULT NULL, ADD academic_qualification_other VARCHAR(255) DEFAULT NULL, ADD professional_certifications LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', ADD professional_certifications_others VARCHAR(1000) DEFAULT NULL, ADD professional_memberships LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', ADD professional_memberships_others VARCHAR(1000) DEFAULT NULL, ADD industry_awards TEXT DEFAULT NULL, ADD community_involvement TEXT DEFAULT NULL, ADD areas_of_competencies TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE financial_adviser ADD CONSTRAINT FK_B1816314483946E3 FOREIGN KEY (company_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_B1816314483946E3 ON financial_adviser (company_address_id)');
        $this->addSql('ALTER TABLE person ADD nric_passport VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD gender VARCHAR(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE client DROP nric_passport');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client ADD nric_passport VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE financial_adviser DROP FOREIGN KEY FK_B1816314483946E3');
        $this->addSql('DROP INDEX IDX_B1816314483946E3 ON financial_adviser');
        $this->addSql('ALTER TABLE financial_adviser DROP company_address_id, DROP nric_number, DROP race, DROP nationality, DROP year_joined_industry, DROP parent_company, DROP professional_contact_number, DROP professional_email_address, DROP academic_qualification, DROP academic_qualification_other, DROP professional_certifications, DROP professional_certifications_others, DROP professional_memberships, DROP professional_memberships_others, DROP industry_awards, DROP community_involvement, DROP areas_of_competencies');
        $this->addSql('ALTER TABLE person DROP nric_passport');
        $this->addSql('ALTER TABLE users DROP gender');
    }
}
