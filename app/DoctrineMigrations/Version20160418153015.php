<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160418153015 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE client_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, deleted_at DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_client_group (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, client_group_id INT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_7FA77A7319EB6921 (client_id), INDEX IDX_7FA77A73D0B2E982 (client_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE client_relation (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, related_client_id INT NOT NULL, client_relation_id INT DEFAULT NULL, relationship_type_id INT NOT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_4C6F778C19EB6921 (client_id), INDEX IDX_4C6F778CB12A353A (related_client_id), INDEX IDX_4C6F778C331DB460 (client_relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_relation ADD CONSTRAINT FK_4C6F778C19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_relation ADD CONSTRAINT FK_4C6F778CB12A353A FOREIGN KEY (related_client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_relation ADD CONSTRAINT FK_4C6F778C331DB460 FOREIGN KEY (client_relation_id) REFERENCES client_relation (id)');

        $this->addSql('CREATE TABLE client_date (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, date_type_id INT NOT NULL, date_type_name VARCHAR(255) DEFAULT NULL, date DATE NOT NULL, reminder TINYINT(1) DEFAULT \'0\' NOT NULL, reminder_date DATE DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_7B02F41819EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_client_group ADD CONSTRAINT FK_7FA77A7319EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_client_group ADD CONSTRAINT FK_7FA77A73D0B2E982 FOREIGN KEY (client_group_id) REFERENCES client_group (id)');

        $this->addSql('ALTER TABLE client_date ADD CONSTRAINT FK_7B02F41819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE person ADD gender VARCHAR(1) DEFAULT NULL after last_name, ADD marital_status_id INT DEFAULT NULL after gender');
        $this->addSql('ALTER TABLE client ADD company_address INT DEFAULT NULL after address_id, ADD referred_by INT DEFAULT NULL after company_address, ADD website1 VARCHAR(255) DEFAULT NULL after referred_by, ADD website2 VARCHAR(255) DEFAULT NULL after website1, ADD website3 VARCHAR(255) DEFAULT NULL after website2, ADD occupation VARCHAR(255) DEFAULT NULL after website3, ADD job_title VARCHAR(255) DEFAULT NULL after occupation');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404552D1C7556 FOREIGN KEY (company_address) REFERENCES address (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404558C0C9F8A FOREIGN KEY (referred_by) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_C74404552D1C7556 ON client (company_address)');
        $this->addSql('CREATE INDEX IDX_C74404558C0C9F8A ON client (referred_by)');

        $this->addSql('ALTER TABLE person ADD telephone_work VARCHAR(32) DEFAULT NULL after mobile');

        $this->addSql('ALTER TABLE client_group ADD financial_adviser_id INT DEFAULT NULL after id');
        $this->addSql('ALTER TABLE client_group ADD CONSTRAINT FK_CEADD872A90145FF FOREIGN KEY (financial_adviser_id) REFERENCES financial_adviser (id)');
        $this->addSql('CREATE INDEX IDX_CEADD872A90145FF ON client_group (financial_adviser_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client_group DROP FOREIGN KEY FK_CEADD872A90145FF');
        $this->addSql('DROP INDEX IDX_CEADD872A90145FF ON client_group');
        $this->addSql('ALTER TABLE client_group DROP financial_adviser_id');

        $this->addSql('ALTER TABLE person DROP telephone_work');
        $this->addSql('ALTER TABLE client_client_group DROP FOREIGN KEY FK_7FA77A73D0B2E982');
        $this->addSql('DROP TABLE client_group');
        $this->addSql('DROP TABLE client_client_group');
        $this->addSql('DROP TABLE client_relation');
        $this->addSql('DROP TABLE client_date');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404552D1C7556');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404558C0C9F8A');
        $this->addSql('DROP INDEX IDX_C74404552D1C7556 ON client');
        $this->addSql('DROP INDEX IDX_C74404558C0C9F8A ON client');
        $this->addSql('ALTER TABLE client DROP company_address, DROP referred_by, DROP website1, DROP website2, DROP website3, DROP occupation, DROP job_title, DROP company_name');
        $this->addSql('ALTER TABLE person DROP gender, DROP marital_status_id');
    }
}
