<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160329150731 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE review_answer ADD review_template_question_id INT DEFAULT NULL after review_id');
        $this->addSql('ALTER TABLE review_answer ADD CONSTRAINT FK_50AB0DE53E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
        $this->addSql('ALTER TABLE review_answer ADD CONSTRAINT FK_50AB0DE549D32434 FOREIGN KEY (review_template_question_id) REFERENCES review_template_question (id)');
        $this->addSql('CREATE INDEX IDX_50AB0DE549D32434 ON review_answer (review_template_question_id)');
        $this->addSql('ALTER TABLE review_answer CHANGE answer_text answer_text LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE review_answer CHANGE answer_text answer_text LONGTEXT NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE review_answer DROP FOREIGN KEY FK_50AB0DE53E2E969B');
        $this->addSql('ALTER TABLE review_answer DROP FOREIGN KEY FK_50AB0DE549D32434');
        $this->addSql('DROP INDEX IDX_50AB0DE549D32434 ON review_answer');
        $this->addSql('ALTER TABLE review_answer DROP review_template_question_id');
    }
}
