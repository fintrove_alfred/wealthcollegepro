<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160318112232 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE country CHANGE name name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE address DROP house_number');
        $this->addSql('ALTER TABLE financial_adviser ADD email VARCHAR(255) DEFAULT NULL after business_name');
        $this->addSql('ALTER TABLE financial_adviser ADD description TEXT DEFAULT NULL after fax');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser DROP description');
        $this->addSql('ALTER TABLE financial_adviser DROP email');
        $this->addSql('ALTER TABLE address ADD house_number VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX idx_country_code ON country');
        $this->addSql('ALTER TABLE country CHANGE name name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');

    }
}
