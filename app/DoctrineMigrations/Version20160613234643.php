<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160613234643 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, currency_code VARCHAR(255) DEFAULT \'\' NOT NULL, title VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payments ADD currency_id INT DEFAULT NULL after amount');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT FK_65D29B3238248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('CREATE INDEX IDX_65D29B3238248176 ON payments (currency_id)');
        $this->addSql('ALTER TABLE orders ADD currency_id INT DEFAULT NULL after total');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE38248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('CREATE INDEX IDX_E52FFDEE38248176 ON orders (currency_id)');
        $this->addSql('ALTER TABLE products ADD currency_id INT DEFAULT NULL after price');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A38248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('CREATE INDEX IDX_B3BA5A5A38248176 ON products (currency_id)');

        $this->addSql("INSERT INTO `currency` (`currency_code`, `title`) VALUES ('SGD', 'Singapore Dollar')");
        $this->addSql("INSERT INTO `currency` (`currency_code`, `title`) VALUES ('GBP', 'British Pound');");
        $this->addSql("INSERT INTO `currency` (`currency_code`, `title`) VALUES ('USD', 'US Dollar');");

        $this->addSql("update products set currency_id = 1");
        $this->addSql("update products set price = 588");


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY FK_65D29B3238248176');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE38248176');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5A38248176');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP INDEX IDX_E52FFDEE38248176 ON orders');
        $this->addSql('ALTER TABLE orders DROP currency_id');
        $this->addSql('DROP INDEX IDX_65D29B3238248176 ON payments');
        $this->addSql('ALTER TABLE payments DROP currency_id');
        $this->addSql('DROP INDEX IDX_B3BA5A5A38248176 ON products');
        $this->addSql('ALTER TABLE products DROP currency_id');
    }
}
