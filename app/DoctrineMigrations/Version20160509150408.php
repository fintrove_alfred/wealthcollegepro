<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160509150408 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE client_wpm_cashflow (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, salary NUMERIC(12, 2) DEFAULT NULL, salary_mode INT DEFAULT NULL, salary_date DATETIME DEFAULT NULL, allowances NUMERIC(12, 2) DEFAULT NULL, allowances_mode INT DEFAULT NULL, allowances_date DATETIME DEFAULT NULL, commissions NUMERIC(12, 2) DEFAULT NULL, commissions_mode INT DEFAULT NULL, commissions_date DATETIME DEFAULT NULL, bonuses NUMERIC(12, 2) DEFAULT NULL, bonuses_mode INT DEFAULT NULL, bonuses_date DATETIME DEFAULT NULL, cashflow_active_others NUMERIC(12, 2) DEFAULT NULL, coupons NUMERIC(12, 2) DEFAULT NULL, coupons_mode INT DEFAULT NULL, coupons_date DATETIME DEFAULT NULL, dividends NUMERIC(12, 2) DEFAULT NULL, dividends_mode INT DEFAULT NULL, dividends_date DATETIME DEFAULT NULL, rentals NUMERIC(12, 2) DEFAULT NULL, rentals_mode INT DEFAULT NULL, rentals_date DATETIME DEFAULT NULL, cashflow_passive_others NUMERIC(12, 2) DEFAULT NULL, home NUMERIC(12, 2) DEFAULT NULL, home_mode INT DEFAULT NULL, home_date DATETIME DEFAULT NULL, transport NUMERIC(12, 2) DEFAULT NULL, transport_mode INT DEFAULT NULL, transport_date DATETIME DEFAULT NULL, personal NUMERIC(12, 2) DEFAULT NULL, personal_mode INT DEFAULT NULL, personal_date DATETIME DEFAULT NULL, food NUMERIC(12, 2) DEFAULT NULL, food_mode INT DEFAULT NULL, food_date DATETIME DEFAULT NULL, medical NUMERIC(12, 2) DEFAULT NULL, medical_mode INT DEFAULT NULL, medical_date DATETIME DEFAULT NULL, children NUMERIC(12, 2) DEFAULT NULL, children_mode INT DEFAULT NULL, children_date DATETIME DEFAULT NULL, loans NUMERIC(12, 2) DEFAULT NULL, loans_mode INT DEFAULT NULL, loans_date DATETIME DEFAULT NULL, cashflow_fixed_others NUMERIC(12, 2) DEFAULT NULL, cashflow_fixed_others_mode INT DEFAULT NULL, cashflow_fixed_others_date DATETIME DEFAULT NULL, recreation NUMERIC(12, 2) DEFAULT NULL, recreation_mode INT DEFAULT NULL, recreation_date DATETIME DEFAULT NULL, miscellaneous NUMERIC(12, 2) DEFAULT NULL, miscellaneous_mode INT DEFAULT NULL, miscellaneous_date DATETIME DEFAULT NULL, insurances NUMERIC(12, 2) DEFAULT NULL, insurances_mode INT DEFAULT NULL, insurances_date DATETIME DEFAULT NULL, savings NUMERIC(12, 2) DEFAULT NULL, savings_mode INT DEFAULT NULL, savings_date DATETIME DEFAULT NULL, gifts NUMERIC(12, 2) DEFAULT NULL, gifts_mode INT DEFAULT NULL, gifts_date DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_C908F50719EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_wpm_estate (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, country_id INT DEFAULT NULL, document_type VARCHAR(255) DEFAULT NULL, documentCreatedDate DATETIME DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, appointedRepresentatives TEXT DEFAULT NULL, beneficiaries TEXT DEFAULT NULL, key_contacts TEXT DEFAULT NULL, remarks TEXT DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_34139F3819EB6921 (client_id), INDEX IDX_34139F38F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_wpm (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, net_estate NUMERIC(12, 2) DEFAULT NULL, gift_estate_planning NUMERIC(12, 2) DEFAULT NULL, active_income NUMERIC(12, 2) DEFAULT NULL, passive_income NUMERIC(12, 2) DEFAULT NULL, fixed_expenses NUMERIC(12, 2) DEFAULT NULL, variable_expenses NUMERIC(12, 2) DEFAULT NULL, investment_retirement_planning NUMERIC(12, 2) DEFAULT NULL, assets_lower_risk NUMERIC(12, 2) DEFAULT NULL, assets_moderate_risk NUMERIC(12, 2) DEFAULT NULL, assets_higher_risk NUMERIC(12, 2) DEFAULT NULL, liabilities_lower_risk NUMERIC(12, 2) DEFAULT NULL, liabilities_moderate_risk NUMERIC(12, 2) DEFAULT NULL, liabilities_higher_risk NUMERIC(12, 2) DEFAULT NULL, investment_net_worth NUMERIC(12, 2) DEFAULT NULL, risk_insurance_planning NUMERIC(12, 2) DEFAULT NULL, death NUMERIC(12, 2) DEFAULT NULL, disability_riders NUMERIC(12, 2) DEFAULT NULL, disability_sup_benefits NUMERIC(12, 2) DEFAULT NULL, pa_std_alone NUMERIC(12, 2) DEFAULT NULL, pa_riders NUMERIC(12, 2) DEFAULT NULL, pa_sup_benefits NUMERIC(12, 2) DEFAULT NULL, ci_std_alone NUMERIC(12, 2) DEFAULT NULL, ci_riders NUMERIC(12, 2) DEFAULT NULL, ci_sup_benefits NUMERIC(12, 2) DEFAULT NULL, hosp_std_alone NUMERIC(12, 2) DEFAULT NULL, hosp_riders NUMERIC(12, 2) DEFAULT NULL, hosp_sup_benefits NUMERIC(12, 2) DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_9567507019EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_wpm_investment (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, country_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, risk_class VARCHAR(255) DEFAULT NULL, ownership_type VARCHAR(255) DEFAULT NULL, owner VARCHAR(1000) DEFAULT NULL, percent_ownership NUMERIC(5, 2) DEFAULT NULL, original_value NUMERIC(12, 2) DEFAULT NULL, original_valuation_date DATETIME DEFAULT NULL, original_loan_amount NUMERIC(12, 2) DEFAULT NULL, current_loan_amount NUMERIC(12, 2) DEFAULT NULL, current_market_value NUMERIC(12, 2) DEFAULT NULL, current_valuation_date DATETIME DEFAULT NULL, investment_mode VARCHAR(255) DEFAULT NULL, source_funds VARCHAR(255) DEFAULT NULL, distribution VARCHAR(255) DEFAULT NULL, special_law VARCHAR(255) DEFAULT NULL, beneficiaries TEXT DEFAULT NULL, remarks TEXT DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_38D578ED19EB6921 (client_id), INDEX IDX_38D578EDF92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_wpm_cashflow ADD CONSTRAINT FK_C908F50719EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_wpm_estate ADD CONSTRAINT FK_34139F3819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_wpm_estate ADD CONSTRAINT FK_34139F38F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE client_wpm ADD CONSTRAINT FK_9567507019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_wpm_investment ADD CONSTRAINT FK_38D578ED19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_wpm_investment ADD CONSTRAINT FK_38D578EDF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');

        $this->addSql('CREATE TABLE client_wpm_insurance_policy (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, document_upload_id INT DEFAULT NULL, location_country_id INT DEFAULT NULL, policy_type VARCHAR(255) DEFAULT NULL, company VARCHAR(255) DEFAULT NULL, policy_number VARCHAR(255) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, ownership_type VARCHAR(255) DEFAULT NULL, owner VARCHAR(255) DEFAULT NULL, main_benefit VARCHAR(255) DEFAULT NULL, sum_assured NUMERIC(12, 2) DEFAULT NULL, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, premium NUMERIC(12, 2) DEFAULT NULL, premium_mode INT DEFAULT NULL, premium_date DATETIME DEFAULT NULL, rider_benefit VARCHAR(255) DEFAULT NULL, rider_sum_assured NUMERIC(12, 2) DEFAULT NULL, rider_start_date DATETIME DEFAULT NULL, rider_end_date DATETIME DEFAULT NULL, rider_premium NUMERIC(12, 2) DEFAULT NULL, rider_premium_mode INT DEFAULT NULL, rider_premium_date DATETIME DEFAULT NULL, sup_benefit VARCHAR(255) DEFAULT NULL, sup_sum_assured NUMERIC(12, 2) DEFAULT NULL, sup_start_date DATETIME DEFAULT NULL, sup_end_date DATETIME DEFAULT NULL, sup_premium NUMERIC(12, 2) DEFAULT NULL, sup_premium_mode INT DEFAULT NULL, sup_premium_date DATETIME DEFAULT NULL, source_funds VARCHAR(255) DEFAULT NULL, gross_cash_surrender_value NUMERIC(12, 2) DEFAULT NULL, policy_loan NUMERIC(12, 2) DEFAULT NULL, net_cash_value NUMERIC(12, 2) DEFAULT NULL, distribution VARCHAR(255) DEFAULT NULL, special_law VARCHAR(255) DEFAULT NULL, beneficiaries TEXT DEFAULT NULL, remarks TEXT DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_D4AA042F19EB6921 (client_id), INDEX IDX_D4AA042FDEF79290 (document_upload_id), INDEX IDX_D4AA042F40073FAF (location_country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_wpm_insurance_policy ADD CONSTRAINT FK_D4AA042F19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_wpm_insurance_policy ADD CONSTRAINT FK_D4AA042FDEF79290 FOREIGN KEY (document_upload_id) REFERENCES upload (id)');
        $this->addSql('ALTER TABLE client_wpm_insurance_policy ADD CONSTRAINT FK_D4AA042F40073FAF FOREIGN KEY (location_country_id) REFERENCES country (id)');

        $this->addSql('ALTER TABLE client_wpm_estate ADD deleted_at DATETIME DEFAULT NULL after remarks');
        $this->addSql('ALTER TABLE client_wpm_insurance_policy ADD deleted_at DATETIME DEFAULT NULL after remarks');
        $this->addSql('ALTER TABLE client_wpm_investment ADD deleted_at DATETIME DEFAULT NULL after remarks');
        $this->addSql('ALTER TABLE client_wpm_estate DROP INDEX UNIQ_34139F3819EB6921, ADD INDEX IDX_34139F3819EB6921 (client_id)');
        $this->addSql('ALTER TABLE client_wpm_insurance_policy CHANGE main_benefit main_benefit LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', CHANGE rider_benefit rider_benefit LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', CHANGE sup_benefit sup_benefit LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE client_wpm_insurance_policy');
        $this->addSql('DROP TABLE client_wpm_insurance_policy');
        $this->addSql('DROP TABLE client_wpm_cashflow');
        $this->addSql('DROP TABLE client_wpm_estate');
        $this->addSql('DROP TABLE client_wpm');
        $this->addSql('DROP TABLE client_wpm_investment');
    }
}
