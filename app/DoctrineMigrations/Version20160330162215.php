<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160330162215 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE calendar_event_attendee (id INT AUTO_INCREMENT NOT NULL, calendar_event_id INT NOT NULL, client_id INT NOT NULL, created DATETIME NOT NULL, INDEX IDX_E53E63E77495C8E3 (calendar_event_id), INDEX IDX_E53E63E719EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE calendar_event_attendee ADD CONSTRAINT FK_E53E63E77495C8E3 FOREIGN KEY (calendar_event_id) REFERENCES calendar_event (id)');
        $this->addSql('ALTER TABLE calendar_event_attendee ADD CONSTRAINT FK_E53E63E719EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE review_answer DROP FOREIGN KEY FK_50AB0DE53E2E969B');
        $this->addSql('ALTER TABLE review_answer CHANGE review_id review_id INT NOT NULL');
        $this->addSql('ALTER TABLE review_answer ADD CONSTRAINT FK_50AB0DE53E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE calendar_event_attendee');
        $this->addSql('ALTER TABLE review_answer CHANGE review_id review_id INT DEFAULT NULL');
    }
}
