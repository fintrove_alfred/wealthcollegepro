<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160323144113 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser ADD profile_image_upload_id INT DEFAULT NULL after description, ADD profile_background_upload_id INT DEFAULT NULL after profile_image_upload_id');
        $this->addSql('ALTER TABLE financial_adviser ADD CONSTRAINT FK_B1816314790BCF4F FOREIGN KEY (profile_image_upload_id) REFERENCES upload (id)');
        $this->addSql('ALTER TABLE financial_adviser ADD CONSTRAINT FK_B18163144B952F24 FOREIGN KEY (profile_background_upload_id) REFERENCES upload (id)');
        $this->addSql('CREATE INDEX IDX_B1816314790BCF4F ON financial_adviser (profile_image_upload_id)');
        $this->addSql('CREATE INDEX IDX_B18163144B952F24 ON financial_adviser (profile_background_upload_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser DROP FOREIGN KEY FK_B1816314790BCF4F');
        $this->addSql('ALTER TABLE financial_adviser DROP FOREIGN KEY FK_B18163144B952F24');
        $this->addSql('DROP INDEX IDX_B1816314790BCF4F ON financial_adviser');
        $this->addSql('DROP INDEX IDX_B18163144B952F24 ON financial_adviser');
        $this->addSql('ALTER TABLE financial_adviser DROP profile_image_upload_id, DROP profile_background_upload_id');
    }
}
