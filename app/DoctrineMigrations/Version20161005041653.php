<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161005041653 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client ADD nationality_country_id INT DEFAULT NULL after religion, ADD race VARCHAR(255) DEFAULT NULL after religion');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404554A08B118 FOREIGN KEY (nationality_country_id) REFERENCES country (id)');
        $this->addSql('CREATE INDEX IDX_C74404554A08B118 ON client (nationality_country_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404554A08B118');
        $this->addSql('DROP INDEX IDX_C74404554A08B118 ON client');
        $this->addSql('ALTER TABLE client DROP nationality_country_id, DROP race');
    }
}
