<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160327021727 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE review_answer DROP FOREIGN KEY FK_50AB0DE53E2E969B');
        $this->addSql('ALTER TABLE review ADD review_template_id INT DEFAULT NULL after client_id');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C67B1CE3C4 FOREIGN KEY (review_template_id) REFERENCES review_template (id)');
        $this->addSql('CREATE INDEX IDX_794381C67B1CE3C4 ON review (review_template_id)');
        $this->addSql('ALTER TABLE person ADD business_name VARCHAR(255) DEFAULT NULL after dob');
        $this->addSql('ALTER TABLE financial_adviser ADD average_rating NUMERIC(4, 2) DEFAULT NULL after profile_background_upload_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser DROP average_rating');
        $this->addSql('ALTER TABLE person DROP business_name');
        $this->addSql('ALTER TABLE review DROP FOREIGN KEY FK_794381C67B1CE3C4');
        $this->addSql('DROP INDEX IDX_794381C67B1CE3C4 ON review');
        $this->addSql('ALTER TABLE review DROP review_template_id');
        $this->addSql('ALTER TABLE review_answer ADD CONSTRAINT FK_50AB0DE53E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
    }
}
