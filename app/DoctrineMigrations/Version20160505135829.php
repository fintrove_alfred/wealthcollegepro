<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160505135829 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE client_event (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, client_event_type_id INT DEFAULT NULL, description TEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_98C3961019EB6921 (client_id), INDEX IDX_98C3961037AD1F08 (client_event_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_event_type (id INT AUTO_INCREMENT NOT NULL, financial_adviser_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, deleted_at DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_34126C3A90145FF (financial_adviser_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_event ADD CONSTRAINT FK_98C3961019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_event ADD CONSTRAINT FK_98C3961037AD1F08 FOREIGN KEY (client_event_type_id) REFERENCES client_event_type (id)');
        $this->addSql('ALTER TABLE client_event_type ADD CONSTRAINT FK_34126C3A90145FF FOREIGN KEY (financial_adviser_id) REFERENCES financial_adviser (id)');

        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('First Contact');");

        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Appointment Made')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Appointment Attended')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Appointment Cancelled')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Appointment Deferred')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Offer Made')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Counter Offer Made')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Follow-Up')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Offer Accepted')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Offer Negotiated')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Offer Declined')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Offer Reviewed')");
        $this->addSql("INSERT INTO `client_event_type` (`name`) VALUES ('Offer Complete')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client_event DROP FOREIGN KEY FK_98C3961037AD1F08');
        $this->addSql('DROP TABLE client_event');
        $this->addSql('DROP TABLE client_event_type');
    }
}
