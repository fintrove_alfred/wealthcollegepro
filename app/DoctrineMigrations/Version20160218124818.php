<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160218124818 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE calendar_event_type (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, handle VARCHAR(255) NOT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX idx_handle (handle), INDEX idx_user (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE calendar_event (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, calendar_event_type_id INT DEFAULT NULL, summary VARCHAR(255) DEFAULT NULL, description VARCHAR(5000) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, start_at DATETIME NOT NULL, end_at DATETIME NOT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_57FA09C9A76ED395 (user_id), INDEX IDX_57FA09C97F4F5D85 (calendar_event_type_id), INDEX idx_type_user (user_id, calendar_event_type_id), INDEX idx_date_range (start_at, end_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE calendar_event_type ADD CONSTRAINT FK_55FDF87A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE calendar_event ADD CONSTRAINT FK_57FA09C9A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE calendar_event ADD CONSTRAINT FK_57FA09C97F4F5D85 FOREIGN KEY (calendar_event_type_id) REFERENCES calendar_event_type (id)');
        $this->addSql('ALTER TABLE users ADD preferences LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\' after last_name');

        $this->addSql('ALTER TABLE calendar_event_type ADD name_plural VARCHAR(255) NOT NULL after name');

        $this->addSql("INSERT INTO `calendar_event_type` (`name`, `name_plural`, `handle`) VALUES ('Event','Events', 'EVENTS');");
        $this->addSql("INSERT INTO `calendar_event_type` (`name`, `name_plural`, `handle`) VALUES ('Occasion','Occasions', 'OCCATIONS');");
        $this->addSql("INSERT INTO `calendar_event_type` (`name`, `name_plural`, `handle`) VALUES ('Appointment','Appointments', 'APPOINTMENTS');");
        $this->addSql("INSERT INTO `calendar_event_type` (`name`, `name_plural`, `handle`) VALUES ('Personal','Personal', 'PERSONAL');");
        $this->addSql("INSERT INTO `calendar_event_type` (`name`, `name_plural`, `handle`) VALUES ('FinTrain Sessions','FinTrain Session', 'FINTRAIN_SESSIONS');");

        $this->addSql('ALTER TABLE calendar_event ADD alert_type_id INT NOT NULL after end_at, ADD alert_days INT DEFAULT NULL after alert_type_id, ADD alert_time TIME DEFAULT NULL after alert_days, ADD alert_date_time DATETIME DEFAULT NULL after alert_time');
        $this->addSql('CREATE INDEX idx_alert_time ON calendar_event (alert_type_id, alert_date_time)');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX idx_alert_time ON calendar_event');
        $this->addSql('ALTER TABLE calendar_event DROP alert_type_id, DROP alert_days, DROP alert_time, DROP alert_date_time');

        $this->addSql('ALTER TABLE calendar_event DROP FOREIGN KEY FK_57FA09C97F4F5D85');
        $this->addSql('DROP TABLE calendar_event_type');
        $this->addSql('DROP TABLE calendar_event');
        $this->addSql('ALTER TABLE users DROP preferences');
        $this->addSql('ALTER TABLE calendar_event_type DROP name_plural');
    }
}
