<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160326151318 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE review_answer (id INT AUTO_INCREMENT NOT NULL, review_id INT DEFAULT NULL, answer_score INT NOT NULL, answer_text LONGTEXT NOT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_50AB0DE53E2E969B (review_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE review (id INT AUTO_INCREMENT NOT NULL, financial_adviser_id INT DEFAULT NULL, client_id INT DEFAULT NULL, type_id INT NOT NULL, status_id INT NOT NULL, email_subject VARCHAR(255) DEFAULT NULL, email_message LONGTEXT DEFAULT NULL, rating INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, feedback LONGTEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_794381C6A90145FF (financial_adviser_id), INDEX IDX_794381C619EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE review_template (id INT AUTO_INCREMENT NOT NULL, financial_adviser_id INT DEFAULT NULL, type_id INT NOT NULL, subject VARCHAR(255) NOT NULL, message LONGTEXT DEFAULT NULL, archived_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_B4229688A90145FF (financial_adviser_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE review_template_question (id INT AUTO_INCREMENT NOT NULL, review_template_id INT DEFAULT NULL, question LONGTEXT NOT NULL, archived_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_752C6AD97B1CE3C4 (review_template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE review_answer ADD CONSTRAINT FK_50AB0DE53E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C6A90145FF FOREIGN KEY (financial_adviser_id) REFERENCES financial_adviser (id)');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C619EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE review_template ADD CONSTRAINT FK_B4229688A90145FF FOREIGN KEY (financial_adviser_id) REFERENCES financial_adviser (id)');
        $this->addSql('ALTER TABLE review_template_question ADD CONSTRAINT FK_752C6AD97B1CE3C4 FOREIGN KEY (review_template_id) REFERENCES review_template (id)');
        $this->addSql('ALTER TABLE users CHANGE dob dob DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE review_template ADD name VARCHAR(255) NOT NULL after type_id');
        $this->addSql('ALTER TABLE review ADD received_at DATETIME DEFAULT NULL after feedback, ADD published_at DATETIME DEFAULT NULL after received_at, ADD archived_at DATETIME DEFAULT NULL after published_at, ADD `key` VARCHAR(255) DEFAULT NULL after archived_at, CHANGE deleted_at sent_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE review CHANGE `key` token VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE review CHANGE token `key` VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE review ADD deleted_at DATETIME DEFAULT NULL, DROP sent_at, DROP received_at, DROP published_at, DROP archived_at, DROP `key`');
        $this->addSql('ALTER TABLE review_template DROP name');
        $this->addSql('ALTER TABLE review_answer DROP FOREIGN KEY FK_50AB0DE53E2E969B');
        $this->addSql('ALTER TABLE review_template_question DROP FOREIGN KEY FK_752C6AD97B1CE3C4');
        $this->addSql('DROP TABLE review_answer');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE review_template');
        $this->addSql('DROP TABLE review_template_question');
        $this->addSql('ALTER TABLE users CHANGE dob dob DATE NOT NULL');
    }
}
