<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160503170346 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client ADD religion VARCHAR(255) DEFAULT NULL after job_title, ADD facebook VARCHAR(255) DEFAULT NULL after religion, ADD twitter VARCHAR(255) DEFAULT NULL after facebook, ADD linkedin VARCHAR(255) DEFAULT NULL after twitter, ADD notes LONGTEXT DEFAULT NULL after linkedin');
        $this->addSql('ALTER TABLE review_template ADD rating_feedback_enabled TINYINT(1) DEFAULT \'1\' NOT NULL after message, ADD comments_enabled TINYINT(1) DEFAULT \'0\' NOT NULL after rating_feedback_enabled');
        $this->addSql('ALTER TABLE review_template ADD comment_question LONGTEXT DEFAULT NULL after comments_enabled');
        $this->addSql('ALTER TABLE review ADD comment LONGTEXT DEFAULT NULL after negative_feedback');

        $this->addSql('ALTER TABLE client ADD photo_upload_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455C131EC10 FOREIGN KEY (photo_upload_id) REFERENCES upload (id)');
        $this->addSql('CREATE INDEX IDX_C7440455C131EC10 ON client (photo_upload_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455C131EC10');
        $this->addSql('DROP INDEX IDX_C7440455C131EC10 ON client');
        $this->addSql('ALTER TABLE client DROP photo_upload_id');
        $this->addSql('ALTER TABLE review DROP comment');
        $this->addSql('ALTER TABLE review_template DROP comment_question');
        $this->addSql('ALTER TABLE review_template DROP rating_feedback_enabled, DROP comments_enabled');
        $this->addSql('ALTER TABLE client DROP religion, DROP facebook, DROP twitter, DROP linkedin, DROP notes');
    }
}
