<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160425042517 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE webinar (id INT AUTO_INCREMENT NOT NULL, calendar_event_id INT NOT NULL, financial_adviser_id INT NOT NULL, url VARCHAR(255) DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_C9E29F4A7495C8E3 (calendar_event_id), INDEX IDX_C9E29F4AA90145FF (financial_adviser_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE webinar ADD CONSTRAINT FK_C9E29F4A7495C8E3 FOREIGN KEY (calendar_event_id) REFERENCES calendar_event (id)');
        $this->addSql('ALTER TABLE webinar ADD CONSTRAINT FK_C9E29F4AA90145FF FOREIGN KEY (financial_adviser_id) REFERENCES financial_adviser (id)');

        $this->addSql('ALTER TABLE webinar ADD dirty TINYINT(1) DEFAULT \'1\' NOT NULL after url, ADD synced_at DATETIME DEFAULT NULL after dirty');
        $this->addSql('ALTER TABLE calendar_event ADD external_id VARCHAR(255) DEFAULT NULL after calendar_event_type_id');

        $this->addSql('ALTER TABLE webinar DROP dirty, DROP synced_at');
        $this->addSql('ALTER TABLE calendar_event ADD dirty TINYINT(1) DEFAULT \'1\' NOT NULL after alert_date_time, ADD synced_at DATETIME DEFAULT NULL after dirty');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE calendar_event DROP dirty, DROP synced_at');
        $this->addSql('ALTER TABLE webinar ADD dirty TINYINT(1) DEFAULT \'1\' NOT NULL, ADD synced_at DATETIME DEFAULT NULL');


        $this->addSql('ALTER TABLE calendar_event DROP external_id');
        $this->addSql('DROP TABLE webinar');
    }
}
