<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160914195015 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE review ADD attributes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\' after comment');

        $this->addSql('ALTER TABLE client_event ADD date DATE DEFAULT NULL after description');
        $this->addSql('UPDATE client_event set date = created');

        $this->addSql('ALTER TABLE financial_adviser ADD areas_of_competencies_others TEXT DEFAULT NULL after areas_of_competencies, CHANGE areas_of_competencies areas_of_competencies LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\'');
        $this->addSql('ALTER TABLE financial_adviser ADD languages LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\' after religion');

        $this->addSql('ALTER TABLE financial_adviser ADD languages_others VARCHAR(1000) DEFAULT NULL AFTER languages, ADD institution VARCHAR(255) DEFAULT NULL AFTER languages_others, ADD balance_score_card VARCHAR(255) DEFAULT NULL AFTER institution, ADD bp_rate NUMERIC(12, 2) DEFAULT NULL AFTER balance_score_card');
        $this->addSql('ALTER TABLE client_wpm ADD annual_cashflow_balance NUMERIC(12, 2) DEFAULT NULL after net_estate');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client_wpm DROP annual_cashflow_balance');
        $this->addSql('ALTER TABLE financial_adviser DROP languages_others, DROP institution, DROP balance_score_card, DROP bp_rate');
        $this->addSql('ALTER TABLE financial_adviser DROP languages');
        $this->addSql('ALTER TABLE financial_adviser DROP areas_of_competencies_others, CHANGE areas_of_competencies areas_of_competencies TEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE client_event DROP date');
        $this->addSql('ALTER TABLE review DROP attributes');
    }
}
