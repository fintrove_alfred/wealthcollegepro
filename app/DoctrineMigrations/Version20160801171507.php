<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160801171507 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE financial_adviser_photo (id INT AUTO_INCREMENT NOT NULL, financial_adviser_id INT DEFAULT NULL, upload_id INT DEFAULT NULL, type_id VARCHAR(255) NOT NULL, sort_order INT DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX IDX_492360EDA90145FF (financial_adviser_id), INDEX IDX_492360EDCCCFBA31 (upload_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE financial_adviser_photo ADD CONSTRAINT FK_492360EDA90145FF FOREIGN KEY (financial_adviser_id) REFERENCES financial_adviser (id)');
        $this->addSql('ALTER TABLE financial_adviser_photo ADD CONSTRAINT FK_492360EDCCCFBA31 FOREIGN KEY (upload_id) REFERENCES upload (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE financial_adviser_photo');
    }
}
