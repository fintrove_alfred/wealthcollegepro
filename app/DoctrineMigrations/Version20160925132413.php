<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160925132413 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser ADD professional_title VARCHAR(255) DEFAULT NULL after business_name');
        $this->addSql('ALTER TABLE financial_adviser ADD type_if_qualification VARCHAR(255) DEFAULT NULL after academic_qualification_other');
        $this->addSql('ALTER TABLE financial_adviser ADD special_interest TEXT DEFAULT NULL after profile_content');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE financial_adviser DROP special_interest');
        $this->addSql('ALTER TABLE financial_adviser DROP type_if_qualification');
        $this->addSql('ALTER TABLE financial_adviser DROP professional_title');
    }
}
