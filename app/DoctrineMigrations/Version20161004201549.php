<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161004201549 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE review_review_attribute (review_id INT NOT NULL, review_attribute_id INT NOT NULL, INDEX IDX_E99E393E3E2E969B (review_id), INDEX IDX_E99E393EEF2BD501 (review_attribute_id), PRIMARY KEY(review_id, review_attribute_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE review_review_attribute ADD CONSTRAINT FK_E99E393E3E2E969B FOREIGN KEY (review_id) REFERENCES review (id)');
        $this->addSql('ALTER TABLE review_review_attribute ADD CONSTRAINT FK_E99E393EEF2BD501 FOREIGN KEY (review_attribute_id) REFERENCES review_attribute (id)');
        $this->addSql('ALTER TABLE review DROP attributes');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE review_review_attribute');
        $this->addSql('ALTER TABLE review ADD attributes LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:simple_array)\'');
    }
}
