<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160212162934 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE upload (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, filename VARCHAR(255) NOT NULL, location VARCHAR(255) NOT NULL, type VARCHAR(255) DEFAULT NULL, size INT DEFAULT NULL, meta_data LONGTEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX idx_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE communication (id INT AUTO_INCREMENT NOT NULL, user_to_id INT DEFAULT NULL, type_id INT DEFAULT 1 NOT NULL, subject VARCHAR(255) DEFAULT NULL, body LONGTEXT DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, link_id INT DEFAULT NULL, to_email VARCHAR(255) DEFAULT NULL, to_name VARCHAR(255) DEFAULT NULL, to_number VARCHAR(255) DEFAULT NULL, read_at DATETIME DEFAULT NULL, status_id INT DEFAULT 1 NOT NULL, sent_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX idx_link (type_id, link, link_id, status_id), INDEX idx_user_to (user_to_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, telephone VARCHAR(32) DEFAULT NULL, mobile VARCHAR(32) DEFAULT NULL, fax VARCHAR(32) DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, house_number VARCHAR(255) DEFAULT NULL, house_name VARCHAR(255) DEFAULT NULL, flat_name VARCHAR(255) DEFAULT NULL, address_1 VARCHAR(255) DEFAULT NULL, address_2 VARCHAR(255) DEFAULT NULL, address_3 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, post_code VARCHAR(255) DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX idx_post_code (post_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, image_upload_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_1483A5E9A0D96FBF (email_canonical), INDEX fk_image_upload_id (image_upload_id), INDEX fk_parent_id (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, user_to_id INT DEFAULT NULL, user_from_id INT DEFAULT NULL, subject VARCHAR(255) DEFAULT NULL, body LONGTEXT DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, link_id INT DEFAULT NULL, read_at DATETIME DEFAULT NULL, trashed_at DATETIME DEFAULT NULL, send_email TINYINT(1) DEFAULT \'0\' NOT NULL, email_sent_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME DEFAULT NULL, INDEX fk_message_user_to_id (user_to_id), INDEX fk_message_user_from_id (user_from_id), INDEX fk_message_link (link, link_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE audit_log (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, link VARCHAR(255) NOT NULL, link_id INT DEFAULT NULL, action_id INT NOT NULL, meta TEXT DEFAULT NULL, is_meta_json TINYINT(1) DEFAULT \'0\' NOT NULL, created DATETIME NOT NULL, INDEX idx_link (link, link_id), INDEX fk_audit_log_user_id (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE application_setting (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, handle VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(10000) DEFAULT NULL, value VARCHAR(10000) DEFAULT NULL, can_edit TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, modified DATETIME NOT NULL, created DATETIME NOT NULL, INDEX idx_handle (handle), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE upload ADD CONSTRAINT FK_17BDE61FA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE communication ADD CONSTRAINT FK_F9AFB5EBD2F7B13D FOREIGN KEY (user_to_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9727ACA70 FOREIGN KEY (parent_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9144D6165 FOREIGN KEY (image_upload_id) REFERENCES upload (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FD2F7B13D FOREIGN KEY (user_to_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F20C3C701 FOREIGN KEY (user_from_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE audit_log ADD CONSTRAINT FK_F6E1C0F5A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9144D6165');
        $this->addSql('ALTER TABLE upload DROP FOREIGN KEY FK_17BDE61FA76ED395');
        $this->addSql('ALTER TABLE communication DROP FOREIGN KEY FK_F9AFB5EBD2F7B13D');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9727ACA70');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FD2F7B13D');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F20C3C701');
        $this->addSql('ALTER TABLE audit_log DROP FOREIGN KEY FK_F6E1C0F5A76ED395');
        $this->addSql('DROP TABLE upload');
        $this->addSql('DROP TABLE communication');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE audit_log');
        $this->addSql('DROP TABLE application_setting');
    }
}
